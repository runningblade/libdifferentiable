#include "ConvHullPBDSimulator.h"
#include "PBDMatrixSolver.h"
#include "JointLimit.h"
#include "SoftJoint.h"
#include <Utils/RotationUtils.h>
#include <Utils/CrossSpatialUtils.h>
#include <Articulated/ArticulatedUtils.h>
#include <time.h>

namespace PHYSICSMOTION {
#define USE_CRBA 0
void moveMesh(Joint& J,const Eigen::Matrix<GEOMETRY_SCALAR,-1,1>& X) {
  std::shared_ptr<MeshExact> mesh=std::dynamic_pointer_cast<MeshExact>(J._mesh);
  mesh->moveMesh(X);
  mesh->init(mesh->vss(),mesh->iss(),true);
}
void setMesh(Joint& J,const Eigen::Matrix<GEOMETRY_SCALAR,-1,1>& X) {
  std::shared_ptr<MeshExact> mesh=std::dynamic_pointer_cast<MeshExact>(J._mesh);
  mesh->setMesh(X);
  mesh->init(mesh->vss(),mesh->iss(),true);
}
ConvHullPBDSimulator::ConvHullPBDSimulator(T dt):Simulator(dt),_gTol(1e-4f),_alpha(1e-6f),_epsV(1e-1f),_coefBarrier(1e-2),_hardLimit(false),_maxIt(1e4) {
  _barrier._x0=0.01;//0.01
}
void ConvHullPBDSimulator::clearShape() {
  _shapes.clear();
  _obs.clear();
  getContactPtr()=NULL;
  _t=0;
}
void ConvHullPBDSimulator::addShape(std::shared_ptr<ShapeExact> shape) {
  _shapes.push_back(shape);
  std::vector<Eigen::Matrix<double,3,1>> vss;
  std::vector<Eigen::Matrix<int,3,1>> iss;
  shape->getMesh(vss,iss);
  std::shared_ptr<MeshExact> mesh(new MeshExact(vss,iss,false));
  _obs.push_back(GJKPolytope<T>(mesh));
  getContactPtr()=NULL;
}
void ConvHullPBDSimulator::setArticulatedBody(std::shared_ptr<ArticulatedBody> body) {
  _sol=NULL;
  ArticulatedUtils(*body).tessellate(true); //tessellate the mesh
  Simulator::setArticulatedBody(body);
  //setGravity(Vec3T::Zero());
  getJRCF().setZero(3,4*_body->nrJ());
  _pos.reset(*body,Vec::Zero(body->nrDOF()));
  _lastPos.reset(*body,Vec::Zero(body->nrDOF()));
  _sol.reset(new PBDMatrixSolverEigen(*_body));
}
ConvHullPBDSimulator::Vec3T ConvHullPBDSimulator::getCentre(int k) {
  return _pos._centre[k];
}
ConvHullPBDSimulator::MatT ConvHullPBDSimulator::getdPTarget() {
  return _pos._HThetaPTarget;
}
ConvHullPBDSimulator::MatT ConvHullPBDSimulator::getdDTarget() {
  return _pos._HThetaDTarget;
}
ConvHullPBDSimulator::MatT ConvHullPBDSimulator::getdL() {
  return _pos._HThetaL;
}
ConvHullPBDSimulator::MatT ConvHullPBDSimulator::getdLL() {
  return _pos._HThetaLL;
}
ConvHullPBDSimulator::MatT ConvHullPBDSimulator::getdD() {
  return _pos._HThetaD;
}
ConvHullPBDSimulator::MatT ConvHullPBDSimulator::getdPos() {
  return _pos._HPos;
}
void ConvHullPBDSimulator::reset() {
  setPos(Vec::Zero(_body->nrDOF()));
  setVel(Vec::Zero(_body->nrDOF()));
  setTime(0);
}
void ConvHullPBDSimulator::resetWithPos(Vec pos) {
  setPos(pos);
  setVel(Vec::Zero(_body->nrDOF()));
  setTime(0);
}
void ConvHullPBDSimulator::getJointPosGrad(GradInfo& grad,int k) {
  const Joint& J=_body->joint(k);
  Vec3T centre=Vec3T::Zero();
  Mat3T Mwt,Mtt,C;
  MatX3T HThetaX;
  int nrDB=_body->nrDOF();
  HThetaX.setZero(nrDB,3);
  if(std::dynamic_pointer_cast<MeshExact>(J._mesh)) {
    std::shared_ptr<MeshExact> local=std::dynamic_pointer_cast<MeshExact>(J._mesh);
    int sz=(int)local->vss().size();
    for(int i=0; i<sz; i++) {
      centre+=local->vss()[i].template cast<T>()/sz;
    }
    C=cross<T>(ROTI(grad._info._TM,k)*centre);
    Mwt=C;
    Mtt=Mat3T::Identity();
    grad._info.JRCSparse(*_body,k,[&](int row,const Vec3T& JR) {
      parallelAdd<T,3>(HThetaX,row,0,JR.transpose()*Mwt);
    },[&](int row,const Vec3T& JC) {
      parallelAdd<T,3>(HThetaX,row,0,JC.transpose()*Mtt);
    });
    parallelAdd<T,-1,-1>(grad._HPos,0,0,HThetaX);
  } else std::cout<<"Invalid Joint !"<<std::endl;
}
ConvHullPBDSimulator::Vec ConvHullPBDSimulator::getConvexPoints() {
  int nrJ=_body->nrJ();
  Vec ConvexPoints;
  ConvexPoints.setZero(_pos._nrVex*3);
  OMP_PARALLEL_FOR_
  for(int k=0; k<nrJ; k++) {
    const Joint& J=_body->joint(k);
    if(std::dynamic_pointer_cast<MeshExact>(J._mesh)) {
      std::shared_ptr<MeshExact> local=std::dynamic_pointer_cast<MeshExact>(J._mesh);
      for(int c=0; c<(int)local->vss().size(); c++) {
        for(int j=0; j<3; j++)
          ConvexPoints(j+(_pos._polytopes[k].getVertexId()[0]+c)*3)=(local->vss()[c].template cast<T>())(j);
      }
    }
  }
  return ConvexPoints;
}
void ConvHullPBDSimulator::step() {
  if(_body->nrDOF()==0)
    return;
  //update kinematic state
  GradInfo newPos(*_body,setKinematic(_pos._info._xM,_t+_dt)),newPos2;
  detectLastContact();
  //ASSERT_MSG(detectLastContact(),"Penetrating initial guess!")
  //normal solve
  Vec D,DE;
  T nu=2,alphaMax=1e6f,alphaMin=1e-6f;
  T e=energy(newPos,&DE),e2=0,rho=0;
  mask(*getBody(),NULL,&DE,&(newPos._HTheta));
  for(int iter=0; iter<_maxIt;) {
    //update configuration
    update(newPos,newPos2,D,DE,newPos._HTheta,_alpha);
    e2=energy(newPos2,NULL);
    //iteration update
    rho=(e2-e)/D.dot(DE+newPos._HTheta*D/2);
    if(isfinite(e2) && (e2<e && rho>0)) {
      _alpha=_alpha*std::max<T>(1./3.,1-(2*rho-1)*(2*rho-1)*(2*rho-1));
      _alpha=std::max<T>(std::min<T>(_alpha,alphaMax),alphaMin);
      nu=2;
      newPos=newPos2;
      e=energy(newPos,&DE);
      mask(*getBody(),NULL,&DE,&(newPos._HTheta));
      iter++;
      if(_output)
        std::cout << "Iter=" << iter << " E=" << e << " gNorm=" << DE.cwiseAbs().maxCoeff() << " alpha=" << _alpha << " nu=" << nu << " rho=" << rho << std::endl;
      //termination: gradient tolerance
      if(DE.cwiseAbs().maxCoeff()<_gTol)
        break;
    } else {
      _alpha=_alpha*nu;
      nu=nu*2;
      if(_output)
        std::cout << "Iter=" << iter << " E=" << e << " alpha=" << _alpha << " nu=" << nu << std::endl;
      //termination: numerical issue
      if(_alpha>=alphaMax)
        break;
    }
  }
  //update
  _lastPos=_pos;
  _pos=newPos;
  _t+=_dt;
}
void ConvHullPBDSimulator::backward() {
  backward(_pos);
}
void ConvHullPBDSimulator::backward(GradInfo& grad,bool debug) {
  detectContact(grad._info._TM);
  T coef=1.0/(_dt*_dt),rho;
  Mat3XT GB,MRR,MRt,MtR,Mtt;
  Mat3XT MRRL,MRtL,MtRL,MttL;
  std::shared_ptr<MeshExact> local;
  int nrJ=_body->nrJ();
  int nrDB=_body->nrDOF();
  _MRR.setZero(3,3*nrJ);
  _MRt.setZero(3,3*nrJ);
  _MtR.setZero(3,3*nrJ);
  _Mtt.setZero(3,3*nrJ);
  _MRRL.setZero(3,3*nrJ);
  _MRtL.setZero(3,3*nrJ);
  _MtRL.setZero(3,3*nrJ);
  _MttL.setZero(3,3*nrJ);
  grad._HThetaD.setZero(nrDB,grad._nrVex*3);
  grad._HThetaPTarget.setZero(nrDB,nrDB);
  grad._HThetaDTarget.setZero(nrDB,nrDB);
  grad._HThetaL.setZero(nrDB,nrDB);
  grad._HThetaLL.setZero(nrDB,nrDB);
  grad._HPos.setZero(nrDB,3);
  OMP_PARALLEL_FOR_
  for(int k=0; k<nrJ; k++) {
    //dynamic
    const Joint& J=_body->joint(k);
    int nrD=J.nrDOF();
    //update MC,MCCT
    Mat3T PPT=Mat3T::Zero();
    Vec3T P=Vec3T::Zero();
    if(std::dynamic_pointer_cast<MeshExact>(J._mesh)) {
      std::shared_ptr<MeshExact> local=std::dynamic_pointer_cast<MeshExact>(J._mesh);
      rho=J._M/(1.0*local->vss().size());
      for(int i=0; i<(int)local->vss().size(); i++) {
        P+=local->vss()[i].template cast<T>()*rho;
        PPT+=(local->vss()[i]*(local->vss()[i]).transpose()).template cast<T>()*rho;
      }
    }
    _MRRL.template block<3,3>(0,k*3)-=invDoubleCrossMatTrace<T>(ROTI(grad._info._TM,k)*PPT*ROTI(_lastPos._info._TM,k).transpose())*coef;
    _MRtL.template block<3,3>(0,k*3)+=cross<T>(ROTI(grad._info._TM,k)*P)*coef;
    _MtRL.template block<3,3>(0,k*3)-=cross<T>(ROTI(_lastPos._info._TM,k)*P)*coef;
    _MttL.template block<3,3>(0,k*3)+=Mat3T::Identity()*J._M*coef;
    _MRR.template block<3,3>(0,k*3)+=2*invDoubleCrossMatTrace<T>(ROTI(grad._info._TM,k)*PPT*ROTI(_pos._info._TM,k).transpose())*coef;
    _MRt.template block<3,3>(0,k*3)-=2*cross<T>(ROTI(grad._info._TM,k)*P)*coef;
    _MtR.template block<3,3>(0,k*3)+=2*cross<T>(ROTI(_pos._info._TM,k)*P)*coef;
    _Mtt.template block<3,3>(0,k*3)-=2*Mat3T::Identity()*J._M*coef;
    Mat3X4T A=TRANSI(grad._info._TM,k)-2*TRANSI(_pos._info._TM,k)+TRANSI(_lastPos._info._TM,k);
    Mat3T Mwt,Mtt,C;
    MatX3T HThetaX;
    if(std::dynamic_pointer_cast<MeshExact>(J._mesh)) {
      std::shared_ptr<MeshExact> local=std::dynamic_pointer_cast<MeshExact>(J._mesh);
      T rho=J._M/(1.0*local->vss().size());
      for(int c=0; c<(int)local->vss().size(); c++) {
        HThetaX.setZero(nrDB,3);
        C=cross<T>(ROTI(grad._info._TM,k)*local->vss()[c].template cast<T>());
        Mwt=C*coef*rho;
        Mtt=Mat3T::Identity()*coef*rho;
        Mwt=Mwt*ROT(A)-cross<T>((CTR(A)*rho+ROT(A)*rho*local->vss()[c].template cast<T>())*coef)*ROTI(grad._info._TM,k);
        Mtt=Mtt*ROT(A);
        grad._info.JRCSparse(*_body,k,[&](int row,const Vec3T& JR) {
          parallelAdd<T,3>(HThetaX,row,0,JR.transpose()*Mwt);
        },[&](int row,const Vec3T& JC) {
          parallelAdd<T,3>(HThetaX,row,0,JC.transpose()*Mtt);
        });
        for(int i=0; i<_body->nrDOF(); i++)
          for(int j=0; j<3; j++)
            parallelAdd(grad._HThetaD(i,j+(grad._polytopes[k].getVertexId()[0]+c)*3),HThetaX(i,j));
      }
    }
    //P controller
    Eigen::Matrix<T,-1,1,0,3,1> coef;
    if(J._control.size()>=nrD)
      coef=J._control.segment(0,nrD).template cast<T>();
    else coef.setOnes(nrD);
    PhysicsParameter& param=getJointPhysicsParameter(k);
    if(param._kp>0) {
      grad._HThetaPTarget.diagonal().segment(J._offDOF,nrD).array()-=coef.array()*param._kp;
    }
    //D controller
    if(param._kd>0) {
      grad._HThetaDTarget.diagonal().segment(J._offDOF,nrD).array()-=coef.array()*param._kd/_dt;
      grad._HThetaL.diagonal().segment(J._offDOF,nrD).array()-=coef.array()*param._kd/_dt/_dt;
    }
    //joint limit
  }
  //pos grad

  //getJointPosGrad(grad,1);
  //contact
  normalEnergy(grad,NULL,true);
  tangentEnergy(grad,NULL,true);
  grad._info.toolA(*_body,_pos._info,mapM(MRR=_MRR),mapM(MRt=_MRt),mapM(MtR=_MtR),mapM(Mtt=_Mtt),[&](int r,int c,T val) {
    grad._HThetaL(r,c)+=val;
  });
  grad._info.toolA(*_body,_lastPos._info,mapM(MRRL=_MRRL),mapM(MRtL=_MRtL),mapM(MtRL=_MtRL),mapM(MttL=_MttL),[&](int r,int c,T val) {
    grad._HThetaLL(r,c)+=val;
  });
  if(!debug) {
    _sol->compute(grad._HTheta);
    grad._HThetaPTarget=-_sol->solve(grad._HThetaPTarget);
    grad._HThetaDTarget=-_sol->solve(grad._HThetaDTarget);
    grad._HThetaD=-_sol->solve(grad._HThetaD);
    grad._HThetaL=-_sol->solve(grad._HThetaL);
    grad._HThetaLL=-_sol->solve(grad._HThetaLL);
  }
}
void ConvHullPBDSimulator::detectCurrentContact() {
  FUNCTION_NOT_IMPLEMENTED
}
ConvHullPBDSimulator::T ConvHullPBDSimulator::gTol() const {
  return _gTol;
}
void ConvHullPBDSimulator::setGTol(T gTol) {
  _gTol=gTol;
}
ConvHullPBDSimulator::T ConvHullPBDSimulator::x0() const {
  return _barrier._x0;
}
void ConvHullPBDSimulator::setX0(T x0) {
  _barrier._x0=(double)x0;
}
bool ConvHullPBDSimulator::hardLimit() const {
  return _hardLimit;
}
void ConvHullPBDSimulator::setHardLimit(bool hardLimit) {
  _hardLimit=hardLimit;
}
ConvHullPBDSimulator::Vec ConvHullPBDSimulator::pos() const {
  return _pos._info._xM;
}
void ConvHullPBDSimulator::setPos(const Vec& pos) {
  _pos.reset(*_body,setKinematic(pos,_t));
}
ConvHullPBDSimulator::Vec ConvHullPBDSimulator::vel() const {
  return (_pos._info._xM-_lastPos._info._xM)/_dt;
}
void ConvHullPBDSimulator::setVel(const Vec& vel) {
  _lastPos.reset(*_body,setKinematic(_pos._info._xM-vel*_dt,_t-_dt));
}
void ConvHullPBDSimulator::setCoefBarrier(T coefBarrier) {
  _coefBarrier=coefBarrier;
}
void ConvHullPBDSimulator::debugEnergy(T scale,const T* customDelta) {
  DEFINE_NUMERIC_DELTA_T(T)
  if(customDelta)
    DELTA=*customDelta;
  while(true) {
    //generate random pose
    GradInfo newPos(*_body,Vec::Random(_body->nrDOF())*scale),newPos2,Pos;
    _pos.reset(*_body,Vec::Random(_body->nrDOF())*scale);
    _lastPos.reset(*_body,Vec::Random(_body->nrDOF())*scale);

    if(!detectLastContact())
      continue;
    Pos.reset(*_body,_pos._info._xM);
    int nrJ=_body->nrJ();

    //generate random joint
    getJoints().clear();
    for(int k=0; k<nrJ; k++) {
      SoftJoint j;
      j.setRandom(*_body);
      getJoints().push_back(j);
    }

    //generate random PD target and joint limit
    Vec P=Vec::Random(_body->nrDOF());
    Vec D=Vec::Random(_body->nrDOF());
    for(int k=0; k<nrJ; k++) {
      Joint& J=_body->joint(k);
      J._limits.row(2).setRandom();

      PhysicsParameter& p=getJointPhysicsParameter(k);
      p._kp=rand()/(T)RAND_MAX;
      p._kd=rand()/(T)RAND_MAX;
      p._tarP=[&](T,int n) {
        return P.segment(J._offDOF,n);
      };
      p._tarD=[&](T,int n) {
        return D.segment(J._offDOF,n);
      };
    }
    Vec DE,DE2,dx=Vec::Random(_body->nrDOF()),dc,X;
    MatT HThetaDL,HThetaD;

    //debug DE/DDE
    T e=energy(newPos,&DE);
    //Vec3T centre=newPos._centre[1];
    backward(newPos,true);
    //MatT HPos=newPos._HPos;
    newPos2.reset(*_body,newPos._info._xM+dx*DELTA);
    T e2=energy(newPos2,&DE2);
    //Vec3T centre2=newPos2._centre[1];
    DEBUG_GRADIENT("DE",DE.dot(dx),DE.dot(dx)-(e2-e)/DELTA)
    DEBUG_GRADIENT("DDE",(newPos._HTheta*dx).norm(),(newPos._HTheta*dx-(DE2-DE)/DELTA).norm())
    //DEBUG_GRADIENT("DDE-Centre",((centre2-centre)/DELTA).norm(),(HPos.transpose()*dx-(centre2-centre)/DELTA).norm())

    //debug DDE-L
    HThetaDL=newPos._HThetaL;
    _pos.reset(*_body,Pos._info._xM+dx*DELTA);
    newPos2.reset(*_body,newPos._info._xM);
    if(!detectLastContact())
      continue;
    energy(newPos2,&DE2);
    DEBUG_GRADIENT("DDE-L",((DE2-DE)/DELTA).norm(),(HThetaDL*dx-(DE2-DE)/DELTA).norm())

    //debug DDE-D
    _pos=Pos;
    if(!detectLastContact())
      continue;
    HThetaD=newPos._HThetaD;
    //move convex hull
    dc=Vec::Random(newPos._nrVex*3);
    for(int k=0; k<nrJ; k++)
      if(newPos._polytopes[k].jid()>=0) {
        Eigen::Matrix<int,2,1> off=newPos._polytopes[k].getVertexId();
        X=dc.segment(off[0]*3,(off[1]-off[0])*3)*DELTA;
        moveMesh(_body->joint(k),X);  //re-merge BVH, need revise
      }
    newPos2.reset(*_body,newPos._info._xM);
    energy(newPos2,&DE2);
    DEBUG_GRADIENT("DDE-D",((DE2-DE)/DELTA).norm(),(HThetaD*dc-(DE2-DE)/DELTA).norm())
    debugBVHEnergy(newPos);
    break;
  }
}
void ConvHullPBDSimulator::updateConvexPoints(Vec X) {
  int nrJ=_body->nrJ();
  for(int k=0; k<nrJ; k++)
    if(_pos._polytopes[k].jid()>=0) {
      Eigen::Matrix<int,2,1> off=_pos._polytopes[k].getVertexId();
      Vec dc=X.segment(off[0]*3,(off[1]-off[0])*3);
      moveMesh(_body->joint(k),dc);
    }
}
void ConvHullPBDSimulator::debugBackward(T scale,const T* customDelta) {
  DEFINE_NUMERIC_DELTA_T(T)
  if(customDelta)
    DELTA=*customDelta;
  while(true) {
    //generate random pose
    GradInfo nextPos,pos,lastPos,pos1,pos2;
    _pos.reset(*_body,Vec::Random(_body->nrDOF())*scale);
    _lastPos.reset(*_body,Vec::Random(_body->nrDOF())*scale);
    if(!detectLastContact())
      continue;
    pos=_pos;
    lastPos=_lastPos;
    int nrJ=_body->nrJ();

    //generate random joint
    getJoints().clear();
    for(int k=0; k<nrJ; k++) {
      SoftJoint j;
      j.setRandom(*_body);
      getJoints().push_back(j);
    }

    //generate random PD target and joint limit
    Vec P=Vec::Random(_body->nrDOF()),P2;
    Vec D=Vec::Random(_body->nrDOF()),D2;
    for(int k=0; k<nrJ; k++) {
      Joint& J=_body->joint(k);
      J._limits.row(2).setRandom();

      PhysicsParameter& p=getJointPhysicsParameter(k);
      p._kp=rand()/(T)RAND_MAX;
      p._kd=rand()/(T)RAND_MAX;
      p._tarP=[&](T,int n) {
        return P.segment(J._offDOF,n);
      };
      p._tarD=[&](T,int n) {
        return D.segment(J._offDOF,n);
      };
    }
    T e=energy(_pos,NULL);
    if(!isfinite(e))
      continue;

    step();
    nextPos=_pos;
    _pos=pos;
    _lastPos=lastPos;
    backward(nextPos,true);
    Vec dx=Vec::Random(_body->nrDOF()),dc,X;
    MatT DDER,DTDL,DTDLL,DTDP,DTDD,DTDXL;
    _sol->compute(DDER=nextPos._HTheta);

    //debug DTDL
    _pos=pos;
    _lastPos=lastPos;
    DTDL=-_sol->solve(nextPos._HThetaL);
    pos1.reset(*_body,nextPos._info._xM);
    _pos.reset(*_body,_pos._info._xM+dx*DELTA);
    step();
    pos2.reset(*_body,_pos._info._xM);
    DEBUG_GRADIENT("DTDL",((pos2._info._xM-pos1._info._xM)/DELTA).norm(),(DTDL*dx-(pos2._info._xM-pos1._info._xM)/DELTA).norm())

    //debug DTDLL
    _pos=pos;
    _lastPos=lastPos;
    DTDLL=-_sol->solve(nextPos._HThetaLL);
    pos1.reset(*_body,nextPos._info._xM);
    _lastPos.reset(*_body,_lastPos._info._xM+dx*DELTA);
    step();
    pos2.reset(*_body,_pos._info._xM);
    DEBUG_GRADIENT("DTDLL",((pos2._info._xM-pos1._info._xM)/DELTA).norm(),(DTDLL*dx-(pos2._info._xM-pos1._info._xM)/DELTA).norm())

    //debug DTDP
    _pos=pos;
    _lastPos=lastPos;
    P2=P+dx*DELTA;
    DTDP=-_sol->solve(nextPos._HThetaPTarget);
    pos1.reset(*_body,nextPos._info._xM);
    for(int k=0; k<nrJ; k++) {
      Joint& J=_body->joint(k);
      PhysicsParameter& p=getJointPhysicsParameter(k);
      p._tarP=[&](T,int n) {
        return P2.segment(J._offDOF,n);
      };
      p._tarD=[&](T,int n) {
        return D.segment(J._offDOF,n);
      };
    }
    step();
    pos2.reset(*_body,_pos._info._xM);
    DEBUG_GRADIENT("DTDP",((pos2._info._xM-pos1._info._xM)/DELTA).norm(),(DTDP*dx-(pos2._info._xM-pos1._info._xM)/DELTA).norm())

    //debug DTDD
    _pos=pos;
    _lastPos=lastPos;
    D2=D+dx*DELTA;
    DTDD=-_sol->solve(nextPos._HThetaDTarget);
    pos1.reset(*_body,nextPos._info._xM);
    for(int k=0; k<nrJ; k++) {
      Joint& J=_body->joint(k);
      PhysicsParameter& p=getJointPhysicsParameter(k);
      p._tarP=[&](T,int n) {
        return P.segment(J._offDOF,n);
      };
      p._tarD=[&](T,int n) {
        return D2.segment(J._offDOF,n);
      };
    }
    step();
    pos2.reset(*_body,_pos._info._xM);
    DEBUG_GRADIENT("DTDD",((pos2._info._xM-pos1._info._xM)/DELTA).norm(),(DTDD*dx-(pos2._info._xM-pos1._info._xM)/DELTA).norm()) //recover D
    //recover
    for(int k=0; k<nrJ; k++) {
      Joint& J=_body->joint(k);
      PhysicsParameter& p=getJointPhysicsParameter(k);
      p._tarP=[&](T,int n) {
        return P.segment(J._offDOF,n);
      };
      p._tarD=[&](T,int n) {
        return D.segment(J._offDOF,n);
      };
    }

    //debug DTDXL
    _pos=pos;
    _lastPos=lastPos;
    DTDXL=-_sol->solve(nextPos._HThetaD);
    pos1.reset(*_body,nextPos._info._xM);
    dc=Vec::Random(_pos._nrVex*3);
    for(int k=0; k<nrJ; k++)
      if(_pos._polytopes[k].jid()>=0) {
        Eigen::Matrix<int,2,1> off=_pos._polytopes[k].getVertexId();
        X=dc.segment(off[0]*3,(off[1]-off[0])*3)*DELTA;
        moveMesh(_body->joint(k),X);
      }
    step();
    pos2.reset(*_body,_pos._info._xM);
    DEBUG_GRADIENT("DTDXL",(DTDXL*dc).norm(),(DTDXL*dc-(pos2._info._xM-pos1._info._xM)/DELTA).norm())
    break;
  }
}
void ConvHullPBDSimulator::debugBVHEnergy(GradInfo& grad) {
  DEFINE_NUMERIC_DELTA_T(T)
  T E1=0,E2=0,val;
  //BVH-assisted
  detectContact(grad._info._TM);
  for(auto& m:getManifolds()) {
    GJKPolytope<T>& mA=m._jidA<0?_obs[m._sidA]:grad._polytopes[m._jidA];
    GJKPolytope<T>& mB=m._jidB<0?_obs[m._sidB]:grad._polytopes[m._jidB];
    if(!mA.mesh() || !mB.mesh())
      continue;
    CCBarrierConvexEnergy<T,Barrier> cc(mA,mB,_barrier,0,&grad,_coefBarrier);
    ASSERT_MSG(cc.eval(&val,_body.get(),&grad,NULL,NULL,NULL),"Invalid debug configuration!")
    E1+=val;
  }
  //brute force
  int nrJ=_body->nrJ();
  int nrS=(int)_obs.size();
  for(int k1=0; k1<nrJ; k1++) {
    GJKPolytope<T>& mA=grad._polytopes[k1];
    if(!mA.mesh())
      continue;
    for(int k2=k1+1; k2<nrJ; k2++) {
      if(getContact().getExclude().find(Eigen::Matrix<int,2,1>(k1,k2))!=getContact().getExclude().end())
        continue;
      GJKPolytope<T>& mB=grad._polytopes[k2];
      if(!mB.mesh())
        continue;
      CCBarrierConvexEnergy<T,Barrier> cc(mA,mB,_barrier,0,&grad,_coefBarrier);
      ASSERT_MSGV(cc.eval(&val,_body.get(),&grad,NULL,NULL,NULL),"Invalid dynamic-dynamic configuration between (%d,%d)!",k1,k2)
      E2+=val;
    }
    for(int k2=0; k2<nrS; k2++) {
      CCBarrierConvexEnergy<T,Barrier> cc(mA,_obs[k2],_barrier,0,&grad,_coefBarrier);
      ASSERT_MSGV(cc.eval(&val,_body.get(),&grad,NULL,NULL,NULL),"Invalid dynamic-static configuration between (%d,%d)!",k1,k2)
      E2+=val;
    }
  }
  DEBUG_GRADIENT("BVH-E",E1,E1-E2)
}
void ConvHullPBDSimulator::setPD(const Vec& P, const Vec& D,int st) {
  _P=P;
  _D=D;
  for(int k=st; k<_body->nrJ(); k++) {
    //control
    Joint& J=_body->joint(k);
    J._control.setOnes(J.nrDOF());
    //param
    auto& param=getJointPhysicsParameter(k);
    param._kp=1e1;
    param._kd=1e0;
    param._tarP=[&](T,int)->Vec {
      return _P.segment(J._offDOF,J.nrDOF());
    };
    param._tarD=[&](T,int)->Vec {
      return _D.segment(J._offDOF,J.nrDOF());
    };
  }
}
//helper
void ConvHullPBDSimulator::detectContact(const Mat3XT& t) {
  getManifolds().clear();
  if(getContactPtr()==NULL)
    getContactPtr().reset(new ContactGenerator(_body,_shapes));
  GEOMETRY_SCALAR x0=(2*_barrier._x0)/(1-_barrier._x0);
  getContactPtr()->generateManifolds(x0,true,getManifolds(),t.template cast<GEOMETRY_SCALAR>());
}
bool ConvHullPBDSimulator::detectLastContact() {
  detectContact(_pos._info._TM);
  Vec DE;
  T e=normalEnergy(_pos,&DE,false);
  if(!isfinite(e))
    return false;
  _manifoldsLast.clear();
  for(auto &m:getManifolds())
    _manifoldsLast.push_back(m);
  return !_manifoldsLast.empty();
}
void ConvHullPBDSimulator::update(const GradInfo& newPos,GradInfo& newPos2,Vec& D,const Vec& DE,const MatT& DDE,T alpha) const {
  MatT DDER=DDE;
  DDER.diagonal().array()+=_alpha;
  //compute
  _sol->compute(DDER);
  //update
  D=-_sol->solve(MatT(DE));
  newPos2.reset(*_body,newPos._info._xM+D);
}
ConvHullPBDSimulator::T ConvHullPBDSimulator::energy(GradInfo& grad,Vec* DE) {
  T E=0;
  Vec3T P,PTotal;
  T MTotal=0;
  Mat3T PPT;
  Mat3XT GB,MRR,MRt,MtR,Mtt;
  detectContact(grad._info._TM);
  int nrJ=_body->nrJ();
  int nrD=_body->nrDOF();
  if(DE) {
    DE->setZero(nrD);
    grad._DTG.setZero(3,4*nrJ);
    _MRR.setZero(3,3*nrJ);
    _MRt.setZero(3,3*nrJ);
    _MtR.setZero(3,3*nrJ);
    _Mtt.setZero(3,3*nrJ);
    grad._HTheta.setZero(nrD,nrD);
  }
  grad._centre.resize(nrJ);
  //contact
  E+=normalEnergy(grad,DE);
  E+=tangentEnergy(grad,DE);
  for(int k=0; k<nrJ; k++) {
    const Joint& J=_body->joint(k);
    nrD=J.nrDOF();
    //update MC,MCCT
    PPT.setZero();
    P.setZero();
    PTotal.setZero();
    if(std::dynamic_pointer_cast<MeshExact>(J._mesh)) {
      std::shared_ptr<MeshExact> local=std::dynamic_pointer_cast<MeshExact>(J._mesh);
      T rho=J._M/(1.0*local->vss().size());
      for(int i=0; i<(int)local->vss().size(); i++) {
        P+=local->vss()[i].template cast<T>()*rho;
        PPT+=(local->vss()[i]*(local->vss()[i]).transpose()).template cast<T>()*rho;
        PTotal+=local->vss()[i].template cast<T>()/(1.0*local->vss().size());
      }
      grad._centre[k]=(ROT(TRANSI(grad._info._TM,k))*PTotal+CTR(TRANSI(grad._info._TM,k)));
      MTotal+=J._M;
    }
    //inertial
    E+=energyInertial(*getBody(),grad._info,_pos._info,_lastPos._info,
                      k,0,J._M,P,PPT,
                      DE?&(grad._DTG):NULL,_MRR,_MRt,_MtR,_Mtt);
    //PD controller
    E+=energyPDController(*getBody(),mapV2CV(grad._info._xM),mapV2CV(_pos._info._xM),k,nrD,DE,&(grad._HTheta));
    //joint limit
    if(_hardLimit) {
      if(!JointLimit::energy(mapV2CV(grad._info._xM),J,nrD,E,DE,&(grad._HTheta),_barrier))
        return std::numeric_limits<double>::infinity();
    } else E+=JointLimit::energy(mapV2CV(grad._info._xM),J,nrD,DE,&(grad._HTheta));
  }
  if(!isfinite(E))
    return E;
  //joints
  for(const auto& joint:getJoints())
    E+=joint.energy(*_body,grad._info,DE,grad._HTheta,grad._DTG,_MRR,_MRt,_MtR,_Mtt,true);
  if(DE) {
    //gradient
    grad._info.DTG(*_body,mapM(GB=grad._DTG),mapV(*DE));
    //hessian
    grad._info.toolAB(*_body,mapM(MRR=_MRR),mapM(MRt=_MRt),mapM(MtR=_MtR),mapM(Mtt=_Mtt),mapM(GB=grad._DTG),[&](int r,int c,T val) {
      grad._HTheta(r,c)+=val;
    });
  }
  return E;
}
ConvHullPBDSimulator::T ConvHullPBDSimulator::normalEnergy(GradInfo& grad,Vec* DE,bool backward) {
  T E=0;
  OMP_PARALLEL_FOR_
  for(int id=0; id<(int)getManifolds().size(); id++) {
    if(!isfinite(E))
      continue;
    ContactManifold& m=getManifolds()[id];
    GJKPolytope<T>& mA=m._jidA<0?_obs[m._sidA]:grad._polytopes[m._jidA];
    GJKPolytope<T>& mB=m._jidB<0?_obs[m._sidB]:grad._polytopes[m._jidB];
    if(!mA.mesh() || !mB.mesh())
      continue;
    //compute energy/gradient/hessian
    T val=0;
    CCBarrierConvexEnergy<T,Barrier> cc(mA,mB,_barrier,0,&grad,_coefBarrier);
    if(!backward) {
      if(!cc.eval(&val,_body.get(),DE?&grad:NULL,&m._DNDX,NULL,NULL))
        parallelAdd<T>(E,std::numeric_limits<T>::infinity());
      else {
        parallelAdd<T>(E,val);
        m._x=cc.getX();
      }
    } else {
      std::vector<MatX3T> HThetaD1,HThetaD2;
      cc.evalBackward(_body.get(),&grad,&HThetaD1,&HThetaD2);
    }
  }
  return E;
}
ConvHullPBDSimulator::T ConvHullPBDSimulator::tangentEnergy(GradInfo& grad,Vec* DE,bool backward) {
  T E=0;
  OMP_PARALLEL_FOR_
  for(int id=0; id<(int)_manifoldsLast.size(); id++) {
    if(!isfinite(E))
      continue;
    ContactManifold& m=_manifoldsLast[id];
    GJKPolytope<T>& mA=m._jidA<0?_obs[m._sidA]:grad._polytopes[m._jidA];
    GJKPolytope<T>& mB=m._jidB<0?_obs[m._sidB]:grad._polytopes[m._jidB];
    GJKPolytope<T>& mALast=m._jidA<0?_obs[m._sidA]:_pos._polytopes[m._jidA];
    GJKPolytope<T>& mBLast=m._jidB<0?_obs[m._sidB]:_pos._polytopes[m._jidB];
    Vec4T x=m._x;
    if(!mA.mesh() || !mB.mesh())
      continue;
    //compute energy/gradient/hessian
    T val=0;
    CCBarrierFrictionEnergy<T,Barrier> cf(mA,mB,mALast,mBLast,x,_barrier,0,&grad,_coefBarrier,_dt);
    if(!backward) {
      if(!cf.eval(&val,_body.get(),DE?&grad:NULL,NULL,NULL))
        parallelAdd<T>(E,std::numeric_limits<T>::infinity());
      else parallelAdd<T>(E,val);
    } else {
      std::vector<MatX3T> HThetaD1,HThetaD2;
      cf.evalBackward(_body.get(),&grad,&_pos,&m._DNDX,&HThetaD1,&HThetaD2);
    }
  }
  return E;
}
}
