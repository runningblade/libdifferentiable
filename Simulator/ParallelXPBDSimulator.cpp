#include "ParallelXPBDSimulator.h"
#include "PBDMatrixSolver.h"
#include "JointLimit.h"
#include "SoftJoint.h"
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
//PBDSimulator
extern void detectIslands(ArticulatedBody& body,Simulator& sim,std::vector<std::shared_ptr<ArticulatedBodyMapped>>& bodies);
ParallelXPBDSimulator::ParallelXPBDSimulator(T dt):XPBDSimulator(dt) {}
void ParallelXPBDSimulator::step() {
  if(_body->nrDOF()==0)
    return;
  //generate contacts
  detectCurrentContact();
  Vec x=setKinematic(_pos._xM,_t+_dt);
  {
    //detect islands
    std::vector<std::shared_ptr<ArticulatedBodyMapped>> bodies;
    detectIslands(*_body,*this,bodies);
    if(_output)
      std::cout << "#Island=" << bodies.size() << std::endl;
    //parallel solve
    OMP_PARALLEL_FOR_
    for(int i=0; i<(int)bodies.size(); i++) {
      //update kinematic state
      Vec xMapped;
      gather(*bodies[i],mapCV(x),xMapped);
      SolverState state(*bodies[i],xMapped),state2;
      auto sol=_sol->copy(*bodies[i]);
      //integrate all constraints
      solveBody(*bodies[i],sol,state,state2);
      //project all constraints
      projectBody(*bodies[i],sol,state);
      //update to x
      scatter(*bodies[i],mapV2CV(state._pos._xM),mapV(x));
    }
    //force deletion of mapped bodies
  }
  //update
  _lastPos=_pos;
  _pos.reset(*_body,x);
  _t+=_dt;
}
void ParallelXPBDSimulator::gather(const ArticulatedBodyMapped& body,VecCM from,Vec& to) const {
  to.resize(body.nrDOF());
  for(int i=0; i<body.nrJ(); i++) {
    const Joint& J=body.joint(i);
    to.segment(J._offDOF,J.nrDOF())=from.segment(body.getOffDOFUnmapped(i),J.nrDOF());
  }
}
void ParallelXPBDSimulator::scatter(const ArticulatedBodyMapped& body,VecCM from,VecM to) const {
  for(int i=0; i<body.nrJ(); i++) {
    const Joint& J=body.joint(i);
    to.segment(body.getOffDOFUnmapped(i),J.nrDOF())=from.segment(J._offDOF,J.nrDOF());
  }
}
}
