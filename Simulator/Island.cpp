#include "PBDSimulator.h"
#include "SoftJoint.h"
#include <Environment/ContactGenerator.h>

namespace PHYSICSMOTION {
void detectIslands(ArticulatedBody& body,Simulator& sim,std::vector<std::shared_ptr<ArticulatedBodyMapped>>& bodies) {
  sim.clearMappedBodyPointer();
  std::vector<int> disjointSet(body.nrJ(),-1);
  auto join=[&](int a,int b) {
    int da=0;
    while(disjointSet[a]>=0) {
      a=disjointSet[a];
      da++;
    }
    int db=0;
    while(disjointSet[b]>=0) {
      b=disjointSet[b];
      db++;
    }
    if(a==b)
      return;
    else if(da>db)
      disjointSet[b]=a;
    else disjointSet[a]=b;
  };
  //merge by connectivity
  for(int jid=0; jid<body.nrJ(); jid++) {
    const Joint& J=body.joint(jid);
    if(J._parent>=0) {
      const Joint& JP=body.joint(J._parent);
      if(JP._typeJoint==Joint::FIX_JOINT && JP._parent==-1)
        continue;
      else join(jid,J._parent);
    }
  }
  //merge by collision
  sim.visitContact(body,[&](ContactGenerator::ContactManifold& m) {
    if(m._jidA>=0 && m._jidB>=0)
      join(m._jidA,m._jidB);
  });
  //merge by soft joint
  sim.visitSoftJoint(body,[&](SoftJoint& joint) {
    if(joint._jidA>=0 && joint._jidB>=0)
      join(joint._jidA,joint._jidB);
  });
  //build bodies
  std::unordered_map<int,std::vector<int>> jidToBodyId;
  for(int jid=0; jid<body.nrJ(); jid++) {
    int rootId=jid;
    while(disjointSet[rootId]>=0)
      rootId=disjointSet[rootId];
    jidToBodyId[rootId].push_back(jid);
  }
  //initialize island
  for(const auto& item:jidToBodyId) {
    bodies.push_back(std::shared_ptr<ArticulatedBodyMapped>(new ArticulatedBodyMapped(body,item.second)));
    if(bodies.back()->nrDOF()==0)
      bodies.pop_back();
    //collisions
    sim.visitContact(*(bodies.back()),[&](ContactGenerator::ContactManifold& m) {
      m._mappedBody=bodies.back().get();
    });
    //joints
    sim.visitSoftJoint(*(bodies.back()),[&](SoftJoint& joint) {
      joint._mappedBody=bodies.back().get();
    });
  }
}
}
