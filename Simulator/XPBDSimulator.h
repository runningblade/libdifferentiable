#ifndef XPBD_SIMULATOR_H
#define XPBD_SIMULATOR_H

#include "PBDSimulator.h"

namespace PHYSICSMOTION {
//This solver implements a modified version of XPBD:
//XPBD: Position-Based Simulation of Compliant Constrained Dynamics
class XPBDSimulator : public PBDSimulator {
 public:
  XPBDSimulator(T dt);
  void step() override;
  void debugEnergy(T scale);
  void setCrossTerm(bool cross,bool CRBA);
 protected:
  T energy(const ArticulatedBodyMapped& body,std::shared_ptr<PBDMatrixSolver> sol,SolverState& state,bool updateTangentBound=false) override;
  void projectBody(const ArticulatedBodyMapped& body,std::shared_ptr<PBDMatrixSolver> sol,SolverState& state);
  void contactNormal(const ArticulatedBodyMapped& body,const GradInfo& newPos,const ContactManifold& m,const ContactPoint& p,std::vector<XPBDConstraint>& Css,int& off);
  void contactTangent(const ArticulatedBodyMapped& body,const GradInfo& newPos,const ContactManifold& m,ContactPoint& p,std::vector<XPBDConstraint>& Css,int d,int& off);
  template <int N=3>
  void project(const ArticulatedBodyMapped& body,std::shared_ptr<PBDMatrixSolver> sol,GradInfo& newPos,std::vector<XPBDConstraint>& Css,int beg,int end,T* limit,bool updateKinematics);
  T _epsDenom;
};
}

#endif
