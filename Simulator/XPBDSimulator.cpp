#include "XPBDSimulator.h"
#include "PBDMatrixSolver.h"
#include "JointLimit.h"
#include "SoftJoint.h"
#include <Utils/RotationUtils.h>
#include <Utils/CrossSpatialUtils.h>

namespace PHYSICSMOTION {
XPBDSimulator::XPBDSimulator(T dt):PBDSimulator(dt) {
  _maxIt=5;
  _gTol=1e-2f;
  _epsDenom=1e-3f;
}
void XPBDSimulator::step() {
  if(_body->nrDOF()==0)
    return;
  //generate contacts
  detectCurrentContact();
  //update kinematic state
  SolverState state(*_body,setKinematic(_pos._xM,_t+_dt)),state2;
  //integrate all constraints
  solveBody(*_body,_sol,state,state2);
  //project all constraints
  projectBody(*_body,_sol,state);
  //update
  _lastPos=_pos;
  _pos=state._pos;
  _t+=_dt;
}
void XPBDSimulator::debugEnergy(T scale) {
  DEFINE_NUMERIC_DELTA_T(T)
  //generate random pose
  SolverState state(*_body,Vec::Random(_body->nrDOF())*scale);
  _pos.reset(*_body,Vec::Random(_body->nrDOF())*scale);
  _lastPos.reset(*_body,Vec::Random(_body->nrDOF())*scale);

  //debug DE/DDE
  MatT DDE,DDE2;
  Vec DE,DE2,dx=Vec::Random(_body->nrDOF());
  SolverState state2(*_body,state._pos._xM+dx*DELTA);
  T e=energy(*_body,_sol,state);
  T e2=energy(*_body,_sol,state2);
  DEBUG_GRADIENT("DE",state._DE.dot(dx),state._DE.dot(dx)-(e2-e)/DELTA)
  DEBUG_GRADIENT("DDE",(state._DDE*dx).norm(),(state._DDE*dx-(state2._DE-state._DE)/DELTA).norm())
}
void XPBDSimulator::setCrossTerm(bool cross,bool CRBA) {
  _crossTerm=cross;
  if(_crossTerm) {
    if(_output)
      std::cout << "XPBD does not support cross term!" << std::endl;
    _crossTerm=false;
  }
  if(CRBA) {
    _sol.reset(new PBDMatrixSolverCRBA(*_body));
    if(_output)
      std::cout << "Choosing CRBA matrix solver!" << std::endl;
  } else {
    _sol.reset(new PBDMatrixSolverABA(*_body));
    if(_output)
      std::cout << "Choosing ABA matrix solver!" << std::endl;
    _JTJ=true;
  }
}
//helper
XPBDSimulator::T XPBDSimulator::energy(const ArticulatedBodyMapped& body,std::shared_ptr<PBDMatrixSolver> sol,SolverState& state,bool updateTangentBound) {
  T E=0,damping;
  int nrJ=body.nrJ();
  int nrD=body.nrDOF();
  state._DE.setZero(nrD);
  state._G.setZero(3,4*nrJ);
  state._MRR.setZero(3,3*nrJ);
  state._MRt.setZero(3,3*nrJ);
  state._MtR.setZero(3,3*nrJ);
  state._Mtt.setZero(3,3*nrJ);
  state._diag.setZero(nrD,nrD);
  state._DDE.setZero(nrD,nrD);
  for(int k=0; k<nrJ; k++) {
    //inertial
    const auto& J=body.joint(k);
    nrD=J.nrDOF();
    damping=0;
    if(J._damping.size()>0 && J._damping.maxCoeff()>0)
      damping=J._damping.maxCoeff();
    E+=energyInertial(body,state._pos,_pos,_lastPos,k,damping,
                      J._M,J._MC.template cast<T>(),J._MCCT.template cast<T>(),
                      &(state._G),state._MRR,state._MRt,state._MtR,state._Mtt);
    //PD controller
    E+=energyPDController(body,mapV2CV(state._pos._xM),mapV2CV(_pos._xM),k,nrD,&(state._DE),&(state._diag));
  }
  //gradient
  state._pos.DTG(body,mapM(state._GB=state._G),mapV(state._DE));
  //hessian
  if(!std::dynamic_pointer_cast<PBDMatrixSolverABA>(sol)) {
    if(_JTJ) {
      state._pos.toolA(body,state._pos,mapM(state._MRR),mapM(state._MRt),mapM(state._MtR),mapM(state._Mtt),[&](int r,int c,T val) {
        state._DDE(r,c)+=val;
      });
    } else {
      state._pos.toolAB(body,mapM(state._MRR),mapM(state._MRt),mapM(state._MtR),mapM(state._Mtt),mapM(state._GB=state._G),[&](int r,int c,T val) {
        state._DDE(r,c)+=val;
      });
    }
    for(int k=0; k<nrJ; k++) {
      const Joint& J=body.joint(k);
      nrD=J.nrDOF();
      state._DDE.template block(J._offDOF,J._offDOF,nrD,nrD)+=state._diag.template block(J._offDOF,J._offDOF,nrD,nrD);
    }
  }
  return E;
}
void XPBDSimulator::projectBody(const ArticulatedBodyMapped& body,std::shared_ptr<PBDMatrixSolver> sol,SolverState& state) {
  std::vector<XPBDConstraint> Css;
  int nrD=body.nrDOF();
  //contact
  visitContact(body,[&](ContactManifold& m) {
    for(auto& p:m._points) {
      p._fA.setZero();
      p._fB.setZero();
    }
  });
  //position project
  for(int i=0,off=0,off0; i<_maxIt; i++,off=0) {
    //project joint limit
    for(int k=0; k<body.nrJ(); k++) {
      const Joint& J=body.joint(k);
      off0=off;
      JointLimit::constraint(mapV2CV(state._pos._xM),J,J.nrDOF(),nrD,Css,off);
      project(body,sol,state._pos,Css,off0,off,NULL,false); //only update at last
    }
    state.reset(body,state._pos._xM);
    //project contact
    visitContact(body,[&](ContactManifold& m) {
      for(auto& p:m._points) {
        Vec2T fT=Vec2T::Zero();
        T fN=0,dfN=0,lmt=0;
        //normal
        off0=off;
        contactNormal(body,state._pos,m,p,Css,off);
        project(body,sol,state._pos,Css,off0,off,NULL,true);
        for(int k=off0; k<off; k++) {
          fN+=Css[k]._lambda;
          dfN+=Css[k]._dLambda;
        }
        //tangent
        if(m._fri>0) {
          off0=off;
          lmt=dfN*m._fri;
          for(int d=0; d<2; d++)
            contactTangent(body,state._pos,m,p,Css,d,off);
          project(body,sol,state._pos,Css,off0,off,&lmt,true);
          for(int k=off0; k<off; k++)
            fT[k-off0]=Css[k]._lambda;
        }
        //update force
        p._fA=-(p._nA2B*(GEOMETRY_SCALAR)fN+p._tA2B*fT.template cast<GEOMETRY_SCALAR>());
        p._fB=-p._fA;
      }
    });
    //project drag
    visitSoftJoint(body,[&](SoftJoint& joint) {
      off0=off;
      joint.constraint(body,state._pos,nrD,Css,off);
      project(body,sol,state._pos,Css,off0,off,NULL,true);
    });
  }
}
void XPBDSimulator::contactNormal(const ArticulatedBodyMapped& body,const GradInfo& newPos,const ContactManifold& m,const ContactPoint& p,std::vector<XPBDConstraint>& Css,int& off) {
  //allocate constraint
  if((int)Css.size()<=off) {
    Css.push_back(XPBDConstraint());
    Css[off]._JC.setZero(body.nrDOF());
  }
  //build position constraint
  XPBDConstraint& C=Css[off++];
  C._JC.setZero();
  Mat3X4T GK;
  Vec3T nA2B=p._nA2B.template cast<T>();
  Vec3T ptA=p._ptA.template cast<T>(),ptALast=ptA;
  Vec3T ptB=p._ptB.template cast<T>(),ptBLast=ptB;
  if(m._jidA>=0) {
    int jidAUnmapped=body.getIndex(m._jidA);
    ptA=ROTI(newPos._TM,m._jidA)*p._ptAL.template cast<T>()+CTRI(newPos._TM,m._jidA);
    ptALast=ROTI(_pos._TM,jidAUnmapped)*p._ptAL.template cast<T>()+CTRI(_pos._TM,jidAUnmapped);
    ROT(GK)=nA2B*p._ptAL.template cast<T>().transpose();
    CTR(GK)=nA2B;
    newPos.DTG(m._jidA,body,GK,[&](int r,T val) {
      C._JC[r]+=val;
    });
  }
  if(m._jidB>=0) {
    int jidBUnmapped=body.getIndex(m._jidB);
    ptB=ROTI(newPos._TM,m._jidB)*p._ptBL.template cast<T>()+CTRI(newPos._TM,m._jidB);
    ptBLast=ROTI(_pos._TM,jidBUnmapped)*p._ptBL.template cast<T>()+CTRI(_pos._TM,jidBUnmapped);
    ROT(GK)=-nA2B*p._ptBL.template cast<T>().transpose();
    CTR(GK)=-nA2B;
    newPos.DTG(m._jidB,body,GK,[&](int r,T val) {
      C._JC[r]+=val;
    });
  }
  C._C=std::max<T>((ptA-ptB).dot(nA2B),0);
  C._alpha=m._kc;
  //build velocity constraint
  T C0=std::max<T>((ptALast-ptBLast).dot(nA2B),0);
  if(C0==0)
    return;
  //allocate constraint
  if((int)Css.size()<=off) {
    Css.push_back(XPBDConstraint());
    Css[off]._JC.setZero(body.nrDOF());
  }
  //build constraint
  XPBDConstraint& CV=Css[off++];
  CV._C=((ptA-ptALast)-(ptB-ptBLast)).dot(nA2B);
  CV._JC=Css[off-2]._JC;
  CV._alpha=Css[off-2]._alpha;
}
void XPBDSimulator::contactTangent(const ArticulatedBodyMapped& body,const GradInfo& newPos,const ContactManifold& m,ContactPoint& p,std::vector<XPBDConstraint>& Css,int d,int& off) {
  //allocate constraint
  if((int)Css.size()<=off) {
    Css.push_back(XPBDConstraint());
    Css[off]._JC.setZero(body.nrDOF());
  }
  //build constraint
  XPBDConstraint& C=Css[off++];
  C._JC.setZero();
  Mat3X4T GK;
  Vec3T tA2B=p._tA2B.col(d).template cast<T>();
  Vec3T ptA=p._ptA.template cast<T>(),ptALast=ptA;
  Vec3T ptB=p._ptB.template cast<T>(),ptBLast=ptB;
  if(m._jidA>=0) {
    int jidAUnmapped=body.getIndex(m._jidA);
    ptA=ROTI(newPos._TM,m._jidA)*p._ptAL.template cast<T>()+CTRI(newPos._TM,m._jidA);
    ptALast=ROTI(_pos._TM,jidAUnmapped)*p._ptAL.template cast<T>()+CTRI(_pos._TM,jidAUnmapped);
    ROT(GK)=tA2B*p._ptAL.template cast<T>().transpose();
    CTR(GK)=tA2B;
    newPos.DTG(m._jidA,body,GK,[&](int r,T val) {
      C._JC[r]+=val;
    });
  }
  if(m._jidB>=0) {
    int jidBUnmapped=body.getIndex(m._jidB);
    ptB=ROTI(newPos._TM,m._jidB)*p._ptBL.template cast<T>()+CTRI(newPos._TM,m._jidB);
    ptBLast=ROTI(_pos._TM,jidBUnmapped)*p._ptBL.template cast<T>()+CTRI(_pos._TM,jidBUnmapped);
    ROT(GK)=-tA2B*p._ptBL.template cast<T>().transpose();
    CTR(GK)=-tA2B;
    newPos.DTG(m._jidB,body,GK,[&](int r,T val) {
      C._JC[r]+=val;
    });
  }
  C._C=((ptA-ptALast)-(ptB-ptBLast)).dot(tA2B);
  C._alpha=m._kc;
}
template <int N>
void XPBDSimulator::project(const ArticulatedBodyMapped& body,std::shared_ptr<PBDMatrixSolver> sol,GradInfo& newPos,std::vector<XPBDConstraint>& Css,int beg,int end,T* limit,bool updateKinematics) {
  Eigen::Matrix<T,-1,1,0,N,1> RHS;
  Eigen::Matrix<T,-1,-1,0,N,N> JTInvMJ;
  RHS.setZero(end-beg);
  JTInvMJ.setZero(end-beg,end-beg);
  ASSERT_MSG(end-beg<=4,"We can only couple at most 4 constraints!")
  if(end<=beg)
    return;
  for(int k=beg; k<end; k++) {
    XPBDConstraint& C=Css[k];
    mask(body,NULL,&(C._JC),NULL);
    C._invMJC=sol->solve(MatT(C._JC));
    RHS[k-beg]=C._C-C._lambda/C._alpha;
    for(int k2=k; k2<end; k2++) {
      const XPBDConstraint& C2=Css[k2];
      JTInvMJ(k2-beg,k-beg)=JTInvMJ(k-beg,k2-beg)=C2._JC.dot(C._invMJC);
    }
    JTInvMJ(k-beg,k-beg)+=1/C._alpha;
  }
  //compute step size
  RHS=JTInvMJ.inverse()*RHS;
  //handle limit
  if(limit) {
    for(int k=beg; k<end; k++)
      if(*limit<=0)
        RHS[k-beg]=0;
      else RHS[k-beg]=std::max<T>(std::min<T>(RHS[k-beg],*limit),-*limit);
  }
  //update position/lambda
  for(int k=beg; k<end; k++) {
    XPBDConstraint& C=Css[k];
    C._dLambda=RHS[k-beg];
    newPos._xM-=C._invMJC*C._dLambda;
    C._lambda+=C._dLambda;
  }
  //update position
  if(updateKinematics)
    newPos.reset(body,newPos._xM);
}
}
