#ifndef PBD_MATRIX_SOLVER_H
#define PBD_MATRIX_SOLVER_H

#include <Articulated/ArticulatedBody.h>
#include <Articulated/NEArticulatedGradientInfo.h>

namespace PHYSICSMOTION {
class PBDMatrixSolver {
 public:
  typedef FLOAT T;
  DECL_MAT_VEC_MAP_TYPES_T
  PBDMatrixSolver(const ArticulatedBodyBase& body);
  virtual ~PBDMatrixSolver();
  virtual void compute(const Eigen::MatrixBase<MatT>& h)=0;
  virtual MatT solve(const Eigen::MatrixBase<MatT>& b) const=0;
  virtual std::shared_ptr<PBDMatrixSolver> copy(const ArticulatedBodyBase& body) const=0;
 protected:
  const ArticulatedBodyBase& _body;
};
class PBDMatrixSolverEigen : public PBDMatrixSolver {
 public:
  PBDMatrixSolverEigen(const ArticulatedBodyBase& body);
  void compute(const Eigen::MatrixBase<MatT>& h) override;
  MatT solve(const Eigen::MatrixBase<MatT>& b) const override;
  std::shared_ptr<PBDMatrixSolver> copy(const ArticulatedBodyBase& body) const override;
 protected:
  Eigen::LDLT<Eigen::Matrix<double,-1,-1>> _ldlt;
};
class PBDMatrixSolverCRBA : public PBDMatrixSolver {
 public:
  typedef NEArticulatedGradientInfo<T> GradInfo;
  PBDMatrixSolverCRBA(const ArticulatedBodyBase& body);
  void compute(const Eigen::MatrixBase<MatT>& h) override;
  MatT solve(const Eigen::MatrixBase<MatT>& b) const override;
  std::shared_ptr<PBDMatrixSolver> copy(const ArticulatedBodyBase& body) const override;
 protected:
  GradInfo _info;
};
class PBDMatrixSolverABA : public PBDMatrixSolver {
 public:
  typedef NEArticulatedGradientInfo<T> GradInfo;
  PBDMatrixSolverABA(const ArticulatedBodyBase& body);
  void compute(const Eigen::MatrixBase<Vec>& q,const Eigen::MatrixBase<Mat3XT>& MRR,const Eigen::MatrixBase<Mat3XT>& MRt,const Eigen::MatrixBase<Mat3XT>& MtR,const Eigen::MatrixBase<Mat3XT>& Mtt,const Eigen::MatrixBase<MatT>& d);
  void compute(const Eigen::MatrixBase<Vec>& q,const Eigen::MatrixBase<Mat6XT>& I,const Eigen::MatrixBase<MatT>& d);
  void compute(const Eigen::MatrixBase<MatT>& h) override;
  MatT solve(const Eigen::MatrixBase<MatT>& b) const override;
  std::shared_ptr<PBDMatrixSolver> copy(const ArticulatedBodyBase& body) const override;
 protected:
  GradInfo _info;
  Mat6XT _I;
  MatT _d;
};
}

#endif
