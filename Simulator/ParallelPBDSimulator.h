#ifndef PARALLEL_PBD_SIMULATOR_H
#define PARALLEL_PBD_SIMULATOR_H

#include "PBDSimulator.h"

namespace PHYSICSMOTION {
//This solver implements PBAD, but uses island-based parallelization
class ParallelPBDSimulator : public PBDSimulator {
 public:
  DECL_MAP_FUNCS
  ParallelPBDSimulator(T dt);
  void step() override;
 protected:
  void gather(const ArticulatedBodyMapped& body,VecCM from,Vec& to) const;
  void scatter(const ArticulatedBodyMapped& body,VecCM from,VecM to) const;
};
}

#endif
