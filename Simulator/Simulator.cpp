#include "Simulator.h"
#include "SoftJoint.h"
#include <Utils/CrossSpatialUtils.h>
#include <Articulated/PBDArticulatedGradientInfo.h>

namespace PHYSICSMOTION {
Simulator::PhysicsParameter::PhysicsParameter()
  :_isKinematic(3,false),_kp(0),_kd(0),_friction(.75f),_kc(1e1f) {
  _kin=_tarP=_tarD=Simulator::_defaultFunction;
}
void Simulator::XPBDConstraint::update(VecM x) {
  ASSERT_MSG(_isLinear,"Cannot update nonlinear XPBDConstraint!")
  T C=x.dot(_JC);
  if(isfinite(_CL) && C<_CL)
    _C=C-_CL;
  else if(isfinite(_CU) && C>_CU)
    _C=C-_CU;
  else _C=0;
}
Simulator::Simulator(T dt):_dt(dt),_t(0),_output(false) {}
Simulator::~Simulator() {}
void Simulator::clearJoint() {
  _joints.clear();
}
void Simulator::addJoint(const SoftJoint& joint) {
  _joints.push_back(joint);
}
const std::vector<SoftJoint>& Simulator::getJoints() const {
  return _joints;
}
std::vector<SoftJoint>& Simulator::getJoints() {
  return _joints;
}
void Simulator::clearShape() {
  if(_body)
    _params.assign(_body->nrJ(),PhysicsParameter());
  else _params.clear();
  _contact=NULL;
  _t=0;
}
void Simulator::addShape(std::shared_ptr<ShapeExact> shape) {
  _shapes.push_back(shape);
  _contact=NULL;
}
void Simulator::setArticulatedBody(std::shared_ptr<ArticulatedBody> body) {
  _body=body;
  if(_body)
    _params.assign(_body->nrJ(),PhysicsParameter());
  else _params.clear();
  _contact=NULL;
}
void Simulator::setHeuristcGuessStiffness(T coef) {
  for(int k=0; k<_body->nrJ(); k++) {
    T Mg=0;
    int parentId=k;
    while(parentId>=0) {
      Mg+=CTRI(_JRCF,parentId).norm();
      parentId=_body->joint(parentId)._parent;
    }
    _params[k]._kc=coef*Mg;
  }
}
Simulator::T Simulator::getHeuristcGuessStiffness() const {
  T num=0,denom=0;
  for(int k=0; k<_body->nrJ(); k++) {
    T Mg=0;
    int parentId=k;
    while(parentId>=0) {
      Mg+=CTRI(_JRCF,parentId).norm();
      parentId=_body->joint(parentId)._parent;
    }
    num+=_params[k]._kc;
    denom+=Mg;
  }
  return num/denom;
}
const std::vector<Simulator::ContactManifold>& Simulator::getManifolds() const {
  return _manifolds;
}
const std::vector<std::shared_ptr<ShapeExact>>& Simulator::getShapes() const {
  return _shapes;
}
const Simulator::PhysicsParameter& Simulator::getJointPhysicsParameter(int jid) const {
  return _params[jid];
}
Simulator::PhysicsParameter& Simulator::getJointPhysicsParameter(int jid) {
  return _params[jid];
}
std::shared_ptr<ArticulatedBody> Simulator::getBody() const {
  return _body;
}
ContactGenerator& Simulator::getContact() {
  if(!_contact)
    _contact.reset(new ContactGenerator(_body,_shapes));
  PBDArticulatedGradientInfo<T> info(*_body,pos());
  ContactGenerator::Mat3XT t=info._TM.template cast<GEOMETRY_SCALAR>();
  _contact->updateBVH(t);
  return *_contact;
}
void Simulator::setGravity(const Vec3T& g) {
  _JRCF.setZero(3,_body->nrJ()*4);
  for(int i=0; i<_body->nrJ(); i++)
    if(_body->joint(i)._M>0) {
      ROTI(_JRCF,i)=-g*_body->joint(i)._MC.transpose().template cast<T>();
      CTRI(_JRCF,i)=-_body->joint(i)._M*g;
    }
}
Simulator::Vec3T Simulator::getGravity() const {
  T denom=0;
  Vec3T g=Vec3T::Zero();
  for(int i=0; i<_body->nrJ(); i++)
    if(_body->joint(i)._M>0) {
      g-=CTRI(_JRCF,i);
      denom+=_body->joint(i)._M;
    }
  return g/denom;
}
void Simulator::setTime(T t) {
  _t=t;
  setPos(setKinematic(pos(),_t));
}
Simulator::T Simulator::getTime() const {
  return _t;
}
void Simulator::setOutput(bool output) {
  _output=output;
}
void Simulator::randomizeContact(const GradInfo& pos) {
  _manifolds.clear();
  int nrJ=_body->nrJ();
  for(int k=0; k<nrJ; k++) {
    ContactPoint p;
    p._ptA=Vec3T::Random().template cast<GEOMETRY_SCALAR>();
    p._ptB=Vec3T::Random().template cast<GEOMETRY_SCALAR>();
    p._nA2B=Vec3T::Random().normalized().template cast<GEOMETRY_SCALAR>();
    if(p.depth()<0)
      p._nA2B*=-1;
    ContactManifold m;
    m._points.push_back(p);
    //add dynamic-static contact
    m._jidA=k;
    m._jidB=-1;
    _manifolds.push_back(m);
    //add static-dynamic contact
    m._jidA=-1;
    m._jidB=k;
    _manifolds.push_back(m);
    //add dynamic-dynamic contact
    m._jidA=rand()%nrJ;
    m._jidB=k;
    if(m._jidA!=m._jidB)
      _manifolds.push_back(m);
  }
  computeLocalContactPos(pos._TM);
}
void Simulator::randomizeSoftJoint() {
  _joints.clear();
  int nrJ=_body->nrJ();
  for(int k=0; k<nrJ; k++) {
    SoftJoint j;
    j.setRandom(*_body);
    _joints.push_back(j);
  }
}
void Simulator::randomizePDController() {
  Vec P=Vec::Random(_body->nrDOF());
  Vec D=Vec::Random(_body->nrDOF());
  for(int k=0; k<_body->nrJ(); k++) {
    Joint& J=_body->joint(k);
    J._limits.row(2).setRandom();

    PhysicsParameter& p=_params[k];
    p._kp=rand()/(T)RAND_MAX;
    p._kd=rand()/(T)RAND_MAX;
    p._tarP=[&](T,int n) {
      return P.segment(J._offDOF,n);
    };
    p._tarD=[&](T,int n) {
      return D.segment(J._offDOF,n);
    };
  }
}
//algorithm for mapped body
void Simulator::clearMappedBodyPointer() {
  for(auto& m:const_cast<std::vector<ContactManifold>&>(_manifolds))
    m._mappedBody=NULL;
  for(auto& joint:const_cast<std::vector<SoftJoint>&>(_joints))
    joint._mappedBody=NULL;
}
void Simulator::visitContact(const ArticulatedBodyMapped& body,std::function<void(ContactManifold&)> f) {
  for(auto& m:const_cast<std::vector<ContactManifold>&>(_manifolds)) {
    if(m._mappedBody!=NULL && m._mappedBody!=&body)
      continue;
    int jidA=-1,jidAMapped=-1;
    int jidB=-1,jidBMapped=-1;
    bool relatedA=body.map(jidA=m._jidA,jidAMapped);
    bool relatedB=body.map(jidB=m._jidB,jidBMapped);
    if(m._mappedBody!=NULL) {
      ASSERT_MSG(relatedA || relatedB,"Island is incorrectly identified!")
    }
    if(relatedA || relatedB) {
      m._jidA=jidAMapped;
      m._jidB=jidBMapped;
      f(m);
      m._jidA=jidA;
      m._jidB=jidB;
    }
  }
}
void Simulator::visitSoftJoint(const ArticulatedBodyMapped& body,std::function<void(SoftJoint&)> f) {
  for(auto& joint:const_cast<std::vector<SoftJoint>&>(_joints)) {
    if(joint._mappedBody!=NULL && joint._mappedBody!=&body)
      continue;
    int jidA=-1,jidAMapped=-1;
    int jidB=-1,jidBMapped=-1;
    bool relatedA=body.map(jidA=joint._jidA,jidAMapped);
    bool relatedB=body.map(jidB=joint._jidB,jidBMapped);
    if(joint._mappedBody!=NULL) {
      ASSERT_MSG(relatedA || relatedB,"Island is incorrectly identified!")
    }
    if(relatedA || relatedB) {
      joint._jidA=jidAMapped;
      joint._jidB=jidBMapped;
      f(joint);
      joint._jidA=jidA;
      joint._jidB=jidB;
    }
  }
}
//helper
std::shared_ptr<ContactGenerator>& Simulator::getContactPtr() {
  return _contact;
}
std::vector<Simulator::ContactManifold>& Simulator::getManifolds() {
  return _manifolds;
}
Simulator::Mat3XT& Simulator::getJRCF() {
  return _JRCF;
}
Simulator::Vec Simulator::setKinematic(const Vec& pos,T time) const {
  Vec posKinematic=pos;
  for(int i=0; i<_body->nrJ(); i++) {
    const Joint& J=_body->joint(i);
    int nrDJ=J.nrDOF();
    Vec curr=_params[i]._kin(time,nrDJ);
    for(int c=0; c<nrDJ; c++) {
      //kinematic target
      if(_params[i]._isKinematic[c])
        posKinematic[J._offDOF+c]=curr[c];
      //constraint
      T l=J._limits(0,c);
      T h=J._limits(1,c);
      bool locked=isfinite(J._limits(2,c)) && J._limits(2,c)>0 && isfinite(l) && isfinite(h) && l==h;
      if(locked)
        posKinematic[J._offDOF+c]=l;
    }
  }
  return posKinematic;
}
Simulator::Vec Simulator::setKinematicVel(const Vec& vel,T time,T dt) const {
  Vec velKinematic=vel;
  for(int i=0; i<_body->nrJ(); i++) {
    int nrDJ=_body->joint(i).nrDOF();
    Vec next=_params[i]._kin(time+dt,nrDJ);
    Vec curr=_params[i]._kin(time,nrDJ);
    for(int c=0; c<nrDJ; c++)
      if(_params[i]._isKinematic[c])
        velKinematic[_body->joint(i)._offDOF+c]=(next[c]-curr[c])/dt;
  }
  return velKinematic;
}
void Simulator::computeLocalContactPos(const Mat3XT& t) {
  for(auto& m:_manifolds) {
    m._kc=std::max<T>(m._jidA>=0?_params[m._jidA]._kc:0,m._jidB>=0?_params[m._jidB]._kc:0);
    m._fri=std::max<T>(m._jidA>=0?_params[m._jidA]._friction:0,m._jidB>=0?_params[m._jidB]._friction:0);
    for(auto& p:m._points) {
      ContactGenerator::Vec3T::Index id;
      p._nA2B.cwiseAbs().minCoeff(&id);
      p._tA2B.col(0)=p._nA2B.cross(ContactGenerator::Vec3T::Unit(id)).template cast<T>().normalized().template cast<ContactGenerator::T>();
      p._tA2B.col(1)=p._nA2B.cross(p._tA2B.col(0));
      if(m._jidA>=0)
        p._ptAL=ROTI(t,m._jidA).template cast<GEOMETRY_SCALAR>().transpose()*(p._ptA-CTRI(t,m._jidA).template cast<GEOMETRY_SCALAR>());
      if(m._jidB>=0)
        p._ptBL=ROTI(t,m._jidB).template cast<GEOMETRY_SCALAR>().transpose()*(p._ptB-CTRI(t,m._jidB).template cast<GEOMETRY_SCALAR>());
    }
  }
}
void Simulator::detectContact(const Mat3XT& t) {
  _manifolds.clear();
  if(!_contact)
    _contact.reset(new ContactGenerator(_body,_shapes));
  _contact->generateManifolds(0,false,_manifolds,t.template cast<GEOMETRY_SCALAR>());
  computeLocalContactPos(t);
}
//algorithm for mapped body
void Simulator::mask(const ArticulatedBodyMapped& body,MatT* diag,Vec* DE,MatT* DDE) const {
  int nrJ=body.nrJ();
  for(int k=0; k<nrJ; k++) {
    const auto& param=_params[body.getIndex(k)];
    const Joint& J=body.joint(k);
    int nrDJ=J.nrDOF();
    for(int c=0; c<nrDJ; c++) {
      T l=J._limits(0,c);
      T h=J._limits(1,c);
      bool locked=isfinite(J._limits(2,c)) && J._limits(2,c)>0 && isfinite(l) && isfinite(h) && l==h;
      if(locked || param._isKinematic[c]) {
        if(diag)
          diag->diagonal()[J._offDOF+c]=std::numeric_limits<double>::infinity();
        if(DE)
          DE->coeffRef(J._offDOF+c)=0;
        if(DDE) {
          DDE->row(J._offDOF+c).setZero();
          DDE->col(J._offDOF+c).setZero();
          DDE->diagonal()[J._offDOF+c]=0;
        }
      }
    }
  }
}
Simulator::T Simulator::energyInertial
(const ArticulatedBodyMapped& body,const GradInfo& next,const GradInfo& curr,const GradInfo& last,
 int jid,T damping,T M,const Vec3T& P,const Mat3T& PPT,
 Mat3XT* G,Mat3XT& MRR,Mat3XT& MRt,Mat3XT& MtR,Mat3XT& Mtt) const {
  int jidUnmapped=body.getIndex(jid);
  T E=0,coef;
  Mat3XT A;
  //dynamic force
  coef=1/(_dt*_dt);
  if(G) {
    MRR.template block<3,3>(0,jid*3)-=invDoubleCrossMatTrace<T>(ROTI(next._TM,jid)*PPT*ROTI(next._TM,jid).transpose())*coef;
    MRt.template block<3,3>(0,jid*3)+=cross<T>(ROTI(next._TM,jid)*P)*coef;
    MtR.template block<3,3>(0,jid*3)-=cross<T>(ROTI(next._TM,jid)*P)*coef;
    Mtt.template block<3,3>(0,jid*3)+=Mat3T::Identity()*M*coef;
  }
  A=TRANSI(next._TM,jid)-2*TRANSI(curr._TM,jidUnmapped)+TRANSI(last._TM,jidUnmapped);
  E+=(ROT(A)*PPT*ROT(A).transpose()+2*CTR(A)*P.transpose()*ROT(A).transpose()+CTR(A)*CTR(A).transpose()*M).trace()*coef/2;
  if(G) {
    ROTI((*G),jid)+=(ROT(A)*PPT+CTR(A)*P.transpose())*coef;
    CTRI((*G),jid)+=(CTR(A)*M+ROT(A)*P)*coef;
  }
  //damping force
  if(damping>0) {
    coef=damping/_dt;
    if(G) {
      MRR.template block<3,3>(0,jid*3)-=invDoubleCrossMatTrace<T>(ROTI(next._TM,jid)*PPT*ROTI(next._TM,jid).transpose())*coef;
      MRt.template block<3,3>(0,jid*3)+=cross<T>(ROTI(next._TM,jid)*P)*coef;
      MtR.template block<3,3>(0,jid*3)-=cross<T>(ROTI(next._TM,jid)*P)*coef;
      Mtt.template block<3,3>(0,jid*3)+=Mat3T::Identity()*M*coef;
    }
    A=TRANSI(next._TM,jid)-TRANSI(curr._TM,jidUnmapped);
    E+=(ROT(A)*PPT*ROT(A).transpose()+2*CTR(A)*P.transpose()*ROT(A).transpose()+CTR(A)*CTR(A).transpose()*M).trace()*coef/2;
    if(G) {
      ROTI((*G),jid)+=(ROT(A)*PPT+CTR(A)*P.transpose())*coef;
      CTRI((*G),jid)+=(CTR(A)*M+ROT(A)*P)*coef;
    }
  }
  //external force
  E+=(TRANSI(next._TM,jid)*TRANSI(_JRCF,jidUnmapped).transpose()).trace();
  if(G)
    TRANSI((*G),jid)+=TRANSI(_JRCF,jidUnmapped);
  return E;
}
Simulator::T Simulator::energyPDController
(const ArticulatedBodyMapped& body,VecCM x,VecCM xL,
 int jid,int nrD,Vec* DE,MatT* DDE) const {
  T E=0;
  int offDOFUnmapped=body.getOffDOFUnmapped(jid);
  const auto& param=_params[body.getIndex(jid)];
  const Joint& J=body.joint(jid);
  Eigen::Matrix<T,-1,1,0,3,1> diff,coef;
  if(J._control.size()>=nrD)
    coef=J._control.segment(0,nrD).template cast<T>();
  else coef.setOnes(nrD);
  //P controller
  if(param._kp>0) {
    diff=x.segment(J._offDOF,nrD)-param._tarP(_t,nrD);
    E+=(diff.array().square()*coef.array()*param._kp).sum()/2;
    if(DE)
      DE->segment(J._offDOF,nrD).array()+=diff.array()*coef.array()*param._kp;
    if(DDE)
      DDE->diagonal().segment(J._offDOF,nrD).array()+=coef.array()*param._kp;
  }
  //D controller
  if(param._kd>0) {
    diff=(x.segment(J._offDOF,nrD)-xL.segment(offDOFUnmapped,nrD))/_dt-param._tarD(_t,nrD);
    E+=(diff.array().square()*coef.array()*param._kd).sum()/2;
    if(DE)
      DE->segment(J._offDOF,nrD).array()+=diff.array()*coef.array()*param._kd/_dt;
    if(DDE)
      DDE->diagonal().segment(J._offDOF,nrD).array()+=coef.array()*param._kd/_dt/_dt;
  }
  return E;
}
Simulator::DOFFunction Simulator::_defaultFunction=[](Simulator::T,int nrDOF) {
  return Simulator::Vec::Zero(nrDOF);
};
}
