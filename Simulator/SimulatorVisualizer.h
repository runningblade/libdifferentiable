#include <Simulator/Simulator.h>

namespace PHYSICSMOTION {
typedef FLOAT T;
DECL_MAT_VEC_MAP_TYPES_T
extern void visualizeSimulator(int argc,char** argv,Simulator& sim,int nSub=1,float maxStiffness=1e4f);
void visualizeTrajectory(int argc,char** argv,Simulator& sim,const std::vector<Vec>* theta,int* frame);
void visualize(Simulator& sim,const std::vector<Vec>* theta,int* frame);
}
