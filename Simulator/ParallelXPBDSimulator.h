#ifndef PARALLEL_XPBD_SIMULATOR_H
#define PARALLEL_XPBD_SIMULATOR_H

#include "XPBDSimulator.h"

namespace PHYSICSMOTION {
//This solver implements PBAD, but uses island-based parallelization
class ParallelXPBDSimulator : public XPBDSimulator {
 public:
  DECL_MAP_FUNCS
  ParallelXPBDSimulator(T dt);
  void step() override;
 protected:
  void gather(const ArticulatedBodyMapped& body,VecCM from,Vec& to) const;
  void scatter(const ArticulatedBodyMapped& body,VecCM from,VecM to) const;
};
}

#endif
