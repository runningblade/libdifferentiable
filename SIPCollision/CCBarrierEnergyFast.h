#ifndef FAST_CC_BARRIER_ENERGY_H
#define FAST_CC_BARRIER_ENERGY_H

#include <ConvexHull/ConvexHullDistanceEnergy.h>
#include "Utils/CrossSpatialUtils.h"
#include "CCSeparatingPlanes.h"

namespace PHYSICSMOTION {
template<typename T, typename PFunc, typename TH=typename HigherPrecisionTraits<T>::TH>
class CCBarrierEnergyFast: public CCBarrierEnergy<T,PFunc,TH> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  using CCBarrierEnergy<T,PFunc,TH>::_coef;
  using CCBarrierEnergy<T,PFunc,TH>::_d0Half;
  using CCBarrierEnergy<T,PFunc,TH>::_p;
  using CCBarrierEnergy<T,PFunc,TH>::_x;
  using CCBarrierEnergy<T,PFunc,TH>::_p1;
  using CCBarrierEnergy<T,PFunc,TH>::_p2;
  using CCBarrierEnergy<T,PFunc,TH>::getX;
  using CCBarrierEnergy<T,PFunc,TH>::optimize;
  typedef Eigen::Matrix<TH,3,1> Vec3TH;
  typedef Eigen::Matrix<TH,4,1> Vec4TH;
  typedef Eigen::Matrix<TH,4,4> Mat4TH;
  typedef GJKPolytope<T> EntityT;
  typedef CCSeparatingPlaneSmart<T> SmartPlaneT;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef Eigen::Triplet<T,int> STrip;
  CCBarrierEnergyFast(const EntityT& p1, const EntityT& p2,const PFunc& p,T d0,const CollisionGradInfo<T>* grad,T coef);
  //GTheta and HTheta are not NULL only when debugging.
  bool eval(T* E,const ArticulatedBody* body,CollisionGradInfo<T>* grad,std::array<Mat3X4T,4>* DNDX,Vec* GTheta,MatT* HTheta,Vec4T* x=NULL) override;
  void assemblePlaneGH(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,SmartPlaneT& plane);
  bool optimizeX(Vec4T& x);
 private:
  bool PNEnergy(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,const Vec4T& x, T& E);
  bool PEnergy(const Vec3T& v,const Vec4T& x,T& E,const Vec3T* vl,const Mat3T* R);
  bool NEnergy(const Vec3T& v,const Vec4T& x,T& E,const Vec3T* vl,const Mat3T* R);
  inline T evalPDist(const Vec3T& v,const Vec4T& x) const {
    return v.dot(x.template segment<3>(0))-x[3];
  }
  inline T evalNDist(const Vec3T& v,const Vec4T& x) const {
    return x[3]-v.dot(x.template segment<3>(0));
  }
  inline T evalBarrier(T dist,T* D=NULL,T* DD=NULL) const {
    return _p.template eval<T>(dist,D,DD,_d0Half,1);
  }
  void HxvToHthetax(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,SMatT& Hthetax);
 private:
  Vec4T _Gx;
  Mat4T _Hxx;
  Mat4X3T _HxvP;
  Mat4X3T _HxvCRvlP;
  Mat4X3T _HxvN;
  Mat4X3T _HxvCRvlN;
};
}
#endif
