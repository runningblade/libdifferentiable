#include "CollisionBarrierEnergyFast.h"
#include <Utils/Timing.h>

namespace PHYSICSMOTION {
template<typename T, typename PFunc, typename CCPlaneT>
CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::CollisionBarrierEnergyFast(const ArticulatedBody &body,
    const Vec &controlPoints,
    const ThetaTrajectory<T> &tt,
    std::vector<std::pair<EntityId<T>, EntityId<T>>> &TTPairs,
    CCSeparatingPlanes<T, CCPlaneT> &CCPlanes, std::unordered_map<T,GradInfo> &gradInfo,
    T d0, T x0, T coef, T minEig, bool JTJApprox, bool implicit):
  CollisionBarrierEnergy<T, PFunc, CCPlaneT>(body, controlPoints, tt, TTPairs, CCPlanes, gradInfo, d0, x0, coef, JTJApprox, false),
  _minEig(minEig) {
  this->setFast(true);
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::resetCCPlanesToUpdate() {
  const std::vector<CCPlaneT*>& planes = this->planeHandles();
  // sequential push back
  _planeToUpdateHandles.clear();
  _CCPairsToUpdate.clear();
  for(int i=0; i<(int)planes.size(); i++)
    if(planes[i]->_toUpdate) {
      _planeToUpdateHandles.push_back(planes[i]);
      _CCPairsToUpdate.push_back(this->CCPairs()[i]);
    }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::localOptimizePlanes() {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    const std::pair<EntityId<T>, EntityId<T>> &pair = _CCPairsToUpdate[i];
    T timeAvg = pair.first.getTimeAvg();
    const EntityGradT& grad = this->gradInfo().at(timeAvg);
    const EntityT& p1 = pair.first.getPolytope(grad);
    const EntityT& p2 = pair.second.getPolytope(grad);
    T timeDiff = pair.first.getTimeDiff();
    if(timeDiff == 0 ) timeDiff = 1;
    CCBarrierEnergyFast<T,PFunc> cc(p1,p2, this->pCC(), this->d0(), &grad,this->_coef* timeDiff * 2);
    cc.setOutput(this->output());
    cc.initialize(_planeToUpdateHandles[i]->_x);
    cc.optimizeX(_planeToUpdateHandles[i]->_x);
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
int CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::numCCPlanesToUpdate() {
  return _planeToUpdateHandles.size();
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::clearCCPlanesInitFlag() {
  const std::vector<CCPlaneT*>& planes = this->planeHandles();
  OMP_PARALLEL_FOR_
  for(auto plane : planes) plane->_initialized = false;
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::assignCCPlanesNewX() {
  OMP_PARALLEL_FOR_
  for(auto plane : _planeToUpdateHandles) plane->_newX = plane->_x;
}
// assign after line search successes
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::assignCCPlanesX() {
  OMP_PARALLEL_FOR_
  for(auto plane : _planeToUpdateHandles) plane->_x = plane->_newX;
}
// activate before line search starts
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::activateCCPlanesNewX() {
  OMP_PARALLEL_FOR_
  for(auto plane : _planeToUpdateHandles) plane->_useNewX = true;

}
// deactivate after line search ends
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::deactivateCCPlanesNewX() {
  //planes can be deleted in line search due to subdivide
  //extract again to avoid segfault
  this->extractCCPairs();
  OMP_PARALLEL_FOR_
  for(auto plane : this->planeHandles()) plane->_useNewX = false;
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::perturbCCPlanes(T delta, const Vec& dx, bool normalize) {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    _planeToUpdateHandles[i]->_x += delta * dx.template segment<4>(4*i);
    if(normalize) _planeToUpdateHandles[i]->_x.template segment<3>(0).normalize();
  }
}
// in line search
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::updateNewCCPlanesX(const Vec &d, T alpha) {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    _planeToUpdateHandles[i]->_newX =  _planeToUpdateHandles[i]->_x + alpha * d.template segment<4>(4*i);
    _planeToUpdateHandles[i]->_newX.template segment<3>(0).normalize();
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::computeSchurGH(Vec& GSchur, MatT& HSchur,const std::vector<int>& fixVar) {
  computeHcpx(GSchur.size(),fixVar);
  computeHxxInv();
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    GSchur -=_planeToUpdateHandles[i]->_Hcpx * _planeToUpdateHandles[i]->_HxxInv * _planeToUpdateHandles[i]->_Gx;
    HSchur -= _planeToUpdateHandles[i]->_Hcpx * _planeToUpdateHandles[i]->_HxxInv  *_planeToUpdateHandles[i]->_Hcpx.transpose();
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T, PFunc, CCPlaneT>::computeHxxInv() {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    Vec4T norm;
    norm<<_planeToUpdateHandles[i]->_x.template segment<3>(0),0;
    const Mat4T& Hxx = _planeToUpdateHandles[i]->_Hxx;
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,4,4>> eigenSol;
    eigenSol.compute(Hxx.template cast<double>(), Eigen::ComputeEigenvectors);
    T minEigIter = std::max<T>(_minEig, _minEig*eigenSol.eigenvalues().maxCoeff());
    Mat4T PDInv = eigenSol.eigenvectors().template cast<T>() * \
                  eigenSol.eigenvalues().template cast<T>().cwiseMax(minEigIter).asDiagonal().inverse() * \
                  eigenSol.eigenvectors().template cast<T>().transpose();
    _planeToUpdateHandles[i]->_HxxInv = PDInv - (PDInv*norm*norm.transpose()*PDInv)/(norm.transpose()*PDInv*norm);
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T, PFunc, CCPlaneT>::makePlaneHxxPD() {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,4,4>> eigenSol;
    eigenSol.compute(_planeToUpdateHandles[i]->_Hxx.template cast<double>(), Eigen::ComputeEigenvectors);
    ASSERT_MSG(eigenSol.info()==Eigen::Success, "EigDecomp fails.")
    T minEigIter = std::max<T>(_minEig, _minEig*eigenSol.eigenvalues().maxCoeff());
    _planeToUpdateHandles[i]->_Hxx =  eigenSol.eigenvectors().template cast<T>() * \
                                      eigenSol.eigenvalues().template cast<T>().cwiseMax(minEigIter).asDiagonal()* \
                                      eigenSol.eigenvectors().template cast<T>().transpose();
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::schurSolveSearchDirection(const Vec& dcp,Eigen::Ref<Vec> dx) {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    dx.template segment<4>(4*i) = _planeToUpdateHandles[i]->_HxxInv * (-_planeToUpdateHandles[i]->_Gx -  _planeToUpdateHandles[i]->_Hcpx.transpose()*dcp);
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::assembleHConstraints(Eigen::Ref<MatT> Hc,Eigen::Ref<MatT> HcT) {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    Hc.template block<1,3>(i,i*4) = _planeToUpdateHandles[i]->_x.template segment<3>(0).transpose();
    HcT.template block<3,1>(i*4,i) = _planeToUpdateHandles[i]->_x.template segment<3>(0);
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::assembleGx(Eigen::Ref<Vec> G,bool proj) {
  if(proj) {
    OMP_PARALLEL_FOR_
    for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) G.template segment<4>(i*4) = _planeToUpdateHandles[i]->_projGx;
  } else {
    OMP_PARALLEL_FOR_
    for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) G.template segment<4>(i*4) =_planeToUpdateHandles[i]->_Gx;
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::assembleHxx(Eigen::Ref<MatT> H) {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    H.template block<4,4>(i*4, i*4) = _planeToUpdateHandles[i]->_Hxx;
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::computeHcpx(int ncp, const std::vector<int>& fixVar) {
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    T timeavg = _CCPairsToUpdate[i].first.getTimeAvg();
    if( _planeToUpdateHandles[i]->_Hcpx.size()==0) _planeToUpdateHandles[i]->_Hcpx.resize(ncp, 4);
    this->_thetaTrajectory.assembleSparseGrad(timeavg, _planeToUpdateHandles[i]->_Hcpx, _planeToUpdateHandles[i]->_Hthetax);
    // fix grad
    OMP_PARALLEL_FOR_
    for (int c=0; c<4; c++) {
      int fixRowIdx=0;
      typename SMatT::InnerIterator it(_planeToUpdateHandles[i]->_Hcpx,c);
      //assume fixVar is sorted in ascending order
      while(it && fixRowIdx<(int)fixVar.size()) {
        if(it.row()==fixVar[fixRowIdx]) {
          it.valueRef() = 0;
          ++fixRowIdx;
          ++it;
        } else if(it.row()>fixVar[fixRowIdx])
          ++fixRowIdx;
        else ++it;
      }
    }
    _planeToUpdateHandles[i]->_Hcpx=_planeToUpdateHandles[i]->_Hcpx.pruned();
  }
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::assembleHcpxHxcp(Eigen::Ref<MatT> Hcpx, Eigen::Ref<MatT> Hxcp, const std::vector<int>& fixVar) {
  computeHcpx(Hcpx.rows(),fixVar);
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) Hcpx.block(0, i*4, Hcpx.rows(), 4) = _planeToUpdateHandles[i]->_Hcpx;
  Hxcp = Hcpx.transpose();
}
template<typename T, typename  PFunc, typename CCPlaneT>
bool CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::evalCC(const std::pair<EntityId<T>,EntityId<T>>& pair,CCPlaneT& plane,EFunc* E,bool DG,bool DH,EntityGradT& grad) {
  const EntityT& p1 = pair.first.getPolytope(grad);
  const EntityT& p2 = pair.second.getPolytope(grad);
  T timeDiff = pair.first.getTimeDiff();
  if(timeDiff == 0)
    timeDiff = 1;
  CCBarrierEnergyFast<T,PFunc> ccEnergy(p1,p2,pCC(),d0(),&grad,_coef*timeDiff*2);
  ASSERT_MSG(plane._toUpdate==false || DG==!plane._useNewX,"For plane to be updated, "
             "plane.newX is activated iff planner is doing line search and DG is false.")
  if(!plane._initialized) {
    if(!ccEnergy.initialize(&(plane._x),&_body))
      return false;
    plane._initialized=true;
  }
  if(!plane._useNewX)ccEnergy.initialize(plane._x);
  else ccEnergy.initialize(plane._newX);
  ccEnergy.setOutput(this->output());
  T energyVal;
  if(!ccEnergy.eval(&energyVal,&_body,(DG||DH)?&grad:NULL,NULL,NULL,NULL)) {
    return false;
  }
  if(E)
    (*E)(energyVal);
  //Not in line search
  if(DG||DH) {
    //valid
    if(energyVal!=0) {
      ccEnergy.assemblePlaneGH(_body,grad,plane);
      plane._toUpdate=true;
    }
    //invalid
    else plane._toUpdate=false;
  }
  return true;
}
template<typename T, typename PFunc, typename CCPlaneT>
void CollisionBarrierEnergyFast<T,PFunc, CCPlaneT>::debugPlanes(const Vec& cp, const Vec& d, T alpha) {
  T E=0,E2=0;
  Vec Gcp,Gcp2;
  MatT Hcp,Hcp2;
  Gcp.setZero(cp.size());
  Gcp2.setZero(cp.size());
  Hcp.setZero(Gcp.size(),Gcp.size());
  Hcp2.setZero(Gcp.size(),Gcp.size());
  SIPEnergy<T>::eval(&E,&Gcp,&Hcp);
  Vec G,G2;
  MatT H,H2;
  int num_planes = numCCPlanesToUpdate();
  ASSERT_MSG(num_planes!=-1, "numCCPlanesToUpdate should be initialized before debug.")
  G.setZero(num_planes*4);
  G2.setZero(num_planes*4);
  H.setZero(G.size(),G.size());
  H2.setZero(G.size(),G.size());
  assembleGx(G);
  assembleHxx(H);
  assignCCPlanesNewX();
  DEFINE_NUMERIC_DELTA_T(T)
  perturbCCPlanes(alpha, d, true);
  SIPEnergy<T>::eval(&E2,&Gcp2,&Hcp2);
  assembleGx(G2);
  assembleHxx(H2);
  assignCCPlanesX();
  std::cout<<"delta="<<alpha<<std::endl;
  std::cout<<"E="<<std::setprecision(32)<<E<<std::endl;
  std::cout<<"E1="<<std::setprecision(32)<<E2<<std::endl;
  std::cout<<"G="<<std::setprecision(32)<<(G).dot(d)<<std::endl;
  std::cout<<"G2="<<std::setprecision(32)<<(G2).dot(d)<<std::endl;
  std::cout<<"G1="<<std::setprecision(32)<<(E2-E)/alpha<<std::endl;
  std::cout<<"H="<<std::setprecision(32)<<((H)*d).norm()<<std::endl;
  std::cout<<"H1="<<std::setprecision(32)<<((G2-G)/alpha).norm()<<std::endl;
  std::cout<<"H2="<<std::setprecision(32)<<((H2)*d).norm()<<std::endl;
  T dE = E2 - E;
  DEBUG_GRADIENT("dE",G.dot(d),G.dot(d)-dE/alpha)
  Vec dG = G2-G;
  DEBUG_GRADIENT("dG",(H*d).norm(),(H*d-dG/alpha).norm())
  std::cout<<"=========================Debug GPlane, HPlane in CCBarriers============================"<<std::endl;
  T CCE, CCE2;
  Vec4T CCG,CCG2;
  Mat4T CCH,CCH2;
  for(int i=0; i<(int)_planeToUpdateHandles.size(); i++) {
    std::cout<<"CCBarrier:"<<i<<std::endl;
    const std::pair<EntityId<T>, EntityId<T>> &pair =_CCPairsToUpdate[i];
    CCPlaneT& plane = *_planeToUpdateHandles[i];
    T timeAvg = pair.first.getTimeAvg();
    EntityGradT grad = this->gradInfo().at(timeAvg);
    const EntityT& p1 = pair.first.getPolytope(grad);
    const EntityT& p2 = pair.second.getPolytope(grad);
    T timeDiff = pair.first.getTimeDiff();
    if(timeDiff == 0 ) timeDiff = 1;
    CCBarrierEnergyFast<T,PFunc> cc(p1,p2,pCC(),d0(),&grad,_coef*timeDiff*2);
    cc.setOutput(this->output());
    cc.initialize(plane._x);
    cc.eval(&CCE,&this->_body,&grad,NULL,NULL,NULL);
    cc.assemblePlaneGH(this->_body,grad,plane);
    CCG = plane._Gx;
    CCH = plane._Hxx;
    CCBarrierEnergyFast<T,PFunc> cc2(p1,p2,pCC(),d0(),&grad,_coef*timeDiff*2);
    cc2.setOutput(this->output());
    Vec4T dplane=d.template segment<4>(i*4);
    plane._newX=plane._x+alpha*dplane;
    plane._newX.template segment<3>(0).normalize();
    cc2.initialize(plane._newX);
    cc2.eval(&CCE2,&_body,&grad,NULL,NULL,NULL);
    cc2.assemblePlaneGH(this->_body,grad,plane);
    CCG2=plane._Gx;
    CCH2=plane._Hxx;
    std::cout<<"alpha*dx="<<std::setprecision(32)<<alpha*dplane<<std::endl;
    std::cout<<"newX-X="<<std::setprecision(32)<<plane._newX - plane._x<<std::endl;
    std::cout<<"x.dot(dx)="<<std::setprecision(32)<<plane._x.template segment<3>(0).dot(dplane.template segment<3>(0))<<std::endl;
    std::cout<<"CCE="<<std::setprecision(32)<<CCE<<std::endl;
    std::cout<<"CCE1="<<std::setprecision(32)<<CCE2<<std::endl;
    std::cout<<"CCG="<<std::setprecision(32)<<(CCG).dot(dplane)<<std::endl;
    std::cout<<"CCG2="<<std::setprecision(32)<<(CCG2).dot(dplane)<<std::endl;
    std::cout<<"CCG1="<<std::setprecision(32)<<(CCE2-CCE)/alpha<<std::endl;
    std::cout<<"CCH="<<std::setprecision(32)<<((CCH)*dplane).norm()<<std::endl;
    std::cout<<"CCH1="<<std::setprecision(32)<<((CCG2-CCG)/alpha).norm()<<std::endl;
    std::cout<<"CCH2="<<std::setprecision(32)<<((CCH2)*dplane).norm()<<std::endl;
    T dCCE=CCE2-CCE;
    DEBUG_GRADIENT("dCCE",CCG.dot(dplane),CCG.dot(dplane)-dCCE/alpha)
    Vec dCCG=CCG2-CCG;
    DEBUG_GRADIENT("dCCG",(CCH*dplane).norm(),(CCH*dplane-dCCG/alpha).norm())
  }
}
//instance
template class CollisionBarrierEnergyFast<FLOAT,Px>;
template class CollisionBarrierEnergyFast<FLOAT,Logx>;
template class CollisionBarrierEnergyFast<FLOAT,CLogx>;
template class CollisionBarrierEnergyFast<FLOAT,Cubicx>;
template class CollisionBarrierEnergyFast<FLOAT,InvQuadraticx>;
}
