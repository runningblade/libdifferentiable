#include "SIPObjective.h"

namespace PHYSICSMOTION {
template <typename T>
SIPObjective<T>::SIPObjective
(const std::vector<int>& fixVar,PenetrationDetector<T>& detector,
 const std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>>& trajEss)
  :_fixVar(fixVar),_detector(detector),_trajEss(trajEss) {
  _init=detector.getControlPoints();
}
template <typename T>
typename SIPObjective<T>::Vec SIPObjective<T>::lb() const {
  Vec lower=_detector.getBody()->lowerLimit().template cast<T>();
  Vec lb=Vec::Constant(inputs(),-SQPObjective<T>::infty());
  //joint limit
  for(int i=0; i<_detector.getBody()->nrDOF(); i++)
    if(isfinite(lower[i])) {
      SMatT theta=_detector.getThetaTrajectory().getControlPointCoeffAll(i);
      for(int k=0; k<theta.outerSize(); ++k)
        for(typename SMatT::InnerIterator it(theta,k); it; ++it)
          lb[it.col()]=lower[i];
    }
  //fix
  for(int i:_fixVar)
    lb[i]=_init[i];
  return lb;
}
template <typename T>
typename SIPObjective<T>::Vec SIPObjective<T>::ub() const {
  Vec upper=_detector.getBody()->upperLimit().template cast<T>();
  Vec ub=Vec::Constant(inputs(),SQPObjective<T>::infty());
  //joint limit
  for(int i=0; i<_detector.getBody()->nrDOF(); i++)
    if(isfinite(upper[i])) {
      SMatT theta=_detector.getThetaTrajectory().getControlPointCoeffAll(i);
      for(int k=0; k<theta.outerSize(); ++k)
        for(typename SMatT::InnerIterator it(theta,k); it; ++it)
          ub[it.col()]=upper[i];
    }
  //fix
  for(int i:_fixVar)
    ub[i]=_init[i];
  return ub;
}
template <typename T>
typename SIPObjective<T>::Vec SIPObjective<T>::gl() const {
  return Vec::Constant(values(),_detector.d0());
}
template <typename T>
typename SIPObjective<T>::Vec SIPObjective<T>::gu() const {
  return Vec::Constant(values(),SQPObjective<T>::infty());
}
template <typename T>
typename SIPObjective<T>::Vec SIPObjective<T>::init() const {
  return _init;
}
//constraint
template <typename T>
int SIPObjective<T>::operator()(const Vec& x,Vec& fvec,MatT* fjac) {
  _detector.setControlPoints(x);
  fvec.setZero(values());
  if(fjac)
    fjac->setZero(values(),inputs());
  std::shared_ptr<ArticulatedBody> body=_detector.getBody();
  const typename ParallelVector<PDEntrySpan<T>>::vector_type& PDEntries=_detector.getPDEntries().getVector();
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int) PDEntries.size(); i++) {
    Vec g,G;
    Mat3T hessian;
    Vec3T n,normal,ctrBG,ctrBInA;
    Eigen::Matrix<int,2,1> feat;
    Mat3X4T transA,transB,DTransA,DTransB;
    const PDEntrySpan<T>& entry=PDEntries[i];
    const GradInfo& info=_detector.getInfo(entry.getTimeAvg());
    //jointA
    if(entry._jidA>=0)
      transA=TRANSI(info._info._TM,entry._jidA);
    else transA.setIdentity();
    //jointB
    if(entry._jidB>=0)
      transB=TRANSI(info._info._TM,entry._jidB);
    else transB.setIdentity();
    //compute constraint violation
    ctrBG=ROT(transB)*entry._pBL+CTR(transB);
    ctrBInA=ROT(transA).transpose()*(ctrBG-CTR(transA));
    if(_detector.getUseSDF())
      fvec[i]=_detector.getBodyPoints()[entry._jidA]->template closestSDF<T>(ctrBInA,normal);
    else fvec[i]=body->joint(entry._jidA)._mesh->template closest<T>(ctrBInA,n,normal,hessian,feat);
    //compute constraint jacobian
    if(fjac) {
      g.setZero(body->nrDOF());
      G.setZero(x.size());
    }
    if(fjac && entry._jidA>=0) {
      //our energy is: PD(RA^T*(ctrBG-TA))
      CTR(DTransA)=-ROT(transA)*normal;
      ROT(DTransA)=(ctrBG-CTR(transA))*normal.transpose();
      info._info.DTG(entry._jidA,*body,DTransA,[&](int col,T val) {
        g[col]+=val;
      });
    }
    if(fjac && entry._jidB>=0) {
      //our energy is: PD(RA^T*([RB*ctrB+TB]-TA))
      CTR(DTransB)=ROT(transA)*normal;
      ROT(DTransB)=ROT(transA)*normal*entry._pBL.transpose();
      info._info.DTG(entry._jidB,*body,DTransB,[&](int col,T val) {
        g[col]+=val;
      });
    }
    if(fjac) {
      _detector.getThetaTrajectory().assembleEnergy(entry.getTimeAvg(),&G,NULL,&g,NULL);
      fjac->row(i)=G;
    }
  }
  return 0;
}

template <typename T>
int SIPObjective<T>::operator()(const Vec& x,Vec& fvec,SMatT* fjac) {
  MatT fjacD;
  int ret=operator()(x,fvec,fjac? &fjacD: NULL);
  if(fjac)
    *fjac=toSparse<T,0,int,MatT>(fjacD,-1);
  return ret;
}

//objective
template <typename T>
T SIPObjective<T>::operator()(const Vec& x,Vec* fgrad) {
  return operator()(x,fgrad,(MatT*) NULL);
}
template <typename T>
T SIPObjective<T>::operator()(const Vec& x,Vec* fvec,MatT* fjac) {
  T E=0;
  _detector.setControlPoints(x);
  if(fvec)
    fvec->setZero(inputs());
  if(fjac)
    fjac->setZero(inputs(),inputs());
  for(std::shared_ptr<TrajectorySIPEnergy<T>> trajE:_trajEss)
    trajE->eval(&E,fvec,fjac);
  return E;
}
template <typename T>
T SIPObjective<T>::operator()(const Vec& x,Vec* fvec,SMatT* fjac) {
  MatT fjacD;
  T ret=operator()(x,fvec,fjac? &fjacD: NULL);
  if(fjac)
    *fjac=toSparse<T,0,int,MatT>(fjacD,-1);
  return ret;
}
//problem size
template <typename T>
int SIPObjective<T>::inputs() const {
  return _init.size();
}
template <typename T>
int SIPObjective<T>::values() const {
  return (int) _detector.getPDEntries().getVector().size();
}


//instances
template class SIPObjective<FLOAT>;
}
