#include "CCBarrierEnergyFast.h"
#include "TrajectorySIPEnergy.h"

namespace PHYSICSMOTION {

template<typename T,typename PFunc,typename TH>
CCBarrierEnergyFast<T,PFunc,TH>::CCBarrierEnergyFast(const EntityT& p1,const EntityT& p2,const PFunc& p,T d0,const CollisionGradInfo<T>* grad,T coef):
  CCBarrierEnergy<T,PFunc,TH>(p1,p2,p,d0,grad,coef,false) {
  _Gx.setZero();
  _Hxx.setZero();
  _HxvP.setZero();
  _HxvCRvlP.setZero();
  _HxvN.setZero();
  _HxvCRvlN.setZero();
}
template<typename T,typename PFunc,typename TH>
bool CCBarrierEnergyFast<T,PFunc,TH>::eval(T* E,const ArticulatedBody* body,CollisionGradInfo<T>* grad,std::array<Mat3X4T,4>* DNDX,Vec* GTheta,MatT* HTheta,Vec4T* x) {
  if(grad) {
    T E2;
    if(!PNEnergy(*body,*grad,getX().template cast<T>(),E2))
      return false;
    if(E)
      *E=_coef*E2;
    //compute gradient
    if(E2!=0) {
      Mat3XT DTG;
      Vec4T G;
      Mat4T H;
      bool hessian=grad && grad->_HTheta.size()>0;
      CCBarrierEnergy<T,PFunc,TH>::computeDTGH(*body,*grad,getX().template cast<T>(),hessian?&G:NULL,hessian?&H:NULL,NULL);
      if(GTheta) {
        GTheta->setZero(body->nrDOF());
        grad->_info.DTG(*body,mapM(DTG=grad->_DTG),mapV(*GTheta));
      }
      if(HTheta) {
        *HTheta=grad->_HTheta;
        grad->_info.toolB(*body,mapM(DTG=grad->_DTG),[&](int r,int c,T val) {
          (*HTheta)(r,c)+=val;
        });
      }
    }
  }
  //in line search
  else {
    TH E2;
    if(!CCBarrierEnergy<T,PFunc,TH>::energy(_x,E2,NULL,NULL))
      return false;
    if(E)
      *E=_coef*(T)E2;
  }
  return true;
}
template<typename T, typename PFunc, typename TH>
bool CCBarrierEnergyFast<T, PFunc, TH>::PNEnergy(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,const Vec4T& x,T& E) {
  E=0;
  Mat3T R;
  std::shared_ptr<MeshExact> local;
  //positive
  if(_p1.jid()>=0) {
    local=std::dynamic_pointer_cast<MeshExact>(body.joint(_p1.jid())._mesh);
    R=ROTI(grad._info._TM,_p1.jid());
    for(int c=0; c<_p1.globalVss().cols(); c++) {
      Vec3T vl=local->vss()[c].template cast<T>();
      if(!PEnergy(_p1.globalVss().col(c),x,E,&vl,&R))
        return false;
    }
  } else {
    for(int c=0; c<(int)_p1.mesh()->vss().size(); c++) {
      if(!PEnergy(_p1.mesh()->vss()[c].template cast<T>(),x,E,NULL,NULL))
        return false;
    }
  }
  //negative
  if(_p2.jid()>=0) {
    local=std::dynamic_pointer_cast<MeshExact>(body.joint(_p2.jid())._mesh);
    R=ROTI(grad._info._TM,_p2.jid());
    for(int c=0; c<_p2.globalVss().cols(); c++) {
      Vec3T vl=local->vss()[c].template cast<T>();
      if(!NEnergy(_p2.globalVss().col(c),x,E,&vl,&R))
        return false;
    }
  } else {
    for(int c=0; c<(int)_p2.mesh()->vss().size(); c++) {
      if(!NEnergy(_p2.mesh()->vss()[c].template cast<T>(),x,E,NULL,NULL))
        return false;
    }
  }
  return true;
}
template<typename T, typename PFunc, typename TH>
bool CCBarrierEnergyFast<T, PFunc, TH>::PEnergy(const Vec3T& v, const Vec4T& x, T& E, const Vec3T* vl, const Mat3T* R) {
  T e, D, DD;
  e=evalBarrier(evalPDist(v, x), &D, &DD);
  if(!isfinite(e)) return false;
  //update E and compute gradient
  if(e!=0) {
    //update E
    E += e;
    //gradient
    Vec4T vh;
    vh<<v,-1;
    _Gx += D*vh;
    //hessian
    _Hxx+=vh*DD*vh.transpose();
    //cross term
    if(vl) {
      Mat4X3T Ih;
      Ih<<Mat3T::Identity(),Vec3T::Zero().transpose();
      Mat3T CRvl=cross<T>((*R)*(*vl));
      Mat4X3T DTerm=D * Ih, DDTerm=vh*DD*getX().template segment<3>(0).transpose();
      DTerm+=DDTerm;
      _HxvP += DTerm;
      _HxvCRvlP += DTerm * CRvl;
    }
  }
  return true;
}
template<typename T, typename PFunc, typename TH>
bool CCBarrierEnergyFast<T, PFunc, TH>::NEnergy(const Vec3T& v, const Vec4T& x, T& E, const Vec3T* vl, const Mat3T* R) {
  T e, D, DD;
  e=evalBarrier(evalNDist(v, x), &D, &DD);
  if(!isfinite(e)) return false;
  //update E and compute gradient
  if(e!=0) {
    //update E
    E += e;
    //gradient
    Vec4T vh;
    vh<<-v,1;
    _Gx += (D*vh);
    //hessian
    _Hxx+=vh*DD*vh.transpose();
    //cross term
    if(vl) {
      Mat4X3T Ih;
      Ih<<Mat3T::Identity(),Vec3T::Zero().transpose();
      Mat3T CRvl=cross<T>((*R)*(*vl));
      Mat4X3T DTerm=-D*Ih,DDTerm=vh*DD*(-getX().template segment<3>(0)).transpose();
      DTerm+=DDTerm;
      _HxvN+=DTerm;
      _HxvCRvlN+=DTerm*CRvl;
    }
  }
  return true;
}
template<typename T, typename PFunc, typename TH>
void CCBarrierEnergyFast<T, PFunc, TH>::assemblePlaneGH(const ArticulatedBody& body, const CollisionGradInfo<T>& grad, SmartPlaneT& plane) {
  plane._Gx=_Gx;
  Vec3T norm=plane._x.template segment<3>(0);
  plane._projGx=_Gx;
  plane._projGx.template segment<3>(0) -= _Gx.template segment<3>(0).dot(norm)*norm;
  plane._Hxx=_Hxx;
  HxvToHthetax(body, grad, plane._Hthetax);
  //scale
  plane._Gx*=_coef;
  plane._projGx*=_coef;
  plane._Hxx*=_coef;
  plane._Hthetax*=_coef;
}
template<typename T, typename PFunc, typename TH>
void CCBarrierEnergyFast<T,PFunc,TH>::HxvToHthetax(const ArticulatedBody &body, const CollisionGradInfo<T> &grad, SMatT &Hthetax) {
  if(Hthetax.size()==0) Hthetax.resize(body.nrDOF(), 4);
  // energyP
  std::vector<STrip> trips;
  if(_p1.jid()>=0) {
    for(int j=0; j<4; j++) {
      std::function<void(int,const Vec3T&)> JR=[&](int i,const Vec3T& J) {
        trips.push_back(STrip(i,j,-_HxvCRvlP.row(j)*J));
      };
      std::function<void(int,const Vec3T&)> JC=[&](int i,const Vec3T& J) {
        trips.push_back(STrip(i,j,_HxvP.row(j)*J));
      };
      grad._info.JRCSparse(body,_p1.jid(),JR,JC);
    }
  }
  // energyN
  if(_p2.jid()>=0) {
    for(int j=0; j<4; j++) {
      std::function<void(int,const Vec3T&)> JR=[&](int i,const Vec3T& J) {
        trips.push_back(STrip(i,j,-_HxvCRvlN.row(j)*J));
      };
      std::function<void(int,const Vec3T&)> JC=[&](int i,const Vec3T& J) {
        trips.push_back(STrip(i,j,_HxvN.row(j)*J));
      };
      grad._info.JRCSparse(body,_p2.jid(),JR,JC);
    }
  }
  Hthetax.setFromTriplets(trips.begin(),trips.end());
}
template<typename T, typename PFunc, typename TH>
bool CCBarrierEnergyFast<T,PFunc,TH>::optimizeX(Vec4T& x) {
  TH E;
  Vec4TH G;
  Mat4TH H;
  if(!optimize(_x,E,G,H))
    return false;
  x=_x.template cast<T>();
  return true;
}
//instance
template class CCBarrierEnergyFast<FLOAT,Px>;
template class CCBarrierEnergyFast<FLOAT,Logx>;
template class CCBarrierEnergyFast<FLOAT,CLogx>;
template class CCBarrierEnergyFast<FLOAT,InvQuadraticx>;
template class CCBarrierEnergyFast<FLOAT,Cubicx>;
}

