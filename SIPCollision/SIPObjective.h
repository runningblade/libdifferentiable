#ifndef SIP_OBJECTIVE_H
#define SIP_OBJECTIVE_H

#include "PenetrationDetector.h"
#include "TrajectorySIPEnergy.h"
#include <Optimizer/SQPObjective.h>

namespace PHYSICSMOTION {
template <typename T>
struct SIPObjective : public SQPObjective<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef typename PenetrationDetector<T>::GradInfo GradInfo;
  SIPObjective(const std::vector<int>& fixVar,PenetrationDetector<T>& detector,
               const std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>>& trajEss);
  virtual Vec lb() const override;
  virtual Vec ub() const override;
  virtual Vec gl() const override;
  virtual Vec gu() const override;
  virtual Vec init() const override;
  //constraint
  virtual int operator()(const Vec& x,Vec& fvec,MatT* fjac) override;
  virtual int operator()(const Vec& x,Vec& fvec,SMatT* fjac) override;
  //objective
  virtual T operator()(const Vec& x,Vec* fvec) override;
  virtual T operator()(const Vec& x,Vec* fvec,MatT* fjac) override;
  virtual T operator()(const Vec& x,Vec* fvec,SMatT* fjac) override;
  //problem size
  virtual int inputs() const;
  virtual int values() const;
 private:
  Vec _init;
  const std::vector<int>& _fixVar;
  //this is per-problem temporary data, not serialized
  PenetrationDetector<T>& _detector;
  const std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>>& _trajEss;
};
}
#endif
