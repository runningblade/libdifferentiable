#ifndef COLLISION_BARRIER_ENERGY_FAST_H
#define COLLISION_BARRIER_ENERGY_FAST_H

#include "CollisionBarrierEnergy.h"
#include "CCBarrierEnergyFast.h"
#include <Utils/DebugGradient.h>

namespace PHYSICSMOTION {


template<typename T, typename  PFunc, typename CCPlaneT=CCSeparatingPlaneSmart<T>>
class CollisionBarrierEnergyFast: public CollisionBarrierEnergy<T,PFunc,CCPlaneT> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef CollisionGradInfo<T> EntityGradT;
  typedef GJKPolytope<T> EntityT;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  typedef CollisionGradInfo<T> GradInfo;
  using TrajectorySIPEnergy<T>::_body;
  using TrajectorySIPEnergy<T>::_coef;
  using CollisionBarrierEnergy<T,PFunc,CCPlaneT>::pCC;
  using CollisionBarrierEnergy<T,PFunc,CCPlaneT>::d0;
  using typename CollisionBarrierEnergy<T,PFunc,CCPlaneT>::EFunc;
  CollisionBarrierEnergyFast(const ArticulatedBody& body,
                             const Vec& controlPoints,const ThetaTrajectory<T>& tt,
                             std::vector<std::pair<EntityId<T>,EntityId<T>>>& TTPairs,CCSeparatingPlanes<T, CCPlaneT>& CCPlanes,
                             std::unordered_map<T,GradInfo>& gradInfo,T d0,T x0,T coef=1, T minEig=1e-3, bool JTJApprox=false, bool implicit=false);
  void perturbCCPlanes(T delta, const Vec& dx, bool normalize=true);
  void assignCCPlanesNewX();
  void assignCCPlanesX();
  void debugPlanes(const Vec& cp, const Vec& d, T alpha);
  void activateCCPlanesNewX();
  void deactivateCCPlanesNewX();
  void clearCCPlanesInitFlag();
  void updateNewCCPlanesX(const Vec &d, T alpha);
  void assembleGx(Eigen::Ref<Vec> G, bool proj=false);
  void assembleHConstraints(Eigen::Ref<MatT> Hc, Eigen::Ref<MatT> HcT);
  void assembleHxx(Eigen::Ref<MatT> H);
  void assembleHcpxHxcp(Eigen::Ref<MatT> Hcpx, Eigen::Ref<MatT> Hxcp,const std::vector<int>& fixVar);
  void computeSchurGH(Vec& GSchur, MatT& HSchur,const std::vector<int>& fixVar);
  void schurSolveSearchDirection(const Vec& dcp, Eigen::Ref<Vec> dx);
  int numCCPlanesToUpdate();
  void resetCCPlanesToUpdate();
  void makePlaneHxxPD();
  void localOptimizePlanes();
 private:
  bool evalCC(const std::pair<EntityId<T>,EntityId<T>>& pair,CCPlaneT& plane,EFunc* E,bool DG,bool DH, EntityGradT& grad) override;
  void computeHcpx(int ncp, const std::vector<int>& fixVar);
  void computeHxxInv();
  T _minEig;
  std::vector<CCPlaneT*> _planeToUpdateHandles;
  std::vector<std::pair<EntityId<T>,EntityId<T>>> _CCPairsToUpdate;
};
}
#endif
