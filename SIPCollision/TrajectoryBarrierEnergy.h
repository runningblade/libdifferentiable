#ifndef TRAJECTORY_BARRIER_ENERGY_H
#define TRAJECTORY_BARRIER_ENERGY_H

#include "TrajectorySIPEnergy.h"

namespace PHYSICSMOTION {
template<typename T, typename PFunc>
class TrajectoryBarrierEnergy:public TrajectorySIPEnergy<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  TrajectoryBarrierEnergy(const ArticulatedBody& body,const Vec& controlPoints,const ThetaTrajectory<T>& tt,T coef,T x0)
    :TrajectorySIPEnergy<T>(body, controlPoints,tt,coef) {
    _p._x0 = (double)x0;
  };
  // Usually, T is exactly double
  inline T barrierActiveDist() {
    return (T)_p._x0;
  };
 protected:
  PFunc _p;
};
}

#endif
