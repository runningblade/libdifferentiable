#include "ConvexHull/ConvexHullMultiFrictionOptimization.h"
#include "ConvexHull/ConvexHullFrictionOptimizationLocal.h"
#include "Articulated/ArticulatedUtils.h"
#include "Articulated/ArticulatedLoader.h"
#include "Environment/EnvironmentUtils.h"

using namespace PHYSICSMOTION;
template <typename T>
void debugCCFrictionOptimization() {
  mpfr_float::default_precision(1000);
  std::shared_ptr<MeshExact> mesh;
  {
    std::vector<Eigen::Matrix<double, 3, 1>> vss;
    std::vector<Eigen::Matrix<int, 3, 1>> iss;
    addBox(vss,iss,Eigen::Matrix<double, 3, 1>(0,0,0),Eigen::Matrix<double, 3, 1>(.2,.2,.2));
    mesh.reset(new MeshExact(vss,iss));
  }

  GJKPolytope<T> p(mesh);
  std::shared_ptr<ArticulatedBody> body(new ArticulatedBody(ArticulatedLoader::readURDF("../data/kuka_lwr/kuka.urdf",true,false)));
  ArticulatedUtils(*body).tessellate(true);
  ArticulatedUtils(*body).BBApproxiate(true);
  CCBarrierFrictionOptimization<T>::debugJacobian(*body,4,7,0.8,0.01);
  CCBarrierFrictionOptimization<T>::debugEnergy(*body,4,7,0.8,0.01);
  CCBarrierFrictionOptimization<T>::debugEnergyDerivative(*body,4,7,0.8,0.01);
  CCBarrierFrictionOptimization<T>::debugEnergyDerivative(*body,p,7,0.8,0.01);
}
template <typename T>
void debugCCFrictionOptimizationLocal() {
  mpfr_float::default_precision(1000);
  std::shared_ptr<MeshExact> mesh;
  {
    std::vector<Eigen::Matrix<double, 3, 1>> vss;
    std::vector<Eigen::Matrix<int, 3, 1>> iss;
    addBox(vss,iss,Eigen::Matrix<double, 3, 1>(0,0,0),Eigen::Matrix<double, 3, 1>(.2,.2,.2));
    mesh.reset(new MeshExact(vss,iss));
  }

  GJKPolytope<T> p(mesh);
  std::shared_ptr<ArticulatedBody> body(new ArticulatedBody(ArticulatedLoader::readURDF("../data/kuka_lwr/kuka.urdf",true,false)));
  ArticulatedUtils(*body).tessellate(true);
  ArticulatedUtils(*body).BBApproxiate(true);
  CCBarrierFrictionOptimizationLocal<T>::debugEnergy(*body,4,7,0.8,0.01);
}
template <typename T>
void debugCCMultiFrictionOptimization() {
  mpfr_float::default_precision(1000);
  std::shared_ptr<MeshExact> mesh;
  {
    std::vector<Eigen::Matrix<double, 3, 1>> vss;
    std::vector<Eigen::Matrix<int, 3, 1>> iss;
    addBox(vss,iss,Eigen::Matrix<double, 3, 1>(0,0,0),Eigen::Matrix<double, 3, 1>(.2,.2,.2));
    mesh.reset(new MeshExact(vss,iss));
  }

  Logx b;
  GJKPolytope<T> p(mesh);
  std::shared_ptr<ArticulatedBody> body(new ArticulatedBody(ArticulatedLoader::readURDF("../data/kuka_lwr/kuka.urdf",true,false)));
  ArticulatedUtils(*body).tessellate(true);
  ArticulatedUtils(*body).BBApproxiate(true);

  T coefN=0.25f;
  CollisionGradInfo<T> grad(*body,CollisionGradInfo<T>::Vec::Zero(body->nrDOF()));
  std::vector<std::shared_ptr<CCBarrierConvexEnergy<T,Logx>>> barriers;
  barriers.push_back(std::shared_ptr<CCBarrierConvexEnergy<T,Logx>>(new CCBarrierConvexEnergy<T,Logx>(p,grad._polytopes[7],b,0,&grad,coefN)));
  barriers.push_back(std::shared_ptr<CCBarrierConvexEnergy<T,Logx>>(new CCBarrierConvexEnergy<T,Logx>(grad._polytopes[4],grad._polytopes[7],b,0,&grad,coefN)));

  std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T>>> frictions;
  frictions.push_back(std::shared_ptr<CCBarrierFrictionOptimization<T>>(new CCBarrierFrictionOptimization<T>(*body,grad,*barriers[0],0.8,0.01,0.5)));
  frictions.push_back(std::shared_ptr<CCBarrierFrictionOptimization<T>>(new CCBarrierFrictionOptimization<T>(*body,grad,*barriers[1],0.8,0.01,0.5)));
  CCBarrierMultiFrictionOptimization<T>::debugEnergy(frictions,0.01);
  CCBarrierMultiFrictionOptimization<T>::debugEnergyDerivative(frictions,0.01);
}
int main() {
  typedef FLOAT T;
  DECL_MAT_VEC_MAP_TYPES_T
  debugCCFrictionOptimization<T>();
  debugCCFrictionOptimizationLocal<T>();
  debugCCMultiFrictionOptimization<T>();
  return 0;
}
