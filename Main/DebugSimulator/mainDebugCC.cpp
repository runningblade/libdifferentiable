#include "ConvexHull/ConvexHullDistanceEnergy.h"
#include "ConvexHull/ConvexHullDistanceConvexEnergy.h"
#include "ConvexHull/ConvexHullDistanceFrictionEnergy.h"
#include "Articulated/ArticulatedUtils.h"
#include "Articulated/ArticulatedLoader.h"
#include "Environment/EnvironmentUtils.h"

using namespace PHYSICSMOTION;
template <typename T,typename CCEnergyType>
void debugCCBarrierEnergy() {
  mpfr_float::default_precision(1000);
  std::shared_ptr<MeshExact> mesh;
  {
    std::vector<Eigen::Matrix<double, 3, 1>> vss;
    std::vector<Eigen::Matrix<int, 3, 1>> iss;
    addBox(vss,iss,Eigen::Matrix<double, 3, 1>(0,0,0),Eigen::Matrix<double, 3, 1>(.2,.2,.2));
    mesh.reset(new MeshExact(vss,iss));
  }

  std::shared_ptr<ArticulatedBody> body(new ArticulatedBody(ArticulatedLoader::readURDF("../data/kuka_lwr/kuka.urdf",true,false)));
  ArticulatedUtils(*body).tessellate(true);
  ArticulatedUtils(*body).BBApproxiate(true);

  Px barrier;
  barrier._x0=1.0;
  T d0=1e-3;
  GJKPolytope<T> p(mesh);
  CCEnergyType::debugGradient(true,p,*body,7,barrier._x0,d0);
  CCEnergyType::debugGradient(false,p,*body,7,barrier._x0,d0);
  CCEnergyType::debugGradient(true,*body,4,7,barrier._x0,d0);
  CCEnergyType::debugGradient(false,*body,4,7,barrier._x0,d0);
}
template <typename T,typename CCEnergyType>
void debugCCBarrierFrictionEnergy() {
  mpfr_float::default_precision(1000);
  std::shared_ptr<MeshExact> mesh;
  {
    std::vector<Eigen::Matrix<double, 3, 1>> vss;
    std::vector<Eigen::Matrix<int, 3, 1>> iss;
    addBox(vss,iss,Eigen::Matrix<double, 3, 1>(0,0,0),Eigen::Matrix<double, 3, 1>(.2,.2,.2));
    mesh.reset(new MeshExact(vss,iss));
  }

  std::shared_ptr<ArticulatedBody> body(new ArticulatedBody(ArticulatedLoader::readURDF("../data/kuka_lwr/kuka.urdf",true,false)));
  ArticulatedUtils(*body).tessellate(true);
  ArticulatedUtils(*body).BBApproxiate(true);

  Px barrier;
  barrier._x0=0.2;
  T d0=1e-3;
  GJKPolytope<T> p(mesh);
  CCEnergyType::debugGradient(true,p,*body,7,barrier._x0,d0);
  CCEnergyType::debugGradient(false,p,*body,7,barrier._x0,d0);
  CCEnergyType::debugGradient(true,*body,4,7,barrier._x0,d0);
  CCEnergyType::debugGradient(false,*body,4,7,barrier._x0,d0);
}
int main() {
  typedef FLOAT T;
  DECL_MAT_VEC_MAP_TYPES_T
  debugCCBarrierEnergy<T,CCBarrierEnergy<T,Px>>();
  debugCCBarrierEnergy<T,CCBarrierConvexEnergy<T,Px>>();
  debugCCBarrierFrictionEnergy<T,CCBarrierFrictionEnergy<T,Px>>();
  return 0;
}
