#include "ArticulatedBodyBase.h"
#include "Joint.h"

namespace PHYSICSMOTION {
//ArticulatedBodyBase
ArticulatedBodyBase::~ArticulatedBodyBase() {}
int ArticulatedBodyBase::nrDOF() const {
  int ret=0;
  for(int i=0; i<(int)nrJ(); i++)
    ret+=joint(i).nrDOF();
  return ret;
}
int ArticulatedBodyBase::nrDDT() const {
  int ret=0;
  for(int i=0; i<(int)nrJ(); i++)
    ret+=joint(i).nrDDT();
  return ret;
}
int ArticulatedBodyBase::rootJointId() const {
  for(int k=nrJ()-1; k>=0; k--)
    if(joint(k).isRoot(*this))
      return k;
  ASSERT_MSG(false,"Cannot find root joint!")
  return -1;
}
}
