#ifndef END_EFFECTOR_INFO_H
#define END_EFFECTOR_INFO_H

#include <Environment/EnvironmentUtils.h>
#include <Utils/Serializable.h>
#include <Utils/Pragma.h>
#include "ArticulatedBody.h"

namespace PHYSICSMOTION {
struct EndEffectorBounds : public SerializableBase {
  typedef ArticulatedBody::T T;
  DECL_MAT_VEC_MAP_TYPES_T
  enum EE_TYPE {
    LEFT    =1<<0,
    RIGHT   =1<<1,
    FRONT   =1<<2,
    BACK    =1<<3,
    UNKNOWN =1<<4,
  };
  EndEffectorBounds();
  EndEffectorBounds(int jid);
  std::vector<int> idDOF(std::shared_ptr<ArticulatedBody> body) const;
  int nrDOF(const ArticulatedBody& body,std::vector<int>* DOFIds=NULL) const;
  int nrDOF(std::shared_ptr<ArticulatedBody> body,std::vector<int>* DOFIds=NULL) const;
  bool exists(const BBoxExact& bb) const;
  bool exists(std::shared_ptr<BBoxExact> bb) const;
  Vec getDOFAll(const ArticulatedBody& body,const Vec3T& pt) const;
  Vec getDOFAll(std::shared_ptr<ArticulatedBody> body,const Vec3T& pt) const;
  Vec getDOFAll(const ArticulatedBody& body,const Vec3T& pt,const Vec3T& nor) const;
  Vec getDOFAll(std::shared_ptr<ArticulatedBody> body,const Vec3T& pt,const Vec3T& nor) const;
  Vec getDOF(const Vec3T& pt) const;
  Vec3T globalPosAll(const ArticulatedBody& body,const Vec& x) const;
  Vec3T globalPosAll(std::shared_ptr<ArticulatedBody> body,const Vec& x) const;
  Vec3T globalPos(const ArticulatedBody& body,const Vec& x) const;
  Vec3T globalPos(std::shared_ptr<ArticulatedBody> body,const Vec& x) const;
  std::string checkIndexError(const ArticulatedBody& body) const;
  std::string checkIndexError(std::shared_ptr<ArticulatedBody> body) const;
  Vec3T getBBCenter() const;
  T getBBRadius() const;
  Vec3T randomPos() const;
  int jointId() const;
  bool isEven() const;
  std::string name() const;
  bool operator==(const EndEffectorBounds& other) const;
  bool read(std::istream& is,IOData* dat) override;
  bool write(std::ostream& os,IOData* dat) const override;
  std::shared_ptr<SerializableBase> copy() const override;
  std::string type() const override;
  //detect end-effector
  static bool detectEndEffector(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees,const std::set<std::string>& fixNames= {});
  static Mat3X4T fitEndEffectorBall(const ArticulatedBody& body,int jid,Vec3T& localPos,T& phi0,const Vec3T& upDir,const Vec* pose,T errThres);
  static Mat3X4T fitEndEffectorPatch(const ArticulatedBody& body,int jid,Mat3XT& localPatch,T phi0,const Vec* pose=NULL);
  static Mat3T detectEndEffectorPose(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees,const Vec* pose=NULL,T errThres=.1);
  //detect reachable region
  void flipLeftRight();
  Vec flipLeftRightCoef() const;
  bool findIndexGrid(const ArticulatedBody& body,Vec DOF);
  bool DOFAlreadyExists(const ArticulatedBody& body,Vec DOF,T errThres=1e-3f) const;
  void findBestBB(const Vec3T* mustContain,const Vec3T* mustCenter,const Eigen::Matrix<int,3,1>& maskVec);
  bool neighborContinuous(const ArticulatedBody& body,const Eigen::Matrix<int,3,1> id,const Vec& DOF,T errThres) const;
  bool solveIKForIndexGrid(const ArticulatedBody& body,const Vec3T& pos,Vec& x,T errThres=1e-3f,bool useIPOPT=false) const;
  bool solveIKNormal(const ArticulatedBody& body,const Vec3T& pos,const Vec3T& nor,Vec& x,T coef,T errThres=1e-3f,bool useIPOPT=false) const;
  bool solveIK(const ArticulatedBody& body,const Vec3T& pos,Vec& x,T errThres=1e-3f,bool useIPOPT=false) const;
  static MatT detectSymmetry(const ArticulatedBody& body,EndEffectorBounds& l,EndEffectorBounds& r);
  static bool detectReachableRegion(const ArticulatedBody& body,EndEffectorBounds& l,EndEffectorBounds& r,T errThres,int nrTrial,bool containLocalPose,bool centerLocalPose,const Eigen::Matrix<int,3,1>& maskVec);
  static bool detectReachableRegion(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees,T errThres,int nrTrial,bool containLocalPose,bool centerLocalPose,int mask);
  static bool betterThan(const Eigen::Matrix<int,6,1>& A,const Eigen::Matrix<int,6,1>& B,const Vec3T* mustContain,const Vec3T* mustCenter,const Eigen::Matrix<int,3,1>& maskVec);
  static int irregularity(const Eigen::Matrix<int,3,1>& ext);
  //data
  std::vector<int> _JID;
  std::unordered_map<Eigen::Matrix<int,3,1>,Vec,TriangleHash> _indices;
  Eigen::Matrix<int,6,1> _bb;
  Mat3T _localFrame,_frame;
  Vec3T _localPos,_ctrBB;
  Mat3XT _localPatch;
  MatT _flipLeftRight;
  int _type;
  T _phi0;
  T _res;
};
}

#endif
