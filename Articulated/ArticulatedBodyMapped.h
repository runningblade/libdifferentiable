#ifndef ARTICULATED_BODY_MAPPED_H
#define ARTICULATED_BODY_MAPPED_H

#include "ArticulatedBody.h"

namespace PHYSICSMOTION {
struct ArticulatedBodyMapped : public ArticulatedBodyBase {
  typedef double T;
  DECL_MAT_VEC_MAP_TYPES_T
  ArticulatedBodyMapped(ArticulatedBody& body);
  ArticulatedBodyMapped(ArticulatedBody& body,const std::vector<int>& indices);
  virtual ~ArticulatedBodyMapped();
  int getIndex(int id) const;
  int getOffDOFUnmapped(int id) const;
  bool map(int id,int& idMapped) const;
  void gather(VecCM from,Vec& to) const;
  void scatter(VecCM from,VecM to) const;
  const Joint& joint(int id) const override;
  int nrJ() const override;
 private:
  ArticulatedBodyMapped(const ArticulatedBodyMapped& other);
  ArticulatedBodyMapped& operator=(const ArticulatedBodyMapped& other);
  void reset();
  ArticulatedBody& _body;
  std::vector<int> _indices;
  std::vector<int> _parents;
  std::vector<int> _offDOFs;
  std::vector<int> _offDDTs;
};
}

#endif
