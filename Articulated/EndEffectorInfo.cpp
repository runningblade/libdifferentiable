#include "EndEffectorInfo.h"
#include "PBDArticulatedGradientInfo.h"
#include <Environment/EnvironmentUtils.h>
#include <Optimizer/InnerSphereDetection.h>
#include <Optimizer/SQPObjective.h>
#include <Optimizer/GUROBIInterface.h>
#include <Utils/Interp.h>
#include <random>

namespace PHYSICSMOTION {
//linear interpolation
Eigen::Matrix<EndEffectorBounds::T,3,1> makeUnit(const Eigen::Matrix<EndEffectorBounds::T,3,1>& v) {
  int id;
  v.cwiseAbs().maxCoeff(&id);
  Eigen::Matrix<EndEffectorBounds::T,3,1> ret;
  ret.setUnit(id);
  if(v[id]<0)
    ret*=-1;
  return ret;
}
std::string toLower(std::string name) {
  std::transform(name.begin(),name.end(),name.begin(),
  [](unsigned char c) {
    return std::tolower(c);
  });
  return name;
}
void detectLeftRight(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees) {
  std::vector<std::string> names;
  for(int i=0; i<(int)ees.size(); i++)
    names.push_back(toLower(body.joint(ees[i]->jointId())._name));
  for(int r=0; r<(int)names.size(); r++)
    for(int c=0; c<(int)names.size(); c++)
      if(r==c)
        continue;
      else if(names[r].find("left")!=std::string::npos && names[c].find("right")!=std::string::npos) {
        ees[r]->_type=EndEffectorBounds::LEFT;
        ees[c]->_type=EndEffectorBounds::RIGHT;
      } else if(names[r].length()==names[c].length()) {
        for(int i=0; i<(int)names[r].length(); i++)
          if(names[r][i]!=names[c][i] && names[r][i]=='l' && names[c][i]=='r') {
            ees[r]->_type=EndEffectorBounds::LEFT;
            ees[c]->_type=EndEffectorBounds::RIGHT;
          }
      }
  for(int r=0; r<(int)names.size(); r++)
    if(!(ees[r]->_type&(EndEffectorBounds::LEFT|EndEffectorBounds::RIGHT)))
      ees[r]->_type=EndEffectorBounds::UNKNOWN;
}
void detectFrontBack(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees) {
  std::vector<std::string> names;
  for(int i=0; i<(int)ees.size(); i++)
    names.push_back(toLower(body.joint(ees[i]->jointId())._name));
  for(int r=0; r<(int)names.size(); r++)
    for(int c=0; c<(int)names.size(); c++)
      if(r==c)
        continue;
      else if(names[r].find("front")!=std::string::npos && names[c].find("back")!=std::string::npos) {
        ees[r]->_type|=EndEffectorBounds::FRONT;
        ees[c]->_type|=EndEffectorBounds::BACK;
      } else if(names[r].length()==names[c].length()) {
        for(int i=0; i<(int)names[r].length(); i++)
          if(names[r][i]!=names[c][i] && names[r][i]=='f' && names[c][i]=='h') {
            ees[r]->_type|=EndEffectorBounds::FRONT;
            ees[c]->_type|=EndEffectorBounds::BACK;
          } else if(names[r][i]!=names[c][i] && names[r][i]=='f' && names[c][i]=='b') {
            ees[r]->_type|=EndEffectorBounds::FRONT;
            ees[c]->_type|=EndEffectorBounds::BACK;
          }
      }
  for(int r=0; r<(int)names.size(); r++)
    if(!(ees[r]->_type&(EndEffectorBounds::FRONT|EndEffectorBounds::BACK)))
      ees[r]->_type=EndEffectorBounds::UNKNOWN;
}
EndEffectorBounds::EndEffectorBounds()
  :_localFrame(Mat3T::Identity()),_frame(Mat3T::Identity()),_localPos(Vec3T::Zero()),_ctrBB(Vec3T::Zero()),_localPatch(Mat3XT::Zero(3,0)),_type(UNKNOWN),_phi0(0),_res(0) {}
EndEffectorBounds::EndEffectorBounds(int jid)
  :_localFrame(Mat3T::Identity()),_frame(Mat3T::Identity()),_localPos(Vec3T::Zero()),_ctrBB(Vec3T::Zero()),_localPatch(Mat3XT::Zero(3,0)),_type(UNKNOWN),_phi0(0),_res(0) {
  _JID.push_back(jid);
}
std::vector<int> EndEffectorBounds::idDOF(std::shared_ptr<ArticulatedBody> body) const {
  std::vector<int> DOFIds;
  nrDOF(*body,&DOFIds);
  return DOFIds;
}
int EndEffectorBounds::nrDOF(const ArticulatedBody& body,std::vector<int>* DOFIds) const {
  int ret=0;
  for(int i=0; i<(int)_JID.size(); i++) {
    const Joint& J=body.joint(_JID[i]);
    ret+=J.nrDOF();
    if(DOFIds)
      for(int j=0; j<J.nrDOF(); j++)
        DOFIds->push_back(J._offDOF+j);
  }
  return ret;
}
int EndEffectorBounds::nrDOF(std::shared_ptr<ArticulatedBody> body,std::vector<int>* DOFIds) const {
  return nrDOF(*body,DOFIds);
}
bool EndEffectorBounds::exists(const BBoxExact& bb) const {
  for(BBoxExact::T x=bb.minCorner()[0]; x<=bb.maxCorner()[0]; x++)
    for(BBoxExact::T y=bb.minCorner()[1]; y<=bb.maxCorner()[1]; y++)
      for(BBoxExact::T z=bb.minCorner()[2]; z<=bb.maxCorner()[2]; z++)
        if(_indices.find(BBoxExact::Vec3T(x,y,z).template cast<int>())==_indices.end())
          return false;
  return true;
}
bool EndEffectorBounds::exists(std::shared_ptr<BBoxExact> bb) const {
  return exists(*bb);
}
EndEffectorBounds::Vec EndEffectorBounds::getDOFAll(const ArticulatedBody& body,const Vec3T& pt) const {
  Vec x=getDOF(pt),xAll=Vec::Zero(body.nrDOF());
  for(int i=0,id=0; i<(int)_JID.size(); i++) {
    const Joint& J=body.joint(_JID[i]);
    for(int j=0; j<J.nrDOF(); j++,id++)
      xAll[J._offDOF+j]=x[id];
  }
  return xAll;
}
EndEffectorBounds::Vec EndEffectorBounds::getDOFAll(std::shared_ptr<ArticulatedBody> body,const Vec3T& pt) const {
  return getDOFAll(*body,pt);
}
EndEffectorBounds::Vec EndEffectorBounds::getDOFAll(const ArticulatedBody& body,const Vec3T& pt,const Vec3T& nor) const {
  Vec x=getDOF(pt),xAll=Vec::Zero(body.nrDOF());
  if(x.size()>3)
    solveIKNormal(body,pt,nor,x,1);
  for(int i=0,id=0; i<(int)_JID.size(); i++) {
    const Joint& J=body.joint(_JID[i]);
    for(int j=0; j<J.nrDOF(); j++,id++)
      xAll[J._offDOF+j]=x[id];
  }
  return xAll;
}
EndEffectorBounds::Vec EndEffectorBounds::getDOFAll(std::shared_ptr<ArticulatedBody> body,const Vec3T& pt,const Vec3T& nor) const {
  return getDOFAll(*body,pt,nor);
}
EndEffectorBounds::Vec EndEffectorBounds::getDOF(const Vec3T& pt) const {
  Vec3T frac=_frame.transpose()*(pt-_ctrBB)/_res;
  Eigen::Matrix<int,3,1> id=frac.cast<int>().cwiseMin(_bb.segment<3>(3)-Eigen::Matrix<int,3,1>::Ones()).cwiseMax(_bb.segment<3>(0));
  Vec id000=_indices.find(id+Eigen::Matrix<int,3,1>(0,0,0))->second;
  Vec id100=_indices.find(id+Eigen::Matrix<int,3,1>(1,0,0))->second;
  Vec id010=_indices.find(id+Eigen::Matrix<int,3,1>(0,1,0))->second;
  Vec id110=_indices.find(id+Eigen::Matrix<int,3,1>(1,1,0))->second;
  Vec id001=_indices.find(id+Eigen::Matrix<int,3,1>(0,0,1))->second;
  Vec id101=_indices.find(id+Eigen::Matrix<int,3,1>(1,0,1))->second;
  Vec id011=_indices.find(id+Eigen::Matrix<int,3,1>(0,1,1))->second;
  Vec id111=_indices.find(id+Eigen::Matrix<int,3,1>(1,1,1))->second;
  return interp3D(id000,id100,id010,id110,
                  id001,id101,id011,id111,
                  frac[0]-id[0],frac[1]-id[1],frac[2]-id[2]);
}
EndEffectorBounds::Vec3T EndEffectorBounds::globalPosAll(const ArticulatedBody& body,const Vec& xAll) const {
  Vec x=Vec::Zero(nrDOF(body));
  for(int i=0,id=0; i<(int)_JID.size(); i++) {
    const Joint& J=body.joint(_JID[i]);
    for(int j=0; j<J.nrDOF(); j++,id++)
      x[id]=xAll[J._offDOF+j];
  }
  return globalPos(body,x);
}
EndEffectorBounds::Vec3T EndEffectorBounds::globalPosAll(std::shared_ptr<ArticulatedBody> body,const Vec& xAll) const {
  return globalPosAll(*body,xAll);
}
EndEffectorBounds::Vec3T EndEffectorBounds::globalPos(const ArticulatedBody& body,const Vec& x) const {
  Vec dof=Vec::Zero(body.nrDOF());
  if(x.size()==dof.size())
    dof=x;
  else {
    for(int i=0,id=0; i<(int)_JID.size(); i++) {
      const Joint& J=body.joint(_JID[i]);
      for(int j=0; j<J.nrDOF(); j++,id++)
        dof[J._offDOF+j]=x[id];
    }
  }
  PBDArticulatedGradientInfo<T> info(body,dof);
  return ROTI(info._TM,_JID[0])*_localPos+CTRI(info._TM,_JID[0]);
}
EndEffectorBounds::Vec3T EndEffectorBounds::globalPos(std::shared_ptr<ArticulatedBody> body,const Vec& x) const {
  return globalPos(*body,x);
}
std::string EndEffectorBounds::checkIndexError(const ArticulatedBody& body) const {
  T maxErr=0,meanErr=0;
  for(const std::pair<const Eigen::Matrix<int,3,1>,Vec>& index:_indices) {
    Vec3T posRef=globalPos(body,index.second);
    Vec3T pos=_frame*index.first.template cast<T>()*_res+_ctrBB;
    meanErr+=(pos-posRef).norm();
    maxErr=std::max(maxErr,(pos-posRef).norm());
  }
  meanErr/=(T)_indices.size();
  return "EE("+name()+",mean="+std::to_string((double)meanErr)+",max="+std::to_string((double)maxErr)+")";
}
std::string EndEffectorBounds::checkIndexError(std::shared_ptr<ArticulatedBody> body) const {
  return checkIndexError(*body);
}
EndEffectorBounds::Vec3T EndEffectorBounds::getBBCenter() const {
  typename EndEffectorBounds::Vec3T ret=(_bb.template segment<3>(0)+_bb.template segment<3>(3)).template cast<T>()*_res/2;
  return _frame*ret+_ctrBB;
}
EndEffectorBounds::T EndEffectorBounds::getBBRadius() const {
  return (_bb.template segment<3>(3)-_bb.template segment<3>(0)).minCoeff()*_res/2;
}
EndEffectorBounds::Vec3T EndEffectorBounds::randomPos() const {
  std::random_device rd;
  std::mt19937 gen(rd());
  Vec3T pos(interp1D(_bb[0]*_res,_bb[3]*_res,std::uniform_real_distribution<>(0,1)(gen)),
            interp1D(_bb[1]*_res,_bb[4]*_res,std::uniform_real_distribution<>(0,1)(gen)),
            interp1D(_bb[2]*_res,_bb[5]*_res,std::uniform_real_distribution<>(0,1)(gen)));
  return _frame*pos+_ctrBB;
}
int EndEffectorBounds::jointId() const {
  return _JID[0];
}
bool EndEffectorBounds::isEven() const {
  bool isFront=_type&FRONT;
  bool isLeft=_type&LEFT;
  return ((int)isFront+(int)isLeft)%2;
}
std::string EndEffectorBounds::name() const {
  bool isFront=_type&EndEffectorBounds::FRONT;
  bool isLeft=_type&EndEffectorBounds::LEFT;
  return std::string(isFront?"Front":"Back")+std::string(isLeft?"Left":"Right")+"Foot";
}
bool EndEffectorBounds::operator==(const EndEffectorBounds& other) const {
  return _JID[0]==other._JID[0] && _localPos==other._localPos;
}
bool EndEffectorBounds::read(std::istream& is,IOData* dat) {
  readBinaryData(_JID,is,dat);
  readBinaryData(_indices,is,dat);
  readBinaryData(_bb,is,dat);
  readBinaryData(_localFrame,is,dat);
  readBinaryData(_frame,is,dat);
  readBinaryData(_localPos,is,dat);
  readBinaryData(_ctrBB,is,dat);
  readBinaryData(_localPatch,is,dat);
  readBinaryData(_flipLeftRight,is,dat);
  readBinaryData(_type,is,dat);
  readBinaryData(_phi0,is,dat);
  readBinaryData(_res,is,dat);
  return is.good();
}
bool EndEffectorBounds::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_JID,os,dat);
  writeBinaryData(_indices,os,dat);
  writeBinaryData(_bb,os,dat);
  writeBinaryData(_localFrame,os,dat);
  writeBinaryData(_frame,os,dat);
  writeBinaryData(_localPos,os,dat);
  writeBinaryData(_ctrBB,os,dat);
  writeBinaryData(_localPatch,os,dat);
  writeBinaryData(_flipLeftRight,os,dat);
  writeBinaryData(_type,os,dat);
  writeBinaryData(_phi0,os,dat);
  writeBinaryData(_res,os,dat);
  return os.good();
}
std::shared_ptr<SerializableBase> EndEffectorBounds::copy() const {
  return std::shared_ptr<SerializableBase>(new EndEffectorBounds());
}
std::string EndEffectorBounds::type() const {
  return typeid(EndEffectorBounds).name();
}
//detect end-effector
bool EndEffectorBounds::detectEndEffector(const ArticulatedBody& body, std::vector<std::shared_ptr<EndEffectorBounds>>& ees, const std::set<std::string>& fixNames) {
  std::function<std::string(int)> getJointName=[&](int id)->std::string {
    for(std::shared_ptr<EndEffectorBounds>& ee:ees)
      if(ee->_type==id)
        return body.joint(ee->jointId())._name;
    return "";
  };
  std::function<std::string(const std::set<std::string>&)> printNames=[](const std::set<std::string>& names)->std::string {
    int i=0;
    std::string ret="{"+std::string(names.empty()?"}":"");
    for(const std::string& name:names)
      ret+=name+std::string((i++)==(int)names.size()-1?"}":",");
    return ret;
  };
  std::cout << "detectEndEffector: " << "fixNames=" << printNames(fixNames) << std::endl;
  ees.clear();
  for(int i=0; i<body.nrJ(); i++)
    if(body.joint(i)._children.empty()) {
      EndEffectorBounds ee(i);
      std::string name=toLower(body.joint(i)._name);
      if(name.find("neck")!=std::string::npos)
        continue;
      if(name.find("head")!=std::string::npos)
        continue;
      if(name.find("arm")!=std::string::npos)
        continue;
      if(name.find("hand")!=std::string::npos)
        continue;
      for(int j=body.joint(i)._parent; j>=0; j=body.joint(j)._parent)
        ee._JID.push_back(j);
      for(int j=0; j<(int)ee._JID.size(); j++)
        if(fixNames.find(body.joint(ee._JID[j])._name)!=fixNames.end())
          ee._JID.erase(ee._JID.begin()+j--);
      ees.push_back(std::shared_ptr<EndEffectorBounds>(new EndEffectorBounds(ee)));
    }
  if(ees.size()==2) {
    //bipedal
    int rootJointId=body.commonRoot(ees[0]->jointId(),ees[1]->jointId());
    for(int d=0; d<(int)ees.size(); d++) {
      std::vector<int>::const_iterator it=std::find(ees[d]->_JID.begin(),ees[d]->_JID.end(),rootJointId);
      ees[d]->_JID.erase(it,ees[d]->_JID.end());
    }
    detectLeftRight(body,ees);
    for(int d=0; d<(int)ees.size(); d++)
      if(ees[d]->_type==EndEffectorBounds::UNKNOWN) {
        std::cout << "Failed detecting foot id!" << std::endl;
        return false;
      }
    std::cout << "Identified bipedal with common root: " << rootJointId << std::endl;
    std::cout << "Left foot joint id: " << getJointName(EndEffectorBounds::LEFT) << std::endl;
    std::cout << "Right foot joint id: " << getJointName(EndEffectorBounds::RIGHT) << std::endl;
    return true;
  } else if(ees.size()==4) {
    //quadruple
    int rootJointId=std::numeric_limits<int>::max();
    for(int r=0; r<(int)ees.size(); r++)
      for(int c=r+1; c<(int)ees.size(); c++)
        rootJointId=std::min(rootJointId,body.commonRoot(ees[r]->jointId(),ees[c]->jointId()));
    for(int d=0; d<(int)ees.size(); d++) {
      std::vector<int>::const_iterator it=std::find(ees[d]->_JID.begin(),ees[d]->_JID.end(),rootJointId);
      ees[d]->_JID.erase(it,ees[d]->_JID.end());
    }
    detectLeftRight(body,ees);
    detectFrontBack(body,ees);
    for(int d=0; d<(int)ees.size(); d++)
      if(ees[d]->_type==EndEffectorBounds::UNKNOWN) {
        std::cout << "Failed detecting foot id!" << std::endl;
        return false;
      }
    std::cout << "Identified quadruple with common root: " << rootJointId << std::endl;
    std::cout << "FrontLeft foot joint id: " << getJointName(EndEffectorBounds::FRONT|EndEffectorBounds::LEFT) << std::endl;
    std::cout << "FrontRight foot joint id: " << getJointName(EndEffectorBounds::FRONT|EndEffectorBounds::RIGHT) << std::endl;
    std::cout << "BackLeft foot joint id: " << getJointName(EndEffectorBounds::BACK|EndEffectorBounds::LEFT) << std::endl;
    std::cout << "BackRight foot joint id: " << getJointName(EndEffectorBounds::BACK|EndEffectorBounds::RIGHT) << std::endl;
    return true;
  } else {
    std::cout << "Unsupported robot with #foot=" << (int)ees.size() << std::endl;
    return false;
  }
}
EndEffectorBounds::Mat3X4T EndEffectorBounds::fitEndEffectorBall(const ArticulatedBody& body,int jid,Vec3T& localPos,T& phi0,const Vec3T& upDir,const Vec* pose,T errThres) {
  const Joint& J=body.joint(jid);
  SQPObjectiveCompound<T> obj;
  typename SQPObjectiveCompound<T>::Vec x;
  Mat3XT tss=body.getT(pose?*pose:Vec::Zero(body.nrDOF()));
  Mat3X4T transIM=TRANSI(tss,jid);
  std::shared_ptr<ConvexHullExact> cHull=std::dynamic_pointer_cast<ConvexHullExact>(J._mesh);
  if(!cHull) {
    std::vector<Eigen::Matrix<double,3,1>> vss;
    std::vector<Eigen::Matrix<int,3,1>> iss;
    J._mesh->getMesh(vss,iss);
    cHull.reset(new ConvexHullExact(vss));
    std::cout << "Building convex hull (#V=" << vss.size() << ") for joint " << jid << " to detect end-effector" << (errThres==0?"-foot!":"!") << std::endl;
  }
  std::shared_ptr<InnerSphereDetection> prob(new InnerSphereDetection(obj,*cHull,transIM,upDir,errThres));
  obj.addComponent(prob);
#ifdef GUROBI_SUPPORT
  ASSERT_MSG(GUROBIInterface<T>().optimize(obj,false,1e-8,1e6,1e6,x=obj.init()),"Gurobi failed!")
#else
  ASSERT_MSG(false,"GUROBI not supported!")
#endif
  localPos=prob->localPos(x);
  phi0=prob->phi0(x);
  return Mat3X4T::Identity();
}
EndEffectorBounds::Mat3X4T EndEffectorBounds::fitEndEffectorPatch(const ArticulatedBody& body,int jid,Mat3XT& localPatch,T phi0,const Vec* pose) {
  const Joint& J=body.joint(jid);
  Mat3XT tss=body.getT(pose?*pose:Vec::Zero(body.nrDOF()));
  Mat3X4T transIM=TRANSI(tss,jid);
  std::vector<Eigen::Matrix<double,3,1>> vss;
  {
    T minZ=std::numeric_limits<T>::max();
    std::vector<Eigen::Matrix<int,3,1>> iss;
    J._mesh->getMesh(vss,iss);
    for(Eigen::Matrix<double,3,1>& v:vss) {
      v=ROT(transIM)*v+CTR(transIM);
      minZ=std::min(minZ,v[2]);
    }
    makeConvexProject(vss);
    for(Eigen::Matrix<double,3,1>& v:vss)
      v[2]=minZ;
  }
  //simplify mesh
  while(true) {
    T minDist=0;
    int minId=-1;
    for(int i=0; i<(int)vss.size(); i++) {
      T dist=(vss[i]-vss[(i+1)%vss.size()]).norm();
      if(minId==-1 || dist<minDist) {
        minDist=dist;
        minId=i;
      }
    }
    if(minDist<phi0)
      vss.erase(vss.begin()+minId);
    else break;
  }
  localPatch.resize(3,vss.size());
  for(int i=0; i<(int)vss.size(); i++)
    localPatch.col(i)=ROTI(tss,jid).transpose()*(vss[i]-CTRI(tss,jid));
  return Mat3X4T::Identity();
}
EndEffectorBounds::Mat3T EndEffectorBounds::detectEndEffectorPose(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees,const Vec* pose,T errThres) {
  Mat3XT tss=body.getT(pose?*pose:Vec::Zero(body.nrDOF()));
  Vec3T left,right,ctr;
  Mat3T frame;
  //left right
  left.setZero();
  right.setZero();
  for(int i=0; i<(int)ees.size(); i++) {
    const Joint& J=body.joint(ees[i]->jointId());
    Mat3X4T transI=TRANSI(tss,ees[i]->jointId());
    Vec3T c=ROT(transI)*J.getC()+CTR(transI);
    if(ees[i]->_type&EndEffectorBounds::LEFT)
      left+=c;
    if(ees[i]->_type&EndEffectorBounds::RIGHT)
      right+=c;
  }
  //ctr
  int rootId=-1;
  for(int i=0; i<body.nrJ(); i++)
    if(body.joint(i).isRoot(body))
      rootId=i;
  Mat3X4T transI=TRANSI(tss,rootId);
  ctr=ROT(transI)*body.joint(rootId).getC()+CTR(transI);
  //detect frame
  frame.col(1)=(left-right).normalized();
  frame.col(1)=makeUnit(frame.col(1));
  frame.col(2)=(ctr-(left+right)/2).normalized();
  frame.col(2)=makeUnit(frame.col(2));
  frame.col(0)=frame.col(1).cross(frame.col(2)).normalized();
  frame.col(2)=frame.col(0).cross(frame.col(1)).normalized();
  std::cout << "Front Axis=" << frame.col(0).transpose() << std::endl;
  std::cout << "Left Axis=" << frame.col(1).transpose() << std::endl;
  std::cout << "Up Axis=" << frame.col(2).transpose() << std::endl;
  //detect local position
  for(int i=0; i<(int)ees.size(); i++) {
    fitEndEffectorBall(body,ees[i]->_JID[0],ees[i]->_localPos,ees[i]->_phi0,frame.col(2),pose,errThres);
    if(ees[i]->nrDOF(body)>3)
      fitEndEffectorPatch(body,ees[i]->_JID[0],ees[i]->_localPatch,ees[i]->_phi0,pose);
    Mat3X4T transI=TRANSI(tss,ees[i]->jointId());
    ees[i]->_localFrame=ROT(transI).transpose()*frame;
    ees[i]->_frame=frame;
  }
  return frame;
}
}
