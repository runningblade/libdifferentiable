#include "ArticulatedBodyMapped.h"

namespace PHYSICSMOTION {
//ArticulatedBodyMapped
ArticulatedBodyMapped::ArticulatedBodyMapped(ArticulatedBody& body):_body(body) {
  for(int i=0; i<body.nrJ(); i++)
    _indices.push_back(i);
  reset();
}
ArticulatedBodyMapped::ArticulatedBodyMapped(ArticulatedBody& body,const std::vector<int>& indices)
  :_body(body),_indices(indices) {
  reset();
}
ArticulatedBodyMapped::~ArticulatedBodyMapped() {
  //restore offDOF,offDDT
  for(int i=0; i<nrJ(); i++) {
    Joint& J=_body.joint(_indices[i]);
    J._offDOF=_offDOFs[i];
    J._offDDT=_offDDTs[i];
    J._parent=_parents[i];
  }
}
int ArticulatedBodyMapped::getIndex(int id) const {
  return _indices[id];
}
int ArticulatedBodyMapped::getOffDOFUnmapped(int id) const {
  return _offDOFs[id];
}
bool ArticulatedBodyMapped::map(int id,int& idMapped) const {
  if(id==-1) {
    idMapped=-1;
    return false;
  }
  auto it=std::lower_bound(_indices.begin(),_indices.end(),id);
  if(it!=_indices.end() && *it==id) {
    idMapped=std::distance(_indices.begin(),it);
    return true;
  } else {
    idMapped=id;
    return false;
  }
}
const Joint& ArticulatedBodyMapped::joint(int id) const {
  return _body.joint(_indices[id]);
}
int ArticulatedBodyMapped::nrJ() const {
  return (int)_indices.size();
}
//helper
ArticulatedBodyMapped::ArticulatedBodyMapped(const ArticulatedBodyMapped& other):_body(other._body) {}
ArticulatedBodyMapped& ArticulatedBodyMapped::operator=(const ArticulatedBodyMapped&) {
  return *this;
}
void ArticulatedBodyMapped::reset() {
  //save offDOF,offDDT and reorder
  int offDOF=0,offDDT=0,parentMapped;
  for(int i=0; i<nrJ(); i++) {
    Joint& J=_body.joint(_indices[i]);
    _offDOFs.push_back(J._offDOF);
    _offDDTs.push_back(J._offDDT);
    _parents.push_back(J._parent);
    //joint
    J._offDOF=offDOF;
    J._offDDT=offDDT;
    if(!map(J._parent,parentMapped))
      J._parent=-1;
    else J._parent=parentMapped;
    //update
    offDOF+=J.nrDOF();
    offDDT+=J.nrDDT();
  }
}
}
