#ifndef ARTICULATED_BODY_BASE_H
#define ARTICULATED_BODY_BASE_H

#include <Utils/Pragma.h>

namespace PHYSICSMOTION {
struct Joint;
struct ArticulatedBodyBase {
  virtual ~ArticulatedBodyBase();
  virtual const Joint& joint(int id) const=0;
  virtual int nrJ() const=0;
  int nrDOF() const;
  int nrDDT() const;
  int rootJointId() const;
};
}

#endif
