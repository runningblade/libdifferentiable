#include "EndEffectorInfo.h"
#include <Optimizer/InverseKinematics.h>
#include <Optimizer/IPOPTInterface.h>
#include <Optimizer/KNInterface.h>
#include <Utils/Interp.h>
#include <random>

namespace PHYSICSMOTION {
//BB metric
std::shared_ptr<EndEffectorBounds>& findEE(std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int type) {
  for(int i=0; i<(int)ees.size(); i++)
    if(ees[i]->_type==type)
      return ees[i];
  ASSERT(false)
  return ees[0];
}
void EndEffectorBounds::flipLeftRight() {
  for(int d=0; d<2; d++) {
    //bb
    _bb[d]*=-1;
    _bb[d+3]*=-1;
    std::swap(_bb[d],_bb[d+3]);
    //frame
    _frame.col(d)*=-1;
    //indices
    std::unordered_map<Eigen::Matrix<int,3,1>,Vec,TriangleHash> indices;
    for(const std::pair<const Eigen::Matrix<int,3,1>,Vec>& item:_indices) {
      Eigen::Matrix<int,3,1> id=item.first;
      id[d]*=-1;
      indices[id]=item.second;
    }
    _indices=indices;
  }
}
EndEffectorBounds::Vec EndEffectorBounds::flipLeftRightCoef() const {
  return _flipLeftRight.diagonal();
}
bool EndEffectorBounds::findIndexGrid(const ArticulatedBody& body,Vec DOF) {
  Vec DOF2;
  //initialize grid
  Eigen::Matrix<int,3,1> id0=(_frame.transpose()*(globalPos(body,DOF)-_ctrBB)/_res).template cast<int>();
  if(!solveIKForIndexGrid(body,_frame*id0.template cast<T>()*_res+_ctrBB,DOF)) {
    std::cout << "Initial IK solve failed!" << std::endl;
    return false;
  }
  _indices.clear();
  _indices[id0]=DOF;
  //extend grid
  std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> front,frontNew;
  front.insert(id0);
  while(!front.empty()) {
    frontNew.clear();
    for(const Eigen::Matrix<int,3,1>& id:front)
      for(int x=-1; x<=1; x++)
        for(int y=-1; y<=1; y++)
          for(int z=-1; z<=1; z++) {
            if(x==0 && y==0 && z==0)
              continue;
            Eigen::Matrix<int,3,1> id2(id[0]+x,id[1]+y,id[2]+z);
            if(_indices.find(id2)==_indices.end())
              if(solveIKForIndexGrid(body,_frame*id2.template cast<T>()*_res+_ctrBB,DOF2=_indices[id]))
                if(neighborContinuous(body,id2,DOF2,_res)) {
                  frontNew.insert(id2);
                  _indices[id2]=DOF2;
                }
          }
    std::cout << "Explored " << _indices.size() << " indices!" << std::endl;
    front.swap(frontNew);
  }
  return true;
}
bool EndEffectorBounds::DOFAlreadyExists(const ArticulatedBody& body,Vec DOF,T errThres) const {
  Eigen::Matrix<int,3,1> id0=(_frame.transpose()*(globalPos(body,DOF)-_ctrBB)/_res).template cast<int>();
  if(!solveIKForIndexGrid(body,_frame*id0.template cast<T>()*_res+_ctrBB,DOF)) {
    std::cout << "Initial IK solve failed!" << std::endl;
    return false;
  }
  if(_indices.find(id0)!=_indices.end() && (_indices.find(id0)->second-DOF).cwiseAbs().maxCoeff()<errThres)
    return true;
  else return false;
}
void EndEffectorBounds::findBestBB(const Vec3T* mustContain,const Vec3T* mustCenter,const Eigen::Matrix<int,3,1>& maskVec) {
  //initialize
  std::unordered_map<Eigen::Matrix<int,3,1>,std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash>,TriangleHash> bbs;
  for(const std::pair<const Eigen::Matrix<int,3,1>,Vec>& index:_indices) {
    const Eigen::Matrix<int,3,1>& id=index.first;
    bool valid=true;
    for(int d=0; d<8 && valid; d++)
      if(_indices.find(Eigen::Matrix<int,3,1>((d&1)?1:0,(d&2)?1:0,(d&4)?1:0)+id)==_indices.end())
        valid=false;
    if(valid)
      bbs[Eigen::Matrix<int,3,1>(1,1,1)].insert(id);
  }
  //main loop of dynamic programming
  std::cout << "Computing best BB!" << std::endl;
  std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash> front,frontNew;
  front.insert(Eigen::Matrix<int,3,1>(1,1,1));
  _bb.setZero();
  while(!front.empty()) {
    frontNew.clear();
    for(const Eigen::Matrix<int,3,1>& sz:front)
      for(int d=0; d<3; d++) {
        Eigen::Matrix<int,3,1> szExt=sz+Eigen::Matrix<int,3,1>::Unit(d);
        Eigen::Matrix<int,3,1> szSlice=sz;
        szSlice[d]=1;
        ASSERT_MSGV(bbs.find(szSlice)!=bbs.end(),"Cannot find index (%d,%d,%d) for BB extension!",szSlice[0],szSlice[1],szSlice[2])
        if(bbs.find(szExt)!=bbs.end())
          continue;
        //extend size
        std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash>& minCsExt=bbs[szExt];
        const std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash>& minCs=bbs[sz];
        const std::unordered_set<Eigen::Matrix<int,3,1>,TriangleHash>& minCsSlice=bbs[szSlice];
        for(auto minC:minCs)
          if(minCsSlice.find(minC+Eigen::Matrix<int,3,1>::Unit(d)*sz[d])!=minCsSlice.end()) {
            minCsExt.insert(minC);
            Eigen::Matrix<int,6,1> bbExt=concat(minC,minC+szExt);
            if(_bb.isZero() || betterThan(bbExt,_bb,mustContain,mustCenter,maskVec))
              _bb=bbExt;
          }
        if(!minCsExt.empty())
          frontNew.insert(szExt);
      }
    front.swap(frontNew);
  }
  //debug
  for(int x=_bb[0]; x<=_bb[3]; x++)
    for(int y=_bb[1]; y<=_bb[4]; y++)
      for(int z=_bb[2]; z<=_bb[5]; z++) {
        ASSERT_MSGV(_indices.find(Eigen::Matrix<int,3,1>(x,y,z))!=_indices.end(),"Cannot find index (%d,%d,%d) in BB result!",x,y,z)
      }
  std::cout << "Best BB=" << _bb.transpose() << std::endl;
}
bool EndEffectorBounds::neighborContinuous(const ArticulatedBody& body,const Eigen::Matrix<int,3,1> id,const Vec& DOF,T errThres) const {
  for(int d=0; d<6; d++) {
    Eigen::Matrix<int,3,1> neighbor=id+Eigen::Matrix<int,3,1>::Unit(d/2)*((d%2)?-1:1);
    if(_indices.find(neighbor)==_indices.end())
      continue;
    Vec neighborDOF=_indices.find(neighbor)->second;
    Vec3T a=globalPos(body,DOF);
    Vec3T b=globalPos(body,neighborDOF);
    Vec3T mid=globalPos(body,(DOF+neighborDOF)/2);
    if((mid-(a+b)/2).norm()>errThres)
      return false;
  }
  return true;
}
bool EndEffectorBounds::solveIKForIndexGrid(const ArticulatedBody& body,const Vec3T& pos,Vec& x,T errThres,bool useIPOPT) const {
  std::vector<int> DOFIds;
  int NDOF=nrDOF(body,&DOFIds);
  SQPObjectiveCompound<T> obj;
  std::shared_ptr<InverseKinematicsPosObj<T>> IK;
  if(NDOF==3)
    IK.reset(new InverseKinematicsPosObj<T>(obj,"",body,pos,_localPos,DOFIds,&x));
  else if(NDOF==5)
    IK.reset(new InverseKinematicsPosNorObj<T>(obj,"",body,pos,_frame.col(0),_localPos,_localFrame.col(0),DOFIds,-1,&x));
  else if(NDOF==6)
    IK.reset(new InverseKinematicsPosFrameObj<T>(obj,"",body,pos,_frame,_localPos,_localFrame,DOFIds,-1,&x));
  //IK->debug(10,0);
  obj.addComponent(IK);
#ifdef IPOPT_SUPPORT
  if(useIPOPT)
    IPOPTInterface<T>::optimize(obj,5,0,1e-8f,1000,1000,x);
#endif
#ifdef KNITRO_SUPPORT
  if(!useIPOPT)
    KNInterface<T>(obj,true).optimize(false,0,1e-8f,1e-8f,false,1000,1000,x);
#endif
  return (globalPos(body,x)-pos).norm()<errThres;
}
bool EndEffectorBounds::solveIKNormal(const ArticulatedBody& body,const Vec3T& pos,const Vec3T& nor,Vec& x,T coef,T errThres,bool useIPOPT) const {
  std::vector<int> DOFIds;
  nrDOF(body,&DOFIds);
  SQPObjectiveCompound<T> obj;
  std::shared_ptr<InverseKinematicsPosObj<T>> IK(new InverseKinematicsPosNorObj<T>(obj,"",body,pos,nor,_localPos,_localFrame.col(2),DOFIds,coef,&x));
  //IK->debug(10,0);
  obj.addComponent(IK);
#ifdef IPOPT_SUPPORT
  if(useIPOPT)
    IPOPTInterface<T>::optimize(obj,5,0,1e-8f,1000,1000,x);
#endif
#ifdef KNITRO_SUPPORT
  if(!useIPOPT)
    KNInterface<T>(obj,true).optimize(false,0,1e-8f,1e-8f,false,1000,1000,x);
#endif
  return (globalPos(body,x)-pos).norm()<errThres;
}
bool EndEffectorBounds::solveIK(const ArticulatedBody& body,const Vec3T& pos,Vec& x,T errThres,bool useIPOPT) const {
  std::vector<int> DOFIds;
  nrDOF(body,&DOFIds);
  SQPObjectiveCompound<T> obj;
  std::shared_ptr<InverseKinematicsPosObj<T>> IK(new InverseKinematicsPosObj<T>(obj,"",body,pos,_localPos,DOFIds,&x));
  //IK->debug(10,0);
  obj.addComponent(IK);
#ifdef IPOPT_SUPPORT
  if(useIPOPT)
    IPOPTInterface<T>::optimize(obj,5,0,1e-8f,1000,1000,x);
#endif
#ifdef KNITRO_SUPPORT
  if(!useIPOPT)
    KNInterface<T>(obj,true).optimize(false,0,1e-8f,1e-8f,false,1000,1000,x);
#endif
  return (globalPos(body,x)-pos).norm()<errThres;
}
EndEffectorBounds::MatT EndEffectorBounds::detectSymmetry(const ArticulatedBody& body,EndEffectorBounds& l,EndEffectorBounds& r) {
  ASSERT_MSGV(l._JID.size()==r._JID.size(),"Left(%d joints) and Right(%d joints) limbs do not have a same number of joints!",(int)l._JID.size(),(int)r._JID.size())
  //find test params
  std::vector<int> LDOFId,RDOFId;
  l.nrDOF(body,&LDOFId);
  r.nrDOF(body,&RDOFId);
  //find best symmetry score
  Vec bestL2r;
  int minId=-1,secondToMinId=-1;
  std::vector<T> scores(std::pow(2,LDOFId.size()),0);
  while(true) {
    //sample random pose
    Vec pose=Vec::Random(LDOFId.size())*M_PI;
    //enumerate all cases
    minId=-1,secondToMinId=-1;
    for(int i=0; i<std::pow(2,LDOFId.size()); i++) {
      Vec l2r=Vec::Zero(LDOFId.size());
      for(int j=0; j<(int)LDOFId.size(); j++)
        l2r[j]=(i&(1<<j))>0?1:-1;
      //compute score
      Vec3T L=l.globalPos(body,pose);
      Vec3T R=r.globalPos(body,l2r.asDiagonal()*pose);
      L+=l._frame.col(1)*(l._ctrBB-L).dot(l._frame.col(1))*2;
      scores[i]+=(L-R).norm();
      //update min/secondToMin
      if(minId==-1 || scores[i]<scores[minId]) {
        bestL2r=l2r;
        minId=i;
      }
      if(secondToMinId==-1 || (i!=minId && scores[i]>=scores[minId] && scores[i]<scores[secondToMinId]))
        secondToMinId=i;
    }
    //as long as the smallest score is much smaller (<0.1) than second to smallest, we stop
    if(scores[minId]<scores[secondToMinId]*0.1f)
      break;
  }
  l._flipLeftRight=r._flipLeftRight=bestL2r.asDiagonal();
  return bestL2r.asDiagonal();
}
bool EndEffectorBounds::detectReachableRegion(const ArticulatedBody& body,EndEffectorBounds& l,EndEffectorBounds& r,T errThres,int nrTrial,bool containLocalPose,bool centerLocalPose,const Eigen::Matrix<int,3,1>& maskVec) {
  l._res=errThres*l._phi0;
  l._bb.setZero();
  if(l.nrDOF(body)!=r.nrDOF(body) || (l.nrDOF(body)!=3 && l.nrDOF(body)!=5 && l.nrDOF(body)!=6)) {
    std::cout << "Cannot compute reachable region for EE.nrDOF()(" << l.nrDOF(body) << ") not in {3,5,6}!" << std::endl;
    return false;
  }
  l._ctrBB=r._ctrBB=(l.globalPos(body,Vec::Zero(body.nrDOF()))+r.globalPos(body,Vec::Zero(body.nrDOF())))/2;
  Vec3T localPos=l._frame.transpose()*(l.globalPos(body,Vec::Zero(body.nrDOF()))-l._ctrBB)/l._res;
  MatT l2r=detectSymmetry(body,l,r);
  std::cout << "l2r=" << std::endl << l2r << std::endl;
  //exhaustive bb search
  std::vector<int> DOFIds;
  l.nrDOF(body,&DOFIds);
  std::random_device rd;
  std::mt19937 gen(rd());
  std::cout << "Choosing index resolution=" << l._res << std::endl;
  std::vector<EndEffectorBounds> triedEEs;
  for(int trial=0; trial<nrTrial; trial++) {
    //choose initial DOF
    Vec DOF=Vec::Zero(DOFIds.size());
    for(int i=0; i<DOF.size(); i++)
      DOF[i]=std::uniform_real_distribution<>(body.lowerLimit()[DOFIds[i]],body.upperLimit()[DOFIds[i]])(gen);
    //check if already exists
    bool exists=false;
    for(const EndEffectorBounds& triedEE:triedEEs)
      if(triedEE.DOFAlreadyExists(body,DOF)) {
        std::cout << "Initial guess already exists!" << std::endl;
        exists=true;
        break;
      }
    if(exists)
      continue;
    //compute grid
    EndEffectorBounds triedEE=l;
    if(!triedEE.findIndexGrid(body,DOF)) {
      std::cout << "Failed creating index grid!" << std::endl;
      continue;
    }
    //compute bestBB
    triedEE.findBestBB(containLocalPose?&localPos:NULL,centerLocalPose?&localPos:NULL,maskVec);
    if(betterThan(triedEE._bb,l._bb,containLocalPose?&localPos:NULL,centerLocalPose?&localPos:NULL,maskVec))
      l=triedEE;
    triedEEs.push_back(triedEE);
  }
  if((l._bb.segment<3>(3)-l._bb.segment<3>(0)).prod()==0) {
    std::cout << "Failed creating index grid by all " << nrTrial << " attempts!" << std::endl;
    return false;
  }
  //reflect
  r._res=l._res;
  for(const std::pair<const Eigen::Matrix<int,3,1>,Vec>& id:l._indices)
    r._indices[(id.first.array()*Eigen::Matrix<int,3,1>(1,-1,1).array()).matrix()]=l2r*id.second;
  r._bb=l._bb;
  r._bb[1]=-l._bb[4];
  r._bb[4]=-l._bb[1];
  return true;
}
bool EndEffectorBounds::detectReachableRegion(const ArticulatedBody& body,std::vector<std::shared_ptr<EndEffectorBounds>>& ees,T errThres,int nrTrial,bool containLocalPose,bool centerLocalPose,int mask) {
  Eigen::Matrix<int,3,1> maskVec((mask&1)?1:0,(mask&2)?1:0,(mask&4)?1:0);
  std::cout << "detectReachableRegion: " << "containLocalPose=" << containLocalPose << " centerLocalPose=" << centerLocalPose << " maskVec=" << maskVec.transpose() << std::endl;
  if(ees.size()==2)
    return detectReachableRegion(body,*findEE(ees,EndEffectorBounds::LEFT),*findEE(ees,EndEffectorBounds::RIGHT),errThres,nrTrial,containLocalPose,centerLocalPose,maskVec);
  else {
    bool ret=true;
    ret=ret&&detectReachableRegion(body,*findEE(ees,EndEffectorBounds::LEFT|EndEffectorBounds::FRONT),*findEE(ees,EndEffectorBounds::RIGHT|EndEffectorBounds::FRONT),errThres,nrTrial,containLocalPose,centerLocalPose,maskVec);
    ret=ret&&detectReachableRegion(body,*findEE(ees,EndEffectorBounds::LEFT|EndEffectorBounds::BACK),*findEE(ees,EndEffectorBounds::RIGHT|EndEffectorBounds::BACK),errThres,nrTrial,containLocalPose,centerLocalPose,maskVec);
    return ret;
  }
}
bool EndEffectorBounds::betterThan(const Eigen::Matrix<int,6,1>& A,const Eigen::Matrix<int,6,1>& B,const Vec3T* mustContain,const Vec3T* mustCenter,const Eigen::Matrix<int,3,1>& maskVec) {
  //contain
  if(mustContain) {
    if((mustContain->array()<A.segment<3>(0).array().template cast<T>()).any())
      return false;
    if((mustContain->array()>A.segment<3>(3).array().template cast<T>()).any())
      return false;
  }
  //center
  if(mustCenter) {
    T ARadius=std::min<T>((mustCenter->segment<2>(0)-A.segment<2>(0).template cast<T>()).minCoeff(),(A.segment<2>(3).template cast<T>()-mustCenter->segment<2>(0)).minCoeff());
    T BRadius=std::min<T>((mustCenter->segment<2>(0)-B.segment<2>(0).template cast<T>()).minCoeff(),(B.segment<2>(3).template cast<T>()-mustCenter->segment<2>(0)).minCoeff());
    if(ARadius>BRadius)
      return true;
    if(ARadius<BRadius)
      return false;
  }
  //size
  Eigen::Matrix<int,3,1> a=A.segment<3>(3)-A.segment<3>(0);
  Eigen::Matrix<int,3,1> b=B.segment<3>(3)-B.segment<3>(0);
  for(int d=0; d<3; d++)
    if(maskVec[d])
      a[d]=b[d]=1;
  if(a.prod()>b.prod())
    return true;
  else if(a.prod()<b.prod())
    return false;
  //regularity
  if(irregularity(a)<irregularity(b))
    return true;
  else if(irregularity(a)>irregularity(b))
    return false;
  return false;
}
int EndEffectorBounds::irregularity(const Eigen::Matrix<int,3,1>& ext) {
  return std::max(std::abs(ext[0]-ext[1]),std::max(std::abs(ext[0]-ext[2]),std::abs(ext[1]-ext[2])));
}
}
