#ifndef ZMP_PLANNER_H
#define ZMP_PLANNER_H

#include "PlannerBase.h"
#include "FootStepPlanner.h"
#include <Optimizer/LaplaceObjective.h>
#include <Optimizer/FootStepSequence.h>
#include <Optimizer/LIPDynamicsSequence.h>

namespace PHYSICSMOTION {
/*@article{winkler2017fast,
  title={Fast trajectory optimization for legged robots using vertex-based zmp constraints},
  author={Winkler, Alexander W and Farshidian, Farbod and Pardo, Diego and Neunert, Michael and Buchli, Jonas},
  journal={IEEE Robotics and Automation Letters},
  volume={2},
  number={4},
  pages={2201--2208},
  year={2017},
  publisher={IEEE}
}*/
template <typename T>
class ZMPPlanner : public PlannerBase<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef typename SQPObjectiveComponent<T>::PolyXT PolyXT;
  typedef typename PolyXT::VECP VECP;
  using PlannerBase<T>::_pt;
  using PlannerBase<T>::_env;
  using PlannerBase<T>::_body;
  using PlannerBase<T>::_bodyVis;
  using PlannerBase<T>::_bodySimplified;
  using PlannerBase<T>::_ees;
  using PlannerBase<T>::_xPoses;
  using PlannerBase<T>::g;
  using PlannerBase<T>::dt;
  using PlannerBase<T>::bodyHeightCoef;
  using PlannerBase<T>::bodyHeightTarget;
  using PlannerBase<T>::useIPOPT;
  using PlannerBase<T>::solveKnitro;
  using PlannerBase<T>::solveIpopt;
  using PlannerBase<T>::buildSimplifiedBody;
  ZMPPlanner();
  ZMPPlanner(std::shared_ptr<Environment<T>> env,
             const std::string& path,
             T normalize);
  ZMPPlanner(std::shared_ptr<Environment<T>> env,
             std::shared_ptr<ArticulatedBody> body,
             std::shared_ptr<ArticulatedBody> bodyVis,
             const std::vector<std::shared_ptr<EndEffectorBounds>>& ees);
  void update(const std::vector<std::shared_ptr<FootStep<T>>>& steps);
  std::vector<std::shared_ptr<FootStep<T>>> stepsSmartPtr() const;
  const std::vector<FootStep<T>>& steps() const;
  void clearProblem();
  void buildProblem();
  bool plan();
  //property
  void singleHorizon(int N);
  int singleHorizon() const;
  void doubleHorizon(int N);
  int doubleHorizon() const;
  void lambdaCoef(T coef);
  T lambdaCoef() const;
  void laplaceCoef(T coef);
  T laplaceCoef() const;
  void footStepCoef(T coef);
  T footStepCoef() const;
  void splineXY(int spline);
  int splineXY() const;
  void splineZT(int spline);
  int splineZT() const;
  void isBoundingCircle(bool circle);
  bool isBoundingCircle() const;
 protected:
  //problem build
  SQPObjectiveCompound<T> _obj;
  std::shared_ptr<LaplaceObjective<T,2>> _smoothBodyXY;
  std::shared_ptr<LaplaceObjective<T,2>> _smoothBodyZT;
  std::shared_ptr<LIPDynamicsSequence<T>> _LIPDynamics;
  std::vector<std::shared_ptr<LaplaceObjective<T,2>>> _smoothFoot;
  std::vector<std::shared_ptr<FootStepSequence<T>>> _footsteps;
  //solver data
  std::vector<FootStep<T>> _steps;
  Vec _x,_fvec;
};
}

#endif
