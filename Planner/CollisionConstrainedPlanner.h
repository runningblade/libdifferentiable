//
// Created by dylanmac on 3/20/24.
//

#ifndef COLLISION_CONSTRAINED_PLANNER_H
#define COLLISION_CONSTRAINED_PLANNER_H
#include <SIPCollision/ThetaTrajectory.h>
#include <Articulated/ArticulatedBody.h>
#include <Utils/SparseUtils.h>
#include <Utils/Pragma.h>
#include <math.h>

namespace PHYSICSMOTION {
 template<typename T>
 class CollisionConstrainedPlanner: public SerializableBase{
 public:
     DECL_MAT_VEC_MAP_TYPES_T
     //obstacle setup
     virtual void addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF)=0;
     virtual void addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot, int maxConvexHulls)=0;
     virtual void addSphere(T r,int res,const Vec3T& pos)=0;
     virtual void addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& rot)=0;
     virtual void addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res)=0;
     virtual void addCapsule(T l,T w,T h,Vec3T pos,T radius,int res)=0;
     virtual void assembleObsBVH()=0;
     virtual std::vector<std::shared_ptr<MeshExact>> getObstacles() const=0;
     //utilities
     virtual void clearEnergy()=0;
     virtual void initializeTheta(const Vec& theta)=0;
     virtual const ThetaTrajectory<T>& getThetaTrajectory() const=0;
     virtual std::shared_ptr<ArticulatedBody> getBody() const=0;
     virtual void setControlPoints(const Vec& newCP)=0;
     virtual const Vec& getControlPoints() const=0;
 };
}

#endif
