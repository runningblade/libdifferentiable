#include "PlannerBase.h"
#include <Articulated/ArticulatedUtils.h>
#include <Articulated/ArticulatedLoader.h>
#include <Articulated/NEArticulatedGradientInfo.h>
#include <Optimizer/KNInterface.h>
#include <Optimizer/IPOPTInterface.h>
#include <Optimizer/GUROBIInterface.h>
#include <Utils/RotationUtils.h>
#include <Utils/Interp.h>
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
template <typename T>
FootStep<T> FootStep<T>::random(int nrFoot,bool isEven) {
  FootStep<T> ret;
  ret._bodyPose.setRandom();
  ret._eePose.setRandom(3,nrFoot);
  ret._isEven=isEven;
  return ret;
}
ArticulatedBody readUnified(const std::string& file,bool convex) {
  if(file=="Spider.urdf") {
    ArticulatedBody body=ArticulatedLoader::createSpider(0);
    ROT(body.joint(0)._trans)=expWGradV<ArticulatedBody::T,ArticulatedBody::Vec3T>(ArticulatedBody::Vec3T::UnitX()*M_PI/2);
    return body;
  } else if(file=="Bird.urdf") {
    ArticulatedBody body=ArticulatedLoader::createBird(0);
    ROT(body.joint(0)._trans)=expWGradV<ArticulatedBody::T,ArticulatedBody::Vec3T>(ArticulatedBody::Vec3T::UnitX()*M_PI/2);
    return body;
  } else if(file=="Bipedal.urdf") {
    ArticulatedBody body=ArticulatedLoader::createBipedal(0,false);
    ROT(body.joint(0)._trans)=expWGradV<ArticulatedBody::T,ArticulatedBody::Vec3T>(ArticulatedBody::Vec3T::UnitX()*M_PI/2);
    return body;
  } else return ArticulatedLoader::readURDF(file,convex,true);
}
template <typename T>
PlannerBase<T>::PlannerBase() {
  _pt.InsertEndChild(_pt.NewElement("root"));
}
template <typename T>
PlannerBase<T>::PlannerBase(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize):_env(env) {
  _pt.InsertEndChild(_pt.NewElement("root"));
  //body
  _body.reset(new ArticulatedBody);
  *_body=readUnified(path,true);
  BBoxExact bb=_body->getBB(ArticulatedBody::Vec(ArticulatedBody::Vec::Zero(_body->nrDOF())));
  T coef=normalize/(T)(bb.maxCorner()-bb.minCorner()).maxCoeff();
  Mat3X4T trans;
  if(normalize>0) {
    Mat3X4T trans0=transformFromSourceFile();
    trans=ArticulatedUtils(*_body).scaleBody((ArticulatedBody::T)coef).template cast<T>();
    APPLY_TRANS(trans0,trans,trans0)
    transformFromSourceFile(trans0);
  }
  {
    Mat3X4T trans0=transformFromSourceFile();
    trans=ArticulatedUtils(*_body).transformTorsoToCOM().template cast<T>();
    APPLY_TRANS(trans0,trans,trans0)
    transformFromSourceFile(trans0);
  }
  ArticulatedUtils(*_body).simplify([&](int,const Joint& J) {
    return J._name.find("imu")!=std::string::npos || J._typeJoint==Joint::FIX_JOINT;
  },ArticulatedBody::Vec::Zero(_body->nrDOF()),10);
  ArticulatedUtils(*_body).addBase(3,ArticulatedUtils::Vec3T::Zero());
  _body->simplify(10);
  //bodyVis
  _bodyVis.reset(new ArticulatedBody);
  *_bodyVis=readUnified(path,false);
  if(normalize>0)
    ArticulatedUtils(*_bodyVis).scaleBody((ArticulatedBody::T)coef);
  ArticulatedUtils(*_bodyVis).transformTorso(ArticulatedBody::Mat3X4T(trans.template cast<ArticulatedBody::T>()));
  ArticulatedUtils(*_bodyVis).simplify([&](int,const Joint& J) {
    return J._name.find("imu")!=std::string::npos || J._typeJoint==Joint::FIX_JOINT;
  },ArticulatedBody::Vec::Zero(_bodyVis->nrDOF()),10);
  ArticulatedUtils(*_bodyVis).addBase(3,ArticulatedUtils::Vec3T::Zero());
  _bodyVis->simplify(10);
}
template <typename T>
PlannerBase<T>::PlannerBase(std::shared_ptr<Environment<T>> env,std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodyVis,const std::vector<std::shared_ptr<EndEffectorBounds>>& ees):_env(env),_body(body),_bodyVis(bodyVis),_ees(ees) {
  _pt.InsertEndChild(_pt.NewElement("root"));
}
template <typename T>
PlannerBase<T>::~PlannerBase() {}
template <typename T>
bool PlannerBase<T>::read(std::istream& is,IOData* dat) {
  std::string pt;
  readBinaryData(pt,is);
  _pt.Clear();
  _pt.Parse(pt.c_str());
  registerType<EnvironmentExact<T>>(dat);
  registerType<EnvironmentHeight<T>>(dat);
  registerType<ArticulatedBody>(dat);
  registerType<EndEffectorBounds>(dat);
  readBinaryData(_env,is,dat);
  readBinaryData(_body,is,dat);
  readBinaryData(_bodyVis,is,dat);
  readBinaryData(_ees,is,dat);
  return is.good();
}
template <typename T>
bool PlannerBase<T>::write(std::ostream& os,IOData* dat) const {
  tinyxml2::XMLPrinter pt;
  _pt.Print(&pt);
  writeBinaryData(std::string(pt.CStr()),os);
  registerType<EnvironmentExact<T>>(dat);
  registerType<EnvironmentHeight<T>>(dat);
  registerType<ArticulatedBody>(dat);
  registerType<EndEffectorBounds>(dat);
  writeBinaryData(_env,os,dat);
  writeBinaryData(_body,os,dat);
  writeBinaryData(_bodyVis,os,dat);
  writeBinaryData(_ees,os,dat);
  return os.good();
}
template <typename T>
std::string PlannerBase<T>::type() const {
  return typeid(PlannerBase<T>).name();
}
template <typename T>
void PlannerBase<T>::applyEdit(const typename ArticulatedBody::Vec& x,const std::vector<char>& excluded,const std::vector<char>& markFoot) {
  typename ArticulatedBody::Vec xNew;
  _ees.clear();
  std::unordered_set<std::string> excludedNames,markFootNames;
  for(int i=0; i<(int)excluded.size(); i++)
    if(excluded[i])
      excludedNames.insert(_body->joint(i)._name);
  for(int i=0; i<(int)markFoot.size(); i++)
    if(markFoot[i])
      markFootNames.insert(_body->joint(i)._name);
  //apply to body
  xNew=x;
  for(int j=0; j<_body->nrJ(); j++) {
    int p=_body->joint(j)._parent;
    if(p>=0 && markFootNames.find(_body->joint(p)._name)!=markFootNames.end())
      xNew=ArticulatedUtils(*_body).mergeChildren(j,xNew);
  }
  xNew=ArticulatedUtils(*_body).eliminate([&](int jid,const Joint& J) {
    for(; jid>=0; jid=_body->joint(jid)._parent)
      if(excludedNames.find(_body->joint(jid)._name)!=excludedNames.end())
        return true;
    return J._typeJoint==Joint::FIX_JOINT;
  },xNew);
  xNew=ArticulatedUtils(*_body).simplify([&](int jid,const Joint&) {
    if(markFootNames.find(_body->joint(jid)._name)!=markFootNames.end())
      return false;
    int p=_body->joint(jid)._parent,pp=-1;
    if(p>=0) {
      if(markFootNames.find(_body->joint(p)._name)!=markFootNames.end())
        return false;
      pp=_body->joint(p)._parent;
      if(pp>=0 && markFootNames.find(_body->joint(pp)._name)!=markFootNames.end())
        return false;
    }
    return true;
  },xNew,10);
  xNew=ArticulatedUtils(*_body).fix([&](int,const Joint& J) {
    return J.isRoot(*_body);
  },xNew);
  ArticulatedUtils(*_body).addBase(3,ArticulatedUtils::Vec3T::Zero());
  ArticulatedUtils(*_body).makeConvex();
  _body->simplify(10);
  //apply to bodyVis
  xNew=x;
  for(int j=0; j<_bodyVis->nrJ(); j++) {
    int p=_bodyVis->joint(j)._parent;
    if(p>=0 && markFootNames.find(_bodyVis->joint(p)._name)!=markFootNames.end())
      xNew=ArticulatedUtils(*_bodyVis).mergeChildren(j,xNew);
  }
  xNew=ArticulatedUtils(*_bodyVis).eliminate([&](int jid,const Joint& J) {
    for(; jid>=0; jid=_bodyVis->joint(jid)._parent)
      if(excludedNames.find(_bodyVis->joint(jid)._name)!=excludedNames.end())
        return true;
    return J._typeJoint==Joint::FIX_JOINT;
  },xNew);
  xNew=ArticulatedUtils(*_bodyVis).simplify([&](int jid,const Joint&) {
    if(markFootNames.find(_bodyVis->joint(jid)._name)!=markFootNames.end())
      return false;
    int p=_bodyVis->joint(jid)._parent,pp=-1;
    if(p>=0) {
      if(markFootNames.find(_bodyVis->joint(p)._name)!=markFootNames.end())
        return false;
      pp=_bodyVis->joint(p)._parent;
      if(pp>=0 && markFootNames.find(_bodyVis->joint(pp)._name)!=markFootNames.end())
        return false;
    }
    return true;
  },xNew,10);
  xNew=ArticulatedUtils(*_bodyVis).fix([&](int,const Joint& J) {
    return J.isRoot(*_bodyVis);
  },xNew);
  ArticulatedUtils(*_bodyVis).addBase(3,ArticulatedUtils::Vec3T::Zero());
  _bodyVis->simplify(10);
}
template <typename T>
void PlannerBase<T>::detectEndEffector(const std::set<std::string>& fixNames,T errThresEE) {
  _ees.clear();
  if(!EndEffectorBounds::detectEndEffector(*_body,_ees,fixNames)) {
    _ees.clear();
    return;
  }
  EndEffectorBounds::detectEndEffectorPose(*_body,_ees,NULL,(ArticulatedBody::T)errThresEE).template cast<T>();
}
template <typename T>
void PlannerBase<T>::computeReachableRegion(bool containLocalPos,bool centerLocalPos,int mask,T errThresReachable) {
  if(_ees.empty())
    return;
  if(!EndEffectorBounds::detectReachableRegion(*_body,_ees,(ArticulatedBody::T)errThresReachable,5,containLocalPos,centerLocalPos,mask)) {
    _ees.clear();
    return;
  }
}
template <typename T>
void PlannerBase<T>::replaceJoint(const typename ArticulatedBody::Vec& x,int jid,const Mat3XT& axes) {
  _ees.clear();
  //apply to body
  ArticulatedUtils(*_body).replaceJoint(x,jid,axes.template cast<ArticulatedBody::T>());
  _body->simplify(10);
  //apply to bodyVis
  ArticulatedUtils(*_bodyVis).replaceJoint(x,jid,axes.template cast<ArticulatedBody::T>());
  _bodyVis->simplify(10);
}
template <typename T>
void PlannerBase<T>::alignDirectionZ() {
  if(_ees.empty())
    return;
  ArticulatedBody::Vec3T currZ=_ees[0]->_frame.col(2);
  if(currZ!=ArticulatedBody::Vec3T::UnitZ()) {
    Mat3X4T trans=Mat3X4T::Identity();
    ROT(trans)=ArticulatedBody::QuatT::FromTwoVectors(currZ,ArticulatedBody::Vec3T::UnitZ()).toRotationMatrix().template cast<T>();
    //apply to body
    Mat3X4T trans0=transformFromSourceFile();
    ArticulatedUtils(*_body).transformTorso(ArticulatedBody::Mat3X4T(trans.template cast<ArticulatedBody::T>()));
    APPLY_TRANS(trans0,trans,trans0)
    transformFromSourceFile(trans0);
    //apply to bodyVis
    ArticulatedUtils(*_bodyVis).transformTorso(ArticulatedBody::Mat3X4T(trans.template cast<ArticulatedBody::T>()));
    _ees.clear();
  }
}
template <typename T>
void PlannerBase<T>::flipLeftRight() {
  for(std::shared_ptr<EndEffectorBounds>& EE:_ees)
    EE->flipLeftRight();
}
template <typename T>
void PlannerBase<T>::normalize(T normalize) {
  _ees.clear();
  ASSERT(normalize>0)
  BBoxExact bb=_body->getBB(ArticulatedBody::Vec(ArticulatedBody::Vec::Zero(_body->nrDOF())));
  T coef=normalize/(T)(bb.maxCorner()-bb.minCorner()).maxCoeff();
  //apply to body
  Mat3X4T trans0=transformFromSourceFile();
  Mat3X4T trans=ArticulatedUtils(*_body).scaleBody((ArticulatedBody::T)coef).template cast<T>();
  APPLY_TRANS(trans0,trans,trans0)
  transformFromSourceFile(trans0);
  //apply to bodyVis
  ArticulatedUtils(*_bodyVis).scaleBody((ArticulatedBody::T)coef);
}
template <typename T>
std::vector<std::shared_ptr<EndEffectorBounds>>& PlannerBase<T>::getEEs() {
  return _ees;
}
template <typename T>
const std::vector<std::shared_ptr<EndEffectorBounds>>& PlannerBase<T>::getEEs() const {
  return _ees;
}
template <typename T>
std::shared_ptr<Environment<T>>& PlannerBase<T>::getEnv() {
  return _env;
}
template <typename T>
void PlannerBase<T>::setEnv(std::shared_ptr<Environment<T>> env) {
  _env=env;
}
template <typename T>
const std::shared_ptr<Environment<T>>& PlannerBase<T>::getEnv() const {
  return _env;
}
template <typename T>
std::shared_ptr<ArticulatedBody> PlannerBase<T>::getBodyVis() const {
  return _bodyVis?_bodyVis:_body;
}
template <typename T>
std::shared_ptr<ArticulatedBody>& PlannerBase<T>::getBodyVis() {
  return _bodyVis?_bodyVis:_body;
}
template <typename T>
std::shared_ptr<ArticulatedBody> PlannerBase<T>::getBody() const {
  return _body;
}
template <typename T>
std::shared_ptr<ArticulatedBody>& PlannerBase<T>::getBody() {
  return _body;
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::getPose(T t,bool& warp) const {
  if(_xPoses.cols()==1)
    return _xPoses;
  t/=dtPlayback();
  if(warp) {
    warp=false;
    while(t<0) {
      t+=_xPoses.cols()-1;
      warp=true;
    }
    while(t>=_xPoses.cols()-1) {
      t-=_xPoses.cols()-1;
      warp=true;
    }
  } else t=std::min<T>(std::max<T>(t,0),_xPoses.cols()-2);
  int id=(int)floor(t);
  ASSERT(id<_xPoses.cols()-1)
  return interp1D<Vec,T>(_xPoses.col(id),_xPoses.col(id+1),t-id);
}
template <typename T>
void PlannerBase<T>::setPoses(const MatT& poses) {
  _xPoses=poses;
}
template <typename T>
const typename PlannerBase<T>::MatT& PlannerBase<T>::getPoses() const {
  return _xPoses;
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::getFirstPose() const {
  return _xPoses.col(0);
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::getLastPose() const {
  return _xPoses.col(_xPoses.cols()-1);
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::stepToDOF(const FootStep<T>& s) const {
  const_cast<PlannerBase<T>*>(this)->buildSimplifiedBody();
  Vec DOF=Vec::Zero(_body->nrDOF());
  DOF.template segment<6>(0)=s._bodyPose;
  NEArticulatedGradientInfo<T> info(*_bodySimplified,s._bodyPose);
  Mat3X4T TJ=info.NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1);
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    Vec3T eeLocal=ROT(TJ).transpose()*(s._eePose.col(eid)-CTR(TJ));
    Vec3T nor=ROT(TJ).transpose()*_env->phiGrad(s._eePose.col(eid));
    DOF+=_ees[eid]->getDOFAll(*_body,eeLocal.template cast<ArticulatedBody::T>(),nor.template cast<ArticulatedBody::T>()).template cast<T>();
  }
  return DOF;
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::stepToDOF(std::shared_ptr<FootStep<T>> s) const {
  return stepToDOF(*s);
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::stepToDOF(const FootStep<T>& s0,const FootStep<T>& s1,T alpha) const {
  FootStep<T> step;
  step._bodyPose=interp1D(s0._bodyPose,s1._bodyPose,alpha);
  step._eePose=interp1D(s0._eePose,s1._eePose,alpha);
  step._isEven=s0._isEven;
  return stepToDOF(step);
}
template <typename T>
typename PlannerBase<T>::Vec PlannerBase<T>::stepToDOF(std::shared_ptr<FootStep<T>> s0,std::shared_ptr<FootStep<T>> s1,T alpha) const {
  return stepToDOF(*s0,*s1,alpha);
}
template <typename T>
std::shared_ptr<FootStep<T>> PlannerBase<T>::DOFToStep(const Vec& DOF,bool isEven) const {
  std::shared_ptr<FootStep<T>> step(new FootStep<T>);
  NEArticulatedGradientInfo<T> info(*_body,DOF);
  step->_bodyPose=DOF.template segment<6>(0);
  step->_eePose.resize(3,(int)_ees.size());
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    std::shared_ptr<EndEffectorBounds> ee=_ees[eid];
    Mat3X4T t=info.NEArticulatedGradientInfoMap<T>::getTrans(ee->jointId());
    step->_eePose.col(eid)=ROT(t)*ee->_localPos.template cast<T>()+CTR(t);
  }
  step->_isEven=isEven;
  return step;
}
//property
template <typename T>
const tinyxml2::XMLDocument& PlannerBase<T>::getPt() const {
  return _pt;
}
template <typename T>
void PlannerBase<T>::parsePtree(int argc,char** argv) {
  parseProps(argc,argv,_pt);
}
template <typename T>
void PlannerBase<T>::mu(T mu) {
  put<T>(_pt,"mu",mu);
}
template <typename T>
T PlannerBase<T>::mu() const {
  return get<T>(_pt,"mu",0.75f);
}
template <typename T>
void PlannerBase<T>::dt(T dt) {
  put<T>(_pt,"dt",dt);
}
template <typename T>
T PlannerBase<T>::dt() const {
  return get<T>(_pt,"dt",0.05f);
}
template <typename T>
void PlannerBase<T>::dtPlayback(T dt) {
  put<T>(_pt,"dtPlayback",dt);
}
template <typename T>
T PlannerBase<T>::dtPlayback() const {
  return get<T>(_pt,"dtPlayback",dt());
}
template <typename T>
void PlannerBase<T>::g(const Vec3T& g) {
  putPtree<Vec3T>(_pt,"gravity",g);
}
template <typename T>
typename PlannerBase<T>::Vec3T PlannerBase<T>::g() const {
  return parsePtreeDef<Vec3T>(_pt,"gravity",Vec3T(0,0,-9.81f));
}
template <typename T>
void PlannerBase<T>::bodyHeightCoef(T hCoef) {
  put<T>(_pt,"bodyHeightCoef",hCoef);
}
template <typename T>
T PlannerBase<T>::bodyHeightCoef() const {
  return get<T>(_pt,"bodyHeightCoef",.01);
}
template <typename T>
void PlannerBase<T>::bodyHeightTarget(T hTarget) {
  put<T>(_pt,"bodyHeightTarget",hTarget);
}
template <typename T>
T PlannerBase<T>::bodyHeightTarget() const {
  try {
    return get<T>(_pt,"bodyHeightTarget");
  } catch(...) {
    //find default body height
    T bodyZ=0;
    NEArticulatedGradientInfo<T> info(*_body,Vec::Zero(_body->nrDOF()));
    for(int i=0; i<_body->nrJ(); i++)
      if(_body->joint(i)._M>0) {
        bodyZ=CTR(info.NEArticulatedGradientInfoMap<T>::getTrans(i))[2];
        break;
      }
    //find default EE height
    T eeZ=0;
    for(int eid=0; eid<(int)_ees.size(); eid++) {
      Mat3X4T TJ=info.NEArticulatedGradientInfoMap<T>::getTrans(_ees[eid]->jointId());
      Vec3T pos=ROT(TJ)*_ees[eid]->_localPos.template cast<T>()+CTR(TJ);
      eeZ+=pos[2];
    }
    eeZ/=(int)_ees.size();
    return bodyZ-eeZ;
  }
}
template <typename T>
void PlannerBase<T>::callback(int callback) {
  put<int>(_pt,"callback",callback);
}
template <typename T>
int PlannerBase<T>::callback() const {
  return get<int>(_pt,"callback",5);
}
template <typename T>
void PlannerBase<T>::ftol(T ftol) {
  put<T>(_pt,"ftol",ftol);
}
template <typename T>
T PlannerBase<T>::ftol() const {
  return get<T>(_pt,"ftol",1E-3f);
}
template <typename T>
void PlannerBase<T>::xtol(T xtol) {
  put<T>(_pt,"xtol",xtol);
}
template <typename T>
T PlannerBase<T>::xtol() const {
  return get<T>(_pt,"xtol",1E-3f);
}
template <typename T>
void PlannerBase<T>::tol(T tol) {
  put<T>(_pt,"tol",tol);
}
template <typename T>
T PlannerBase<T>::tol() const {
  return get<T>(_pt,"tol",1E-3f);
}
template <typename T>
void PlannerBase<T>::useIPOPT(bool useIPOPT) {
  put<bool>(_pt,"useIPOPT",useIPOPT);
}
template <typename T>
bool PlannerBase<T>::useIPOPT() const {
  return get<bool>(_pt,"useIPOPT",false);
}
template <typename T>
void PlannerBase<T>::derivCheck(T derivCheck) {
  put<T>(_pt,"derivCheck",derivCheck);
}
template <typename T>
T PlannerBase<T>::derivCheck() const {
  return get<T>(_pt,"derivCheck",0);
}
template <typename T>
void PlannerBase<T>::muInit(T muInit) {
  put<T>(_pt,"muInit",muInit);
}
template <typename T>
T PlannerBase<T>::muInit() const {
  return get<T>(_pt,"muInit",10);
}
template <typename T>
void PlannerBase<T>::maxIter(int maxIter) {
  put<int>(_pt,"maxIter",maxIter);
}
template <typename T>
int PlannerBase<T>::maxIter() const {
  return get<int>(_pt,"maxIter",200);
}
template <typename T>
void PlannerBase<T>::maxNode(int nNode) {
  put<int>(_pt,"maxNode",nNode);
}
template <typename T>
int PlannerBase<T>::maxNode() const {
  return get<int>(_pt,"maxNode",1E6);
}
template <typename T>
void PlannerBase<T>::maxTime(int nTime) {
  put<int>(_pt,"maxTime",nTime);
}
template <typename T>
int PlannerBase<T>::maxTime() const {
  return get<int>(_pt,"maxTime",1E6);
}
template <typename T>
void PlannerBase<T>::resolve(bool resolve) {
  put<bool>(_pt,"resolve",resolve);
}
template <typename T>
bool PlannerBase<T>::resolve() const {
  return get<bool>(_pt,"resolve",true);
}
template <typename T>
void PlannerBase<T>::useDirectOrCG(bool direct) {
  put<bool>(_pt,"useDirectOrCG",direct);
}
template <typename T>
bool PlannerBase<T>::useDirectOrCG() const {
  return get<bool>(_pt,"useDirectOrCG",false);
}
template <typename T>
void PlannerBase<T>::transformFromSourceFile(const Mat3X4T& trans) {
  putPtree<Eigen::Matrix<T,12,1>>(_pt,"transformFromSourceFile",Eigen::Map<const Eigen::Matrix<T,12,1>>(trans.data()));
}
template <typename T>
typename PlannerBase<T>::Mat3X4T PlannerBase<T>::transformFromSourceFile() const {
  Mat3X4T t=Mat3X4T::Identity();
  Eigen::Matrix<T,12,1> ret=parsePtreeDef<Eigen::Matrix<T,12,1>>(_pt,"transformFromSourceFile",Eigen::Map<Eigen::Matrix<T,12,1>>(t.data()));
  return Eigen::Map<Mat3X4T>(ret.data());
}
//helper
template <typename T>
bool PlannerBase<T>::solveKnitro(SQPObjectiveCompound<T>& obj,Vec& x,Vec* fvec) {
#ifdef KNITRO_SUPPORT
  KNInterface<T> solver(obj);
  return solver.optimize(callback()>0,(double)derivCheck(),(double)ftol(),(double)xtol(),useDirectOrCG(),maxIter(),(double)muInit(),x,fvec);
#else
  ASSERT_MSG(false,"Knitro not supported!")
#endif
}
template <typename T>
bool PlannerBase<T>::solveIpopt(SQPObjectiveCompound<T>& obj,Vec& x) {
  bool ret=false;
#ifdef IPOPT_SUPPORT
  ret=IPOPTInterface<T>::optimize(obj,callback(),(double)derivCheck(),(double)xtol(),maxIter(),(double)muInit(),x);
#else
  ASSERT_MSG(false,"IPOPT not supported!")
#endif
  return ret;
}
template <typename T>
bool PlannerBase<T>::solveGurobi(SQPObjectiveCompound<T>& obj,Vec& x) {
  bool ret=false;
#ifdef GUROBI_SUPPORT
  if(!_gurobiSolver)
    _gurobiSolver.reset(new GUROBIInterface<T>());
  ret=_gurobiSolver->optimize(obj,callback(),(double)tol(),maxNode(),maxTime(),x=obj.init());
#else
  ASSERT_MSG(false,"GUROBI not supported!")
#endif
  return ret;
}
template <typename T>
void PlannerBase<T>::buildSimplifiedBody() {
  if(_bodySimplified)
    return;
  _bodySimplified.reset(new ArticulatedBody);
  for(int i=0; i<_body->nrJ(); i++)
    if(_body->joint(i)._M>0) {
      *_bodySimplified=_body->resizeJoints(i+1);
      break;
    }
}
template class FootStep<FLOAT>;
template class PlannerBase<FLOAT>;
}
