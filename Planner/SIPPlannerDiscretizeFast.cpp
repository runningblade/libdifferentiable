#include "SIPPlannerDiscretizeFast.h"
#include <Utils/Timing.h>

namespace PHYSICSMOTION {

template<typename T,typename CCPlaneT>
SIPPlannerDiscretizeFast<T, CCPlaneT>::SIPPlannerDiscretizeFast(T d0,T x0Obs,T x0Self,T L2,T eta,T terminateGrad,T minEig,T colMinEig,
    bool useConvexHullObs,bool useConvexHullSelf, bool schurSolve,
    const std::unordered_map<int, std::unordered_set<int>> &skipSelfCCD)
  :SIPPlannerDiscretize<T, CCPlaneT>(d0,x0Obs,x0Self,L2,eta,terminateGrad,minEig,useConvexHullObs,useConvexHullSelf,skipSelfCCD),_schurSolve(schurSolve),_colMinEig(colMinEig) {}
template<typename T,typename CCPlaneT>
SIPPlannerDiscretizeFast<T, CCPlaneT>::SIPPlannerDiscretizeFast(std::shared_ptr<ArticulatedBody> body, int order,
    int totalTime,T d0,T x0Obs,T x0Self,T L2,T eta,T terminateGrad,T minEig,T colMinEig,
    T randomInitialize, bool useConvexHullObs,
    bool useConvexHullSelf, bool neutralInit,bool schurSolve,
    const std::unordered_map<int, std::unordered_set<int>>& skipSelfCCD)
  :SIPPlannerDiscretize<T, CCPlaneT>(body,order,totalTime,d0,x0Obs,x0Self,L2,eta,terminateGrad,minEig,randomInitialize,useConvexHullObs,
                                     useConvexHullSelf,neutralInit,skipSelfCCD),_schurSolve(schurSolve),_colMinEig(colMinEig) {}
template<typename T,typename CCPlaneT>
std::string SIPPlannerDiscretizeFast<T,CCPlaneT>::type() const {
  return typeid(SIPPlannerDiscretizeFast).name();
}
template<typename T,typename CCPlaneT>
std::shared_ptr<SerializableBase> SIPPlannerDiscretizeFast<T, CCPlaneT>::copy() const {
  return std::shared_ptr<SerializableBase>(new SIPPlannerDiscretizeFast(
           this->handlerObs()->d0(),this->handlerObs()->x0(),this->handlerSelf()->x0(),this->handlerObs()->l2(),this->handlerObs()->eta(),
           this->terminateGrad(),this->minEig(),_colMinEig,this->useConvexHullObs(),this->useConvexHullSelf(),_schurSolve,this->handlerSelf()->getSkipJIDPairs()
         ));
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::setupEnergy(T smoothnessCoef,T obsBarrierCoef,T selfBarrierCoef,T limitCoef,bool implicit,bool JTJApprox) {
  if(obsBarrierCoef>0)
    this->trajEss().push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                              (new CollisionBarrierEnergyFast<T,Barrier>(*(this->handlerObs()->getBody()),
                                  this->handlerObs()->getControlPoints(),
                                  this->handlerObs()->getThetaTrajectory(),
                                  this-> handlerObs()->getObsTTPairs(),
                                  this->handlerObs()->getObsCCPlanes(),
                                  this->handlerObs()->getInfoLookup(),
                                  this->handlerObs()->d0(),
                                  this->handlerObs()->x0(),
                                  obsBarrierCoef,_colMinEig,
                                  JTJApprox,false)));
  if(selfBarrierCoef>0)
    this->trajEss().push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                              (new CollisionBarrierEnergyFast<T,Barrier>(*(this->handlerObs()->getBody()),
                                  this->handlerSelf()->getControlPoints(),
                                  this->handlerSelf()->getThetaTrajectory(),
                                  this->handlerSelf()->getSelfTTPairs(),
                                  this->handlerSelf()->getSelfCCPlanes(),
                                  this->handlerSelf()->getInfoLookup(),
                                  this->handlerSelf()->d0(),
                                  this->handlerSelf()->x0(),
                                  selfBarrierCoef,_colMinEig,
                                  JTJApprox,false)));
  if(smoothnessCoef>0)
    this->trajEss().push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                              (new SmoothnessEnergy<T>(*(this->handlerObs()->getBody()),
                                  this->handlerObs()->getControlPoints(),
                                  this->handlerObs()->getThetaTrajectory(),smoothnessCoef,1)));
  if(limitCoef>0)
    this->trajEss().push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                              (new JointLimitEnergy<T>(*(this->handlerObs()->getBody()),
                                  this->handlerObs()->getControlPoints(),
                                  this->handlerObs()->getThetaTrajectory(),limitCoef)));
}
template<typename T,typename CCPlaneT>
LineSearchState SIPPlannerDiscretizeFast<T, CCPlaneT>::computeNewX(T& E,const Vec& Gcp,const MatT& Hcp,Vec& d,Vec& newXBody,T& alpha,bool newtonMethod,bool debug) {
  Vec G;
  MatT H;
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e)e->resetCCPlanesToUpdate();
  }
  solveSearchDirection(Gcp,Hcp,G,H,d,newtonMethod);
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e)e->activateCCPlanesNewX();
  }
  LineSearchState state=this->lineSearch(E,G,d,alpha,newXBody,debug);
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e)e->deactivateCCPlanesNewX();
  }
  return state;
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::solveSearchDirection(const Vec& Gcp,const MatT& Hcp,Vec& G,MatT& H,Vec &d,bool newtonMethod) {
  if(newtonMethod) {
    if(!_schurSolve) {
      assembleFullGH(Gcp,Hcp,&G,&H);
      Vec GKKT, d_KKT;
      MatT HKKT;
      assembleKKTGH(G,H,GKKT,HKKT);
      safePDSolveSearchDirection(GKKT,HKKT,d_KKT);
      d=d_KKT.segment(0,G.size());
    } else {
      Vec GSchur;
      MatT HSchur;
      TBEG("Compute SchurGH");
      computeSchurGH(Gcp,Hcp,GSchur,HSchur);
      TEND();
      assembleFullGH(Gcp,Hcp,&G);
      schurSolveSearchDirection(GSchur,HSchur,d);
    }
  } else {
    assembleFullGH(Gcp,Hcp,&G);
    Vec projG;
    assembleProjectedG(Gcp,projG);
    d=-projG;
  }
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::computeSchurGH(const Vec& G,const MatT& H,Vec& GSchur,MatT& HSchur) {
  GSchur=G;
  HSchur=H;
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) e->computeSchurGH(GSchur, HSchur,this->fixVar());
  }
}
template<typename T,typename CCPlaneT>
int SIPPlannerDiscretizeFast<T, CCPlaneT>::numCCPlanesToUpdate() {
  int ans=0;
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) ans+=e->numCCPlanesToUpdate();
  }
  return ans;
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::schurSolveSearchDirection(const Vec& GSchur,const MatT& HSchur,Vec& d) {
  Vec dcp;
  TBEG("Schur solve dcp");
  SIPPlannerDiscretize<T, CCPlaneT>::solveSearchDirection(GSchur,HSchur,dcp,true);
  TEND();
  Vec dpx;
  dpx.setZero(4*numCCPlanesToUpdate());
  int start_pos=0;
  TBEG("Schur solve dpx");
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->schurSolveSearchDirection(dcp,dpx.segment(start_pos,4*e->numCCPlanesToUpdate()));
      start_pos+=4*e->numCCPlanesToUpdate();
    }
  }
  TEND();
  d.resize(dcp.size()+dpx.size());
  d<<dcp,dpx;
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::assembleProjectedG(const Vec& Gcp,Vec& projG) {
  projG.setZero(Gcp.size()+4*numCCPlanesToUpdate());
  projG.segment(0,Gcp.size())=Gcp;
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  int start_pos=Gcp.size();
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->assembleGx(projG.segment(start_pos,4*e->numCCPlanesToUpdate()),true);
      start_pos+=4*e->numCCPlanesToUpdate();
    }
  }
}
template<typename T,typename CCPlaneT>
bool SIPPlannerDiscretizeFast<T, CCPlaneT>::isZeroGrad(const Vec& Gcp) {
  Vec projG;
  assembleProjectedG(Gcp, projG);
  std::cout<<"projGNorm="<<projG.cwiseAbs().maxCoeff()<<std::endl;
  return projG.cwiseAbs().maxCoeff()<this->terminateGrad();
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::assembleKKTGH(const Vec& G,const MatT& H,Vec& GKKT,MatT& HKKT) {
  GKKT.setZero(G.size()+numCCPlanesToUpdate());
  GKKT.segment(0,G.size())=G;
  HKKT.setZero(GKKT.size(),GKKT.size());
  HKKT.block(0,0,G.size(),G.size())=H;
  int start_pos1=G.size();
  int start_pos2=G.size()-4*(GKKT.size()-G.size());
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->assembleHConstraints(HKKT.block(start_pos1, start_pos2,e->numCCPlanesToUpdate(),4*e->numCCPlanesToUpdate()),
                              HKKT.block(start_pos2, start_pos1,4*e->numCCPlanesToUpdate(),e->numCCPlanesToUpdate()));
      start_pos1+=e->numCCPlanesToUpdate();
      start_pos2+=4*e->numCCPlanesToUpdate();
    }
  }
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::assembleFullGH(const Vec& Gcp,const MatT& Hcp,Vec* G,MatT* H) {
  //initialize
  int num_planes=numCCPlanesToUpdate();
  G->resize(Gcp.size()+4*num_planes);
  G->segment(0,Gcp.size())=Gcp;
  if(H) {
    H->setZero(Hcp.rows()+4*num_planes, Hcp.cols()+4*num_planes);
    H->block(0,0,Hcp.rows(), Hcp.cols())=Hcp;
  }
  //assemble
  int G_start_pos=Gcp.size();
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->assembleGx(G->segment(G_start_pos,4*e->numCCPlanesToUpdate()),false);
      if(H) {
        e->makePlaneHxxPD();
        e->assembleHxx(H->block(G_start_pos,G_start_pos,4*e->numCCPlanesToUpdate(),4*e->numCCPlanesToUpdate()));
        e->assembleHcpxHxcp(H->block(0,G_start_pos,Hcp.rows(),4*e->numCCPlanesToUpdate()),
                            H->block(G_start_pos,0,4*e->numCCPlanesToUpdate(),Hcp.cols()),this->fixVar());
      }
      G_start_pos+=4*e->numCCPlanesToUpdate();
    }
  }
}
template<typename T,typename CCPlaneT>
T SIPPlannerDiscretizeFast<T,CCPlaneT>::safePDSolveSearchDirection(const Vec& G,const MatT& H,Vec& d) {
  int cp_size=this->handlerObs()->getControlPoints().size();
  int dof=cp_size+4*numCCPlanesToUpdate();
  this->sol().compute(H.block(0,0,cp_size,cp_size).template cast<double>(),Eigen::ComputeEigenvectors);
  MatT sigma=this->sol().eigenvalues().template cast<T>().asDiagonal();
  MatT H_aug=H;
  int max_itr=100;
  T eps=1e-4;
  for(int i=0; i<max_itr; i++) {
    H_aug.block(0,0,cp_size,cp_size)=this->sol().eigenvectors().template cast<T>()*
                                     (sigma + eps*MatT::Identity(cp_size, cp_size))*
                                     this->sol().eigenvectors().transpose().template cast<T>();
    Eigen::LLT<MatT> pd_info(H_aug.block(0,0,dof,dof));
    if(pd_info.info() != Eigen::NumericalIssue) {
      d=H_aug.fullPivLu().solve(-G);
      return eps;
    }
    /*d=H_aug.fullPivLu().solve(-G);
    if(d.dot(G) < 0) return;*/
    eps*=2;
  }
  for(int i=0; i<numCCPlanesToUpdate(); i++) {
    Eigen::LLT<MatT> pd_info(H.template block<4,4>(cp_size+i*4, cp_size+i*4));
    if(pd_info.info() == Eigen::NumericalIssue) {
      std::cout<<"Hpx"<<i<<" is not p.d."<<std::endl;
      std::cout<<"Hpx"<<i<<"="<<std::setprecision(32)<<H.template block<4,4>(cp_size+i*4, cp_size+i*4)<<std::endl;
      this->sol().compute(H.template block<4,4>(cp_size+i*4, cp_size+i*4).template cast<double>());
      std::cout<<"Eigenvalues are "<<std::setprecision(32)<< this->sol().eigenvalues()<<std::endl;
    }
  }
  ASSERT_MSG(false, "H_aug should be p.d. after finite times of adjusting.")
  return eps;
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::updateInLineSearch(const Vec& xBody,const Vec& d,const T& alpha,Vec& newXBody) {
  //update body's x
  newXBody=xBody+alpha*d.segment(0,xBody.size());
  //update planes' x
  int d_start_pos=xBody.size();
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->updateNewCCPlanesX(d.segment(d_start_pos,4*e->numCCPlanesToUpdate()), alpha);
      d_start_pos+=4* e->numCCPlanesToUpdate();
    }
  }
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::updateInOptimize(const Vec& newXBody,Vec& xBody) {
  //update body's x
  xBody=newXBody;
  //update planes' x
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  TBEG("Locally optimize planes");
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->assignCCPlanesX();
      e->localOptimizePlanes();
    }
  }
  TEND();
}
template<typename T,typename CCPlaneT>
void SIPPlannerDiscretizeFast<T, CCPlaneT>::debug(const Vec* cp,const Vec* d,const T* alpha,T perturbRange) {
  std::cout<<"=========================Debug full G, H============================"<<std::endl;
  Vec controlPoints;
  if(!cp) {
    controlPoints=Vec::Random(this->handlerObs()->getThetaTrajectory().getNumDOF())*perturbRange;
    // planes need to be re-initialized when eval()
    std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
    for(const auto& e0:this->trajEss()) {
      e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
      if(e) e->clearCCPlanesInitFlag();
    }
  } else controlPoints=*cp;
  std::cout<<"controlPoints="<<controlPoints.transpose()<<std::endl;
  if(d) std::cout<<"searchDireciton="<<(*d).transpose()<<std::endl;
  T E,E2;
  MatT H, H2, Hcp,Hcp2;
  Vec G, G2, Gcp,Gcp2,dcp, dpx, dx;
  //evaluate
  std::cout<<"=========================Assemble the first time============================"<<std::endl;
  if(this->assembleEnergy(controlPoints,&E,&Gcp,&Hcp,true,true,true)!=LSS_Succ) {
    std::cout<<"Debug failed!"<<std::endl;
    return;
  }
  assembleFullGH(Gcp,Hcp,&G,&H);

  //perturb and evaluate
  DEFINE_NUMERIC_DELTA_T(T)
  T delta;
  if(!alpha) delta=DELTA;
  else delta=*alpha;
  int num_planes=numCCPlanesToUpdate();
  int cp_size=this->handlerObs()->getThetaTrajectory().getNumDOF();
  if(!d) {
    dcp=Vec::Random(cp_size);
    for(const auto& fixLoc:this->fixVar()) dcp(fixLoc)=0;
    dpx=Vec::Random(4*num_planes);
  } else {
    dcp=(*d).segment(0, cp_size);
    dpx=(*d).segment(cp_size, 4*num_planes);
  }

  Vec perturbed_cp=controlPoints+dcp*delta;
  int start_pos=0;
  std::shared_ptr<CollisionBarrierEnergyFast<T,Barrier>> e;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->assignCCPlanesNewX();
      e->perturbCCPlanes(delta, dpx.segment(start_pos, 4 * e->numCCPlanesToUpdate()),true);
      start_pos+=4*e->numCCPlanesToUpdate();
    }
  }

  std::cout<<"=========================Assemble the second time============================"<<std::endl;
  if(this->assembleEnergy(perturbed_cp,&E2,&Gcp2,&Hcp2,true,true,true)!=LSS_Succ) {
    std::cout<<"Debug failed!"<<std::endl;
    return;
  }
  assembleFullGH(Gcp2,Hcp2, &G2, &H2);
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      e->assignCCPlanesX();
    }
  }
  dx.setZero(G.size());
  dx << dcp, dpx;

  Vec Gpx=G.segment(cp_size, dpx.size()), Gpx2=G2.segment(cp_size, dpx.size());
  MatT Hcppx=H.block(0, cp_size, cp_size, dpx.size()), Hpxcp=H.block(cp_size, 0, dpx.size(), cp_size),\
             Hpx=H.block(cp_size,cp_size, dpx.size(), dpx.size());
  std::cout<<"delta="<<delta<<std::endl;
  std::cout<<"E="<<std::setprecision(32)<<E<<std::endl;
  std::cout<<"E1="<<std::setprecision(32)<<E2<<std::endl;
  std::cout<<"G="<<std::setprecision(32)<<(G).dot(dx)<<std::endl;
  std::cout<<"G2="<<std::setprecision(32)<<(G2).dot(dx)<<std::endl;
  std::cout<<"G1="<<std::setprecision(32)<<(E2-E)/delta<<std::endl;
  std::cout<<"H="<<std::setprecision(32)<<((H)*dx).norm()<<std::endl;
  std::cout<<"H1="<<std::setprecision(32)<<((G2-G)/delta).norm()<<std::endl;
  std::cout<<"H2="<<std::setprecision(32)<<((H2)*dx).norm()<<std::endl;
  std::cout<<"Hcp="<<std::setprecision(32)<<(Hcppx*dpx).norm()<<std::endl;
  std::cout<<"Hcp1="<<std::setprecision(32)<<((Gcp2-Gcp)/delta-Hcp*dcp).norm()<<std::endl;
  std::cout<<"Hpx="<<std::setprecision(32)<<(Hpxcp*dcp).norm()<<std::endl;
  std::cout<<"Hpx1="<<std::setprecision(32)<<((Gpx2 - Gpx)/delta-Hpx*dpx).norm()<<std::endl;
  T dE=E2 - E;
  DEBUG_GRADIENT("dE",G.dot(dx),G.dot(dx)-dE/delta)
  Vec dG=G2-G;
  DEBUG_GRADIENT("dG",(H*dx).norm(),(H*dx-dG/delta).norm())
  Vec dGcp=Gcp2 - Gcp;
  Vec dGpx=Gpx2-Gpx;
  DEBUG_GRADIENT("dGcp", (Hcppx*dpx).norm(), (Hcppx*dpx + Hcp*dcp - dGcp/delta).norm())
  DEBUG_GRADIENT("dGpx", (Hpxcp*dcp).norm(), (Hpxcp*dcp + Hpx*dpx - dGpx/delta).norm())
  std::cout<<"=========================Debug GPlane, HPlane of collision barriers============================"<<std::endl;
  int cb_idx=0;
  start_pos=0;
  for(const auto& e0:this->trajEss()) {
    e=std::dynamic_pointer_cast<CollisionBarrierEnergyFast<T,Barrier>>(e0);
    if(e) {
      std::cout<<"Collision barrier:"<<cb_idx<<std::endl;
      e->debugPlanes(controlPoints, dpx.segment(start_pos,  4 * e->numCCPlanesToUpdate()),delta);
      start_pos+=4*e->numCCPlanesToUpdate();
      cb_idx++;
    }
  }
  std::cout<<"=========================Debug Gcp, Hcp============================"<<std::endl;
  SIPPlannerDiscretize<T,CCPlaneT>::debug(cp, &dcp, alpha, perturbRange);
}
//instance
template class SIPPlannerDiscretizeFast<FLOAT>;
}
