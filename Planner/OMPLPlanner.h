#ifndef OMPL_PLANNER_H
#define OMPL_PLANNER_H
#ifdef OMPL_SUPPORT

#include <Articulated/ArticulatedBody.h>
#include <SIPCollision/CollisionHandler.h>
#include <SIPCollision/TargetEnergy.h>
#include <Utils/SparseUtils.h>
#include <Utils/Pragma.h>

namespace PHYSICSMOTION {
template <typename T>
class OMPLPlanner : public SerializableBase {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  explicit OMPLPlanner(std::shared_ptr<ArticulatedBody> body,T d0,T epsTime,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD);
  bool read(std::istream& is,IOData* dat);
  bool write(std::ostream& os,IOData* dat) const;
  std::shared_ptr<SerializableBase> copy() const;
  std::string type() const;
  //setup
  void setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit);
  void addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF);
  void addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot, int maxConvexHulls);
  void addSphere(T r,int res,const Vec3T& pos);
  void addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& rot);
  void addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res);
  void addCapsule(T l,T w,T h,Vec3T pos,T radius,int res);
  void assembleBodyBVH(bool useConvexHull);
  void assembleObsBVH();
  std::vector<std::shared_ptr<MeshExact>> getObstacles() const;
  T getEpsTime() const;
  T getInterval() const;
  T getRuntime() const;
  T getThreshold() const;
  T getSmoothness() const;
  void setEpsTime(T epsTime);
  void setInterval(T interval);
  void setRuntime(T runtime);
  void setThreshold(T thres);
  void setSmoothness(T smooth);
  //main function
  void clearEnergy();
  void setupEnergy(int JID,const Vec3T& target,const Vec3T& dir,const Vec3T& orientation,T targetCoefP,T targetCoefD,bool JTJApprox=false);
  bool optimize(int maxIter=1e4,bool output=true,bool newtonMethod=false);
  //utilitizes
  const Vec& getControlPoints() const;
  void setControlPoints(const Vec& newCP);
  const ThetaTrajectory<T>& getThetaTrajectory() const;
  std::shared_ptr<ArticulatedBody> getBody() const;
  const std::vector<Vec>& getSolution() const;
 private:
  std::vector<std::shared_ptr<TargetEnergy<T>>> _trajEss;
  std::shared_ptr<CollisionHandler<T>> _handler;
  T _epsTime,_interval,_runtime,_threshold,_smoothness;
  std::vector<Vec> _states;
};
}

#endif
#endif
