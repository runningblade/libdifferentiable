#ifndef CDM_PLANNER_H
#define CDM_PLANNER_H

#include "PlannerBase.h"
#include <Optimizer/PhaseSequence.h>
#include <Optimizer/ForceSequence.h>
#include <Optimizer/PositionSequence.h>
#include <Optimizer/PBDDynamicsSequence.h>

namespace PHYSICSMOTION {
/*implementation of the following papers:
@ARTICLE{8283570,
  author={Winkler, Alexander W. and Bellicoso, C. Dario and Hutter, Marco and Buchli, Jonas},
  journal={IEEE Robotics and Automation Letters},
  title={Gait and Trajectory Optimization for Legged Systems Through Phase-Based End-Effector Parameterization},
  year={2018},
  volume={3},
  number={3},
  pages={1560-1567},
  doi={10.1109/LRA.2018.2798285}
}
@inproceedings{10.1145/3309486.3340246,
  author = {Pan, Zherong and Ren, Bo and Manocha, Dinesh},
  title = {GPU-Based Contact-Aware Trajectory Optimization Using a Smooth Force Model},
  year = {2019},
  isbn = {9781450366779},
  publisher = {Association for Computing Machinery},
  address = {New York, NY, USA},
  url = {https:doi.org/10.1145/3309486.3340246},
  doi = {10.1145/3309486.3340246},
  booktitle = {Proceedings of the 18th Annual ACM SIGGRAPH/Eurographics Symposium on Computer Animation},
  articleno = {4},
  numpages = {12},
  keywords = {trajectory optimization, position-based dynamics, articulated bodies, deformable bodies},
  location = {Los Angeles, California},
  series = {SCA '19}
}*/
template <typename T>
class CDMPlanner : public PlannerBase<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  enum DYNAMICS_MODEL {
    POSITION_BASED,
    NEWTON_EULER,
    SIMPLIFIED_NEWTON_EULER,
    SIMPLIFIED_NEWTON_EULER_SPLINE,
    PBCDM,
    PBCDM_SPLINE,
    UNKNOWN,
  };
  using PlannerBase<T>::_pt;
  using PlannerBase<T>::_env;
  using PlannerBase<T>::_body;
  using PlannerBase<T>::_bodyVis;
  using PlannerBase<T>::_bodySimplified;
  using PlannerBase<T>::_ees;
  using PlannerBase<T>::_xPoses;
  using PlannerBase<T>::stepToDOF;
  using PlannerBase<T>::mu;
  using PlannerBase<T>::dt;
  using PlannerBase<T>::g;
  using PlannerBase<T>::callback;
  using PlannerBase<T>::ftol;
  using PlannerBase<T>::xtol;
  using PlannerBase<T>::useIPOPT;
  using PlannerBase<T>::solveKnitro;
  using PlannerBase<T>::solveIpopt;
  using PlannerBase<T>::buildSimplifiedBody;
  CDMPlanner();
  CDMPlanner(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize);
  CDMPlanner(std::shared_ptr<Environment<T>> env,
             std::shared_ptr<ArticulatedBody> body,
             std::shared_ptr<ArticulatedBody> bodyVis,
             const std::vector<std::shared_ptr<EndEffectorBounds>>& ees);
  std::shared_ptr<SerializableBase> copy() const override;
#ifndef SWIG
  void debug(int trial,T thres,std::function<bool(const std::string&)> f=[](const std::string&) {
    return true;
  });
#endif
  void setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv=NULL);
  void update(const std::vector<std::shared_ptr<FootStep<T>>>& steps);
  std::vector<std::shared_ptr<FootStep<T>>> stepsSmartPtr() const;
  const std::vector<FootStep<T>>& steps() const;
  bool computeInitialPose(int nrTrial=10,const Vec2T* xyInit=NULL);
  void clearProblem();
  void buildProblemStaticPose();
  void buildProblemSteps();
  bool plan();
  void checkViolation();
  const Vec& getSolution() const;
  const PositionSequence<T>& getPosSeq(int footId) const;
  const ForceSequence<T>& getForceSeq(int footId) const;
  Vec3T getForce(int footId,T t) const;
  Mat3T getInitFrame() const;
  bool hasForceSeq() const;
  //property
  void singleHorizon(int N);
  int singleHorizon() const;
  void doubleHorizon(int N);
  int doubleHorizon() const;
  void eps(T eps);
  T eps() const;
  void horizon(T horizon);
  T horizon() const;
  void stage(int stage);
  int stage() const;
  void stageTorso(int stageTorso);
  int stageTorso() const;
  void subStage(int stage);
  int subStage() const;
  void stencilSize(int stencilSize);
  int stencilSize() const;
  void period(const Vec2T& period);
  Vec2T period() const;
  void staticToSwing(T staticToSwing);
  T staticToSwing() const;
  void targetVelocity(const Vec4T& v);
  Vec4T targetVelocity() const;
  void targetHeading(const Vec4T& h);
  Vec4T targetHeading() const;
  void terminalHeading(const Vec4T& h);
  Vec4T terminalHeading() const;
  void terminalPosition(const Vec4T& p);
  Vec4T terminalPosition() const;
  void laplaceCoef(T coef);
  T laplaceCoef() const;
  void laplaceTorsoCoef(T coef);
  T laplaceTorsoCoef() const;
  void nrInitialFrameToFix(int nrFrame);
  int nrInitialFrameToFix() const;
  //if((EndEffectorBounds._type&climbingFootMask())!=0),
  //  then this is a climbing foot equipped with a sucker.
  //  such a foot can provide force in the normal cone with a center direction opposite to gravity
  void climbingFootMask(int mask);
  int climbingFootMask() const;
  void DOFToFix(const Eigen::Matrix<int,6,1>& DOF);
  Eigen::Matrix<int,6,1> DOFToFix() const;
  void dynamicsModel(DYNAMICS_MODEL model);
  DYNAMICS_MODEL dynamicsModel() const;
  void savePath(const std::string& path);
  std::string savePath() const;
 private:
  bool loadSolution();
  void saveSolution();
  void buildDynamics();
  void buildObjective();
  void fixDOF(const Eigen::Matrix<int,-1,1>& DOF);
  void initDOF(const Eigen::Matrix<int,-1,1>& DOF,const Vec& pose,bool fix);
  //solver data
  SQPObjectiveCompound<T> _obj;
  std::vector<std::shared_ptr<PhaseSequence<T>>> _phases;
  std::vector<std::shared_ptr<ForceSequence<T>>> _forces;
  std::vector<std::shared_ptr<PositionSequence<T>>> _poses;
  std::vector<std::shared_ptr<SQPObjectiveComponent<T>>> _posCons;
  std::vector<std::shared_ptr<SQPObjectiveComponent<T>>> _forceCons;
  std::vector<std::shared_ptr<SQPObjectiveComponent<T>>> _objectives;
  std::shared_ptr<PBDDynamicsSequence<T>> _PBDDynamics;
  std::shared_ptr<DeformedEnvironment<T>> _DEnv;
  std::vector<FootStep<T>> _steps;
  Vec _initPose;
  Vec _x,_fvec;
};
}

#endif
