#include "GridPathPlanner.h"
#include <Utils/Timing.h>
#include <SIPCollision/TargetEnergy.h>
#include <Planner/SIPPlannerDiscretize.h>
#include <Planner/SIPPlannerExchange.h>

namespace PHYSICSMOTION {
template <typename T>
GridPlanner<T>::GridPlanner() {}
template <typename T>
GridPlanner<T>::GridPlanner(std::shared_ptr<SIPPlannerBase<T>> planner,
                            T targetCoefP,
                            T targetCoefD,
                            T barrierCoef,
                            T selfBarrierCoef,
                            T limitCoef,
                            T smoothnessCoef,
                            bool implicit,
                            bool useConvexHullObs,
                            bool useConvexHullSelf,
                            int maxIter,
                            bool output,
                            bool newtonMethod,
                            T gridSize,
                            bool useRobotBB)
  :_planner(planner),
   _targetCoefP(targetCoefP),
   _targetCoefD(targetCoefD),
   _barrierCoef(barrierCoef),
   _selfBarrierCoef(selfBarrierCoef),
   _limitCoef(limitCoef),
   _smoothnessCoef(smoothnessCoef),
   _implicit(implicit),
   _useConvexHullObs(useConvexHullObs),
   _useConvexHullSelf(useConvexHullSelf),
   _output(output),
   _newtonMethod(newtonMethod),
   _maxIter(maxIter),
   _gridSize(gridSize),
   _initialized(false),
   _optimized(false) {
  initSampledTargets(_gridSize,useRobotBB);
  std::cout << "Target size=" << targetSize() << std::endl;
  _controlPointsAllPath.resize(_targets.size());
  _targetSucc.resize(_targets.size());
}
template <typename T>
bool GridPlanner<T>::read(std::istream& is,IOData* dat) {
  registerType<SIPPlannerDiscretize<T>>(dat);
  registerType<SIPPlannerExchange<T>>(dat);
  //std::cout << "reading planner" << std::endl;
  readBinaryData(_planner,is,dat);
  //std::cout << "read planner" << std::endl;
  readBinaryData(_controlPointsAllPath,is);
  readBinaryData(_targets,is);
  readBinaryData(_targetSucc,is);
  readBinaryData(_targetCoefP,is);
  readBinaryData(_targetCoefD,is);
  readBinaryData(_barrierCoef,is);
  readBinaryData(_selfBarrierCoef,is);
  readBinaryData(_limitCoef,is);
  readBinaryData(_smoothnessCoef,is);
  readBinaryData(_implicit,is);
  readBinaryData(_useConvexHullObs,is);
  readBinaryData(_useConvexHullSelf,is);
  readBinaryData(_output,is);
  readBinaryData(_newtonMethod,is);
  readBinaryData(_maxIter,is);
  readBinaryData(_dimX,is);
  readBinaryData(_dimY,is);
  readBinaryData(_dimZ,is);
  readBinaryData(_gridSize,is);
  readBinaryData(_initialized,is);
  readBinaryData(_optimized,is);
  std::cout << "Printing Info..." << std::endl;
  std::cout << "targetCoefP=" << _targetCoefP << " targetCoefD=" << _targetCoefD << " barrierCoef=" << _barrierCoef << " selfBarrier=" << _selfBarrierCoef << " limitCoef=" << _limitCoef
            << " smoothCoef=" << _smoothnessCoef << " implicit=" << _implicit << " useConvexHullObs=" << _useConvexHullObs << " useConvexHullSelf=" << _useConvexHullSelf << " output=" << _output << " newtonMethod=" << _newtonMethod
            << " maxIter=" << _maxIter << " dimX=" << _dimX << " dimY=" << _dimY << " dimZ=" << _dimZ << " gridSize=" << _gridSize << " initialized" << _initialized << " optimized=" << _optimized << std::endl;
  return is.good();
}
template <typename T>
bool GridPlanner<T>::write(std::ostream& os,IOData* dat) const {
  registerType<SIPPlannerDiscretize<T>>(dat);
  registerType<SIPPlannerExchange<T>>(dat);
  writeBinaryData(_planner,os,dat);
  writeBinaryData(_controlPointsAllPath,os);
  writeBinaryData(_targets,os);
  writeBinaryData(_targetSucc,os);
  writeBinaryData(_targetCoefP,os);
  writeBinaryData(_targetCoefD,os);
  writeBinaryData(_barrierCoef,os);
  writeBinaryData(_selfBarrierCoef,os);
  writeBinaryData(_limitCoef,os);
  writeBinaryData(_smoothnessCoef,os);
  writeBinaryData(_implicit,os);
  writeBinaryData(_useConvexHullObs,os);
  writeBinaryData(_useConvexHullSelf,os);
  writeBinaryData(_output,os);
  writeBinaryData(_newtonMethod,os);
  writeBinaryData(_maxIter,os);
  writeBinaryData(_dimX,os);
  writeBinaryData(_dimY,os);
  writeBinaryData(_dimZ,os);
  writeBinaryData(_gridSize,os);
  writeBinaryData(_initialized,os);
  writeBinaryData(_optimized,os);
  std::cout << "Printing Info..." << std::endl;
  std::cout << "targetCoefP=" << _targetCoefP << " targetCoefD=" << _targetCoefD << " barrierCoef=" << _barrierCoef << " selfBarrier=" << _selfBarrierCoef << " limitCoef=" << _limitCoef
            << " smoothCoef=" << _smoothnessCoef << " implicit=" << _implicit << " useConvexHullObs=" << _useConvexHullObs << " useConvexHullSelf=" << _useConvexHullSelf << " output=" << _output << " newtonMethod=" << _newtonMethod
            << " maxIter=" << _maxIter << " dimX=" << _dimX << " dimY=" << _dimY << " dimZ=" << _dimZ << " gridSize=" << _gridSize << " initialized" << _initialized << " optimized=" << _optimized << std::endl;
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> GridPlanner<T>::copy() const {
  FUNCTION_NOT_IMPLEMENTED
}
template <typename T>
std::string GridPlanner<T>::type() const {
  return typeid(GridPlanner).name();
}
template <typename T>
void GridPlanner<T>::init(T distThreshold,bool neutralInit,Vec3T globalTargetDirection,Vec3T globalOrientation,bool JTJApprox) {
  for(int i=0; i<(int)_targets.size(); ++i) {
    auto& tar=_targets[i];
    if(tar.hasNaN()) {
      std::cout << "=================================================Initializing target" << i << " NAN. Skipping=====================================================" << std::endl;
      continue;
    }
    std::cout << "=================================================Initializing target" << i << ":"<<tar<<"=====================================================" << std::endl;
    _planner->clearEnergy();
    _planner->setupEnergy(_planner->getBody()->nrJ()-1,tar,globalTargetDirection,globalOrientation,_targetCoefP,_targetCoefD,JTJApprox);
    _planner->setupEnergy(_smoothnessCoef,_barrierCoef,_selfBarrierCoef,_limitCoef,_implicit,JTJApprox);
    //run planner
    TBEG("optimize"+std::to_string(i));
    _planner->optimize(_maxIter,_output,_newtonMethod);
    TEND();
    bool succReach=successCheck(i,distThreshold,globalTargetDirection,globalOrientation,JTJApprox);
    _targetSucc[i]=succReach;
    if(succReach)
      _controlPointsAllPath[i].push_back(_planner->getControlPoints());
    _planner->extendControlPointsSequence(true,neutralInit);
    std::cout << "SuccessRate at init Iter" << i << "=" << getSuccessRate()*100 << std::endl;
  }
  std::cout << "SuccessRate After Initialization=" << getSuccessRate()*100 << std::endl;
  for(int i=0; i<(int)_controlPointsAllPath.size(); ++i)
    std::cout << "_controlPointsAllPath[" << i << "].size()" << _controlPointsAllPath[i].size() << std::endl;
  _initialized=true;
}
template <typename T>
void GridPlanner<T>::optimize(T distThreshold,Vec3T globalTargetDirection,Vec3T globalOrientation,bool JTJApprox) {
  std::cout << "optimized?" << _optimized << std::endl;
  if(_optimized) return;
  int iter=0;
  while(true) {
    std::vector<int> oldTargetSucc=_targetSucc;
    for(int i=0; i<(int)_targets.size(); ++i) {
      bool success=false;
      if(_targets[i].hasNaN())
        continue;
      if(!_targetSucc[i]) {
        for(int axis=0; axis<3; ++axis) {
          for(int direction=-1; direction<2; direction+=2) {
            std::cout << "optimizing target[" << i << "] From Neighbor[" << axis << "," << direction << "]" << std::endl;
            success=optimizeFromOneDirection(i,axis,direction,distThreshold,globalTargetDirection,globalOrientation,JTJApprox);
            std::cout << "optimizing target[" << i << "] From Neighbor[" << axis << "," << direction << "]" << " finished" << std::endl;
            if(success) {
              std::cout << "Success in neighbor search! Exiting direction Optimization at direction" << std::endl;
              break;
            }
          }
          if(success) {
            std::cout << "Success in neighbor search! Exiting direction Optimization at axis" << std::endl;
            break;
          }
        }
      }
    }
    T successRate=getSuccessRate();
    std::cout << "SuccessRate After Iter" << iter << "=" << successRate*100 << std::endl;
    bool converged=true;
    for(int i=0; i<(int)_targetSucc.size(); ++i) {
      if(_targets[i].hasNaN()) continue;
      if(_targetSucc[i]!=oldTargetSucc[i]) {
        converged=false;
        break;
      }
    }
    if(converged) {
      _optimized=true;
      break;
    }
    iter++;
  }
}
template <typename T>
bool GridPlanner<T>::optimizeFromOneDirection(int i,int axis,int direction,T distThreshold,Vec3T globalTargetDirection,Vec3T globalOrientation,bool JTJApprox) {
#define AT(i,j,k) ((i)*_dimY*_dimZ+(j)*_dimZ+(k))
#define FIND(id,x,y,z) \
  int x=(id)/(_dimY*_dimZ); \
  int y=(id)%(_dimY*_dimZ)/_dimZ;\
  int z=(id)%(_dimY*_dimZ)%_dimZ;

  FIND(i,dimx,dimy,dimz)
  int directionalID=-1;
  std::string identifier;
  switch(axis) {
  case 0:
    if(direction==-1) {
      if(dimx-1<0) return false;
      directionalID=AT((dimx-1),dimy,dimz);
      identifier="x-1";
    } else if(direction==1) {
      if(dimx+1>=_dimX) return false;
      directionalID=AT((dimx+1),dimy,dimz);
      identifier="x+1";
    }
    break;
  case 1:
    if(direction==-1) {
      if(dimy-1<0) return false;
      identifier="y-1";
      directionalID=AT(dimx,(dimy-1),dimz);
    } else if(direction==1) {
      if(dimy+1>=_dimY) return false;
      directionalID=AT(dimx,(dimy+1),dimz);
      identifier="y+1";
    }
    break;
  case 2:
    if(direction==-1) {
      if(dimz-1<0) return false;
      directionalID=AT(dimx,dimy,(dimz-1));
      identifier="z-1";
    } else if(direction==1) {
      if(dimz+1>=_dimZ) return false;
      directionalID=AT(dimx,dimy,(dimz+1));
      identifier="z+1";
    }
    break;
  default:
    ASSERT_MSG(false,"Wrong Axis Number!")
  }
  std::cout << "directionalID=" << directionalID << " identifier=" << identifier << std::endl;
  if(_targets[directionalID].hasNaN()) {
    std::cout << "neighbor has NAN, stop" << std::endl;
    return false;
  }
  if(_targetSucc[directionalID]) {
    _planner->setControlPoints(_controlPointsAllPath[directionalID].back());
    _planner->extendControlPointsSequence(false,false);
    _planner->clearEnergy();
    _planner->setupEnergy(_planner->getBody()->nrJ()-1,_targets[i],globalTargetDirection,globalOrientation,_targetCoefP,_targetCoefD,JTJApprox);
    _planner->setupEnergy(_smoothnessCoef,_barrierCoef,_selfBarrierCoef,_limitCoef,_implicit);
    //run planner
    TBEG("optimize iter"+std::to_string(i)+" "+identifier);
    _planner->optimize(_maxIter,_output,_newtonMethod);
    TEND();
    bool succReach=successCheck(i,distThreshold,globalOrientation,globalOrientation,JTJApprox);
    _targetSucc[i]=succReach;
    if(succReach) {
      _controlPointsAllPath[i]=_controlPointsAllPath[directionalID];
      _controlPointsAllPath[i].push_back(_planner->getControlPoints());
    }
    return succReach;
  } else {
    std::cout << "Neighbor not success, stop" << std::endl;
    return false;
  }
#undef AT
#undef FIND
}
template <typename T>
T GridPlanner<T>::getSuccessRate() const {
  int cnt=0;
  int totalNum=0;
  for(int i=0; i<(int)_targetSucc.size(); ++i) {
    if(_targets[i].hasNaN())
      continue;
    if(_targetSucc[i])
      cnt++;
    totalNum++;
  }
  return (T) cnt/(T) totalNum;
}
template <typename T>
bool GridPlanner<T>::initialized() const {
  return _initialized;
}
template <typename T>
std::shared_ptr<SIPPlannerBase<T>> GridPlanner<T>::getPlanner() const {
  return _planner;
}
template <typename T>
const std::vector<std::vector<typename GridPlanner<T>::Vec>>& GridPlanner<T>::getControlPointsAllPaths() const {
  return _controlPointsAllPath;
}
template <typename T>
void GridPlanner<T>::defaultVisConfig() {
  for(int i=0; i<(int)_targets.size(); ++i)
    _controlPointsAllPath[i].push_back(Vec::Zero(_planner->getThetaTrajectory().getNumDOF()));
}
template <typename T>
int GridPlanner<T>::targetSize() const {
  int totalNum=0;
  for(int i=0; i<(int)_targets.size(); ++i) {
    if(_targets[i].hasNaN())
      continue;
    totalNum++;
  }
  return totalNum;
}
template <typename T>
const std::vector<typename GridPlanner<T>::Vec3T>& GridPlanner<T>::getTargets() const {
  return _targets;
}
template <typename T>
const std::vector<int>& GridPlanner<T>::getTargetSuccessInfo() const {
  return _targetSucc;
}
template <typename T>
void GridPlanner<T>::initSampledTargets(T gridSize,bool useRobotBB) {
  const std::vector<std::shared_ptr<MeshExact>>& obs=_planner->getObstacles();
  BBoxExact bb;
  for(const auto& ob:obs)
    bb.setUnion(ob->getBB());
  Vec3T maxRobotReachRange;
  Vec3T minRobotReachRange;
  if(useRobotBB) {
    BBoxExact robotBB=_planner->getBody()->getBB(ArticulatedBody::Vec(ArticulatedBody::Vec::Zero(_planner->getBody()->nrDOF())));
    std::cout << "robotMaxCorner=" << robotBB.maxCorner().transpose() << " robotMinCorner=" << robotBB.minCorner().transpose() << std::endl;
    T robotLen=(T)(robotBB.maxCorner()[2]-robotBB.minCorner()[2]);
    std::cout << "robotLen=" << robotLen << std::endl;
    Eigen::Matrix<T,3,1> center=(robotBB.maxCorner()+robotBB.minCorner()).template cast<T>()/2.;
    std::cout << "center=" << center.transpose() << std::endl;
    maxRobotReachRange=center+Vec3T(robotLen/2.,robotLen/2.,robotLen/2.);
    minRobotReachRange=center-Vec3T(robotLen/2.,robotLen/2.,robotLen/2.);
  } else {
    maxRobotReachRange=bb.maxCorner().template cast<T>();
    minRobotReachRange=bb.minCorner().template cast<T>();
  }
  std::cout<<"maxRobotRangeRange="<<maxRobotReachRange.transpose()<<" minRobotReachRange="<<minRobotReachRange.transpose()<<std::endl;
  Vec3T maxIntersectedCorner=maxRobotReachRange.cwiseMin(bb.maxCorner().template cast<T>());
  Vec3T minIntersectedCorner=minRobotReachRange.cwiseMax(bb.minCorner().template cast<T>());
  std::cout<<"maxIntersectedCorner="<<maxIntersectedCorner.transpose()<<" minIntersectedCorner="<<minIntersectedCorner.transpose()<<std::endl;
  int maxX=(int)ceil((maxIntersectedCorner[0]-minIntersectedCorner[0])/gridSize);
  int maxY=(int)ceil((maxIntersectedCorner[1]-minIntersectedCorner[1])/gridSize);
  int maxZ=(int)ceil((maxIntersectedCorner[2]-minIntersectedCorner[2])/gridSize);
  _dimX=maxX;
  _dimY=maxY;
  _dimZ=maxZ;
  std::string path="targets.txt";
  std::ofstream fout(path);
  std::cout<<"path="<<path<<std::endl;
  for(int i=0; i<maxX; ++i)
    for(int j=0; j<maxY; ++j)
      for(int k=0; k<maxZ; ++k) {
        Vec3T tar=Vec3T(minIntersectedCorner[0]+i*gridSize,minIntersectedCorner[1]+j*gridSize,minIntersectedCorner[2]+k*gridSize);
        bool valid=true;
        for(const auto& ob:obs) {
          //const BBoxExact& obBB=ob->getBB();
          //if( tar[0]>=obBB.minCorner()[0] && tar[0]<=obBB.maxCorner()[0] &&
          //    tar[1]>=obBB.minCorner()[1] && tar[1]<=obBB.maxCorner()[1] &&
          //    tar[2]>=obBB.minCorner()[2] && tar[2]<=obBB.maxCorner()[2]) {
          if(ob->getBB().contain(tar.template cast<BBoxExact::T>())) {
            valid=false;
            break;
          }
        }
        if(valid) {
          fout<<tar.transpose()<<std::endl;
          _targets.push_back(tar);
        } else _targets.push_back(Vec3T(NAN,NAN,NAN));
      }
  fout.close();
  std::cout<<"file closed"<<std::endl;
}
template <typename T>
bool GridPlanner<T>::successCheck(int i,T distThreshold,Vec3T globalDirection,Vec3T globalOrientation,bool JTJApprox) {
  TargetEnergy<T> targetEnergy(*_planner->getBody(),
                               _planner->getControlPoints(),
                               _planner->getThetaTrajectory(),
                               _planner->getBody()->nrJ()-1,
                               _targets[i],
                               globalDirection,
                               globalOrientation,
                               _targetCoefP,
                               _targetCoefD,
                               JTJApprox);
  T distSqr;
  targetEnergy.SIPEnergy<T>::eval(&distSqr,NULL,NULL);
  if(sqrt(distSqr)<=distThreshold)
    return true;
  else return false;
}
//instance
template class GridPlanner<FLOAT>;
}
