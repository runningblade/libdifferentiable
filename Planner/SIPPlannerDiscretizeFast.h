#ifndef SIP_PLANNER_DISCRETIZE_FAST_H
#define SIP_PLANNER_DISCRETIZE_FAST_H

#include "SIPPlannerDiscretize.h"
#include "SIPCollision/CCBarrierEnergyFast.h"
#include "SIPCollision/CollisionBarrierEnergyFast.h"
#include "SIPCollision/SmoothnessEnergy.h"
#include "SIPCollision/JointLimitEnergy.h"

namespace  PHYSICSMOTION {
template<typename T,typename CCPlaneT=CCSeparatingPlaneSmart<T>>
class SIPPlannerDiscretizeFast : public SIPPlannerDiscretize<T,CCPlaneT> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Px Barrier;
  explicit SIPPlannerDiscretizeFast(T d0=1e-3,T x0Obs=1.,T x0Self=1.,T L2=0.01,T eta=7./24.,T terminateGrad=1e-4,T minEig=1e-3,T colMinEig=1e-3, bool useConvexHullObs=true,bool useConvexHullSelf=true, bool schurSolve=false,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD= {});
  explicit SIPPlannerDiscretizeFast(std::shared_ptr<ArticulatedBody> body,
                                    int order,
                                    int totalTime,
                                    T d0=1e-3,
                                    T x0Obs=1.,
                                    T x0Self=1.,
                                    T L2=0.01,
                                    T eta=7./24.,
                                    T terminateGrad=1e-4,
                                    T minEig=1e-3,
                                    T colminEig=1e-3,
                                    T randomInitialize=1.0,
                                    bool useConvexHullObs=true,
                                    bool useConvexHullSelf=true,
                                    bool neutralInit=false,
                                    bool schurSolve=false,
                                    const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD= {});
  std::shared_ptr<SerializableBase> copy() const override;
  std::string type() const override;
  void setupEnergy(T smoothnessCoef,T obsBarrierCoef,T selfBarrierCoef,T limitCoef, bool implicit, bool JTJApprox) override;
  void debug(const Vec* cp,const Vec* d,const T* alpha,T perturbRange) override;
 private:
  LineSearchState computeNewX(T& E,const Vec& Gcp,const MatT& Hcp,
                              Vec&d,Vec& newX,T& alpha,
                              bool newtonMethod, bool debug) override;
  void updateInLineSearch(const Vec& xBody,const Vec& d,const T& alpha,Vec& newXBody) override;
  void updateInOptimize(const Vec& newX,Vec& x) override;
  bool isZeroGrad(const Vec& Gcp) override;
  void solveSearchDirection(const Vec& Gcp,const MatT& Hcp,Vec& G, MatT& H,Vec& d, bool newtonMethod);
  void assembleFullGH(const Vec& Gcp,const MatT& Hcp,Vec* G, MatT* H=nullptr);
  void assembleKKTGH(const Vec& G,const MatT& H,Vec& GKKT, MatT& HKKT);
  void assembleProjectedG(const Vec& Gcp,Vec& projG);
  T safePDSolveSearchDirection(const Vec& G,const MatT& H,Vec& d);
  void computeSchurGH(const Vec& G,const MatT& H,Vec& GSchur, MatT& HSchur);
  void schurSolveSearchDirection(const Vec& GSchur,const MatT& HSchur,Vec& d);
  int numCCPlanesToUpdate();
  bool _schurSolve;
  T _colMinEig;
};
}
#endif
