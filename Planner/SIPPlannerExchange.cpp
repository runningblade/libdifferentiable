#include "SIPPlannerExchange.h"
#include <SIPCollision/TargetEnergy.h>
#include <SIPCollision/GravityEnergy.h>
#include <SIPCollision/SmoothnessEnergy.h>
#include <SIPCollision/SIPObjective.h>
#include <Articulated/ArticulatedBody.h>
#include <Articulated/ArticulatedUtils.h>
#include <Articulated/ArticulatedLoader.h>
#include <Optimizer/IPOPTInterface.h>
#include <Utils/Timing.h>
#include <Utils/Utils.h>
#include <random>

namespace PHYSICSMOTION {
//initialize
template <typename T>
SIPPlannerExchange<T>::SIPPlannerExchange(T d0,T epsTime,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD):_maxIterInner(1e4) {
  _detector.reset(new PenetrationDetector<T>(d0,epsTime,skipSelfCCD));
}
template <typename T>
SIPPlannerExchange<T>::SIPPlannerExchange(std::shared_ptr<ArticulatedBody> body,int order,int totalTime,
    T d0,T epsTime,T randomInitialize,bool neutralInit,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD):_maxIterInner(1e4) {
  _detector.reset(new PenetrationDetector<T>(body,order,totalTime,d0,epsTime,randomInitialize,neutralInit,skipSelfCCD));
}
template <typename T>
bool SIPPlannerExchange<T>::read(std::istream& is,IOData* dat) {
  registerType<ArticulatedBody>(dat);
  registerType<PenetrationDetector<T>>(dat);
  readBinaryData(_maxIterInner,is);
  readBinaryData(_fixVar,is);
  _detector->read(is,dat);
  return is.good();
}
template <typename T>
bool SIPPlannerExchange<T>::write(std::ostream& os,IOData* dat) const {
  registerType<ArticulatedBody>(dat);
  registerType<PenetrationDetector<T>>(dat);
  writeBinaryData(_maxIterInner,os);
  writeBinaryData(_fixVar,os);
  _detector->write(os,dat);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> SIPPlannerExchange<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new SIPPlannerExchange(_detector->d0(),_detector->epsTime()));
}
template <typename T>
std::string SIPPlannerExchange<T>::type() const {
  return typeid(SIPPlannerExchange).name();
}
//setup
template <typename T>
void SIPPlannerExchange<T>::setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit) {
  _detector->setRobot(body,randomInitialize,neutralInit);
}
template <typename T>
void SIPPlannerExchange<T>::addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF) {
  _detector->addRobot(body,DOF);
}
template <typename T>
void SIPPlannerExchange<T>::addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot,int maxConvexHulls) {
  _detector->addObject(path,scale,pos,rot,maxConvexHulls);
}
template <typename T>
void SIPPlannerExchange<T>::addSphere(T r,int res,const Vec3T& pos) {
  _detector->addSphere(r,res,pos);
}
template <typename T>
void SIPPlannerExchange<T>::addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& R) {
  _detector->addCuboid(l,w,h,pos,R);
}
template <typename T>
void SIPPlannerExchange<T>::addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res) {
  _detector->addCapsule(a,b,radius,res);
}
template <typename T>
void SIPPlannerExchange<T>::addCapsule(T l,T w,T h,Vec3T pos,T radius,int res) {
  _detector->addCapsule(l,w,h,pos,radius,res);
}
template <typename T>
std::vector<std::shared_ptr<MeshExact>> SIPPlannerExchange<T>::getObstacles() const {
  return _detector->getObstacles();
}
template <typename T>
const typename SIPPlannerExchange<T>::Vec& SIPPlannerExchange<T>::getControlPoints() const {
  return _detector->getControlPoints();
}
template <typename T>
void SIPPlannerExchange<T>::setControlPoints(const Vec& newCP) {
  updateControlPoints(newCP);
  assembleBodyBVH(_detector->getDCDObs(),_detector->getDCDSelf());
  updateControlPoints(newCP);
}
template <typename T>
const ThetaTrajectory<T>& SIPPlannerExchange<T>::getThetaTrajectory() const {
  return _detector->getThetaTrajectory();
}
template <typename T>
std::shared_ptr<ArticulatedBody> SIPPlannerExchange<T>::getBody() const  {
  return _detector->getBody();
}
template <typename T>
void SIPPlannerExchange<T>::assembleBodyBVH(bool DCDObs,bool DCDSelf) {
  _detector->assembleBodyBVH();
  _detector->setDCDObs(DCDObs);
  _detector->setDCDSelf(DCDSelf);
}
//optimization
template <typename T>
std::shared_ptr<PenetrationDetector<T>> SIPPlannerExchange<T>::getDetector() const {
  return _detector;
}
template <typename T>
void SIPPlannerExchange<T>::clearEnergy() {
  _trajEss.clear();
}
template <typename T>
void SIPPlannerExchange<T>::setupEnergy(int JID,const Vec3T& tar,const Vec3T& dir,const Vec3T& ori,T targetCoefP,T targetCoefD,bool JTJApprox) {
  if(targetCoefP>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new TargetEnergy<T>(*(_detector->getBody()),
                                            _detector->getControlPoints(),
                                            _detector->getThetaTrajectory(),
                                            JID,tar,dir,ori,targetCoefP,targetCoefD,JTJApprox)));
  else if(targetCoefD>0) {
    std::set<int> children=_detector->getBody()->children(JID);
    children.insert(JID);
    for(int CID:children)
      if(_detector->getBody()->joint(CID)._mesh)
        _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                           (new GravityEnergy<T>(*(_detector->getBody()),
                               _detector->getControlPoints(),
                               _detector->getThetaTrajectory(),
                               CID,dir,targetCoefD,JTJApprox)));
  }
}

template <typename T>
void SIPPlannerExchange<T>::setupEnergy(T smoothnessCoef,T,T,T,bool,bool) {
  if(smoothnessCoef>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new SmoothnessEnergy<T>(*(_detector->getBody()),
                           _detector->getControlPoints(),
                           _detector->getThetaTrajectory(),smoothnessCoef,1)));
}

template <typename T>
bool SIPPlannerExchange<T>::optimize(int maxIter,bool output,bool) {
#ifdef IPOPT_SUPPORT
  bool warmStart=true;
  T gTol=1e-2,muInit=1e5;
#endif
  Vec x=_detector->getControlPoints(),C;
  _detector->getPDEntries().clear();
  for(int iter=0; iter<maxIter; iter++) {
    int nPD=(int)_detector->getPDEntries().getVector().size();
    //solve NLP
    SIPObjective<T> obj(_fixVar,*_detector,_trajEss);
#ifdef IPOPT_SUPPORT
    TBEG("SQPSolve");
    if(!warmStart)
      x=Vec::Zero(0);
    std::cout<<"inputs="<<obj.inputs()<<" values="<<obj.values()<<std::endl;
    if(!IPOPTInterface<T>::optimize(obj,0,false,(double)gTol,_maxIterInner,(double)muInit,x)) {
      if(output)
        std::cout<<"SQP failed!"<<std::endl;
      warmStart=false;
    } else warmStart=true;
    TEND();
#else
    ASSERT_MSG(false,"IPOPT not supported!")
#endif
    //check feasibility
    _detector->setControlPoints(x);
    obj(x,C,(MatT*)NULL);
    for(int i=0; i<C.size(); i++)
      if(C[i]<=obj.gl()[i]-_detector->d0()/2)
        if(output)
          std::cout<<"Constraint["<<i<<"]="<<C[i]<<" violated!"<<std::endl;
    //update PD
    TBEG("PD");
    //writeConfiguration();
    if(!_detector->DCD(false,false,output))
      if(output)
        std::cout<<"Detected duplicate PDEntry!"<<std::endl;
    //_detector->writeVTK("PD");
    TEND();
    int nPDNew=(int)_detector->getPDEntries().getVector().size();
    if(output)
      std::cout<<"Iter="<<iter<<" #PD="<<nPDNew<<std::endl;
    if(nPDNew==nPD)
      return true;
  }
  return false;
}
template <typename T>
void SIPPlannerExchange<T>::fixThetaAt(T t) {
  std::unordered_set<int> fixVar(_fixVar.begin(),_fixVar.end());
  //find related variable by perturbation analysis and fix
  Vec theta=Vec::Zero(_detector->getThetaTrajectory().getNumDOF());
  Vec pt=_detector->getThetaTrajectory().getPoint(theta,t);
  for(int i=0; i<(int)theta.size(); ++i) {
    Vec tmpTheta=theta;
    tmpTheta[i]+=1;
    Vec pt2=_detector->getThetaTrajectory().getPoint(tmpTheta,t);
    if(pt2!=pt)
      fixVar.insert(i);
  }
  _fixVar.assign(fixVar.begin(),fixVar.end());
}
template <typename T>
void SIPPlannerExchange<T>::initializeTheta(const Vec& theta) {
  Vec thetaAll=Vec::Zero(_detector->getThetaTrajectory().getNumDOF());
  for(int i=0; i<thetaAll.size(); i+=theta.size())
    thetaAll.segment(i,theta.size())=theta;
  _detector->setControlPoints(thetaAll);
}
template <typename T>
void SIPPlannerExchange<T>::setUseSDF(bool use) {
  _detector->setUseSDF(use);
}
template <typename T>
bool SIPPlannerExchange<T>::getUseSDF() const {
  return _detector->getUseSDF();
}
//debug
template <typename T>
void SIPPlannerExchange<T>::writeConfiguration(const std::string& path) {
  std::ofstream os(path,std::ios::binary);
  writeStr(path);
}
template <typename T>
void SIPPlannerExchange<T>::debugAtConfiguration(const std::string& path) {
  Vec C;
  if(exists(path)) {
    readStr(path);
    //check constraint
    SIPObjective<T> obj(_fixVar,*_detector,_trajEss);
    obj(getControlPoints(),C,(MatT*)NULL);
    for(int i=0; i<C.size(); i++)
      if(C[i]<=obj.gl()[i]-_detector->d0()/2) {
        exit(EXIT_FAILURE);
      }
    //check DCD
    if(!_detector->DCD(false,false,true)) {
      exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);
  }
}
//helper
template <typename T>
void SIPPlannerExchange<T>::updateControlPoints(const Vec& controlPoints) {
  _detector->setControlPoints(controlPoints);
}
template <typename T>
void SIPPlannerExchange<T>::assembleObsBVH() {
  _detector->assembleObsBVH();
}
//instance
template class SIPPlannerExchange<FLOAT>;
}
