#include "FootStepPlanner.h"
#include <Articulated/ArticulatedUtils.h>
#include <Articulated/ArticulatedLoader.h>
#include <Articulated/NEArticulatedGradientInfo.h>
#include <Utils/RotationUtils.h>
#include <Utils/Interp.h>
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
template <typename T>
FootStepPlanner<T>::FootStepPlanner():PlannerBase<T>() {}
template <typename T>
FootStepPlanner<T>::FootStepPlanner(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize):PlannerBase<T>(env,path,normalize) {}
template <typename T>
FootStepPlanner<T>::FootStepPlanner
(std::shared_ptr<Environment<T>> env,
 std::shared_ptr<ArticulatedBody> body,
 std::shared_ptr<ArticulatedBody> bodyVis,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees):PlannerBase<T>(env,body,bodyVis,ees) {}
template <typename T>
std::shared_ptr<SerializableBase> FootStepPlanner<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new FootStepPlanner<T>);
}
template <typename T>
void FootStepPlanner<T>::computeInitialPose(const Vec3T& target,bool isEven) {
  computeInitialPose(&target,isEven);
}
template <typename T>
void FootStepPlanner<T>::computeInitialPose(const Vec3T* target,bool isEven) {
  clearProblem();
  //create reachable
  Eigen::Matrix<int,3,1> bodyPoseDOF;
  std::vector<Eigen::Matrix<int,2,1>> eesDOF(_ees.size());
  for(int d=0; d<3; d++)
    bodyPoseDOF[d]=_obj.addVar("bodyPose"+std::to_string(d),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  for(int eid=0; eid<(int)_ees.size(); eid++)
    for(int d=0; d<2; d++)
      eesDOF[eid][d]=_obj.addVar("EE["+std::to_string(eid)+"]"+std::to_string(d),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  std::shared_ptr<MixedIntegerReachable<T>> reach(new MixedIntegerReachable<T>(_obj,"test",resRotation(),0,_env,true,bodyPoseDOF,_ees,eesDOF));
  if(target)
    for(int d=0; d<3; d++)
      reach->addPolynomialComponentObjective((_obj.addVarPoly(bodyPoseDOF[d])-PolyXT(target->coeff(d)))*(_obj.addVarPoly(bodyPoseDOF[d])-PolyXT(target->coeff(d))));
  //reachable target
  T hTarget=bodyHeightTarget();
  T hCoef=bodyHeightCoef();
  if(hCoef>0)
    reach->setTargetHeightEnergy(_obj,hTarget,hCoef);
  _obj.addComponent(reach);

  //optimize
  Vec x=_obj.init();
  solveGurobi(_obj,x);
  Vec DOF=reach->getBodyPose(x,*_body);
  setInitialStep(DOF,isEven);
  _xPoses=DOF;
}
template <typename T>
void FootStepPlanner<T>::setInitialStep(const Vec& DOF,bool isEven) {
  _steps.resize(1);
  _steps[0]._bodyPose=DOF.template segment<6>(0);
  _steps[0]._eePose.resize(3,_ees.size());
  NEArticulatedGradientInfo<T> info(*_body,DOF);
  Mat3XT TJ=info.getTrans();
  for(int eid=0; eid<(int)_ees.size(); eid++)
    _steps[0]._eePose.col(eid)=ROTI(TJ,_ees[eid]->jointId())*_ees[eid]->_localPos.template cast<T>()+CTRI(TJ,_ees[eid]->jointId());
  _steps[0]._isEven=isEven;
}
template <typename T>
void FootStepPlanner<T>::setSteps(const std::vector<std::shared_ptr<FootStep<T>>>& ss) {
  _steps.clear();
  for(const std::shared_ptr<FootStep<T>>& step:ss)
    _steps.push_back(*step);
}
template <typename T>
std::vector<std::shared_ptr<FootStep<T>>> FootStepPlanner<T>::stepsSmartPtr() const {
  std::vector<std::shared_ptr<FootStep<T>>> ret;
  for(const FootStep<T>& step:steps())
    ret.push_back(std::shared_ptr<FootStep<T>>(new FootStep<T>(step)));
  return ret;
}
template <typename T>
const std::vector<FootStep<T>>& FootStepPlanner<T>::steps() const {
  return _steps;
}
template <typename T>
void FootStepPlanner<T>::clearProblem() {
  _obj=SQPObjectiveCompound<T>();
  _smoothBody=NULL;
  _smoothFoot.clear();
  _reachable.clear();
  _targetVelocityObj=NULL;
  _targetHeadingObj=NULL;
  _terminalHeadingObj=NULL;
  _terminalPositionObj=NULL;
}
template <typename T>
void FootStepPlanner<T>::buildProblem() {
  clearProblem();
  int N=horizon();
  _steps.resize(N+1);
  _obj=SQPObjectiveCompound<T>();
  //smoothness of body
  _smoothBody.reset(new LaplaceObjective<T,3>(_obj,"BodyPose",N+1,laplaceCoef()));
  _obj.addVar(_smoothBody->getVar()(0,0)).fix(_steps[0]._bodyPose[0]);
  _obj.addVar(_smoothBody->getVar()(1,0)).fix(_steps[0]._bodyPose[1]);
  _obj.addVar(_smoothBody->getVar()(2,0)).fix(_steps[0]._bodyPose[5]);
  _obj.addComponent(_smoothBody);
  //smoothness of foot
  _smoothFoot.resize(_ees.size());
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    int nPiece=(_ees[eid]->isEven()==_steps[0]._isEven)?N/2:(N+1)/2;
    _smoothFoot[eid].reset(new LaplaceObjective<T,2>(_obj,"EndEffector"+std::to_string(eid),nPiece+1,laplaceCoef())); //there is always an inital step
    _obj.addVar(_smoothFoot[eid]->getVar()(0,0)).fix(_steps[0]._eePose(0,eid));
    _obj.addVar(_smoothFoot[eid]->getVar()(1,0)).fix(_steps[0]._eePose(1,eid));
    _obj.addComponent(_smoothFoot[eid]);
  }
  //reachability
  T hTarget=bodyHeightTarget();
  T hCoef=bodyHeightCoef();
  T refRotation=_steps[0]._bodyPose[5];
  for(int i=1; i<=N; i++) {
    std::vector<Eigen::Matrix<int,2,1>> eesDOF;
    for(int eid=0; eid<(int)_ees.size(); eid++) {
      int idEEStep=(_ees[eid]->isEven()==_steps[0]._isEven)?i/2:(i+1)/2;
      eesDOF.push_back(_smoothFoot[eid]->getVar().col(idEEStep));
      if(callback()>0)
        std::cout << "EE[" << eid << "][Step=" << i << "]: using footId=" << idEEStep << "/" << _smoothFoot[eid]->getVar().cols() << std::endl;
    }
    _reachable.push_back(std::shared_ptr<MixedIntegerReachable<T>>(new MixedIntegerReachable<T>(_obj,"EndEffector"+std::to_string(i),resRotation(),refRotation,_env,true,_smoothBody->getVar().col(i),_ees,eesDOF)));
    if(hCoef>0)
      _reachable.back()->setTargetHeightEnergy(_obj,hTarget,hCoef);
    _obj.addComponent(_reachable.back());
  }
  //target
  if(targetVelocity()[2]>0) {
    _targetVelocityObj.reset(new PolynomialObjective<T>(_obj,"TargetVelocity",PolyXT()));
    VECP a,b;
    PolyXT objPoly;
    a.resize(2);
    b.resize(2);
    for(int i=1; i<N; i++) {
      a[0]=_obj.addVarPoly(_smoothBody->getVar()(0,i));
      a[1]=_obj.addVarPoly(_smoothBody->getVar()(1,i));
      b[0]=_obj.addVarPoly(_smoothBody->getVar()(0,i-1));
      b[1]=_obj.addVarPoly(_smoothBody->getVar()(1,i-1));
      objPoly+=(a-b-targetVelocity().template segment<2>(0).template cast<PolyXT>()).squaredNorm();
    }
    _targetVelocityObj->setPolynomialComponentObjective(objPoly*PolyXT(targetVelocity()[2]/2));
    _obj.addComponent(_targetVelocityObj);
    if(syncHeading()) {
      T heading=updateHeading(targetVelocity().template segment<2>(0));
      targetHeading(Vec2T(heading,targetHeading()[1]));
    }
  }
  if(targetHeading()[1]>0) {
    _targetHeadingObj.reset(new PolynomialObjective<T>(_obj,"TargetHeading",PolyXT()));
    PolyXT objPoly,a;
    for(int i=1; i<N; i++) {
      a=_obj.addVarPoly(_smoothBody->getVar()(2,i))-PolyXT(targetHeading()[0]);
      objPoly+=a*a;
    }
    _targetHeadingObj->setPolynomialComponentObjective(objPoly*PolyXT(targetHeading()[1]/2));
    _obj.addComponent(_targetHeadingObj);
  }
  if(terminalHeading()[1]!=0) {
    if(terminalHeading()[1]<=0)
      _terminalHeadingObj.reset(new PolynomialObjective<T>(_obj,"TargetHeading",PolyXT(),Vec2T(0,0)));
    else _terminalHeadingObj.reset(new PolynomialObjective<T>(_obj,"TargetHeading",PolyXT()));
    PolyXT delta=_obj.addVarPoly(_smoothBody->getVar()(2,N-1))-PolyXT(terminalHeading()[0]);
    if(terminalHeading()[1]>0)
      _terminalHeadingObj->setPolynomialComponentObjective(delta*delta*PolyXT(terminalHeading()[1]/2));
    else _terminalHeadingObj->setPolynomialComponentConstraint(0,delta);
    _obj.addComponent(_terminalHeadingObj);
  }
  if(terminalPosition()[2]!=0) {
    if(terminalPosition()[2]<=0)
      _terminalPositionObj.reset(new PolynomialObjective<T>(_obj,"TerminalPosition",PolyXT(),Vec2T(0,0)));
    else _terminalPositionObj.reset(new PolynomialObjective<T>(_obj,"TerminalPosition",PolyXT()));
    VECP delta;
    delta.resize(2);
    delta[0]=_obj.addVarPoly(_smoothBody->getVar()(0,N-1));
    delta[1]=_obj.addVarPoly(_smoothBody->getVar()(1,N-1));
    if(terminalPosition()[2]>0)
      _terminalPositionObj->setPolynomialComponentObjective((delta-terminalPosition().template segment<2>(0).template cast<PolyXT>()).squaredNorm()*PolyXT(terminalPosition()[2]/2));
    else {
      _terminalPositionObj->setPolynomialComponentConstraint(0,delta[0]-PolyXT(terminalPosition()[0]));
      _terminalPositionObj->setPolynomialComponentConstraint(1,delta[1]-PolyXT(terminalPosition()[1]));
    }
    _obj.addComponent(_terminalPositionObj);
    if(syncHeading()) {
      T heading=updateHeading(terminalPosition().template segment<2>(0)-_steps[0]._bodyPose.template segment<2>(0));
      terminalHeading(Vec2T(heading,terminalHeading()[1]));
    }
  }
}
template <typename T>
bool FootStepPlanner<T>::plan(bool interpolate) {
  buildProblem();
  //optimize
  Vec x;
  if(_obstacles.empty()) {
    if(!solveGurobi(_obj,x))
      return false;
  } else if(!lazyObstacle()) {
    T bigM=getBigM();
    T clearance=getClearance();
    SQPObjectiveCompound<T> objObstacle=_obj;
    for(std::shared_ptr<MixedIntegerObstacle<T>> obs:_obstacles) {
      obs->clear();
      objObstacle.addComponent(obs);
    }
    for(std::shared_ptr<MixedIntegerObstacle<T>> obs:_obstacles)
      for(int i=1; i<(int)_steps.size(); i++) {
        Eigen::Matrix<int,2,1> bodyXY(_smoothBody->getVar()(0,i),_smoothBody->getVar()(1,i));
        obs->addConstraint(objObstacle,i,bodyXY,clearance,bigM);
      }
    if(!solveGurobi(objObstacle,x))
      return false;
  } else {
    T bigM=getBigM();
    T clearance=getClearance();
    SQPObjectiveCompound<T> objObstacle=_obj;
    for(std::shared_ptr<MixedIntegerObstacle<T>> obs:_obstacles) {
      obs->clear();
      objObstacle.addComponent(obs);
    }
    std::unordered_map<std::shared_ptr<MixedIntegerObstacle<T>>,std::unordered_set<int>> consMap; //maintain already added constraints to avoid duplication
    bool more=true;
    do {
      if(!solveGurobi(objObstacle,x))
        return false;
      more=false; //test whether there are new constraint violations
      for(std::shared_ptr<MixedIntegerObstacle<T>> obs:_obstacles)
        for(int i=1; i<(int)_steps.size(); i++) {
          Eigen::Matrix<int,2,1> bodyXY(_smoothBody->getVar()(0,i),_smoothBody->getVar()(1,i));
          if(consMap[obs].find(i)==consMap[obs].end() && !obs->satisfyConstraint(x,bodyXY,clearance)) {
            obs->addConstraint(objObstacle,i,bodyXY,clearance,bigM);
            consMap[obs].insert(i);
            more=true;
          }
        }
    } while(more);
  }

  //assign
  for(int i=1; i<(int)_steps.size(); i++) {
    _steps[i]._bodyPose=_reachable[i-1]->getTorsoPose(x);
    _reachable[i-1]->getEEs(x,_steps[i]._eePose);
    _steps[i]._isEven=!_steps[i-1]._isEven;
  }
  if(interpolate)
    getPosesInterpolated();
  return true;
}
template <typename T>
void FootStepPlanner<T>::buildProblemFeasibility() {
  clearProblem();
  _terminalPositionObj.reset(new PolynomialObjective<T>(_obj,"BodyPoseFeasibility",PolyXT(),Vec2T(0,0)));
  _obj.addComponent(_terminalPositionObj);
  PolyXT objPoly;
  //body DOF
  {
    _smoothBody.reset(new LaplaceObjective<T,3>(_obj,"BodyPose",1,0));
    _obj.addComponent(_smoothBody);
    PolyXT diffX=_obj.addVarPoly(_smoothBody->getVar()(0,0))-PolyXT(_steps[0]._bodyPose[0]);
    PolyXT diffY=_obj.addVarPoly(_smoothBody->getVar()(1,0))-PolyXT(_steps[0]._bodyPose[1]);
    PolyXT diffT=_obj.addVarPoly(_smoothBody->getVar()(2,0))-PolyXT(_steps[0]._bodyPose[5]);
    objPoly+=diffX*diffX+diffY*diffY+diffT*diffT;
  }
  //foot DOF
  _smoothFoot.resize(_ees.size());
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    _smoothFoot[eid].reset(new LaplaceObjective<T,2>(_obj,"EndEffector"+std::to_string(eid),1,0));
    PolyXT diffX=_obj.addVarPoly(_smoothFoot[eid]->getVar()(0,0))-PolyXT(_steps[0]._eePose(0,eid));
    PolyXT diffY=_obj.addVarPoly(_smoothFoot[eid]->getVar()(1,0))-PolyXT(_steps[0]._eePose(1,eid));
    _obj.addComponent(_smoothFoot[eid]);
    objPoly+=diffX*diffX+diffY*diffY;
  }
  //reachability
  //T hTarget=bodyHeightTarget();
  //T hCoef=bodyHeightCoef();
  T refRotation=_steps[0]._bodyPose[5];
  std::vector<Eigen::Matrix<int,2,1>> eesDOF;
  for(int eid=0; eid<(int)_ees.size(); eid++)
    eesDOF.push_back(_smoothFoot[eid]->getVar().col(0));
  _reachable.push_back(std::shared_ptr<MixedIntegerReachable<T>>(new MixedIntegerReachable<T>(_obj,"EndEffector",resRotation(),refRotation,_env,true,_smoothBody->getVar().col(0),_ees,eesDOF)));
  PolyXT diffZ=_obj.addVarPoly(_reachable.back()->getBodyZ())-PolyXT(_steps[0]._bodyPose[2]);
  _obj.addComponent(_reachable.back());
  objPoly+=diffZ*diffZ;
  //if(hCoef>0)
  //  _reachable.back()->setTargetHeightEnergy(_obj,hTarget,hCoef);
  //add objective
  _terminalPositionObj->setPolynomialComponentObjective(objPoly);
}
template <typename T>
bool FootStepPlanner<T>::makeFeasible() {
  buildProblemFeasibility();
  //optimize
  Vec x;
  if(!solveGurobi(_obj,x))
    return false;

  //assign
  _steps[0]._bodyPose=_reachable[0]->getTorsoPose(x);
  _reachable[0]->getEEs(x,_steps[0]._eePose);
  return true;
}
template <typename T>
void FootStepPlanner<T>::update() {
  _steps[0]=_steps.back();
  _steps.resize(1);
}
template <typename T>
void FootStepPlanner<T>::update(const Vec& x) {
  _steps[0]=_steps.back();
  _steps[0]._bodyPose=x.template segment<6>(0);
  NEArticulatedGradientInfo<T> info(*_body,x);
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    const std::shared_ptr<EndEffectorBounds>& ee=_ees[eid];
    Mat3X4T trans=info.NEArticulatedGradientInfoMap<T>::getTrans(ee->jointId());
    _steps[0]._eePose.col(eid)=ROT(trans)*ee->_localPos.template cast<T>()+CTR(trans);
  }
  _steps.resize(1);
}
template <typename T>
void FootStepPlanner<T>::update(std::shared_ptr<FootStep<T>> step0) {
  _steps.assign(1,*step0);
}
template <typename T>
void FootStepPlanner<T>::addObstacle(std::shared_ptr<ShapeExact> obstacle) {
  std::string name="FootStepObstacle"+std::to_string(_obstacles.size());
  _obstacles.insert(std::shared_ptr<MixedIntegerObstacle<T>>(new MixedIntegerObstacle<T>(_obj,name,obstacle)));
}
template <typename T>
const std::unordered_set<std::shared_ptr<MixedIntegerObstacle<T>>>& FootStepPlanner<T>::getObstacles() const {
  return _obstacles;
}
template <typename T>
void FootStepPlanner<T>::getPosesInterpolated() {
  //main loop
  FootStep<T> s;
  int res=previewResolution();
  s._eePose.resize(3,(int)_ees.size());
  _xPoses.resize(_body->nrDOF(),((int)_steps.size()-1)*res);
  for(int i=0,off=0; i<(int)_steps.size()-1; i++)
    for(int t=0; t<res; t++,off++)
      _xPoses.col(off)=stepToDOF(_steps[i],_steps[i+1],t/(T)res);
}
//property
template <typename T>
void FootStepPlanner<T>::resRotation(int res) {
  put<int>(_pt,"resRotation",res);
}
template <typename T>
int FootStepPlanner<T>::resRotation() const {
  return get<int>(_pt,"resRotation",16);
}
template <typename T>
void FootStepPlanner<T>::horizon(int N) {
  put<int>(_pt,"horizon",N);
}
template <typename T>
int FootStepPlanner<T>::horizon() const {
  return get<int>(_pt,"horizon",5);
}
template <typename T>
void FootStepPlanner<T>::previewResolution(int res) {
  put<int>(_pt,"previewResolution",res);
}
template <typename T>
int FootStepPlanner<T>::previewResolution() const {
  return get<int>(_pt,"previewResolution",30);
}
template <typename T>
void FootStepPlanner<T>::laplaceCoef(T coef) {
  put<T>(_pt,"laplaceCoef",coef);
}
template <typename T>
T FootStepPlanner<T>::laplaceCoef() const {
  return get<T>(_pt,"laplaceCoef",1e-2f);
}
template <typename T>
void FootStepPlanner<T>::targetVelocity(const Vec3T& v) {
  putPtree<Vec3T>(_pt,"targetVelocity",v);
  ASSERT_MSG(v[2]>=0,"TargetVelocity can only be an energy!")
}
template <typename T>
typename FootStepPlanner<T>::Vec3T FootStepPlanner<T>::targetVelocity() const {
  return parsePtreeDef<Vec3T>(_pt,"targetVelocity",Vec3T(0,0,0));
}
template <typename T>
void FootStepPlanner<T>::targetHeading(const Vec2T& h) {
  putPtree<Vec2T>(_pt,"targetHeading",h);
  ASSERT_MSG(h[1]>=0,"TargetHeading can only be an energy!")
}
template <typename T>
typename FootStepPlanner<T>::Vec2T FootStepPlanner<T>::targetHeading() const {
  return parsePtreeDef<Vec2T>(_pt,"targetHeading",Vec2T(0,0));
}
template <typename T>
void FootStepPlanner<T>::terminalHeading(const Vec2T& h) {
  putPtree<Vec2T>(_pt,"terminalHeading",h);
  ASSERT_MSG(h[1]>=0,"TerminalHeading can only be an energy!")
}
template <typename T>
typename FootStepPlanner<T>::Vec2T FootStepPlanner<T>::terminalHeading() const {
  return parsePtreeDef<Vec2T>(_pt,"terminalHeading",Vec2T(0,0));
}
template <typename T>
void FootStepPlanner<T>::terminalPosition(const Vec3T& p) {
  putPtree<Vec3T>(_pt,"terminalPosition",p);
  ASSERT_MSG(p[2]>=0,"TerminalPosition can only be an energy!")
}
template <typename T>
typename FootStepPlanner<T>::Vec3T FootStepPlanner<T>::terminalPosition() const {
  Vec3T pos=parsePtreeDef<Vec3T>(_pt,"terminalPosition",Vec3T(0,0,0));
  Vec2T delta=pos.template segment<2>(0)-_steps[0]._bodyPose.template segment<2>(0);
  if(delta.norm()>limitDistance())
    delta*=limitDistance()/delta.norm();
  delta+=_steps[0]._bodyPose.template segment<2>(0);
  pos.template segment<2>(0)=delta;
  return pos;
}
template <typename T>
void FootStepPlanner<T>::lazyObstacle(bool lazyObstacle) {
  put<bool>(_pt,"lazyObstacle",lazyObstacle);
}
template <typename T>
bool FootStepPlanner<T>::lazyObstacle() const {
  return get<bool>(_pt,"lazyObstacle",false);
}
template <typename T>
void FootStepPlanner<T>::syncHeading(bool syncHeading) {
  put<bool>(_pt,"syncHeading",syncHeading);
}
template <typename T>
bool FootStepPlanner<T>::syncHeading() const {
  return get<bool>(_pt,"syncHeading",true);
}
template <typename T>
void FootStepPlanner<T>::limitDistance(T lmtVel) {
  put<T>(_pt,"limitDistance",lmtVel);
}
template <typename T>
T FootStepPlanner<T>::limitDistance() const {
  return get<T>(_pt,"limitDistance",std::numeric_limits<double>::max());
}
//helpler
template <typename T>
T FootStepPlanner<T>::getBigM() const {
  BBoxExact::Vec3T ext=_env->getBB().maxCorner()-_env->getBB().minCorner();
  return ext.template cast<T>().norm();
}
template <typename T>
T FootStepPlanner<T>::getClearance() const {
  BBoxExact bb;
  for(const std::shared_ptr<EndEffectorBounds>& ee:_ees) {
    for(int x: {
          ee->_bb[0],ee->_bb[3]
        })
      for(int y: {
            ee->_bb[1],ee->_bb[4]
          })
        for(int z: {
              ee->_bb[2],ee->_bb[5]
            })
          bb.setUnion((ee->_frame*EndEffectorBounds::Vec3T(x,y,z)*ee->_res+ee->_ctrBB).template cast<BBoxExact::T>());
  }
  return (T)(bb.maxCorner()-bb.minCorner()).template cast<T>().norm()/2;
}
template <typename T>
T FootStepPlanner<T>::updateHeading(const Vec2T& dir,T eps) const {
  if(dir.norm()<eps)
    return _steps[0]._bodyPose[5];
  Vec3T posX=_ees[0]->_frame.col(0).template cast<T>();
  Mat3T R=expWZ<T,Vec3T>(_steps[0]._bodyPose[5],NULL);
  Vec2T currDir=(R*posX).template segment<2>(0);
  Vec2T currDirPerp(-currDir[1],currDir[0]);
  return atan2(dir.dot(currDirPerp),dir.dot(currDir))+_steps[0]._bodyPose[5];
}
template class FootStepPlanner<FLOAT>;
}
