#include "SIPPlannerDiscretize.h"
#include <SIPCollision/TargetEnergy.h>
#include <SIPCollision/GravityEnergy.h>
#include <SIPCollision/SmoothnessEnergy.h>
#include <SIPCollision/JointLimitEnergy.h>
#include <SIPCollision/CollisionBarrierEnergy.h>
#include <Articulated/ArticulatedBody.h>
#include <Articulated/ArticulatedUtils.h>
#include <Articulated/ArticulatedLoader.h>
#include <Utils/Timing.h>
#include <Utils/Utils.h>
#include <random>
#include <algorithm>

namespace PHYSICSMOTION {
//initialize
template <typename T, typename CCPlaneT>
SIPPlannerDiscretize<T,CCPlaneT>::SIPPlannerDiscretize(T d0,T x0Obs,T x0Self,T l2,T eta,T terminateGrad, T minEig, bool useConvexHullObs,bool useConvexHullSelf,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD)
  :_useConvexHullObs(useConvexHullObs),_useConvexHullSelf(useConvexHullSelf),_useAlphaReset(true),_terminateGrad(terminateGrad),_minEig(minEig) {
  _handlerObs.reset(new CollisionHandler<T,CCPlaneT>(d0,x0Obs,l2,eta));
  _handlerObs->setCCDObs(x0Obs>=0);
  _handlerObs->setCCDSelf(false);

  _handlerSelf.reset(new CollisionHandler<T, CCPlaneT>(d0,x0Self,l2,eta,skipSelfCCD));
  _handlerSelf->setCCDObs(false);
  _handlerSelf->setCCDSelf(x0Self>=0);
}
template <typename T, typename CCPlaneT>
SIPPlannerDiscretize<T, CCPlaneT>::SIPPlannerDiscretize(std::shared_ptr<ArticulatedBody> body,int order,int totalTime,
    T d0,T x0Obs,T x1Self,T l2,T eta,T terminateGrad, T minEig, T randomInitialize,bool useConvexHullObs,bool useConvexHullSelf,bool neutralInit,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCDf)
  :_useConvexHullObs(useConvexHullObs),_useConvexHullSelf(useConvexHullSelf),_useAlphaReset(true),_terminateGrad(terminateGrad),_minEig(minEig) {
  _handlerObs.reset(new CollisionHandler<T,CCPlaneT>(body,order,totalTime,d0,x0Obs,l2,eta,randomInitialize,neutralInit));
  _handlerObs->setCCDObs(x0Obs>=0);
  _handlerObs->setCCDSelf(false);

  _handlerSelf.reset(new CollisionHandler<T, CCPlaneT>(body,order,totalTime,d0,x1Self,l2,eta,randomInitialize,neutralInit,skipSelfCCDf));
  _handlerSelf->setCCDObs(false);
  _handlerSelf->setCCDSelf(x1Self>=0);
}
template <typename T, typename CCPlaneT>
bool SIPPlannerDiscretize<T, CCPlaneT>::read(std::istream& is,IOData* dat) {
  registerType<ArticulatedBody>(dat);
  registerType<CollisionHandler<T, CCPlaneT>>(dat);
  readBinaryData(_fixVar,is);
  readBinaryData(_useConvexHullObs,is);
  readBinaryData(_useConvexHullSelf,is);
  readBinaryData(_useAlphaReset,is);
  _handlerObs->read(is,dat);
  _handlerSelf->read(is,dat);
  return is.good();
}
template <typename T, typename CCPlaneT>
bool SIPPlannerDiscretize<T, CCPlaneT>::write(std::ostream& os,IOData* dat) const {
  registerType<ArticulatedBody>(dat);
  registerType<CollisionHandler<T, CCPlaneT>>(dat);
  writeBinaryData(_fixVar,os);
  writeBinaryData(_useConvexHullObs,os);
  writeBinaryData(_useConvexHullSelf,os);
  writeBinaryData(_useAlphaReset,os);
  _handlerObs->write(os,dat);
  _handlerSelf->write(os,dat);
  return os.good();
}
template <typename T, typename CCPlaneT>
std::shared_ptr<SerializableBase> SIPPlannerDiscretize<T, CCPlaneT>::copy() const {
  return std::shared_ptr<SerializableBase>(new SIPPlannerDiscretize(_handlerObs->d0(),_handlerObs->x0(),_handlerSelf->x0(),_handlerObs->l2(),_handlerObs->eta(),
         _terminateGrad, _minEig,_useConvexHullObs, _useConvexHullSelf, _handlerSelf->getSkipJIDPairs()));
}
template <typename T, typename CCPlaneT>
std::string SIPPlannerDiscretize<T, CCPlaneT>::type() const {
  return typeid(SIPPlannerDiscretize).name();
}
//setup
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit) {
  _handlerObs->setRobot(body,randomInitialize,neutralInit);
  _handlerSelf->setRobot(body,randomInitialize,neutralInit);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF) {
  _handlerObs->addRobot(body,DOF);
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot,int maxConvexHulls) {
  _handlerObs->addObject(path,scale,pos,rot,maxConvexHulls);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::addSphere(T r,int res,const Vec3T& pos) {
  _handlerObs->addSphere(r,res,pos);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& R) {
  _handlerObs->addCuboid(l,w,h,pos,R);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res) {
  _handlerObs->addCapsule(a,b,radius,res);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::addCapsule(T l,T w,T h,Vec3T pos,T radius,int res) {
  _handlerObs->addCapsule(l,w,h,pos,radius,res);
}
template <typename T, typename CCPlaneT>
std::vector<std::shared_ptr<MeshExact>> SIPPlannerDiscretize<T, CCPlaneT>::getObstacles() const {
  return _handlerObs->getObstacles();
}
template <typename T, typename CCPlaneT>
const typename SIPPlannerDiscretize<T, CCPlaneT>::Vec& SIPPlannerDiscretize<T, CCPlaneT>::getControlPoints() const {
  return _handlerObs->getControlPoints();
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::setControlPoints(const Vec& newCP) {
  updateControlPoints(newCP);
  assembleBodyBVH(_useConvexHullObs,_useConvexHullSelf);
  updateControlPoints(newCP);
}
template <typename T, typename CCPlaneT>
const ThetaTrajectory<T>& SIPPlannerDiscretize<T, CCPlaneT>::getThetaTrajectory() const {
  return _handlerObs->getThetaTrajectory();
}
template <typename T, typename CCPlaneT>
std::shared_ptr<ArticulatedBody> SIPPlannerDiscretize<T, CCPlaneT>::getBody() const  {
  return _handlerObs->getBody();
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::assembleBodyBVH(bool useConvexHullObs,bool useConvexHullSelf) {
  _handlerObs->assembleBodyBVH(useConvexHullObs);
  _handlerSelf->assembleBodyBVH(useConvexHullSelf);
}
//optimization
template <typename T, typename CCPlaneT>
std::shared_ptr<CollisionHandler<T, CCPlaneT>> SIPPlannerDiscretize<T, CCPlaneT>::getHandlerObs() const {
  return _handlerObs;
}
template <typename T, typename CCPlaneT>
std::shared_ptr<CollisionHandler<T, CCPlaneT>> SIPPlannerDiscretize<T, CCPlaneT>::getHandlerSelf() const {
  return _handlerSelf;
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::clearEnergy() {
  _trajEss.clear();
}
template <typename T, typename CCPlaneT>
std::shared_ptr<CollisionBarrierEnergy<T,typename SIPPlannerDiscretize<T, CCPlaneT>::Barrier,CCPlaneT>> SIPPlannerDiscretize<T, CCPlaneT>::getObsBarrierEnergy() const {
  for(auto e:_trajEss) {
    bool isObs=true;
    if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)) {
      //determine the type of collision barrier function
      if(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerObs->getObsTTPairs()))
        isObs=true;
      else {
        ASSERT_MSG(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerSelf->getSelfTTPairs()),
                   "Strange error, energy bound to neither handlerObs nor handlerSelf")
        isObs=false;
      }
      if(isObs)
        return std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e);
    }
  }
  return NULL;
}
template <typename T, typename CCPlaneT>
std::shared_ptr<CollisionBarrierEnergy<T,typename SIPPlannerDiscretize<T, CCPlaneT>::Barrier, CCPlaneT>> SIPPlannerDiscretize<T, CCPlaneT>::getSelfBarrierEnergy() const {
  for(auto e:_trajEss) {
    bool isObs=true;
    if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)) {
      //determine the type of collision barrier function
      if(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerObs->getObsTTPairs()))
        isObs=true;
      else {
        ASSERT_MSG(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerSelf->getSelfTTPairs()),
                   "Strange error, energy bound to neither handlerObs nor handlerSelf")
        isObs=false;
      }
      if(!isObs)
        return std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e);
    }
  }
  return NULL;
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::setupEnergy(int JID,const Vec3T& tar,const Vec3T& dir,const Vec3T& ori,T targetCoefP,T targetCoefD,bool JTJApprox) {
  if(targetCoefP>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new TargetEnergy<T>(*(_handlerObs->getBody()),
                                            _handlerObs->getControlPoints(),
                                            _handlerObs->getThetaTrajectory(),
                                            JID,tar,dir,ori,targetCoefP,targetCoefD,JTJApprox)));
  else if(targetCoefD>0) {
    std::set<int> children=_handlerObs->getBody()->children(JID);
    children.insert(JID);
    for(int CID:children)
      if(_handlerObs->getBody()->joint(CID)._mesh)
        _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                           (new GravityEnergy<T>(*(_handlerObs->getBody()),
                               _handlerObs->getControlPoints(),
                               _handlerObs->getThetaTrajectory(),
                               CID,dir,targetCoefD,JTJApprox)));
  }
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::setupEnergy(T smoothnessCoef,T obsBarrierCoef,T selfBarrierCoef,T limitCoef,bool implicit,bool JTJApprox) {
  if(obsBarrierCoef>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new CollisionBarrierEnergy<T, Barrier, CCPlaneT>(*(_handlerObs->getBody()),
                           _handlerObs->getControlPoints(),
                           _handlerObs->getThetaTrajectory(),
                           _handlerObs->getObsTTPairs(),
                           _handlerObs->getObsCCPlanes(),
                           _handlerObs->getInfoLookup(),
                           _handlerObs->d0(),
                           _handlerObs->x0(),
                           obsBarrierCoef,JTJApprox,implicit)));
  if(selfBarrierCoef>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new CollisionBarrierEnergy<T, Barrier, CCPlaneT>(*(_handlerObs->getBody()),
                           _handlerSelf->getControlPoints(),
                           _handlerSelf->getThetaTrajectory(),
                           _handlerSelf->getSelfTTPairs(),
                           _handlerSelf->getSelfCCPlanes(),
                           _handlerSelf->getInfoLookup(),
                           _handlerSelf->d0(),
                           _handlerSelf->x0(),
                           selfBarrierCoef,JTJApprox,implicit)));
  if(smoothnessCoef>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new SmoothnessEnergy<T>(*(_handlerObs->getBody()),
                           _handlerObs->getControlPoints(),
                           _handlerObs->getThetaTrajectory(),smoothnessCoef,1)));
  if(limitCoef>0)
    _trajEss.push_back(std::shared_ptr<TrajectorySIPEnergy<T>>
                       (new JointLimitEnergy<T>(*(_handlerObs->getBody()),
                           _handlerObs->getControlPoints(),
                           _handlerObs->getThetaTrajectory(),limitCoef)));
}

template <typename T, typename CCPlaneT>
bool SIPPlannerDiscretize<T, CCPlaneT>::optimize(int maxIter,bool output,bool newtonMethod) {
  MatT h;
  Vec newX,g,d;
  int succLineSearch=0;
  int alphaResetInterval=10;
  bool safe=false,updateCCPlanes=true;
  Vec x=_handlerObs->getControlPoints();
  T e=0,alpha=1,newAlpha;
  if(output) {
    std::cout<<" ControlPoints before optimization="<<_handlerObs->getControlPoints().transpose()
             <<" #obsBVHBody="<<_handlerObs->getBVHBody().size()<<" #selfBVHBody="<<_handlerSelf->getBVHBody().size()<<std::endl;
  }
  //initialize
  if(hasCollisionEnergy()) {
    if(output)
      std::cout<<"Feasible Initialization for HandlerObs"<<std::endl;
    _handlerObs->exhaustiveSubdivide(false,true);
    if(output)
      std::cout<<"Feasible Initialization for HandlerSelf"<<std::endl;
    _handlerSelf->exhaustiveSubdivide(false,true);
    _handlerObs->getObsCCPlanes().clear();
    _handlerSelf->getSelfCCPlanes().clear();
  }
  //main loop
  if(output)
    recreate("iterations");
  for(int iter=0; iter<maxIter; iter++) {
    if(output) {
      /*
      char mesg[20];
      sprintf(mesg,"%s","Iter:");
      sprintf(mesg+strlen("Iter:"),"%d",iter);
      IMPORTANT_MESG(mesg);*/
      std::cout<<std::endl<<"Iter: "<<iter<<std::endl;
      //writeStr("iterations/iter"+std::to_string(iter)+".dat");
    }
    //find search direction
    TBEG("Iteration"+std::to_string(iter));
    assembleEnergy(x,&e,&g,newtonMethod? &h: NULL,updateCCPlanes,output,true);
    LineSearchState state = computeNewX(e,g,h, d, newX=x, newAlpha=alpha, newtonMethod, output);
    //debug(&x,&d,nullptr,1e-10);
    if(state==LSS_Fail) {
      if(output)
        std::cout<<"Iter="<<iter<<" lineSearch failed!"<<std::endl;
      writeConfiguration(x);
      debug(&x,&d,&newAlpha,1e-10);
      if(output) {
        std::cout<<"ControlPoints after optimization="<<_handlerObs->getControlPoints().transpose()
                 <<" #obsBVHBody="<<_handlerObs->getBVHBody().size()<<" #selfBVHBody="<<_handlerSelf->getBVHBody().size()<<std::endl;
      }
      TEND();
      return false;
    }
    if(state==LSS_CCUpdate) {
      if(output) {
        std::cout<<"Iter="<<iter<<" CCUpdate, re-evaluate!"<<std::endl;
      }
      TEND();
      continue;
    }
    ASSERT(state==LSS_Succ)
    //safety check (no need to build tri pairs)
    if(hasCollisionEnergy()) {
      safe=_handlerObs->CCD(true,false,false);
      safe=_handlerSelf->CCD(true,false,false) && safe;
    }
    //safe=CollisionHandler<T, CCPlaneT>::CCD(true,false,false);
    if(!hasCollisionEnergy() || safe) {
      if(output)
        std::cout<<"Iter="<<iter<<" CCD pass, alpha="<<alpha
                 <<" gNorm="<<g.cwiseAbs().maxCoeff()
                 <<" dNorm="<<d.cwiseAbs().maxCoeff()
                 <<" E="<<e<<" length="<<getThetaTrajectory().getLength(newX)<<" target="<<getTargetEnergy()<<" smoothness="<<getSmoothnessEnergy()<<std::endl;
      if(isZeroGrad(g)) {
        //_handlerObs->setControlPoints(x);
        //_handlerSelf->setControlPoints(x);
        updateControlPoints(x);
        if(output) {
          std::cout<<"ControlPoints after optimization="<<_handlerObs->getControlPoints().transpose()
                   <<" #obsBVHBody="<<_handlerObs->getBVHBody().size()<<" #selfBVHBody="<<_handlerSelf->getBVHBody().size()<<std::endl;
        }
        TEND();
        writeStr("iterations/final_state.dat");
        return true;
      }
      //CCD pass, apply new solution
      updateCCPlanes=true;
      alpha=newAlpha;
      updateInOptimize(newX, x);
      //occasionally reset alpha
      if(_useAlphaReset && ((++succLineSearch)%alphaResetInterval)==0) {
        if(output)
          std::cout<<"alpha reset!"<<std::endl;
        succLineSearch=0;
        alpha=1;
      }
    } else {
      if(output)
        std::cout<<"Iter="<<iter<<" CCD fail, alpha="<<alpha
                 <<" #obs-subd="<<_handlerObs->getSubdivideOffsets().size()<<(_handlerObs->penetrated()? " Penetrated": " Not penetrated")
                 <<" #self-subd="<<_handlerSelf->getSubdivideOffsets().size()<<(_handlerSelf->penetrated()? " Penetrated": " Not penetrated")<<std::endl;
      //CCD fail, subdivide and try again, use alpha as hint
      updateCCPlanes=true;
      alpha=std::min<T>(newAlpha,alpha);
      if(_handlerObs->penetrated() || _handlerSelf->penetrated())
        alpha*=0.6;
      _handlerObs->subdivide(ST_All,output);
      _handlerSelf->subdivide(ST_All,output);
    }
    TEND();
  }
  updateControlPoints(x);
  if(output) {
    std::cout<<"ControlPoints after optimization="<<_handlerObs->getControlPoints().transpose()
             <<" #obsBVHBody="<<_handlerObs->getBVHBody().size()<<" #selfBVHBody="<<_handlerSelf->getBVHBody().size()<<std::endl;
  }
  writeStr("iterations/final_state.dat");
  return false;
}

template <typename T, typename CCPlaneT>
bool SIPPlannerDiscretize<T, CCPlaneT>::isZeroGrad(const Vec& g) {
  return g.cwiseAbs().maxCoeff()<_terminateGrad;
}

template <typename T, typename CCPlaneT>
LineSearchState SIPPlannerDiscretize<T, CCPlaneT>::computeNewX(T &e, const Vec &g, const MatT &h,
    Vec& d, Vec &newX, T &alpha,
    bool newtonMethod, bool debug) {
  solveSearchDirection(g,h,d,newtonMethod);
  //line search
  LineSearchState state=lineSearch(e,g,d,alpha,newX, debug);
  return state;
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::solveSearchDirection(const Vec& G, const MatT& H, Vec& d, bool newtonMethod) {
  if(newtonMethod) {
    _sol.compute(H.template cast<double>(),Eigen::ComputeEigenvectors);
    T minEigIter=std::max<T>(_minEig,_minEig*_sol.eigenvalues().maxCoeff());
    d=-(_sol.eigenvectors().template cast<T>()*
        (_sol.eigenvalues().template cast<T>().cwiseMax(minEigIter).asDiagonal().inverse()*
         (_sol.eigenvectors().transpose().cast<T>()*G).eval()).eval()).eval();
  } else d=-G;
}


template<typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::updateInLineSearch(const Vec &x,const Vec &d, const T &alpha, Vec &newX) {
  newX=x+alpha*d;
}
template<typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::updateInOptimize(const Vec &newX, Vec &x) {
  x = newX;
}

template <typename T, typename CCPlaneT>
bool SIPPlannerDiscretize<T, CCPlaneT>::armijoConditionCheck(T deltaE, const Vec &g, const Vec &d, T alpha, T c) {
  /*
  std::cout<<"alpha="<<alpha<<std::endl;
  std::cout<<"deltaE="<<deltaE<<std::endl;
  std::cout<<"deltaE/alpha="<<deltaE/alpha<<std::endl;
  std::cout<<"g.dot(d)="<<g.dot(d)<<std::endl;
  std::cout<<"c*g.dot(d)*alpha="<<c*g.dot(d)*alpha<<std::endl;
  */
  return deltaE < c*g.dot(d)*alpha;
}

template <typename T, typename CCPlaneT>
LineSearchState SIPPlannerDiscretize<T, CCPlaneT>::lineSearch(T& newE,const Vec& g,const Vec& d,T& alpha,Vec& newX,bool output) {
  //we want to find: x+alpha*d
  T E=newE;
  Vec x=newX;
  //For the Wolfe's condition, we use an extremely small value of "c" to be more resilient to low floating point precision
  T alphaMin=1e-10,alphaDec=0.6,alphaInc=1.1,c=1e-5;//0.01;
  std::cout<<"d.dot(g)="<<d.dot(g)<<std::endl;
  while(alpha>alphaMin) {
    updateInLineSearch(x, d, alpha, newX);
    LineSearchState ret=assembleEnergy(newX,&newE,NULL,NULL,false,output);
    if(ret==LSS_CCUpdate) {
      return LSS_CCUpdate;
    } else if(ret==LSS_Succ && armijoConditionCheck(newE-E, g,d, alpha, c)) {
      alpha*=alphaInc;
      return LSS_Succ;
    } else {
      alpha*=alphaDec;
    }
  }
  return LSS_Fail;
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::fixGrad(Vec* G,MatT* H) const {
  if(G)
    for (const auto &fixLoc: _fixVar)
      (*G)[fixLoc] = 0;
  if(H)
    for(const auto& fixLoc:_fixVar) {
      (*H).col(fixLoc).setZero();
      (*H).row(fixLoc).setZero();
      (*H)(fixLoc,fixLoc)=1.;
    }
}

template <typename T, typename CCPlaneT>
bool SIPPlannerDiscretize<T, CCPlaneT>::hasCollisionEnergy() const {
  for(auto e:_trajEss)
    if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e))
      return true;
  return false;
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::fixThetaAt(T t) {
  std::unordered_set<int> fixVar(_fixVar.begin(),_fixVar.end());
  //find related variable by perturbation analysis and fix
  Vec theta=Vec::Zero(_handlerObs->getThetaTrajectory().getNumDOF());
  std::cout<<"theta_size="<<theta.size()<<std::endl;
  Vec pt=_handlerObs->getThetaTrajectory().getPoint(theta,t);
  for(int i=0; i<(int)theta.size(); ++i) {
    Vec tmpTheta=theta;
    tmpTheta[i]+=1;
    Vec pt2=_handlerObs->getThetaTrajectory().getPoint(tmpTheta,t);
    if(pt2!=pt)
      fixVar.insert(
        i);
  }
  _fixVar.assign(fixVar.begin(),fixVar.end());
  std::sort(_fixVar.begin(), _fixVar.end());
}

template <typename T,  typename CCPlaneT>
void SIPPlannerDiscretize<T,CCPlaneT>::initializeTheta(const Vec& theta) {
  Vec thetaAll=Vec::Zero(_handlerObs->getThetaTrajectory().getNumDOF());
  for(int i=0; i<thetaAll.size(); i+=theta.size())
    thetaAll.segment(i,theta.size())=theta;
  _handlerObs->setControlPoints(thetaAll);
  _handlerSelf->setControlPoints(thetaAll);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T,CCPlaneT>::setUseAlphaReset(bool use) {
  _useAlphaReset=use;
}
//debug
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::debug(const Vec* cp, const Vec* d, const T* alpha, T perturbRange) {
  Vec controlPoints;
  if(!cp)
    controlPoints=Vec::Random(_handlerObs->getThetaTrajectory().getNumDOF())*perturbRange;
  else controlPoints=*cp;
  std::cout<<"controlPoints="<<controlPoints.transpose()<<std::endl;
  //turn on output mode
  //for(auto e:_trajEss)
  //  if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e))
  //    std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->setOutput(true);
  //test energy
  T E,E2;
  MatT H,H2;
  Vec G,G2,dx;
  std::cout<<"=========================Assemble the first time============================"<<std::endl;
  if(assembleEnergy(controlPoints,&E,&G,&H,true,true,true, true)!=LSS_Succ) {
    std::cout<<"Debug failed!"<<std::endl;
    return;
  }
  std::vector<T> trajEss=_trajEssVals;
  std::vector<Vec> trajGss=_trajGssVals;
  std::vector<MatT> trajHss=_trajHssVals;
  //perturb and evaluate again
  DEFINE_NUMERIC_DELTA_T(T)
  T delta;
  if(!alpha) delta = DELTA;
  else delta = *alpha;
  if(!d) {
    dx=Vec::Random(_handlerObs->getThetaTrajectory().getNumDOF());
    for(const auto& fixLoc:_fixVar) dx(fixLoc)=0;
  } else dx = *d;
  controlPoints+=dx*delta;
  std::cout<<"=========================Assemble the second time============================"<<std::endl;
  if(assembleEnergy(controlPoints,&E2,&G2,&H2,true,true,true, true)!=LSS_Succ) {
    std::cout<<"Debug failed!"<<std::endl;
    return;
  }
  std::vector<T> trajEss2=_trajEssVals;
  std::vector<Vec> trajGss2=_trajGssVals;
  std::vector<MatT> trajHss2=_trajHssVals;
  //output
  std::cout<<"delta="<<delta<<std::endl;
  std::cout<<"E="<<std::setprecision(32)<<E<<std::endl;
  std::cout<<"E1="<<std::setprecision(32)<<E2<<std::endl;
  std::cout<<"G="<<std::setprecision(32)<<(G).dot(dx)<<std::endl;
  std::cout<<"G2="<<std::setprecision(32)<<(G2).dot(dx)<<std::endl;
  std::cout<<"G1="<<std::setprecision(32)<<(E2-E)/delta<<std::endl;
  std::cout<<"H="<<std::setprecision(32)<<((H)*dx).norm()<<std::endl;
  std::cout<<"H1="<<std::setprecision(32)<<((G2-G)/delta).norm()<<std::endl;
  std::cout<<"H2="<<std::setprecision(32)<<((H2)*dx).norm()<<std::endl;
  DEBUG_GRADIENT("dE",(G).dot(dx),(G).dot(dx)-(E2-E)/delta)
  DEBUG_GRADIENT("dG",((H)*dx).norm(),((H)*dx-(G2-G)/delta).norm())
  T totE = 0, totE2=0;
  Vec totG,totG2;
  totG.setZero(G.size());
  totG2.setZero(G.size());
  MatT totH,totH2;
  totH.setZero(G.size(), G.size());
  totH2.setZero(G.size(), G.size());
  for(int i=0; i<(int)_trajEss.size(); i++) {
    const auto& e = _trajEss[i];
    totE += trajEss[i];
    totE2 += trajEss2[i];
    totG += trajGss[i];
    totG2 += trajGss2[i];
    totH += trajHss[i];
    totH2 += trajHss2[i];
    if(std::dynamic_pointer_cast<TargetEnergy<T>>(e)) {
      std::cout<<"TargetE="<<std::setprecision(32)<<trajEss[i]<<std::endl;
      std::cout<<"TargetE1="<<std::setprecision(32)<<trajEss2[i]<<std::endl;
      std::cout<<"TargetG="<<std::setprecision(32)<<(trajGss[i]).dot(dx)<<std::endl;
      std::cout<<"TargetG2="<<std::setprecision(32)<<(trajGss2[i]).dot(dx)<<std::endl;
      std::cout<<"TargetG1="<<std::setprecision(32)<<(trajEss2[i]-trajEss[i])/delta<<std::endl;
      std::cout<<"TargetH="<<std::setprecision(32)<<((trajHss[i])*dx).norm()<<std::endl;
      std::cout<<"TargetH1="<<std::setprecision(32)<<((trajGss2[i]-trajGss[i])/delta).norm()<<std::endl;
      std::cout<<"TargetH2="<<std::setprecision(32)<<((trajHss2[i])*dx).norm()<<std::endl;
      DEBUG_GRADIENT("dTragetE",(trajGss[i]).dot(dx),(trajGss[i]).dot(dx)-(trajEss2[i]-trajEss[i])/delta)
      DEBUG_GRADIENT("dTargetG",((trajHss[i])*dx).norm(),((trajHss[i])*dx-(trajGss2[i]-trajGss[i])/delta).norm())
    } else if(std::dynamic_pointer_cast<GravityEnergy<T>>(e)) {
      std::cout<<"GravityE="<<std::setprecision(32)<<trajEss[i]<<std::endl;
      std::cout<<"GravityE1="<<std::setprecision(32)<<trajEss2[i]<<std::endl;
      std::cout<<"GravityG="<<std::setprecision(32)<<(trajGss[i]).dot(dx)<<std::endl;
      std::cout<<"GravityG2="<<std::setprecision(32)<<(trajGss2[i]).dot(dx)<<std::endl;
      std::cout<<"GravityG1="<<std::setprecision(32)<<(trajEss2[i]-trajEss[i])/delta<<std::endl;
      std::cout<<"GravityH="<<std::setprecision(32)<<((trajHss[i])*dx).norm()<<std::endl;
      std::cout<<"GravityH1="<<std::setprecision(32)<<((trajGss2[i]-trajGss[i])/delta).norm()<<std::endl;
      std::cout<<"GravityH2="<<std::setprecision(32)<<((trajHss2[i])*dx).norm()<<std::endl;
      DEBUG_GRADIENT("dGravityE",(trajGss[i]).dot(dx),(trajGss[i]).dot(dx)-(trajEss2[i]-trajEss[i])/delta)
      DEBUG_GRADIENT("dGravityG",((trajHss[i])*dx).norm(),((trajHss[i])*dx-(trajGss2[i]-trajGss[i])/delta).norm())
    } else if(std::dynamic_pointer_cast<JointLimitEnergy<T>>(e)) {
      std::cout<<"JointLimitE="<<std::setprecision(32)<<trajEss[i]<<std::endl;
      std::cout<<"JointLimitE1="<<std::setprecision(32)<<trajEss2[i]<<std::endl;
      std::cout<<"JointLimitG="<<std::setprecision(32)<<(trajGss[i]).dot(dx)<<std::endl;
      std::cout<<"JointLimitG2="<<std::setprecision(32)<<(trajGss2[i]).dot(dx)<<std::endl;
      std::cout<<"JointLimitG1="<<std::setprecision(32)<<(trajEss2[i]-trajEss[i])/delta<<std::endl;
      std::cout<<"JointLimitH="<<std::setprecision(32)<<((trajHss[i])*dx).norm()<<std::endl;
      std::cout<<"JointLimitH1="<<std::setprecision(32)<<((trajGss2[i]-trajGss[i])/delta).norm()<<std::endl;
      std::cout<<"JointLimitH2="<<std::setprecision(32)<<((trajHss2[i])*dx).norm()<<std::endl;
      DEBUG_GRADIENT("dJointLimitE",(trajGss[i]).dot(dx),(trajGss[i]).dot(dx)-(trajEss2[i]-trajEss[i])/delta)
      DEBUG_GRADIENT("dJointLimitG",((trajHss[i])*dx).norm(),((trajHss[i])*dx-(trajGss2[i]-trajGss[i])/delta).norm())
    } else if(std::dynamic_pointer_cast<SmoothnessEnergy<T>>(e)) {
      std::cout<<"SmoothnessE="<<std::setprecision(32)<<trajEss[i]<<std::endl;
      std::cout<<"SmoothnessE1="<<std::setprecision(32)<<trajEss2[i]<<std::endl;
      std::cout<<"SmoothnessG="<<std::setprecision(32)<<(trajGss[i]).dot(dx)<<std::endl;
      std::cout<<"SmoothnessG2="<<std::setprecision(32)<<(trajGss2[i]).dot(dx)<<std::endl;
      std::cout<<"SmoothnessG1="<<std::setprecision(32)<<(trajEss2[i]-trajEss[i])/delta<<std::endl;
      std::cout<<"SmoothnessH="<<std::setprecision(32)<<((trajHss[i])*dx).norm()<<std::endl;
      std::cout<<"SmoothnessH1="<<std::setprecision(32)<<((trajGss2[i]-trajGss[i])/delta).norm()<<std::endl;
      std::cout<<"SmoothnessH2="<<std::setprecision(32)<<((trajHss2[i])*dx).norm()<<std::endl;
      DEBUG_GRADIENT("dSmoothnessE",(trajGss[i]).dot(dx),(trajGss[i]).dot(dx)-(trajEss2[i]-trajEss[i])/delta)
      DEBUG_GRADIENT("dSmoothnessG",((trajHss[i])*dx).norm(),((trajHss[i])*dx-(trajGss2[i]-trajGss[i])/delta).norm())
    } else {
      if(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerObs->getObsTTPairs())) {
        std::cout<<"BarrierObsE="<<std::setprecision(32)<<trajEss[i]<<std::endl;
        std::cout<<"BarrierObsE1="<<std::setprecision(32)<<trajEss2[i]<<std::endl;
        std::cout<<"BarrierObsG="<<std::setprecision(32)<<(trajGss[i]).dot(dx)<<std::endl;
        std::cout<<"BarrierObsG2="<<std::setprecision(32)<<(trajGss2[i]).dot(dx)<<std::endl;
        std::cout<<"BarrierObsG1="<<std::setprecision(32)<<(trajEss2[i]-trajEss[i])/delta<<std::endl;
        std::cout<<"BarrierObsH="<<std::setprecision(32)<<((trajHss[i])*dx).norm()<<std::endl;
        std::cout<<"BarrierObsH1="<<std::setprecision(32)<<((trajGss2[i]-trajGss[i])/delta).norm()<<std::endl;
        std::cout<<"BarrierObsH2="<<std::setprecision(32)<<((trajHss2[i])*dx).norm()<<std::endl;
        DEBUG_GRADIENT("dBarrierObsE",(trajGss[i]).dot(dx),(trajGss[i]).dot(dx)-(trajEss2[i]-trajEss[i])/delta)
        DEBUG_GRADIENT("dBarrierObsG",((trajHss[i])*dx).norm(),((trajHss[i])*dx-(trajGss2[i]-trajGss[i])/delta).norm())
      } else {
        std::cout<<"BarrierSelfE="<<std::setprecision(32)<<trajEss[i]<<std::endl;
        std::cout<<"BarrierSelfE1="<<std::setprecision(32)<<trajEss2[i]<<std::endl;
        std::cout<<"BarrierSelfG="<<std::setprecision(32)<<(trajGss[i]).dot(dx)<<std::endl;
        std::cout<<"BarrierSelfG2="<<std::setprecision(32)<<(trajGss2[i]).dot(dx)<<std::endl;
        std::cout<<"BarrierSelfG1="<<std::setprecision(32)<<(trajEss2[i]-trajEss[i])/delta<<std::endl;
        std::cout<<"BarrierSelfH="<<std::setprecision(32)<<((trajHss[i])*dx).norm()<<std::endl;
        std::cout<<"BarrierSelfH1="<<std::setprecision(32)<<((trajGss2[i]-trajGss[i])/delta).norm()<<std::endl;
        std::cout<<"BarrierSelfH2="<<std::setprecision(32)<<((trajHss2[i])*dx).norm()<<std::endl;
        DEBUG_GRADIENT("dBarrierSelfE",(trajGss[i]).dot(dx),(trajGss[i]).dot(dx)-(trajEss2[i]-trajEss[i])/delta)
        DEBUG_GRADIENT("dBarrierSelfG",((trajHss[i])*dx).norm(),((trajHss[i])*dx-(trajGss2[i]-trajGss[i])/delta).norm())
      }
    }
  }
  std::cout<<"totE="<<std::setprecision(32)<<totE<<std::endl;
  std::cout<<"totE2="<<std::setprecision(32)<<totE2<<std::endl;
  std::cout<<"totG="<<std::setprecision(32)<<totG.dot(dx)<<std::endl;
  std::cout<<"totG2="<<std::setprecision(32)<<totG2.dot(dx)<<std::endl;
  std::cout<<"totH="<<std::setprecision(32)<<(totH*dx).norm()<<std::endl;
  std::cout<<"totH2="<<std::setprecision(32)<<(totH2*dx).norm()<<std::endl;
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::writeConfiguration(const Vec& x,const std::string& path) {
  _handlerObs->setControlPoints(x);
  _handlerSelf->setControlPoints(x);
  std::ofstream os(path,std::ios::binary);
  writeStr(path);
  //debug(&x,nullptr,nullptr,1e-5);
}
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::debugAtConfiguration(T perturbRange,const std::string& path) {
  if(exists(path)) {
    readStr(path);
    Vec x=_handlerObs->getControlPoints();
    debug(&x,nullptr,nullptr,perturbRange);
    exit(EXIT_SUCCESS);
  }
}
//helper
template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::updateControlPoints(const Vec& controlPoints) {
  _handlerObs->setControlPoints(controlPoints);
  _handlerObs->update();
  _handlerSelf->setControlPoints(controlPoints);
  _handlerSelf->update();
}

template <typename T, typename CCPlaneT>
LineSearchState SIPPlannerDiscretize<T, CCPlaneT>::assembleEnergy(const Vec& controlPoints,T* E,Vec* G,MatT* H,bool updateCCPlanes,bool debug,bool mustSucceed, bool storeTrajEss) {
  _handlerObs->getObsCCPlanes().clearUpdateFlag();
  _handlerSelf->getSelfCCPlanes().clearUpdateFlag();
  std::ostringstream oss;
  TBEG("AssembleEnergy");
  updateControlPoints(controlPoints);
  (*E)=0;
  if(G)
    (*G).setZero(controlPoints.size());
  if(H)
    (*H).setZero(controlPoints.size(),controlPoints.size());
  bool res=true;
  if(debug)
    oss<<"Assembling energy: ";
  if(storeTrajEss) {
    _trajEssVals.clear();
    _trajGssVals.clear();
    _trajHssVals.clear();
  }
  for(auto e:_trajEss) {
    T tmpE=*E;
    bool isObs=true;
    if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)) {
      //determine the type of collision barrier function
      bool implicit=std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->implicit();
      if(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerObs->getObsTTPairs()))
        isObs=true;
      else {
        ASSERT_MSG(&(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->getTriPairs())==&(_handlerSelf->getSelfTTPairs()),
                   "Strange error, energy bound to neither handlerObs nor handlerSelf")
        isObs=false;
      }
      //detect collision and insert new planes
      if(isObs) _handlerObs->CCD(false,true,true);
      else _handlerSelf->CCD(false,true,true);
      //update plane (automatically ignored if implicit)
      // true when not in line search
      if(updateCCPlanes) {
        if(!implicit) {
          // update CC planes("local phase" in AO)
          bool succ=std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)->updateCCPlanes();
          ASSERT_MSG(succ,"Strange error, failed updating convex hull's separating plane!")
          if(debug)
            oss<<" CCUpdated";
        }
      }
      // updateCCplanes==false in line search
      else if(_handlerObs->getObsCCPlanes().hasUpdateFlag() || _handlerSelf->getSelfCCPlanes().hasUpdateFlag()) {
        //user wants to use existing CCPlanes, but new planes have been introduced, then exit
        TEND();
        std::cout<<"LSS_CCUpdate"<<std::endl;
        if(debug)
          std::cout<<oss.str()<<std::endl;
        return LSS_CCUpdate;
      }
      //output CCD information
      if(debug) {
        if(isObs)
          oss<<" #obsTT="<<_handlerObs->getObsTTPairs().size()
             <<" #obsCC="<<_handlerObs->getObsCCPlanes().size()
             <<" #obsBVH="<<_handlerObs->getBVHBody().size();
        else
          oss<<" #selfTT="<<_handlerSelf->getSelfTTPairs().size()
             <<" #selfCC="<<_handlerSelf->getSelfCCPlanes().size()
             <<" #selfBVH="<<_handlerSelf->getBVHBody().size();
      }
    }
    std::string eName;
    if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)) {
      eName=isObs? "Barrier obs": "Barrier self";
    } else if(std::dynamic_pointer_cast<TargetEnergy<T>>(e)) {
      eName="TargetEnergy";
    } else if(std::dynamic_pointer_cast<GravityEnergy<T>>(e)) {
      eName="GravityEnergy";
    } else if(std::dynamic_pointer_cast<JointLimitEnergy<T>>(e)) {
      eName="JointLimitEnergy";
    } else if(std::dynamic_pointer_cast<SmoothnessEnergy<T>>(e)) {
      eName="SmoothnessEnergy";
    } else {
      ASSERT_MSG(false,"Wrong Energy!")
    }
    //evaluate energy, its gradient and hessian
    if(!storeTrajEss) {
      res=e->SIPEnergy<T>::eval(E,G,H);
    } else {
      T orig_E = *E;
      Vec orig_G = *G;
      MatT orig_H = *H;
      res = e->SIPEnergy<T>::eval(E, G, H);
      _trajEssVals.push_back(*E - orig_E);
      _trajGssVals.push_back(*G - orig_G);
      _trajHssVals.push_back(*H - orig_H);
    }
    if(!res) {
      TEND();
      if(debug)
        std::cout<<oss.str()<<std::endl;
      if(mustSucceed) {
        if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e)) {
          if(isObs)
            _handlerObs->traceError(*std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e));
          else _handlerSelf->traceError(*std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e));
        } else {
          std::cout<<eName<<" Error Occured!"<<std::endl;
        }
        ASSERT_MSG(false,"Strange error, assembleEnergy failed!")
      }
      return LSS_Fail;
    }
    //output energy information(energy increased)
    if(std::dynamic_pointer_cast<CollisionBarrierEnergy<T, Barrier, CCPlaneT>>(e) && debug)
      oss<<(isObs? " obs": " self")<<"Barrier="<<(*E)-tmpE;
  }
  //invalidate gradient and hessian for constants
  fixGrad(G,H);
  TEND();
  if(debug)
    std::cout<<oss.str()<<std::endl;
  return LSS_Succ;
}
template <typename T, typename CCPlaneT>
const std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>>& SIPPlannerDiscretize<T, CCPlaneT>::getEss() const {
  return _trajEss;
}

template <typename T, typename CCPlaneT>
T SIPPlannerDiscretize<T,CCPlaneT>::getTargetEnergy() const {
  T E=0,totalE=0;
  for(auto e:_trajEss)
    if(std::dynamic_pointer_cast<TargetEnergy<T>>(e)) {
      E=0;
      std::dynamic_pointer_cast<TargetEnergy<T>>(e)->SIPEnergy<T>::eval(&E,(Vec*)NULL,(MatT*)NULL);
      totalE+=E;
    }
  totalE+=E;
  return totalE;
}

template <typename T, typename CCPlaneT>
T SIPPlannerDiscretize<T,CCPlaneT>::getSmoothnessEnergy() const {
  T E=0;
  for(auto e:_trajEss)
    if(std::dynamic_pointer_cast<SmoothnessEnergy<T>>(e))
      e->eval(&E,(Vec*)NULL,(MatT*)NULL);
  return E;
}

template <typename T, typename CCPlaneT>
void SIPPlannerDiscretize<T, CCPlaneT>::assembleObsBVH() {
  _handlerObs->assembleObsBVH();
  _handlerSelf->assembleObsBVH();
}

//instance
template class SIPPlannerDiscretize<FLOAT>;
template class SIPPlannerDiscretize<FLOAT, CCSeparatingPlaneSmart<FLOAT> >;
}
