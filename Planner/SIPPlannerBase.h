#ifndef SIP_PLANNER_BASE_H
#define SIP_PLANNER_BASE_H

#include <SIPCollision/ThetaTrajectory.h>
#include <Articulated/ArticulatedBody.h>
#include <Utils/SparseUtils.h>
#include <Utils/Pragma.h>
#include <math.h>
#include "CollisionConstrainedPlanner.h"

namespace PHYSICSMOTION {
template <typename T>
class SIPPlannerBase : public CollisionConstrainedPlanner<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  //setup
  virtual void setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit)=0;
  virtual void setupEnergy(int JID,const Vec3T& target,const Vec3T& dir,const Vec3T& orientation,T targetCoefP,T targetCoefD,bool JTJApprox)=0;
  virtual void setupEnergy(T smoothnessCoef,T obsBarrierCoef=0,T selfBarrierCoef=0,T limitCoef=0,bool implicit=false,bool JTJApprox=false)=0;
  //optimization
  virtual bool optimize(int maxIter=1e4,bool output=true,bool newtonMethod=true)=0;
  //utility
  virtual void fixThetaAt(T t)=0;
  //utilities already declared in the base class
  virtual void setControlPoints(const Vec& newCP)=0;
  virtual const Vec& getControlPoints() const=0;
  virtual const ThetaTrajectory<T>& getThetaTrajectory() const=0;
  virtual std::shared_ptr<ArticulatedBody> getBody() const=0;
  //utility for all SIPPlanner
  void extendControlPointsSequence(bool reset=false,bool neutralInit=false) {
    Vec newCP;
    if(reset) {
      if(neutralInit) {
        Vec theta=Vec::Zero(getBody()->nrDOF());
        for(int i=0; i<getBody()->nrDOF(); ++i) {
          T upper=getBody()->upperLimit()[i];
          T lower=getBody()->lowerLimit()[i];
          if(isfinite(upper) && isfinite(lower)) {
            theta[i]=(upper+lower)/2;
          } else if(isfinite(upper) && (!isfinite(lower))) {
            if(upper>0)theta[i]=0;
            else theta[i]=upper-0.1;
          } else if(isfinite(lower) && (!isfinite(upper))) {
            if(lower<0) theta[i]=0;
            else theta[i]=lower+0.1;
          }
        }
        MatT neutralCP=theta.rowwise().replicate(getThetaTrajectory().getNumDOF()/getBody()->nrDOF());
        newCP=Eigen::Map<Vec>(neutralCP.data(),getThetaTrajectory().getNumDOF());
      } else newCP=Vec::Zero(getThetaTrajectory().getNumDOF());
    } else {
      MatT cp=Eigen::Map<MatT>(const_cast<Vec&>(getControlPoints()).data(),getBody()->nrDOF(),getThetaTrajectory().getNumDOF()/getBody()->nrDOF());
      MatT cpTimeReversed=cp.rowwise().reverse();
      MatT cpStayAtLastMoment=cpTimeReversed.col(0).rowwise().replicate(cpTimeReversed.cols());
      newCP=Eigen::Map<Vec>(cpStayAtLastMoment.data(),getThetaTrajectory().getNumDOF());
    }
    setControlPoints(newCP);
  }
};
}
#endif
