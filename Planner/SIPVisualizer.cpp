#include "SIPVisualizer.h"
#include <TinyVisualizer/MakeMesh.h>
#include <SIPCollision/TargetEnergy.h>
#include <Articulated/PBDArticulatedGradientInfo.h>
#include <Environment/EnvironmentVisualizer.h>

namespace PHYSICSMOTION {
template <typename T>
SIPVisualizer<T>::SIPVisualizer(const CollisionConstrainedPlanner<T>& planner,DRAWER::Drawer& drawer,
                                std::shared_ptr<ArticulatedBody> bodyVis,T rad,
                                bool visualizeAllTargetInfo,
                                bool visualizeTrajectory,
                                bool useBodyColor,
                                const Eigen::Matrix<GLfloat,3,1>& colorO,
                                const Eigen::Matrix<GLfloat,3,1>& colorT,
                                const Eigen::Matrix<GLfloat,3,1>& colorA)
  :_bodyVis(bodyVis),_planner(planner),_drawer(drawer),_colorT(colorT),_rad(rad),_visualizeAllTargetInfo(visualizeAllTargetInfo),_visualizeTrajectory(visualizeTrajectory),_trajFinished(false),_trajAdded(false) {
  if(!bodyVis)
    _bodyVis=planner.getBody();
  //obstacles,target,BB
  std::vector<Eigen::Matrix<double,3,1>> vss;
  std::vector<Eigen::Matrix<int,3,1>> iss;
  if(_rad>0) {
    PHYSICSMOTION::addSphere(vss,iss,Eigen::Matrix<double,3,1>::Zero(),(double) _rad,2);
    std::shared_ptr<ShapeExact> target(new MeshExact(vss,iss,true));
    _red=visualizeShapeExact(target,false);
    _red->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    _red->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    _red->setColorDiffuse(GL_TRIANGLES,1,0,0);
    _blue=visualizeShapeExact(target,false);
    _blue->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    _blue->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    _blue->setColorDiffuse(GL_TRIANGLES,0,0,1);
    _green=visualizeShapeExact(target,false);
    _green->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    _green->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    _green->setColorDiffuse(GL_TRIANGLES,0,1,0);
  }
  visualizeObstacles(colorO);
  //arm
  if(useBodyColor) {
    _shapeArm=visualizeArticulated(_bodyVis);
    _drawer.addShape(_shapeArm);
  } else {
    _shapeArm=visualizeArticulated(_bodyVis,colorA);
    _shapeArm->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    _shapeArm->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    _shapeArm->setColorDiffuse(GL_TRIANGLES,colorA[0],colorA[1],colorA[2]);
    _drawer.addShape(_shapeArm);
  }
}
template <typename T>
void SIPVisualizer<T>::updateRobot(std::shared_ptr<ArticulatedBody> bodyVis,bool useBodyColor,const Eigen::Matrix<GLfloat,3,1>& colorA) {
  if(!bodyVis)
    bodyVis=_planner.getBody();
  if(useBodyColor) {
    _shapeArm=visualizeArticulated(bodyVis);
    _drawer.addShape(_shapeArm);
  } else {
    _shapeArm=visualizeArticulated(bodyVis,colorA);
    _shapeArm->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    _shapeArm->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    _shapeArm->setColorDiffuse(GL_TRIANGLES,colorA[0],colorA[1],colorA[2]);
    _drawer.addShape(_shapeArm);
  }
}
template <typename T>
void SIPVisualizer<T>::setupEnergy(int JID,const Vec3T& tar,const Vec3T& dir,const Vec3T& ori) {
  if(std::find(_targets.begin(),_targets.end(),tar)==_targets.end())
    _targets.push_back(tar);
  if(std::find(_orientations.begin(),_orientations.end(),ori+tar)==_orientations.end())
    _orientations.push_back(ori+tar);
  TargetEnergy<T> target(*(_planner.getBody()),
                         _planner.getControlPoints(),
                         _planner.getThetaTrajectory(),
                         JID,tar,dir,ori,1,1,true);
  if(dir!=Vec3T::Zero()) {
    _localPos[JID]=target.localPos();
    _localDir[JID]=target.localDir();
  }
  if(_rad>0)
    visualizeTarget(_rad,_colorT);
}

template <typename T>
void SIPVisualizer<T>::update(const std::vector<Vec,Eigen::aligned_allocator<Vec>>& cps,T t) {
  //wrap
  T horizon=_planner.getThetaTrajectory().getNumSegment()*cps.size();
  while(t>horizon) {
    if(_trajAdded)
      t-=horizon;
    else
      t=horizon;
    _trajFinished=true;
  }
  while(t<0)
    t+=horizon;

  //find theta
  for(int i=0; i<(int)cps.size(); ++i) {
    const auto& cp=cps[i];
    if(t<=_planner.getThetaTrajectory().getNumSegment()) {
      Vec theta=_planner.getThetaTrajectory().getPoint(cp,t);
      updateArticulatedBody(_shapeArm,_bodyVis,ArticulatedBody::Vec(theta.template cast<double>()));
      updateTarget(ArticulatedBody::Vec(theta.template cast<double>()));
      break;
    } else t-=_planner.getThetaTrajectory().getNumSegment();
  }
}
//helper
template <typename T>
void SIPVisualizer<T>::visualizeTarget(T rad,const Eigen::Matrix<GLfloat,3,1>& color) {
  //target
  if(_shapeTarget)
    _drawer.removeShape(_shapeTarget);
  _shapeTarget.reset(new DRAWER::CompositeShape);
  for(auto& tar:_targets) {
    if(tar.hasNaN())
      continue;
    std::shared_ptr<DRAWER::Bullet3DShape> st(new DRAWER::Bullet3DShape);
    st->setLocalTranslate(tar.template cast<GLfloat>());
    st->addShape(_blue);
    _shapeTarget->addShape(st);
  }
  _drawer.addShape(_shapeTarget);
  //orientation
  if(_visualizeAllTargetInfo) {
    if(_shapeOrientation)
      _drawer.removeShape(_shapeOrientation);
    _shapeOrientation.reset(new DRAWER::CompositeShape);
    for(auto& ori:_orientations) {
      std::shared_ptr<DRAWER::Bullet3DShape> st(new DRAWER::Bullet3DShape);
      st->setLocalTranslate(ori.template cast<GLfloat>());
      st->addShape(_green);
      _shapeOrientation->addShape(st);
    }
    _drawer.addShape(_shapeOrientation);
    //localPos and localDir
    if(!_localPos.empty() && !_localDir.empty()) {
      if(_local)
        _drawer.removeShape(_local);
      _local.reset(new DRAWER::CompositeShape);
      for(auto& pos:_localPos) {
        std::shared_ptr<DRAWER::Bullet3DShape> st(new DRAWER::Bullet3DShape);
        st->setLocalTranslate(pos.second.template cast<GLfloat>());
        st->addShape(_blue);
        _local->addShape(st);
      }
      for(auto& pos:_localDir) {
        std::shared_ptr<DRAWER::Bullet3DShape> st(new DRAWER::Bullet3DShape);
        st->setLocalTranslate(pos.second.template cast<GLfloat>());
        st->addShape(_green);
        _local->addShape(st);
      }
      _drawer.addShape(_local);
    }
  }
}
template <typename T>
void SIPVisualizer<T>::updateTarget(const ArticulatedBody::Vec& theta) {
  PBDArticulatedGradientInfo<double> info(*_bodyVis,theta);
  int offset=0;
  if(_visualizeAllTargetInfo) {
    if(!_localPos.empty() && !_localDir.empty()) {
      for(auto& pos:_localPos) {
        int JID=pos.first;
        Eigen::Matrix<double,3,1> point=pos.second.template cast<double>();
        //Eigen::Matrix<double,3,1> globalPos=(ROTI(info._TM,JID)*point+CTRI(info._TM,JID));
        std::shared_ptr<DRAWER::Bullet3DShape> st=std::dynamic_pointer_cast<DRAWER::Bullet3DShape>(_local->getChild(offset));
        st->setLocalTranslate((ROTI(info._TM,JID)*point+CTRI(info._TM,JID)).template cast<GLfloat>());
        offset++;
      }
      for(auto& pos:_localDir) {
        int JID=pos.first;
        Eigen::Matrix<double,3,1> point=pos.second.template cast<double>();
        std::shared_ptr<DRAWER::Bullet3DShape> st=std::dynamic_pointer_cast<DRAWER::Bullet3DShape>(_local->getChild(offset));
        st->setLocalTranslate((ROTI(info._TM,JID)*point+CTRI(info._TM,JID)).template cast<GLfloat>());
        offset++;
      }
    }
  }
  if(_visualizeTrajectory && !_localPos.empty() && !_trajAdded) {
    //bool trajAdded=true;
    //if(!_traj){
    //  _traj.reset(new DRAWER::CompositeShape);
    //  trajAdded=false;
    //}
    for(auto& pos:_localPos) {
      int JID=pos.first;
      Vec3T point=pos.second;
      Vec3T globalPos=(ROTI(info._TM,JID).template cast<T>()*point+CTRI(info._TM,JID).template cast<T>());
      //std::shared_ptr<DRAWER::Bullet3DShape> st(new DRAWER::Bullet3DShape);
      //st->setLocalTranslate(globalPos.template cast<GLfloat>());
      //st->addShape(_green);
      //_traj->addShape(st);
      if(_curve.find(JID)==_curve.end()) {
        _curve[JID]=std::shared_ptr<DRAWER::BezierCurveShape>();
        _curve[JID].reset(new DRAWER::BezierCurveShape(0.003f,true,16));
        _curve[JID]->setLineWidth(2);
        _curve[JID]->setColorDiffuse(GL_QUADS,0.7,0.7,0.7);
        _curve[JID]->setColorDiffuse(GL_LINE_STRIP,0.7,0.7,0.7);
        //_traj->addShape(_curve[JID]);
      }
      if(_prePos.find(JID)!=_prePos.end()) {
        _curve[JID]->addControlPoint(globalPos.template cast<GLfloat>()-_prePos[JID].template cast<GLfloat>());
      }
      _curve[JID]->addControlPoint(globalPos.template cast<GLfloat>());
      _prePos[JID]=globalPos;
    }
    if(_trajFinished) {
      for(const auto& curve:_curve) {
        _drawer.addShape(curve.second);
      }
      _trajAdded=true;
    }
//    if(!trajAdded){
//      _drawer.addShape(_traj);
//    }
  }

}
template <typename T>
void SIPVisualizer<T>::visualizeObstacles(const Eigen::Matrix<GLfloat,3,1>& color) {
  for(const auto& obstacle:_planner.getObstacles()) {
    std::shared_ptr<DRAWER::Shape> s=visualizeShapeExact(obstacle,false);
    s->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    s->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    s->setColorDiffuse(GL_TRIANGLES,color[0],color[1],color[2]);
    _drawer.addShape(s);
  }
}
//instance
template class SIPVisualizer<FLOAT>;
}
