#ifndef GRID_VISUALIZER_H
#define GRID_VISUALIZER_H

#include "GridPathPlanner.h"
#include "SIPPlannerBase.h"
#include <Articulated/ArticulatedBody.h>
#include <TinyVisualizer/Drawer.h>
#include <TinyVisualizer/CompositeShape.h>

namespace PHYSICSMOTION {
template <typename T>
class GridVisualizer {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  GridVisualizer(std::shared_ptr<GridPlanner<T>> gridPlanner,DRAWER::Drawer& drawer,std::shared_ptr<ArticulatedBody> bodyVis,const Eigen::Matrix<GLfloat,3,1>& colorO=Eigen::Matrix<GLfloat,3,1>(1,1,1));
  void visualizeObstacles(const Eigen::Matrix<GLfloat,3,1>& color);
  void visualizeTarget();
  void update(T t);
  T totalTime() const;
 private:
  std::shared_ptr<GridPlanner<T>> _gridPlanner;
  DRAWER::Drawer& _drawer;
  std::shared_ptr<DRAWER::CompositeShape> _shapeTarget;
  std::shared_ptr<DRAWER::Shape> _shapeArm;
  std::shared_ptr<ArticulatedBody> _bodyVis;
};
}

#endif
