#ifndef SIP_PLANNER_EXCHANGE_H
#define SIP_PLANNER_EXCHANGE_H

#include "SIPPlannerBase.h"
#include <ConvexHull/Barrier.h>
#include <SIPCollision/TrajectorySIPEnergy.h>
#include <SIPCollision/PenetrationDetector.h>

namespace PHYSICSMOTION {
template <typename T>
class SIPPlannerExchange : public SIPPlannerBase<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using SIPPlannerBase<T>::readStr;
  using SIPPlannerBase<T>::writeStr;
  //initialize
  explicit SIPPlannerExchange(T d0=1e-3,T epsTime=1e-3,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD= {});
  explicit SIPPlannerExchange(std::shared_ptr<ArticulatedBody> body,int order,int totalTime,
                              T d0=1e-3,T epsTime=1e-3,T randomInitialize=1.0,bool neutralInit=false,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD= {});
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  //setup
  void setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit) override;
  void addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF) override;
  void addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot, int maxConvexHulls) override;
  void addSphere(T r,int res,const Vec3T& pos) override;
  void addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& R) override;
  void addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res) override;
  void addCapsule(T l,T w,T h,Vec3T pos,T radius,int res) override;
  void assembleObsBVH() override;
  std::vector<std::shared_ptr<MeshExact>> getObstacles() const override;
  const Vec& getControlPoints() const override;
  void setControlPoints(const Vec& newCP) override;
  const ThetaTrajectory<T>& getThetaTrajectory() const override;
  std::shared_ptr<ArticulatedBody> getBody() const override;
  void assembleBodyBVH(bool DCDObs,bool DCDSelf);
  //optimization
  std::shared_ptr<PenetrationDetector<T>> getDetector() const;
  void clearEnergy() override;
  void setupEnergy(int JID,const Vec3T& target,const Vec3T& dir,const Vec3T& orientation,T targetCoefP,T targetCoefD,bool JTJApprox=false) override;
  void setupEnergy(T smoothnessCoef,T obsBarrierCoef=0,T selfBarrierCoef=0,T limitCoef=0,bool implicit=false,bool JTJApprox=false) override;
  bool optimize(int maxIter=1e4,bool output=true,bool newtonMethod=true) override;
  void fixThetaAt(T t) override;
  void initializeTheta(const Vec& theta) override;
  void setUseSDF(bool use);
  bool getUseSDF() const;
  //debug
  void writeConfiguration(const std::string& path="errorConfig.dat");
  void debugAtConfiguration(const std::string& path="errorConfig.dat");
 private:
  void updateControlPoints(const Vec& controlPoints);
  //data
  int _maxIterInner;
  std::vector<int> _fixVar;
  //this is per-problem temporary data, not serialized
  std::shared_ptr<PenetrationDetector<T>> _detector;
  std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>> _trajEss;
};
}
#endif
