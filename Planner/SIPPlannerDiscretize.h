#ifndef SIP_PLANNER_DISCRETIZE_H
#define SIP_PLANNER_DISCRETIZE_H

#include "SIPPlannerBase.h"
#include <ConvexHull/Barrier.h>
#include <SIPCollision/TrajectorySIPEnergy.h>
#include <SIPCollision/CollisionHandler.h>

namespace PHYSICSMOTION {
enum LineSearchState {
  LSS_Succ,
  LSS_Fail,
  LSS_CCUpdate,
};
template <typename T,typename PFunc, typename CCPlaneT>
class CollisionBarrierEnergy;

template <typename T, typename CCPlaneT=CCSeparatingPlane<T> >
class SIPPlannerDiscretize : public SIPPlannerBase<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using SIPPlannerBase<T>::readStr;
  using SIPPlannerBase<T>::writeStr;
  typedef Px Barrier;
  //initialize
  explicit SIPPlannerDiscretize(T d0=1e-3,T x0Obs=1.,T x0Self=1.,T L2=0.01,T eta=7./24.,T terminateGrad=1e-4, T minEig=1e-3, bool useConvexHullObs=true,bool useConvexHullSelf=true,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD= {});
  explicit SIPPlannerDiscretize(std::shared_ptr<ArticulatedBody> body,
                                int order,
                                int totalTime,
                                T d0=1e-3,
                                T x0Obs=1.,
                                T x0Self=1.,
                                T L2=0.01,
                                T eta=7./24.,
                                T terminateGrad=1e-4,
                                T minEig=1e-3,
                                T randomInitialize=1.0,
                                bool useConvexHullObs=true,
                                bool useConvexHullSelf=true,
                                bool neutralInit=false,
                                const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD= {});
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual bool optimize(int maxIter=1e4,bool output=true,bool newtonMethod=false) override;
  //setup
  void setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit) override;
  void addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF) override;
  void addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot,int maxConvexHulls) override;
  void addSphere(T r,int res,const Vec3T& pos) override;
  void addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& R) override;
  void addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res) override;
  void addCapsule(T l,T w,T h,Vec3T pos,T radius,int res) override;
  void assembleObsBVH() override;
  std::vector<std::shared_ptr<MeshExact>> getObstacles() const override;
  const Vec& getControlPoints() const override;
  void setControlPoints(const Vec& newCP) override;
  const ThetaTrajectory<T>& getThetaTrajectory() const override;
  std::shared_ptr<ArticulatedBody> getBody() const override;
  void assembleBodyBVH(bool useConvexHullObs,bool useConvexHullSelf);
  //optimization
  std::shared_ptr<CollisionHandler<T, CCPlaneT>> getHandlerObs() const;
  std::shared_ptr<CollisionHandler<T, CCPlaneT>> getHandlerSelf() const;
  void clearEnergy() override;
  std::shared_ptr<CollisionBarrierEnergy<T,Barrier, CCPlaneT>> getObsBarrierEnergy() const;
  std::shared_ptr<CollisionBarrierEnergy<T,Barrier, CCPlaneT>> getSelfBarrierEnergy() const;
  virtual void setupEnergy(int JID,const Vec3T& target,const Vec3T& dir,const Vec3T& orientation,T targetCoefP,T targetCoefD,bool JTJApprox=false) override;
  virtual void setupEnergy(T smoothnessCoef,T obsBarrierCoef,T selfBarrierCoef,T limitCoef,bool implicit=false,bool JTJApprox=false) override;
  LineSearchState lineSearch(T& newE,const Vec& g,const Vec& d,T& alpha,Vec& newX,bool debug);
  void fixGrad(Vec* G,MatT* H) const;
  bool hasCollisionEnergy() const;
  void fixThetaAt(T t) override;
  void initializeTheta(const Vec& theta) override;
  void setUseAlphaReset(bool use);
  //debug
  virtual void debug(const Vec* cp, const Vec* d, const T* alpha, T perturbRange);
  void writeConfiguration(const Vec& x,const std::string& path="errorConfig.dat");
  void debugAtConfiguration(T perturbRange,const std::string& path="errorConfig.dat");
 protected:
  LineSearchState assembleEnergy(const Vec& controlPoints,T* E,Vec* G,MatT* H,bool updateCCPlanes,bool debug,bool mustSucceed=false, bool storeTrajEss=false);
  inline const std::shared_ptr<CollisionHandler<T, CCPlaneT>>& handlerObs() const {
    return _handlerObs;
  }
  inline const std::shared_ptr<CollisionHandler<T, CCPlaneT>>& handlerSelf() const {
    return _handlerSelf;
  }
  inline std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>>& trajEss() {
    return _trajEss;
  }
  inline Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,-1,-1>> & sol() {
    return _sol;
  }
  inline const std::vector<int>& fixVar() {
    return _fixVar;
  }
  inline T terminateGrad() const {
    return _terminateGrad;
  }
  inline T minEig() const {
    return _minEig;
  }
  inline bool useConvexHullObs() const {
    return _useConvexHullObs;
  }
  inline bool useConvexHullSelf() const {
    return _useConvexHullSelf;
  }
 private:
  virtual LineSearchState computeNewX(T& e, const Vec& g, const MatT& h,  Vec&d, Vec& newX, T& alpha,
                                      bool newtonMethod, bool debug);
  virtual void updateInLineSearch(const Vec& x, const Vec& d, const T& alpha, Vec& newX);
  virtual void updateInOptimize(const Vec& newX, Vec& x);
  virtual bool isZeroGrad(const Vec& g);
  bool armijoConditionCheck(T deltaE, const Vec& x, const Vec& d, T alpha, T c);
 protected:
  void solveSearchDirection(const Vec& G, const MatT& H, Vec& d, bool newtonMethod);
 private:
  void updateControlPoints(const Vec& controlPoints);
  const std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>>& getEss() const;
  T getTargetEnergy() const;
  T getSmoothnessEnergy() const;
  //data
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double,-1,-1>> _sol;
  std::vector<int> _fixVar;
  bool _useConvexHullObs,_useConvexHullSelf,_useAlphaReset;
  //this is per-problem temporary data, not serialized
  std::shared_ptr<CollisionHandler<T, CCPlaneT>> _handlerObs,_handlerSelf;
  std::vector<std::shared_ptr<TrajectorySIPEnergy<T>>> _trajEss;
  //for debug
  std::vector<T> _trajEssVals;
  std::vector<Vec> _trajGssVals;
  std::vector<MatT> _trajHssVals;
  T _terminateGrad;
  T _minEig;
};
}
#endif
