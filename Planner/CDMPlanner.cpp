#include "CDMPlanner.h"
#include <Optimizer/NEDynamicsSequence.h>
#include <Optimizer/NESimplifiedDynamicsSequence.h>
#include <Optimizer/NESimplifiedDynamicsSequenceSpline.h>
#include <Optimizer/PBCDMDynamicsSequence.h>
#include <Optimizer/PBCDMDynamicsSequenceSpline.h>
#include <Optimizer/ForceSequenceInCone.h>
#include <Optimizer/PositionSequenceAboveGround.h>
#include <Optimizer/TargetHeadingObj.h>
#include <Optimizer/TargetVelocityObj.h>
#include <Optimizer/LaplaceObjective.h>
#include <Optimizer/OrientationObj.h>
#include <Optimizer/PositionObj.h>
#include <Optimizer/InitialPose.h>
#include <Environment/DeformedEnvironment.h>
#include <Articulated/ArticulatedUtils.h>
#include <Articulated/ArticulatedLoader.h>
#include <Utils/RotationUtils.h>
#include <Utils/Interp.h>
#include <Utils/Utils.h>
#include <random>

namespace PHYSICSMOTION {
template <typename T>
CDMPlanner<T>::CDMPlanner():PlannerBase<T>() {}
template <typename T>
CDMPlanner<T>::CDMPlanner(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize):PlannerBase<T>(env,path,normalize) {}
template <typename T>
CDMPlanner<T>::CDMPlanner
(std::shared_ptr<Environment<T>> env,
 std::shared_ptr<ArticulatedBody> body,
 std::shared_ptr<ArticulatedBody> bodyVis,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees):PlannerBase<T>(env,body,bodyVis,ees) {}
template <typename T>
std::shared_ptr<SerializableBase> CDMPlanner<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new CDMPlanner<T>);
}
template <typename T>
void CDMPlanner<T>::debug(int trial,T thres,std::function<bool(const std::string&)> f) {
  for(int i=0; i<(int)_phases.size(); i++)
    if(f(_phases[i]->_name))
      _phases[i]->debug(_obj.inputs(),trial,thres);

  for(int i=0; i<(int)_poses.size(); i++)
    if(f(_poses[i]->_name))
      _poses[i]->debug(_obj.inputs(),trial,thres);

  for(int i=0; i<(int)_posCons.size(); i++)
    if(f(_posCons[i]->_name))
      _posCons[i]->debug(_obj.inputs(),trial,thres);

  for(int i=0; i<(int)_forces.size(); i++)
    if(f(_forces[i]->_name))
      _forces[i]->debug(_obj.inputs(),trial,thres);

  for(int i=0; i<(int)_forceCons.size(); i++)
    if(f(_forceCons[i]->_name))
      _forceCons[i]->debug(_obj.inputs(),trial,thres);

  for(int i=0; i<(int)_objectives.size(); i++)
    if(f(_objectives[i]->_name))
      _objectives[i]->debug(_obj.inputs(),trial,thres);

  if(_PBDDynamics)
    if(f(_PBDDynamics->_name))
      _PBDDynamics->debug(_obj.inputs(),trial,thres);
}
template <typename T>
void CDMPlanner<T>::setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv) {
  _DEnv=DEnv;
  _env=DEnv->getOptimizedEnvironment();
}
template <typename T>
void CDMPlanner<T>::update(const std::vector<std::shared_ptr<FootStep<T>>>& ss) {
  _steps.clear();
  for(const std::shared_ptr<FootStep<T>>& step:ss)
    _steps.push_back(*step);
}
template <typename T>
std::vector<std::shared_ptr<FootStep<T>>> CDMPlanner<T>::stepsSmartPtr() const {
  std::vector<std::shared_ptr<FootStep<T>>> ret;
  for(const FootStep<T>& step:steps())
    ret.push_back(std::shared_ptr<FootStep<T>>(new FootStep<T>(step)));
  return ret;
}
template <typename T>
const std::vector<FootStep<T>>& CDMPlanner<T>::steps() const {
  return _steps;
}
template <typename T>
bool CDMPlanner<T>::computeInitialPose(int nrTrial,const Vec2T* xyInit) {
  clearProblem();
  //try load from file
  if(loadSolution() && _initPose.size()==_body->nrDOF()) {
    std::cout << "Loaded solution file: " << savePath() << std::endl;
    return true;
  }
  if(xyInit)
    _x=*xyInit;
  std::shared_ptr<InitialPose<T>> pose(new InitialPose<T>(_obj,"",_ees[0]->_frame.template cast<T>(),
                                       Mat3T::Identity(),_env,_body,true,xyInit?&_x:NULL,_ees));
  const Eigen::Matrix<int,-1,1>& DOFVars=pose->DOFVars();
  //pose->SQPObjectiveComponent<T>::debug(10,0);
  _obj.addComponent(pose);
  //try optimize
  bool found=false;
  std::random_device rd;
  std::mt19937 gen(rd());
  _x.setZero(_body->nrDOF());
  if(xyInit)
    _x.template segment<2>(0)=*xyInit;
  _initPose.setZero(_body->nrDOF());
  T bestObjVal=std::numeric_limits<double>::max();
  for(int trial=0; trial<nrTrial; trial++) {
    //randomize initial
    for(int i=0; i<=_body->rootJointId(); i++)
      if(_body->joint(i).isRotational())
        for(int j=_body->joint(i).RBegEnd()[0]; j<_body->joint(i).RBegEnd()[1]; j++)
          _x[j]=std::uniform_real_distribution<>(-M_PI,M_PI)(gen);
    for(int r=0; r<DOFVars.rows(); r++) {
      auto& var=_obj.addVar(DOFVars[r]);
      var._init=_x[r];
      if(xyInit && r<2)
        var._l=var._u=var._init;
    }
    //solve
    bool ret=true;
    if(!useIPOPT())
      ret=solveKnitro(_obj,_x,&_fvec);
    else ret=solveIpopt(_obj,_x);
    if(!ret)
      continue;
    found=true;
    T objVal=_obj(_x,NULL);
    if(objVal<bestObjVal) {
      bestObjVal=objVal;
      //assign
      for(int i=0; i<=_body->rootJointId(); i++)
        if(_body->joint(i).isRotational())
          for(int j=_body->joint(i).RBegEnd()[0]; j<_body->joint(i).RBegEnd()[1]; j++) {
            while(_x[j]<-M_PI)
              _x[j]+=M_PI*2;
            while(_x[j]>M_PI)
              _x[j]-=M_PI*2;
          }
      for(int r=0; r<DOFVars.rows(); r++)
        _initPose[r]=_x[DOFVars[r]];
    }
  }
  if(found)
    saveSolution();
  std::cout << "Best objective function value=" << bestObjVal << std::endl;
  _xPoses=_initPose;
  return found;
}
template <typename T>
void CDMPlanner<T>::clearProblem() {
  _obj=SQPObjectiveCompound<T>();
  _phases.clear();
  _poses.clear();
  _forces.clear();
  _posCons.clear();
  _forceCons.clear();
  _objectives.clear();
}
template <typename T>
void CDMPlanner<T>::buildProblemStaticPose() {
  clearProblem();
  //loop over joints
  ASSERT_MSG(_initPose.size()==_body->nrDOF(),"Must initialize robot pose before initStaticPose()!")
  Mat3XT trans=_body->getT(_initPose.template cast<ArticulatedBody::T>()).template cast<T>();
  for(const std::shared_ptr<EndEffectorBounds>& ee:_ees) {
    //define components
    _phases.push_back(std::shared_ptr<PhaseSequence<T>>(new PhaseSequence<T>(_obj,ee->name(),eps(),horizon(),stage()*2+1,subStage())));
    _phases.back()->initSameSpan(_obj,ee->isEven(),staticToSwing());
    _obj.addComponent(_phases.back());
    _poses.push_back(std::shared_ptr<PositionSequence<T>>(new PositionSequence<T>(_obj,ee->name(),stencilSize(),ee->_phi0,false,_phases.back(),_env)));
    _poses.back()->setDeformedEnvironment(_DEnv);
    _poses.back()->initConstant(ROTI(trans,ee->jointId())*ee->_localPos.template cast<T>()+CTRI(trans,ee->jointId()),_obj);
    _obj.addComponent(_poses.back());
    if(ee->_type&climbingFootMask()) {
      //mark no force constraint:
      _forces.push_back(std::shared_ptr<ForceSequence<T>>(new ForceSequence<T>(_obj,ee->name(),-1,mu(),true,Vec3T::Zero(),_poses.back())));
      //previously we use gravity as normal:
      //_forces.push_back(std::shared_ptr<ForceSequence<T>>(new ForceSequence<T>(_obj,ee->name(),stencilSize(),mu(),true,-g(),_poses.back())));
    } else _forces.push_back(std::shared_ptr<ForceSequence<T>>(new ForceSequence<T>(_obj,ee->name(),stencilSize(),mu(),true,Vec3T::Zero(),_poses.back())));
    _forces.back()->setDeformedEnvironment(_DEnv);
    _forces.back()->initConstant(Vec3T::Zero(),_obj);
    _obj.addComponent(_forces.back());
    if(stencilSize()==2) {  //in this case, we time interpolate the position sequence, and check each interpolated position is above ground
      _posCons.push_back(std::shared_ptr<PositionSequenceAboveGround<T>>(new PositionSequenceAboveGround<T>(_obj,ee->name(),true,dt(),_poses.back())));
      _obj.addComponent(_posCons.back());
      if(ee->_type&climbingFootMask()) {
        //do nothing, previously we use gravity as normal:
        //_forceCons.push_back(std::shared_ptr<ForceSequenceInCone<T>>(new ForceSequenceInCone<T>(_obj,ee->name(),dt(),-g(),_forces.back())));
      } else _forceCons.push_back(std::shared_ptr<ForceSequenceInCone<T>>(new ForceSequenceInCone<T>(_obj,ee->name(),dt(),_poses.back(),_forces.back())));
      _obj.addComponent(_forceCons.back());
      ASSERT_MSG(!_DEnv,"DeformedEnvironment does not support explicit position/force constraints!")
    }
  }
  buildDynamics();
  if(_DEnv) {
    ASSERT(_bodySimplified)
    PBCentroidBodyDynamicsGradientInfo<T> info(*_bodySimplified,_ees.size(),g(),dt(),_DEnv);
    _initPose.template segment<6>(0)=info.invReset(*_bodySimplified,_initPose.template segment<6>(0));
  }
  if(dynamicsModel()==POSITION_BASED || dynamicsModel()==NEWTON_EULER || dynamicsModel()==SIMPLIFIED_NEWTON_EULER) {
    //we constraint the initial configuration by fixing two frames
    const Eigen::Matrix<int,-1,-1>& DOFVars=_PBDDynamics->DOFVar();
    for(int c=0; c<DOFVars.cols(); c++)
      initDOF(DOFVars.col(c),_initPose,c<nrInitialFrameToFix());
  } else if(dynamicsModel()==PBCDM) {
    //we constraint the initial configuration by fixing two frames of root
    const Eigen::Matrix<int,-1,-1>& DOFVars=_PBDDynamics->DOFVar();
    for(int c=0; c<DOFVars.cols(); c++)
      initDOF(DOFVars.col(c),_initPose.template segment<6>(0),c<nrInitialFrameToFix());
  } else if(dynamicsModel()==SIMPLIFIED_NEWTON_EULER_SPLINE || dynamicsModel()==PBCDM_SPLINE) {
    //we constraint the initial configuration by fixing one frame and one diff
    const Eigen::Matrix<int,-1,-1>& DOFDiffVars=
      dynamicsModel()==SIMPLIFIED_NEWTON_EULER_SPLINE?
      std::dynamic_pointer_cast<NESimplifiedDynamicsSequenceSpline<T>>(_PBDDynamics)->DOFSeq()->getDiffVar()
      :std::dynamic_pointer_cast<PBCDMDynamicsSequenceSpline<T>>(_PBDDynamics)->DOFSeq()->getDiffVar();
    const Eigen::Matrix<int,-1,-1>& DOFVars=_PBDDynamics->DOFVar();
    for(int c=0; c<DOFVars.cols(); c++) {
      initDOF(DOFVars.col(c),_initPose.template segment<6>(0),c==0 && 0<nrInitialFrameToFix());
      initDOF(DOFDiffVars.col(c),Vec6T::Zero(),c==0 && 1<nrInitialFrameToFix());
    }
  } else {
    ASSERT_MSGV(false,"Unsupported dynamics model (%d) for trajectory optimization!",dynamicsModel())
  }
  buildObjective();
}
template <typename T>
void CDMPlanner<T>::buildProblemSteps() {
  clearProblem();
  //loop over joints
  ASSERT_MSG(!_DEnv,"buildProblemSteps does not support DeformedEnvironment!")
  ASSERT_MSG(!_steps.empty(),"Must initialize robot pose before initStep()!")
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    std::shared_ptr<EndEffectorBounds> ee=_ees[eid];
    //define horizon
    T newSpan=0,totalHorizon=0;
    bool lastIsStatic=false,newIsStatic=false;
    std::vector<std::pair<Vec3T,Vec3T>> lineSegments;
    std::vector<T> spans; //<isStatic,span>
    for(int s=0; s<(int)_steps.size(); s++) {
      {
        newIsStatic=true;
        newSpan=(s==0||s==(int)_steps.size()-1?.5:1.)*doubleHorizon()*dt();
        if(lastIsStatic==newIsStatic)
          spans.back()+=newSpan;
        else spans.push_back(newSpan);
        totalHorizon+=newSpan;
        lastIsStatic=newIsStatic;
      }
      if(s<(int)_steps.size()-1) {
        newIsStatic=ee->isEven()==_steps[s]._isEven;
        newSpan=singleHorizon()*dt();
        if(!newIsStatic)
          lineSegments.push_back(std::make_pair(_steps[s]._eePose.col(eid),_steps[s+1]._eePose.col(eid)));
        if(lastIsStatic==newIsStatic)
          spans.back()+=newSpan;
        else spans.push_back(newSpan);
        totalHorizon+=newSpan;
        lastIsStatic=newIsStatic;
      }
    }
    horizon(totalHorizon);  //synchronize to param
    //define components
    _phases.push_back(std::shared_ptr<PhaseSequence<T>>(new PhaseSequence<T>(_obj,ee->name(),eps(),totalHorizon,(int)spans.size(),subStage())));
    _phases.back()->initSpanSequence(_obj,spans);
    _obj.addComponent(_phases.back());
    _poses.push_back(std::shared_ptr<PositionSequence<T>>(new PositionSequence<T>(_obj,ee->name(),stencilSize(),ee->_phi0,false,_phases.back(),_env)));
    _poses.back()->initLineSegments(lineSegments,_obj);
    _obj.addComponent(_poses.back());
    if(ee->_type&climbingFootMask())
      _forces.push_back(std::shared_ptr<ForceSequence<T>>(new ForceSequence<T>(_obj,ee->name(),stencilSize(),mu(),true,-g(),_poses.back())));
    else _forces.push_back(std::shared_ptr<ForceSequence<T>>(new ForceSequence<T>(_obj,ee->name(),stencilSize(),mu(),true,Vec3T::Zero(),_poses.back())));
    _forces.back()->initConstant(Vec3T::Zero(),_obj);
    _obj.addComponent(_forces.back());
    if(stencilSize()==2) {  //in this case, we time interpolate the position sequence, and check each interpolated position is above ground
      _posCons.push_back(std::shared_ptr<PositionSequenceAboveGround<T>>(new PositionSequenceAboveGround<T>(_obj,ee->name(),true,dt(),_poses.back())));
      _obj.addComponent(_posCons.back());
      if(ee->_type&climbingFootMask())
        _forceCons.push_back(std::shared_ptr<ForceSequenceInCone<T>>(new ForceSequenceInCone<T>(_obj,ee->name(),dt(),-g(),_forces.back())));
      else _forceCons.push_back(std::shared_ptr<ForceSequenceInCone<T>>(new ForceSequenceInCone<T>(_obj,ee->name(),dt(),_poses.back(),_forces.back())));
      _obj.addComponent(_forceCons.back());
    }
  }
  buildDynamics();
  const Eigen::Matrix<int,-1,-1>& DOFVars=_PBDDynamics->DOFVar();
  MatT DOF=MatT::Zero(DOFVars.rows(),DOFVars.cols());
  {
    int cDOF=0;
    T newSpan=0,totalHorizon=0;
    for(int s=0; s<(int)_steps.size(); s++) {
      {
        newSpan=(s==0||s==(int)_steps.size()-1?.5:1.)*doubleHorizon()*dt();
        while(cDOF<DOFVars.cols() && totalHorizon+newSpan>cDOF*dt()) {
          if(DOFVars.rows()==6)
            DOF.col(cDOF)=_steps[s]._bodyPose;
          else DOF.col(cDOF)=stepToDOF(_steps[s]);
          cDOF++;
        }
        totalHorizon+=newSpan;
      }
      if(s<(int)_steps.size()-1) {
        newSpan=singleHorizon()*dt();
        while(cDOF<DOFVars.cols() && totalHorizon+newSpan>cDOF*dt()) {
          T alpha=(cDOF*dt()-totalHorizon)/newSpan;
          if(DOFVars.rows()==6)
            DOF.col(cDOF)=interp1D(_steps[s]._bodyPose,_steps[s+1]._bodyPose,alpha);
          else DOF.col(cDOF)=stepToDOF(_steps[s],_steps[s+1],alpha);
          cDOF++;
        }
        totalHorizon+=newSpan;
      }
    }
    while(cDOF<DOFVars.cols()) {
      if(DOFVars.rows()==6)
        DOF.col(cDOF)=_steps.back()._bodyPose;
      else DOF.col(cDOF)=stepToDOF(_steps.back());
      cDOF++;
    }
  }
  if(dynamicsModel()==POSITION_BASED || dynamicsModel()==NEWTON_EULER || dynamicsModel()==SIMPLIFIED_NEWTON_EULER) {
    //we constraint the initial configuration by fixing two frames
    for(int c=0; c<DOFVars.cols(); c++)
      initDOF(DOFVars.col(c),DOF.col(c),c<nrInitialFrameToFix());
  } else if(dynamicsModel()==SIMPLIFIED_NEWTON_EULER_SPLINE) {
    //we constraint the initial configuration by fixing one frame and one diff
    const Eigen::Matrix<int,-1,-1>& DOFDiffVars=std::dynamic_pointer_cast<NESimplifiedDynamicsSequenceSpline<T>>(_PBDDynamics)->DOFSeq()->getDiffVar();
    std::dynamic_pointer_cast<NESimplifiedDynamicsSequenceSpline<T>>(_PBDDynamics)->DOFSeq()->initRegularInterval(DOF,dt(),_obj,true,true);
    if(0<nrInitialFrameToFix())
      fixDOF(DOFVars.col(0));
    if(1<nrInitialFrameToFix())
      fixDOF(DOFDiffVars.col(0));
  } else {
    ASSERT_MSGV(false,"Unsupported dynamics model (%d) for trajectory optimization!",dynamicsModel())
  }
  buildObjective();
}
template <typename T>
bool CDMPlanner<T>::plan() {
  if(_initPose.size()==_body->nrDOF())
    buildProblemStaticPose();
  else if(!_steps.empty())
    buildProblemSteps();
  else {
    ASSERT_MSG(false,"Failed initializing problem!")
  }
  if(loadSolution() && _x.size()==_obj.inputs()) {
    std::cout << "Loaded solution file: " << savePath() << std::endl;
    return true;
  }

  bool ret=true;
  if(!useIPOPT())
    ret=solveKnitro(_obj,_x,&_fvec);
  else ret=solveIpopt(_obj,_x);

  saveSolution();
  _obj.checkViolation(&_x);
  if(_PBDDynamics)
    _xPoses=_PBDDynamics->getPoses(_x);
  else _xPoses.resize(0,0);
  return ret;
}
template <typename T>
void CDMPlanner<T>::checkViolation() {
  if(getSolution().size()==_obj.inputs()) {
    Vec x=getSolution();
    _obj.checkViolation(&x);
  } else {
    Vec x=_obj.init();
    _obj.checkViolation(&x);
  }
}
template <typename T>
const typename CDMPlanner<T>::Vec& CDMPlanner<T>::getSolution() const {
  return _x;
}
template <typename T>
const PositionSequence<T>& CDMPlanner<T>::getPosSeq(int footId) const {
  for(int i=0; i<(int)_ees.size(); i++)
    if(_ees[i]->jointId()==footId)
      return *(_poses[i]);
  ASSERT_MSGV(false,"Cannot find PosSeq for joint %d",footId)
  return *(_poses[0]);
}
template <typename T>
const ForceSequence<T>& CDMPlanner<T>::getForceSeq(int footId) const {
  for(int i=0; i<(int)_ees.size(); i++)
    if(_ees[i]->jointId()==footId)
      return *(_forces[i]);
  ASSERT_MSGV(false,"Cannot find ForceSeq for joint %d",footId)
  return *(_forces[0]);
}
template <typename T>
typename CDMPlanner<T>::Vec3T CDMPlanner<T>::getForce(int footId,T t) const {
  while(t<0)
    t+=horizon();
  while(t>=horizon())
    t-=horizon();
  return getForceSeq(footId).timeInterpolateStencil(_x,t,NULL,NULL);
}
template <typename T>
typename CDMPlanner<T>::Mat3T CDMPlanner<T>::getInitFrame() const {
  Mat3XT TM=_body->getT(_initPose.template cast<ArticulatedBody::T>()).template cast<T>();
  Mat3XT ret=ROTI(TM,0)*_ees.front()->_frame.template cast<T>();
  return ret;
}
template <typename T>
bool CDMPlanner<T>::hasForceSeq() const {
  return _forces.size()==_ees.size();
}
//property
template <typename T>
void CDMPlanner<T>::singleHorizon(int N) {
  put<int>(_pt,"singleHorizon",N);
}
template <typename T>
int CDMPlanner<T>::singleHorizon() const {
  return get<int>(_pt,"singleHorizon",10);
}
template <typename T>
void CDMPlanner<T>::doubleHorizon(int N) {
  put<int>(_pt,"doubleHorizon",N);
}
template <typename T>
int CDMPlanner<T>::doubleHorizon() const {
  return get<int>(_pt,"doubleHorizon",10);
}
template <typename T>
void CDMPlanner<T>::eps(T eps) {
  put<T>(_pt,"minPhaseTime",eps);
}
template <typename T>
T CDMPlanner<T>::eps() const {
  return get<T>(_pt,"minPhaseTime",0.1f);
}
template <typename T>
void CDMPlanner<T>::horizon(T horizon) {
  put<T>(_pt,"horizon",horizon);
}
template <typename T>
T CDMPlanner<T>::horizon() const {
  return get<T>(_pt,"horizon",10.0f);
}
template <typename T>
void CDMPlanner<T>::stage(int stage) {
  put<int>(_pt,"stage",stage);
}
template <typename T>
int CDMPlanner<T>::stage() const {
  return get<int>(_pt,"stage",5);
}
template <typename T>
void CDMPlanner<T>::stageTorso(int stageTorso) {
  put<int>(_pt,"stageTorso",stageTorso);
}
template <typename T>
int CDMPlanner<T>::stageTorso() const {
  return get<int>(_pt,"stageTorso",-std::abs(stage()*2));
}
template <typename T>
void CDMPlanner<T>::subStage(int stage) {
  put<int>(_pt,"subStage",stage);
}
template <typename T>
int CDMPlanner<T>::subStage() const {
  return get<int>(_pt,"subStage",3);
}
template <typename T>
void CDMPlanner<T>::stencilSize(int stencilSize) {
  put<int>(_pt,"stencilSize",stencilSize);
}
template <typename T>
int CDMPlanner<T>::stencilSize() const {
  return get<int>(_pt,"stencilSize",7);
}
template <typename T>
void CDMPlanner<T>::period(const Vec2T& period) {
  putPtree<Vec2T>(_pt,"period",period);
}
template <typename T>
typename CDMPlanner<T>::Vec2T CDMPlanner<T>::period() const {
  return parsePtreeDef<Vec2T>(_pt,"period",Vec2T(0,SQPObjective<T>::infty()));
}
template <typename T>
void CDMPlanner<T>::staticToSwing(T staticToSwing) {
  put<T>(_pt,"staticToSwing",staticToSwing);
}
template <typename T>
T CDMPlanner<T>::staticToSwing() const {
  return get<T>(_pt,"staticToSwing",2);
}
template <typename T>
void CDMPlanner<T>::targetVelocity(const Vec4T& v) {
  putPtree<Vec4T>(_pt,"targetVelocity",v);
}
template <typename T>
typename CDMPlanner<T>::Vec4T CDMPlanner<T>::targetVelocity() const {
  return parsePtreeDef<Vec4T>(_pt,"targetVelocity",Vec4T::Zero());
}
template <typename T>
void CDMPlanner<T>::targetHeading(const Vec4T& h) {
  putPtree<Vec4T>(_pt,"targetHeading",h);
}
template <typename T>
typename CDMPlanner<T>::Vec4T CDMPlanner<T>::targetHeading() const {
  return parsePtreeDef<Vec4T>(_pt,"targetHeading",Vec4T::Zero());
}
template <typename T>
void CDMPlanner<T>::terminalHeading(const Vec4T& h) {
  putPtree<Vec4T>(_pt,"terminalHeading",h);
}
template <typename T>
typename CDMPlanner<T>::Vec4T CDMPlanner<T>::terminalHeading() const {
  return parsePtreeDef<Vec4T>(_pt,"terminalHeading",Vec4T::Zero());
}
template <typename T>
void CDMPlanner<T>::terminalPosition(const Vec4T& pos) {
  putPtree<Vec4T>(_pt,"terminalPosition",pos);
}
template <typename T>
typename CDMPlanner<T>::Vec4T CDMPlanner<T>::terminalPosition() const {
  return parsePtreeDef<Vec4T>(_pt,"terminalPosition",Vec4T::Zero());
}
template <typename T>
void CDMPlanner<T>::laplaceCoef(T coef) {
  put<T>(_pt,"laplaceCoef",coef);
}
template <typename T>
T CDMPlanner<T>::laplaceCoef() const {
  return get<T>(_pt,"laplaceCoef",1e-2f);
}
template <typename T>
void CDMPlanner<T>::laplaceTorsoCoef(T coef) {
  put<T>(_pt,"laplaceTorsoCoef",coef);
}
template <typename T>
T CDMPlanner<T>::laplaceTorsoCoef() const {
  return get<T>(_pt,"laplaceTorsoCoef",laplaceCoef());
}
template <typename T>
void CDMPlanner<T>::nrInitialFrameToFix(int nrFrame) {
  put<int>(_pt,"nrInitialFrameToFix",nrFrame);
}
template <typename T>
int CDMPlanner<T>::nrInitialFrameToFix() const {
  return get<int>(_pt,"nrInitialFrameToFix",1);
}
template <typename T>
void CDMPlanner<T>::climbingFootMask(int mask) {
  put<int>(_pt,"climbingFootMask",mask);
}
template <typename T>
int CDMPlanner<T>::climbingFootMask() const {
  return get<int>(_pt,"climbingFootMask",0);
}
template <typename T>
void CDMPlanner<T>::DOFToFix(const Eigen::Matrix<int,6,1>& DOF) {
  putPtree<Eigen::Matrix<int,6,1>>(_pt,"DOFToFix",DOF);
}
template <typename T>
Eigen::Matrix<int,6,1> CDMPlanner<T>::DOFToFix() const {
  Eigen::Matrix<int,6,1> DOFDefault;
  DOFDefault << 1,1,0,0,0,0;  //default to only fix the X,Y translation
  return parsePtreeDef<Eigen::Matrix<int,6,1>>(_pt,"DOFToFix",DOFDefault);
}
template <typename T>
void CDMPlanner<T>::dynamicsModel(DYNAMICS_MODEL model) {
  put<int>(_pt,"dynamicsModel",model);
}
template <typename T>
typename CDMPlanner<T>::DYNAMICS_MODEL CDMPlanner<T>::dynamicsModel() const {
  return (DYNAMICS_MODEL)get<int>(_pt,"dynamicsModel",POSITION_BASED);
}
template <typename T>
void CDMPlanner<T>::savePath(const std::string& path) {
  put<std::string>(_pt,"savePath",path);
}
template <typename T>
std::string CDMPlanner<T>::savePath() const {
  return get<std::string>(_pt,"savePath","");
}
//helper
template <typename T>
bool CDMPlanner<T>::loadSolution() {
  if(savePath().empty() || !exists(savePath()))
    return false;
  std::ifstream is(savePath(),std::ios::binary);
  readBinaryData(_initPose,is);
  readBinaryData(_x,is);
  if(_PBDDynamics && _x.size()==_obj.inputs())
    _xPoses=_PBDDynamics->getPoses(_x);
  return true;
}
template <typename T>
void CDMPlanner<T>::saveSolution() {
  if(!savePath().empty()) {
    std::ofstream os(savePath(),std::ios::binary);
    writeBinaryData(_initPose,os);
    writeBinaryData(_x,os);
    std::cout << "Saved solution file: " << savePath() << std::endl;
  }
}
template <typename T>
void CDMPlanner<T>::buildDynamics() {
  if(dynamicsModel()==POSITION_BASED)
    _PBDDynamics.reset(new PBDDynamicsSequence<T>(_obj,"",dt(),period(),_body,g().template cast<T>(),_poses,_forces,_ees));
  else if(dynamicsModel()==NEWTON_EULER)
    _PBDDynamics.reset(new NEDynamicsSequence<T>(_obj,"",dt(),period(),_body,g().template cast<T>(),_poses,_forces,_ees));
  else if(dynamicsModel()==SIMPLIFIED_NEWTON_EULER) {
    buildSimplifiedBody();
    _PBDDynamics.reset(new NESimplifiedDynamicsSequence<T>(_obj,"",dt(),period(),_body,_bodySimplified,g().template cast<T>(),_poses,_forces,_ees));
  } else if(dynamicsModel()==SIMPLIFIED_NEWTON_EULER_SPLINE) {
    buildSimplifiedBody();
    _phases.push_back(std::shared_ptr<PhaseSequence<T>>(new PhaseSequence<T>(_obj,"Torso",eps(),horizon(),stageTorso(),subStage())));
    _phases.back()->initSameSpan(_obj,2);
    _obj.addComponent(_phases.back());
    _PBDDynamics.reset(new NESimplifiedDynamicsSequenceSpline<T>(_obj,"",dt(),_body,_bodySimplified,g().template cast<T>(),_phases.back(),_poses,_forces,_ees));
  } else if(dynamicsModel()==PBCDM) {
    buildSimplifiedBody();
    _PBDDynamics.reset(new PBCDMDynamicsSequence<T>(_obj,"",dt(),period(),_body,_bodySimplified,g().template cast<T>(),_poses,_forces,_ees));
  } else if(dynamicsModel()==PBCDM_SPLINE) {
    buildSimplifiedBody();
    _phases.push_back(std::shared_ptr<PhaseSequence<T>>(new PhaseSequence<T>(_obj,"Torso",eps(),horizon(),stageTorso(),subStage())));
    _phases.back()->initSameSpan(_obj,2);
    _obj.addComponent(_phases.back());
    _PBDDynamics.reset(new PBCDMDynamicsSequenceSpline<T>(_obj,"",dt(),_body,_bodySimplified,g().template cast<T>(),_phases.back(),_poses,_forces,_ees));
  } else {
    ASSERT_MSGV(false,"Unsupported dynamics model (%d) for trajectory optimization!",dynamicsModel())
  }
  _obj.addComponent(_PBDDynamics);
  if(_DEnv) {
    std::shared_ptr<PBCDMDynamicsSequence<T>> PBCDM=
        std::dynamic_pointer_cast<PBCDMDynamicsSequence<T>>(_PBDDynamics);
    ASSERT_MSG(PBCDM,"DeformedEnvironment only supports PBCDM!")
    PBCDM->setDeformedEnvironment(_DEnv);
  }
}
template <typename T>
void CDMPlanner<T>::buildObjective() {
  if(targetVelocity()[3]>0) {
    std::shared_ptr<TargetVelocityObj<T>> diffObj(new TargetVelocityObj<T>(_obj,"Velocity",targetVelocity().template segment<3>(0),targetVelocity()[3],_PBDDynamics));
    _objectives.push_back(diffObj);
    _obj.addComponent(diffObj);
  }
  if(targetHeading()[3]>0) {
    std::shared_ptr<TargetHeadingObj<T>> diffObj(new TargetHeadingObj<T>(_obj,"Heading",targetHeading().template segment<3>(0),targetHeading()[3],_PBDDynamics));
    _objectives.push_back(diffObj);
    _obj.addComponent(diffObj);
  }
  if(terminalHeading()[3]!=0) {
    const Eigen::Matrix<int,-1,-1>& DOFVars=_PBDDynamics->DOFVar();
    std::shared_ptr<OrientationObj<T>> rotObj(new OrientationObj<T>(_obj,"",DOFVars.cols()-1,expWGradV<T,Vec3T>(terminalHeading().template segment<3>(0)),terminalHeading()[3],_PBDDynamics));
    _objectives.push_back(rotObj);
    _obj.addComponent(rotObj);
  }
  if(terminalPosition()[3]!=0) {
    const Eigen::Matrix<int,-1,-1>& DOFVars=_PBDDynamics->DOFVar();
    std::shared_ptr<PositionObj<T>> posObj(new PositionObj<T>(_obj,"",DOFVars.cols()-1,terminalPosition().template segment<3>(0),Eigen::Matrix<T,2,3>::Identity(),terminalPosition()[3],_PBDDynamics));
    _objectives.push_back(posObj);
    _obj.addComponent(posObj);
  }
  if(laplaceCoef()>0)
    for(int i=0; i<(int)_ees.size(); i++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[i];
      std::shared_ptr<PositionSequence<T>> pos=_poses[i];
      std::shared_ptr<LaplaceObjective<T>> smoothObj(new LaplaceObjective<T>(_obj,ee->name(),pos->getVar(),pos->getDiffVar(),laplaceCoef()));
      _objectives.push_back(smoothObj);
      _obj.addComponent(smoothObj);
    }
  if(laplaceTorsoCoef()>0) {
    //Only torso trajectories represented using splines can have laplace objective functions
    //Case I: PBCDMDynamicsSequenceSpline
    //Case II: NESimplifiedDynamicsSequenceSpline
    std::shared_ptr<PBCDMDynamicsSequenceSpline<T>> CDMSpline=std::dynamic_pointer_cast<PBCDMDynamicsSequenceSpline<T>>(_PBDDynamics);
    std::shared_ptr<NESimplifiedDynamicsSequenceSpline<T>> NESpline=std::dynamic_pointer_cast<NESimplifiedDynamicsSequenceSpline<T>>(_PBDDynamics);
    if(CDMSpline || NESpline) {
      std::shared_ptr<VectorSequence<T,6>> DOFSeq=CDMSpline ? CDMSpline->DOFSeq() : NESpline->DOFSeq();
      std::shared_ptr<LaplaceObjective<T>> smoothObj(new LaplaceObjective<T>(_obj,CDMSpline ? CDMSpline->_name : NESpline->_name,
                                        DOFSeq->getVar(),DOFSeq->getDiffVar(),laplaceTorsoCoef()));
      _objectives.push_back(smoothObj);
      _obj.addComponent(smoothObj);
    }
  }
}
template <typename T>
void CDMPlanner<T>::fixDOF(const Eigen::Matrix<int,-1,1>& DOF) {
  for(int r=0; r<DOF.size(); r++)
    _obj.addVar(DOF[r])._l=_obj.addVar(DOF[r])._u=_obj.addVar(DOF[r])._init;
}
template <typename T>
void CDMPlanner<T>::initDOF(const Eigen::Matrix<int,-1,1>& DOF,const Vec& pose,bool fix) {
  const Eigen::Matrix<int,6,1> fixedDOFs=DOFToFix();
  ASSERT(DOF.size()==pose.size())
  for(int r=0; r<DOF.size(); r++) {
    _obj.setVarInit(DOF[r],pose[r]);
    if(fix && r<6 && fixedDOFs[r]==1)
      _obj.addVar(DOF[r])._l=_obj.addVar(DOF[r])._u=pose[r];
  }
}
template class CDMPlanner<FLOAT>;
}
