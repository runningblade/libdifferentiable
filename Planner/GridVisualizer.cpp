#include "GridVisualizer.h"
#include <Environment/EnvironmentVisualizer.h>
#include <Articulated/ArticulatedVisualizer.h>

namespace PHYSICSMOTION {
template <typename T>
GridVisualizer<T>::GridVisualizer(std::shared_ptr<GridPlanner<T>> gridPlanner,DRAWER::Drawer& drawer,std::shared_ptr<ArticulatedBody> bodyVis,const Eigen::Matrix<GLfloat,3,1>& colorO)
  :_gridPlanner(gridPlanner),
   _drawer(drawer),
   _bodyVis(bodyVis) {
  visualizeObstacles(colorO);
  std::cout << "Obstacle visualized" << std::endl;
  _shapeArm=visualizeArticulated(_bodyVis);
  std::cout << "Body Visualized" << std::endl;
  _drawer.addShape(_shapeArm);
  visualizeTarget();
  std::cout << "Targets Visualized" << std::endl;
}
template <typename T>
void GridVisualizer<T>::visualizeObstacles(const Eigen::Matrix<GLfloat,3,1>& color) {
  for(const auto& obstacle:_gridPlanner->getPlanner()->getObstacles()) {
    std::shared_ptr<DRAWER::Shape> s=visualizeShapeExact(obstacle,false);
    s->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
    s->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
    s->setColorDiffuse(GL_TRIANGLES,color[0],color[1],color[2]);
    _drawer.addShape(s);
  }
}
template <typename T>
void GridVisualizer<T>::visualizeTarget() {
  //sphere
  T rad=0.025;
  std::vector<Eigen::Matrix<double,3,1>> vss;
  std::vector<Eigen::Matrix<int,3,1>> iss;
  PHYSICSMOTION::addSphere(vss,iss,Eigen::Matrix<double,3,1>::Zero(),(double) rad,2);
  std::shared_ptr<ShapeExact> target(new MeshExact(vss,iss,true));
  std::shared_ptr<DRAWER::Shape> failure=visualizeShapeExact(target,false);
  failure->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
  failure->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
  failure->setColorDiffuse(GL_TRIANGLES,1,0,0);
  std::shared_ptr<DRAWER::Shape> success=visualizeShapeExact(target,false);
  success->setColorAmbient(GL_TRIANGLES,.5,.5,.5);
  success->setColorSpecular(GL_TRIANGLES,0.1,0.1,0.1);
  success->setColorDiffuse(GL_TRIANGLES,0,1,0);
  _shapeTarget.reset(new DRAWER::CompositeShape);
  for(int i=0; i<(int)_gridPlanner->getTargets().size(); ++i) {
    const auto& tar=_gridPlanner->getTargets()[i];
    std::cout << "Tar=" << tar.transpose() << std::endl;
    if(tar.hasNaN())
      continue;
    std::shared_ptr<DRAWER::Bullet3DShape> st(new DRAWER::Bullet3DShape);
    st->setLocalTranslate(tar.template cast<GLfloat>());
    if(_gridPlanner->getTargetSuccessInfo()[i])
      st->addShape(success);
    else
      st->addShape(failure);
    _shapeTarget->addShape(st);
  }
  _drawer.addShape(_shapeTarget);
}
template <typename T>
void GridVisualizer<T>::update(T t) {
  for(int i=0; i<(int)_gridPlanner->getControlPointsAllPaths().size(); ++i) {
    const auto& cps=_gridPlanner->getControlPointsAllPaths()[i];
    if(cps.size()==0) continue;
    T totalTime=_gridPlanner->getPlanner()->getThetaTrajectory().getNumSegment()*cps.size();
    if(t<=totalTime) {
      for(int j=0; j<(int)cps.size(); ++j) {
        const auto& cp=cps[j];
        T totalTimeForOneTarget=_gridPlanner->getPlanner()->getThetaTrajectory().getNumSegment();
        if(t<=totalTimeForOneTarget) {
          Vec theta=_gridPlanner->getPlanner()->getThetaTrajectory().getPoint(cp,t);
          updateArticulatedBody(_shapeArm,_bodyVis,ArticulatedBody::Vec(theta.template cast<double>()));
          break;
        } else t-=_gridPlanner->getPlanner()->getThetaTrajectory().getNumSegment();
      }
      break;
    } else t-=_gridPlanner->getPlanner()->getThetaTrajectory().getNumSegment()*cps.size();
  }
}
template <typename T>
T GridVisualizer<T>::totalTime() const {
  T totalTime=0;
  for(int i=0; i<(int)_gridPlanner->getControlPointsAllPaths().size(); ++i) {
    const auto& cps=_gridPlanner->getControlPointsAllPaths()[i];
    totalTime+=_gridPlanner->getPlanner()->getThetaTrajectory().getNumSegment()*cps.size();
  }
  return totalTime;
}
template class GridVisualizer<FLOAT>;
}
