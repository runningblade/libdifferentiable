#ifndef FOOT_STEP_PLANNER_H
#define FOOT_STEP_PLANNER_H

#include "PlannerBase.h"
#include <Environment/ConvexHullExact.h>
#include <Articulated/EndEffectorInfo.h>
#include <Optimizer/MixedIntegerObstacle.h>
#include <Optimizer/MixedIntegerReachable.h>
#include <Optimizer/LaplaceObjective.h>

namespace PHYSICSMOTION {
/*implementation of the following paper:
@inproceedings{deits2014footstep,
  title={Footstep planning on uneven terrain with mixed-integer convex optimization},
  author={Deits, Robin and Tedrake, Russ},
  booktitle={2014 IEEE-RAS international conference on humanoid robots},
  pages={279--286},
  year={2014},
  organization={IEEE}
}
step scheduling:
#
| #
|   #
|   * #
|   *   #
|   * # |
|   #   |
| # *   |
#   *   |
| # *   |
|   #   |
|   * # |
|   *   #
|   * # |
|   #   |
| # *   |
#   *   |
| # *   |
|   #   |
|   * # |
|   *   #
|   * #
|   #
| #
#
notation:
tilted   # symbol: robot state (struct FootStep)
vertical | symbol: motion of EE from step(i)->step(i+1) (must satisfy ee.isEven()!=step(i)._isEven)
vertical * symbol: body torso motion
*/
template <typename T>
class FootStepPlanner : public PlannerBase<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef typename SQPObjectiveComponent<T>::PolyXT PolyXT;
  typedef typename PolyXT::VECP VECP;
  using PlannerBase<T>::_pt;
  using PlannerBase<T>::_env;
  using PlannerBase<T>::_body;
  using PlannerBase<T>::_bodyVis;
  using PlannerBase<T>::_ees;
  using PlannerBase<T>::_xPoses;
  using PlannerBase<T>::stepToDOF;
  using PlannerBase<T>::mu;
  using PlannerBase<T>::dt;
  using PlannerBase<T>::g;
  using PlannerBase<T>::bodyHeightCoef;
  using PlannerBase<T>::bodyHeightTarget;
  using PlannerBase<T>::callback;
  using PlannerBase<T>::tol;
  using PlannerBase<T>::solveGurobi;
  FootStepPlanner();
  FootStepPlanner(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize);
  FootStepPlanner(std::shared_ptr<Environment<T>> env,
                  std::shared_ptr<ArticulatedBody> body,
                  std::shared_ptr<ArticulatedBody> bodyVis,
                  const std::vector<std::shared_ptr<EndEffectorBounds>>& ees);
  std::shared_ptr<SerializableBase> copy() const override;
  //res: number of timesteps to discretize
  void computeInitialPose(const Vec3T& target,bool isEven=true);
  void computeInitialPose(const Vec3T* target=NULL,bool isEven=true);
  void setInitialStep(const Vec& DOF,bool isEven);
  void setSteps(const std::vector<std::shared_ptr<FootStep<T>>>& steps);
  std::vector<std::shared_ptr<FootStep<T>>> stepsSmartPtr() const;
  const std::vector<FootStep<T>>& steps() const;
  void clearProblem();
  void buildProblem();
  bool plan(bool interpolate=true);
  void buildProblemFeasibility();
  bool makeFeasible();
  void update();
  void update(const Vec& x);
  void update(std::shared_ptr<FootStep<T>> step0);
  void addObstacle(std::shared_ptr<ShapeExact> obstacle);
  const std::unordered_set<std::shared_ptr<MixedIntegerObstacle<T>>>& getObstacles() const;
  void getPosesInterpolated();
  //property
  void resRotation(int r);
  int resRotation() const;
  void horizon(int N);
  int horizon() const;
  void previewResolution(int res);
  int previewResolution() const;
  void laplaceCoef(T coef);
  T laplaceCoef() const;
  void targetVelocity(const Vec3T& v);
  Vec3T targetVelocity() const;
  void targetHeading(const Vec2T& h);
  Vec2T targetHeading() const;
  void terminalHeading(const Vec2T& h);
  Vec2T terminalHeading() const;
  void terminalPosition(const Vec3T& p);
  Vec3T terminalPosition() const;
  void lazyObstacle(bool lazyObstacle);
  bool lazyObstacle() const;
  void syncHeading(bool syncHeading);
  bool syncHeading() const;
  void limitDistance(T lmtVel);
  T limitDistance() const;
 protected:
  T getBigM() const;
  T getClearance() const;
  T updateHeading(const Vec2T& dir,T eps=1e-3f) const;
  //solver data
  SQPObjectiveCompound<T> _obj;
  std::shared_ptr<LaplaceObjective<T,3>> _smoothBody;
  std::vector<std::shared_ptr<LaplaceObjective<T,2>>> _smoothFoot;
  std::vector<std::shared_ptr<MixedIntegerReachable<T>>> _reachable;
  std::unordered_set<std::shared_ptr<MixedIntegerObstacle<T>>> _obstacles;
  std::shared_ptr<PolynomialObjective<T>> _targetVelocityObj;
  std::shared_ptr<PolynomialObjective<T>> _targetHeadingObj;
  std::shared_ptr<PolynomialObjective<T>> _terminalHeadingObj;
  std::shared_ptr<PolynomialObjective<T>> _terminalPositionObj;
  std::vector<FootStep<T>> _steps;
};
}

#endif
