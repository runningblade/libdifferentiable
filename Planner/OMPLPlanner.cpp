#include "OMPLPlanner.h"
#ifdef OMPL_SUPPORT
#include <SIPCollision/TargetEnergy.h>
#include <Utils/Timing.h>
#include <stack>
//base
#include <ompl/base/samplers/informed/PathLengthDirectInfSampler.h>
#include <ompl/base/objectives/PathLengthOptimizationObjective.h>
#include <ompl/base/objectives/MinimaxObjective.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/ProblemDefinition.h>
#include <ompl/base/PlannerDataGraph.h>
#include <ompl/base/PlannerData.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/goals/GoalRegion.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>

namespace ob = ompl::base;
namespace og = ompl::geometric;
namespace PHYSICSMOTION {
//collision checker
template <typename T>
class ValidityChecker : public ob::StateValidityChecker {
 public:
  ValidityChecker(std::shared_ptr<CollisionHandler<T>> handler,const ob::SpaceInformationPtr& si)
    :ob::StateValidityChecker(si),_handler(handler) {}
  bool isValid(const ob::State* state) const override {
    //set handler state
    CollisionHandler<T> handler=*_handler;
    const auto* stateR=state->as<ob::RealVectorStateSpace::StateType>();
    Eigen::Map<Eigen::Matrix<double,-1,1>> values(stateR->values,handler.getBody()->nrDOF());
    handler.setControlPoints(values.template cast<T>());
    handler.update();
    //check CCD (we copy the structure to make it thread-safe
    handler.getObsTTPairs().clear();
    handler.getObsCCPlanes().clear();
    handler.getSelfTTPairs().clear();
    handler.getSelfCCPlanes().clear();
    handler.CCD(false,true,false);
    return handler.getObsTTPairs().empty() &&
           handler.getObsCCPlanes().getCCPlanes().empty() &&
           handler.getSelfTTPairs().empty() &&
           handler.getSelfCCPlanes().getCCPlanes().empty();
  }
  double clearance(const ob::State* state) const override {
    ASSERT_MSG(false,"Clearance function is not supported!")
    return 0;
  }
  std::shared_ptr<CollisionHandler<T>> _handler;
};
//goal checker
template <typename T>
class GoalRegion : public ob::GoalRegion {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  GoalRegion(std::shared_ptr<CollisionHandler<T>> handler,std::vector<std::shared_ptr<TargetEnergy<T>>> trajEss,const ob::SpaceInformationPtr& si)
    :ob::GoalRegion(si),_handler(handler),_trajEss(trajEss) {}
  double distanceGoal(const ob::State* s) const {
    const auto* sR=s->as<ob::RealVectorStateSpace::StateType>();
    Eigen::Map<const Eigen::Matrix<double,-1,1>> values(sR->values,_handler->getBody()->nrDOF());
    Vec CP=values.template cast<T>();
    //compute max target energy
    T E=0,maxE=0;
    for(auto& targetPtr:_trajEss) {
      E=0;
      TargetEnergy<T> target(*targetPtr,CP,_handler->getThetaTrajectory());
      target.SIPEnergy<T>::eval(&E,(Vec*)NULL,(MatT*)NULL);
      maxE=std::max(E,maxE);
    }
    return (double)sqrt(maxE);
  }
  static ob::Cost goalRegionCostToGo(const ob::State* state,const ob::Goal* goal) {
    const GoalRegion* goalRegion=goal->as<GoalRegion>();
    return ob::Cost(std::max(goalRegion->distanceGoal(state)-goalRegion->getThreshold(),0.0));
  }
  std::shared_ptr<CollisionHandler<T>> _handler;
  std::vector<std::shared_ptr<TargetEnergy<T>>> _trajEss;
};
//objective computer
template <typename T>
class TargetObjective : public ob::MinimaxObjective {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  TargetObjective(std::shared_ptr<CollisionHandler<T>> handler,std::vector<std::shared_ptr<TargetEnergy<T>>> trajEss,const ob::SpaceInformationPtr& si)
    :ob::MinimaxObjective(si),_handler(handler),_trajEss(trajEss) {
    description_="Custom";
  }
  ob::Cost stateCost(const ob::State* s) const {
    const auto* sR=s->as<ob::RealVectorStateSpace::StateType>();
    Eigen::Map<const Eigen::Matrix<double,-1,1>> values(sR->values,_handler->getBody()->nrDOF());
    Vec CP=values.template cast<T>();
    //compute total target energy
    T E=0,totalE=0;
    for(auto& targetPtr:_trajEss) {
      E=0;
      TargetEnergy<T> target(*targetPtr,CP,_handler->getThetaTrajectory());
      target.SIPEnergy<T>::eval(&E,(Vec*)NULL,(MatT*)NULL);
      totalE+=E;
    }
    return ob::Cost((double)totalE);
  }
  std::shared_ptr<CollisionHandler<T>> _handler;
  std::vector<std::shared_ptr<TargetEnergy<T>>> _trajEss;
};
//extract solution
template <typename T>
std::vector<int> bestSolution(const ob::PlannerData& data,std::shared_ptr<ob::MultiOptimizationObjective> obj,double* length=NULL,double* stateCost=NULL) {
  auto tar=std::dynamic_pointer_cast<TargetObjective<T>>(obj->getObjective(0));
  std::unordered_map<int,std::pair<int,T>> prevCost;
  //init
  int minId=data.getStartIndex(0),startId=minId;
  double minCost=tar->stateCost(data.getVertex(minId).getState()).value();
  for(int i=0; i<(int)data.numVertices(); i++) {
    double cost=tar->stateCost(data.getVertex(i).getState()).value();
    if(cost<minCost) {
      minId=i;
      minCost=cost;
    }
  }
  //get path
  std::vector<int> path;
  path.push_back(minId);
  while(path.back()!=startId) {
    std::vector<unsigned int> last;
    ASSERT(data.getIncomingEdges(path.back(),last)==1)
    path.push_back(last[0]);
  }
  std::reverse(path.begin(),path.end());
  //path length
  if(length) {
    *length=0;
    for(int i=0; i<(int)path.size()-1; i++)
      *length+=obj->getSpaceInformation()->distance(data.getVertex(path[i]).getState(),data.getVertex(path[i+1]).getState());
  }
  if(stateCost)
    *stateCost=tar->stateCost(data.getVertex(path.back()).getState()).value();
  return path;
}
//OMPLPlanner
template <typename T>
OMPLPlanner<T>::OMPLPlanner(std::shared_ptr<ArticulatedBody> body,T d0,T epsTime,const std::unordered_map<int,std::unordered_set<int>>& skipSelfCCD) {
  _handler.reset(new CollisionHandler<T>(body,5,0,d0,0,0,0,0,false,skipSelfCCD));
  _epsTime=epsTime;
  _interval=10; //check every 10 seconds
  _runtime=600; //at most run for 10 minutes
  _threshold=1e-3;
  _smoothness=0;
}
template <typename T>
bool OMPLPlanner<T>::read(std::istream& is,IOData* dat) {
  registerType<ArticulatedBody>(dat);
  registerType<CollisionHandler<T>>(dat);
  readBinaryData(_epsTime,is);
  readBinaryData(_interval,is);
  readBinaryData(_runtime,is);
  readBinaryData(_threshold,is);
  readBinaryData(_smoothness,is);
  readBinaryData(_states,is);
  _handler->read(is,dat);
  return is.good();
}
template <typename T>
bool OMPLPlanner<T>::write(std::ostream& os,IOData* dat) const {
  registerType<ArticulatedBody>(dat);
  registerType<CollisionHandler<T>>(dat);
  writeBinaryData(_epsTime,os);
  writeBinaryData(_interval,os);
  writeBinaryData(_runtime,os);
  writeBinaryData(_threshold,os);
  writeBinaryData(_smoothness,os);
  writeBinaryData(_states,os);
  _handler->write(os,dat);
  return os.good();
}
template <typename T>
std::shared_ptr<SerializableBase> OMPLPlanner<T>::copy() const {
  return std::shared_ptr<SerializableBase>(new OMPLPlanner(_handler->getBody(),_handler->d0(),_epsTime,_handler->getSkipJIDPairs()));
}
template <typename T>
std::string OMPLPlanner<T>::type() const {
  return typeid(OMPLPlanner<T>).name();
}
//setup
template <typename T>
void OMPLPlanner<T>::setRobot(std::shared_ptr<ArticulatedBody> body,bool randomInitialize,bool neutralInit) {
  _handler->setRobot(body,randomInitialize,neutralInit);
}
template <typename T>
void OMPLPlanner<T>::addRobot(std::shared_ptr<ArticulatedBody> body,const Vec& DOF) {
  _handler->addRobot(body,DOF);
}
template <typename T>
void OMPLPlanner<T>::addObject(const std::string& path,T scale,const Vec3T& pos,const Mat3T& rot,int maxConvexHulls) {
  _handler->addObject(path,scale,pos,rot,maxConvexHulls);
}
template <typename T>
void OMPLPlanner<T>::addSphere(T r,int res,const Vec3T& pos) {
  _handler->addSphere(r,res,pos);
}
template <typename T>
void OMPLPlanner<T>::addCuboid(T l,T w,T h,const Vec3T& pos,const Mat3T& R) {
  _handler->addCuboid(l,w,h,pos,R);
}
template <typename T>
void OMPLPlanner<T>::addCapsule(const Vec3T& a,const Vec3T& b,T radius,int res) {
  _handler->addCapsule(a,b,radius,res);
}
template <typename T>
void OMPLPlanner<T>::addCapsule(T l,T w,T h,Vec3T pos,T radius,int res) {
  _handler->addCapsule(l,w,h,pos,radius,res);
}
template <typename T>
void OMPLPlanner<T>::assembleBodyBVH(bool useConvexHull) {
  _handler->assembleBodyBVH(useConvexHull);
}
template <typename T>
void OMPLPlanner<T>::assembleObsBVH() {
  _handler->assembleObsBVH();
}
template <typename T>
std::vector<std::shared_ptr<MeshExact>> OMPLPlanner<T>::getObstacles() const {
  return _handler->getObstacles();
}
template <typename T>
T OMPLPlanner<T>::getEpsTime() const {
  return _epsTime;
}
template <typename T>
T OMPLPlanner<T>::getInterval() const {
  return _interval;
}
template <typename T>
T OMPLPlanner<T>::getRuntime() const {
  return _runtime;
}
template <typename T>
T OMPLPlanner<T>::getThreshold() const {
  return _threshold;
}
template <typename T>
T OMPLPlanner<T>::getSmoothness() const {
  return _smoothness;
}
template <typename T>
void OMPLPlanner<T>::setEpsTime(T epsTime) {
  _epsTime=epsTime;
}
template <typename T>
void OMPLPlanner<T>::setInterval(T interval) {
  _interval=interval;
}
template <typename T>
void OMPLPlanner<T>::setRuntime(T runtime) {
  _runtime=runtime;
}
template <typename T>
void OMPLPlanner<T>::setThreshold(T thres) {
  _threshold=thres;
}
template <typename T>
void OMPLPlanner<T>::setSmoothness(T smooth) {
  _smoothness=smooth;
}
//main function
template <typename T>
void OMPLPlanner<T>::clearEnergy() {
  _trajEss.clear();
}
template <typename T>
void OMPLPlanner<T>::setupEnergy(int JID,const Vec3T& tar,const Vec3T& dir,const Vec3T& ori,T targetCoefP,T targetCoefD,bool JTJApprox) {
  _trajEss.push_back(std::shared_ptr<TargetEnergy<T>>
                     (new TargetEnergy<T>(*(_handler->getBody()),
                                          _handler->getControlPoints(),
                                          _handler->getThetaTrajectory(),
                                          JID,tar,dir,ori,targetCoefP,targetCoefD,JTJApprox)));
}
template <typename T>
bool OMPLPlanner<T>::optimize(int maxIter,bool output,bool newtonMethod) {
  disableTiming();  //no need timing, we use OMPL timing system
  std::shared_ptr<ArticulatedBody> body=_handler->getBody();
  //set space
  auto space=std::make_shared<ob::RealVectorStateSpace>(body->nrDOF());
  auto lb=body->lowerLimit(),ub=body->upperLimit();
  ob::RealVectorBounds bounds(body->nrDOF());
  for(int i=0; i<body->nrDOF(); i++) {
    if(isfinite(lb[i]))
      bounds.setLow(i,lb[i]);
    else bounds.setLow(i,std::numeric_limits<double>::min());
    if(isfinite(ub[i]))
      bounds.setHigh(i,ub[i]);
    else bounds.setHigh(i,std::numeric_limits<double>::max());
  }
  space->setBounds(bounds);
  //collision checker
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  ob::StateValidityCheckerPtr checker=std::make_shared<ValidityChecker<T>>(_handler,si);
  si->setStateValidityChecker(checker);
  si->setup();
  //problem
  auto pdef=std::make_shared<ob::ProblemDefinition>(si);
  //start
  Vec init=_handler->getThetaTrajectory().getPoint(_handler->getControlPoints(),0);
  ob::ScopedState<> start(space),goal(space);
  for(int i=0; i<body->nrDOF(); i++)
    start->as<ob::RealVectorStateSpace::StateType>()->values[i]=(double)init[i];
  pdef->addStartState(start->as<ob::RealVectorStateSpace::StateType>());
  //goal
  auto region=std::make_shared<GoalRegion<T>>(_handler,_trajEss,si);
  region->setThreshold((double)_threshold);
  pdef->setGoal(region);
  //optimize
  auto obj=std::make_shared<ob::MultiOptimizationObjective>(si);
  obj->addObjective(std::make_shared<TargetObjective<T>>(_handler,_trajEss,si),1);
  obj->addObjective(std::make_shared<ob::PathLengthOptimizationObjective>(si),(double)_smoothness);
  pdef->setOptimizationObjective(obj);
  std::shared_ptr<ob::Planner> sol=std::make_shared<og::RRTstar>(si);
  sol->setProblemDefinition(pdef);
  sol->setup();
  //main loop
  double time=0;
  sol->solve([&]() {
    time+=(double)_interval;
    ob::PlannerData data(si);
    double length=0,stateCost=0;
    sol->getPlannerData(data);
    if(data.numStartVertices()==0 || data.getStartIndex(0)<0)
      return false;
    std::vector<int> path=bestSolution<T>(data,obj,&length,&stateCost);
    if(output)
      std::cout << "Time=" << time << "/" << _runtime << " #Vertex=" << data.numVertices() << " #Edge=" << data.numEdges()
                << " #Start=" << data.numStartVertices() << " #Goal=" << data.numGoalVertices()
                << " Length=" << length << " TargetCost=" << stateCost << std::endl;
    return time>_runtime;
  },(double)_interval);
  //extract state
  if(pdef->getSolutionPath()) {
    _states.clear();
    ob::PlannerData data(si);
    sol->getPlannerData(data);
    std::vector<int> path=bestSolution<T>(data,obj);
    for(auto vid:path) {
      auto ss=data.getVertex(vid).getState();
      for(int i=0; i<body->nrDOF(); i++)
        init[i]=ss->as<ob::RealVectorStateSpace::StateType>()->values[i];
      _states.push_back(init);
    }
    _handler->setControlPoints(init);
  }
  return pdef->getSolutionPath()!=NULL;
}
//utilities
template <typename T>
const typename OMPLPlanner<T>::Vec& OMPLPlanner<T>::getControlPoints() const {
  return _handler->getControlPoints();
}
template <typename T>
void OMPLPlanner<T>::setControlPoints(const Vec& newCP) {
  _handler->setControlPoints(newCP);
}
template <typename T>
const ThetaTrajectory<T>& OMPLPlanner<T>::getThetaTrajectory() const {
  return _handler->getThetaTrajectory();
}
template <typename T>
std::shared_ptr<ArticulatedBody> OMPLPlanner<T>::getBody() const {
  return _handler->getBody();
}
template <typename T>
const std::vector<typename OMPLPlanner<T>::Vec>& OMPLPlanner<T>::getSolution() const {
  return _states;
}
//instance
template class OMPLPlanner<FLOAT>;
}

#endif
