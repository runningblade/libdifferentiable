#ifndef SIP_VISUALIZER_H
#define SIP_VISUALIZER_H

#include "CollisionConstrainedPlanner.h"
#include <Articulated/ArticulatedVisualizer.h>
#include <TinyVisualizer/CompositeShape.h>
#include <TinyVisualizer/BezierCurveShape.h>

namespace PHYSICSMOTION {
template <typename T>
class SIPVisualizer {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  explicit SIPVisualizer(const CollisionConstrainedPlanner<T>& planner,DRAWER::Drawer& drawer,
                         std::shared_ptr<ArticulatedBody> bodyVis=NULL,T rad=.025,
                         bool visualizeLocalDir=false,
                         bool visualizeTrajectory=false,
                         bool useBodyColor=false,
                         const Eigen::Matrix<GLfloat,3,1>& colorO=Eigen::Matrix<GLfloat,3,1>(.2,.2,.2),
                         const Eigen::Matrix<GLfloat,3,1>& colorT=Eigen::Matrix<GLfloat,3,1>(.2,.2,.7),
                         const Eigen::Matrix<GLfloat,3,1>& colorA=Eigen::Matrix<GLfloat,3,1>(.7,.2,.2));
  void updateRobot(std::shared_ptr<ArticulatedBody> bodyVis,bool useBodyColor,const Eigen::Matrix<GLfloat,3,1>& colorA);
  void setupEnergy(int JID,const Vec3T& target,const Vec3T& dir,const Vec3T& orientation);
  void update(const std::vector<Vec,Eigen::aligned_allocator<Vec>>& cps,T t);
 private:
  void visualizeTarget(T rad,const Eigen::Matrix<GLfloat,3,1>& color);
  void updateTarget(const ArticulatedBody::Vec& theta);
  void visualizeObstacles(const Eigen::Matrix<GLfloat,3,1>& color);
  //data
  std::shared_ptr<DRAWER::CompositeShape> _shapeBB,_shapeTarget,_shapeOrientation,_local,_traj;
  std::shared_ptr<DRAWER::Shape> _shapeArm,_red,_blue,_green;
  std::shared_ptr<ArticulatedBody> _bodyVis;
  std::unordered_map<int,std::shared_ptr<DRAWER::BezierCurveShape>> _curve;
  std::unordered_map<int,Vec3T> _prePos;
  const CollisionConstrainedPlanner<T>& _planner;
  std::vector<Vec3T> _targets;
  std::vector<Vec3T> _orientations;
  std::unordered_map<int,Vec3T> _localPos;
  std::unordered_map<int,Vec3T> _localDir;
  DRAWER::Drawer& _drawer;
  //style
  Eigen::Matrix<GLfloat,3,1> _colorT;
  T _rad;
  bool _visualizeAllTargetInfo,_visualizeTrajectory,_trajFinished,_trajAdded;
};
}
#endif
