#ifndef TRAJECTORY_VISUALIZER_H
#define TRAJECTORY_VISUALIZER_H

#include "PlannerBase.h"
#include "CDMPlanner.h"
#include <Optimizer/ForceSequence.h>
#include <Optimizer/PositionSequence.h>
#include <Articulated/ArticulatedVisualizer.h>
#include <TinyVisualizer/Bullet3DShape.h>
#include <TinyVisualizer/BezierCurveShape.h>
#include <TinyVisualizer/ArrowShape.h>
#include <TinyVisualizer/MakeMesh.h>

namespace PHYSICSMOTION {
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeVectorSequence(std::shared_ptr<VectorSequence<T>> vss,const typename VectorSequence<T>::Vec& x,GLfloat thickness,
    const Eigen::Matrix<GLfloat,3,1>& colorTraj=Eigen::Matrix<GLfloat,3,1>(.7,.7,.7),
    const Eigen::Matrix<GLfloat,3,1>& colorBall=Eigen::Matrix<GLfloat,3,1>(.7,0,0),
    const Eigen::Matrix<GLfloat,3,1>& colorBallSep=Eigen::Matrix<GLfloat,3,1>(0,.7,0)) {
  using namespace DRAWER;
  Eigen::Matrix<GLfloat,-1,1> cp;
  const Eigen::Matrix<int,3,-1>& var=vss->getVar();
  const Eigen::Matrix<int,3,-1>& diffVar=vss->getDiffVar();
  const Eigen::Matrix<int,-1,1>& timeVar=vss->getPhase()->timeSpanVar();
  cp.resize(var.size()*2);
  for(int i=0; i<var.cols(); i++) {
    int stageId=vss->getPhase()->segmentId(i)*2+(vss->even()?0:1);
    GLfloat span=(GLfloat)x[timeVar[stageId]]/vss->getPhase()->subStage();
    for(int r=0; r<3; r++)
      cp[i*6+r]=(GLfloat)(var(r,i)>=0?x[var(r,i)]:0);
    for(int r=0; r<3; r++)
      cp[i*6+r+3]=(GLfloat)(diffVar(r,i)>=0?x[diffVar(r,i)]:0)*span;
  }
  std::shared_ptr<BezierCurveShape> curve(new BezierCurveShape(thickness));
  curve->addControlPoint<Eigen::Matrix<GLfloat,-1,1>>(cp);
  curve->setColor(GL_QUADS,colorTraj[0],colorTraj[1],colorTraj[2]);
  curve->setCastShadow(false);
  //trackball
  std::shared_ptr<Bullet3DShape> ballT(new Bullet3DShape);
  std::shared_ptr<MeshShape> ball=makeSphere(8,true,thickness*2);
  ball->setColor(GL_TRIANGLES,colorBall[0],colorBall[1],colorBall[2]);
  ballT->addShape(ball);
  //bundle together
  std::shared_ptr<Bullet3DShape> shapeBundle(new Bullet3DShape);
  shapeBundle->addShape(curve);
  shapeBundle->addShape(ballT);
  //separator
  std::shared_ptr<MeshShape> ballS=makeSphere(8,true,thickness*2);
  for(int i=0; i<var.cols()-1; i++)
    if(diffVar(0,i)<0 && diffVar(0,i+1)<0) {
      std::shared_ptr<Bullet3DShape> ballST(new Bullet3DShape);
      ballST->addShape(ballS);
      ballST->setLocalTranslate(cp.segment<3>(i*6));
      ballST->setColor(GL_TRIANGLES,colorBallSep[0],colorBallSep[1],colorBallSep[2]);
      shapeBundle->addShape(ballST);
    }
  return shapeBundle;
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizePositionSequence(std::shared_ptr<PositionSequence<T>> vss,const typename VectorSequence<T>::Vec& x,GLfloat thickness,
    const Eigen::Matrix<GLfloat,3,1>& colorTraj=Eigen::Matrix<GLfloat,3,1>(.4,.4,.4),
    const Eigen::Matrix<GLfloat,3,1>& colorBall=Eigen::Matrix<GLfloat,3,1>(.7,0,0),
    const Eigen::Matrix<GLfloat,3,1>& colorBallSep=Eigen::Matrix<GLfloat,3,1>(0,.7,0),
    const Eigen::Matrix<GLfloat,3,1>& colorOnGround=Eigen::Matrix<GLfloat,3,1>(0,0,0),
    const Eigen::Matrix<GLfloat,3,1>& colorAboveGround=Eigen::Matrix<GLfloat,3,1>(0,0,.7)) {
  using namespace DRAWER;
  std::shared_ptr<Shape> shapeBundle=visualizeVectorSequence(std::shared_ptr<VectorSequence<T>>(vss),x,thickness,colorTraj,colorBall,colorBallSep);
  {
    std::shared_ptr<MeshShape> ballS=makeSphere(8,true,thickness*4);
    for(typename PositionSequence<T>::Vec3T p:vss->getPositionOnGroundStencil(x)) {
      std::shared_ptr<Bullet3DShape> ballST(new Bullet3DShape);
      ballST->addShape(ballS);
      ballST->setLocalTranslate(p.template cast<GLfloat>());
      ballST->setColor(GL_TRIANGLES,colorOnGround[0],colorOnGround[1],colorOnGround[2]);
      std::dynamic_pointer_cast<Bullet3DShape>(shapeBundle)->addShape(ballST);
    }
  }
  {
    std::shared_ptr<MeshShape> ballS=makeSphere(8,true,thickness*4);
    for(typename PositionSequence<T>::Vec3T p:vss->getPositionAboveGroundStencil(x)) {
      std::shared_ptr<Bullet3DShape> ballST(new Bullet3DShape);
      ballST->addShape(ballS);
      ballST->setLocalTranslate(p.template cast<GLfloat>());
      ballST->setColor(GL_TRIANGLES,colorAboveGround[0],colorAboveGround[1],colorAboveGround[2]);
      std::dynamic_pointer_cast<Bullet3DShape>(shapeBundle)->addShape(ballST);
    }
  }
  return shapeBundle;
}
template <typename T>
std::shared_ptr<DRAWER::Shape> visualizeForceSequence(std::shared_ptr<ForceSequence<T>> vss,const typename VectorSequence<T>::Vec& x,GLfloat thickness,
    const Eigen::Matrix<GLfloat,3,1>& colorTraj=Eigen::Matrix<GLfloat,3,1>(.7,.7,.7),
    const Eigen::Matrix<GLfloat,3,1>& colorBall=Eigen::Matrix<GLfloat,3,1>(.7,0,0),
    const Eigen::Matrix<GLfloat,3,1>& colorBallSep=Eigen::Matrix<GLfloat,3,1>(0,.7,0),
    const Eigen::Matrix<GLfloat,3,1>& colorInCone=Eigen::Matrix<GLfloat,3,1>(0,0,0)) {
  using namespace DRAWER;
  std::shared_ptr<Shape> shapeBundle=visualizeVectorSequence(std::dynamic_pointer_cast<VectorSequence<T>>(vss),x,thickness,colorTraj,colorBall,colorBallSep);
  {
    std::shared_ptr<MeshShape> ballS=makeSphere(8,true,thickness*4);
    for(typename PositionSequence<T>::Vec3T p:vss->getForceInConeStencil(x)) {
      std::shared_ptr<Bullet3DShape> ballST(new Bullet3DShape);
      ballST->addShape(ballS);
      ballST->setLocalTranslate(p.template cast<GLfloat>());
      ballST->setColor(GL_TRIANGLES,colorInCone[0],colorInCone[1],colorInCone[2]);
      std::dynamic_pointer_cast<Bullet3DShape>(shapeBundle)->addShape(ballST);
    }
  }
  return shapeBundle;
}
std::shared_ptr<DRAWER::Shape> visualizeForce(const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,GLfloat angle,GLfloat thickness,GLfloat thicknessOuter) {
  using namespace DRAWER;
  std::shared_ptr<Bullet3DShape> shape(new Bullet3DShape);
  for(int i=0; i<(int)ees.size(); i++)
    shape->addShape(std::shared_ptr<ArrowShape>(new ArrowShape(angle,thickness,thicknessOuter)));
  return shape;
}
template <typename T>
void updateTrajectory(std::shared_ptr<DRAWER::Shape> articulated,std::shared_ptr<PlannerBase<T>> opt,T time,bool& warp) {
  using namespace DRAWER;
  typename PlannerBase<T>::Vec x=opt->getPose(time,warp);
  updateArticulatedBody(articulated,opt->getBody(),ArticulatedBody::Vec(x.template cast<typename ArticulatedBody::T>()));
}
template <typename T>
void updateTrajectory(std::shared_ptr<DRAWER::Shape> articulated,std::shared_ptr<DRAWER::Shape> force,std::shared_ptr<PlannerBase<T>> opt,T time,bool& warp,T forceScale=1) {
  int offset=0;
  using namespace DRAWER;
  typename PlannerBase<T>::Vec x=opt->getPose(time,warp);
  updateArticulatedBody(articulated,opt->getBody(),ArticulatedBody::Vec(x.template cast<typename ArticulatedBody::T>()));
  const std::vector<std::shared_ptr<EndEffectorBounds>>& ees=opt->getEEs();
  typename PlannerBase<T>::Mat3XT trans=opt->getBody()->getT(x.template cast<typename ArticulatedBody::T>()).template cast<T>();
  for(const std::shared_ptr<EndEffectorBounds>& ee:ees)
    if(std::dynamic_pointer_cast<CDMPlanner<T>>(opt) && std::dynamic_pointer_cast<CDMPlanner<T>>(opt)->hasForceSeq()) {
      typename PlannerBase<T>::Vec3T p=ROTI(trans,ee->jointId())*ee->_localPos.template cast<T>()+CTRI(trans,ee->jointId());
      typename PlannerBase<T>::Vec3T f=std::dynamic_pointer_cast<CDMPlanner<T>>(opt)->getForce(ee->jointId(),time)*forceScale;
      std::dynamic_pointer_cast<ArrowShape>(std::dynamic_pointer_cast<Bullet3DShape>(force)->getChild(offset))->
      setArrow(p.template cast<GLfloat>(),(p+f).template cast<GLfloat>());
      offset++;
    }
}
template <typename T>
void updateVectorSequenceTrackball(std::shared_ptr<VectorSequence<T>> vss,const typename VectorSequence<T>::Vec& x,GLfloat time,std::shared_ptr<DRAWER::Shape>& s) {
  using namespace DRAWER;
  typename VectorSequence<T>::Vec3T pos=vss->timeInterpolateStencil(x,fmod(time,vss->getPhase()->horizon(x)),NULL,NULL);
  std::dynamic_pointer_cast<Bullet3DShape>(std::dynamic_pointer_cast<Bullet3DShape>(s)->getChild(1))->setLocalTranslate(pos.template cast<GLfloat>());
}
}

#endif
