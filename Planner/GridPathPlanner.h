#ifndef GRID_PATH_PLANNER_H
#define GRID_PATH_PLANNER_H

#include "SIPPlannerBase.h"

namespace PHYSICSMOTION {
template <typename T>
class GridPlanner : public SerializableBase {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  GridPlanner(std::shared_ptr<SIPPlannerBase<T>> planner,
              T targetCoefP,
              T targetCoefD,
              T barrierCoef,
              T selfBarrierCoef,
              T limitCoef,
              T smoothnessCoef,
              bool implicit,
              bool useConvexHullObs,
              bool useConvesHullSelf,
              int maxIter,
              bool output,
              bool newtonMethod,
              T gridSize,
              bool useRobotBB=true);
  GridPlanner();
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual std::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  void init(T distThreshold=0.07,bool neutralInit=false,Vec3T globalTargetDirection=Vec3T(0,0,1),Vec3T globalOrientation=Vec3T(0,0,1),bool JTJApprox=false);
  void optimize(T distThreshold=0.07,Vec3T globalTargetDirection=Vec3T(0,0,1),Vec3T globalOrientation=Vec3T(0,0,1),bool JTJApprox=false);
  bool optimizeFromOneDirection(int i,int axis,int direction,T distThreshold=0.07,Vec3T globalTargetDirection=Vec3T(0,0,1),Vec3T globalOrientation=Vec3T(0,0,1),bool JTJApprox=false);
  bool initialized() const;
  T getSuccessRate() const;
  void defaultVisConfig();
  int targetSize() const;
  std::shared_ptr<SIPPlannerBase<T>> getPlanner() const;
  const std::vector<std::vector<Vec>>& getControlPointsAllPaths() const;
  const std::vector<Vec3T>& getTargets() const;
  const std::vector<int>& getTargetSuccessInfo() const;
  bool successCheck(int i,T distThreshold,Vec3T globalDirection,Vec3T globalOrientation,bool JTJApprox);
 private:
  void initSampledTargets(T gridSize,bool useRobotBB);
  std::shared_ptr<SIPPlannerBase<T>> _planner;
  std::vector<std::vector<Vec>> _controlPointsAllPath;
  std::vector<Vec3T> _targets;
  std::vector<int> _targetSucc;
  T _targetCoefP,_targetCoefD,_barrierCoef,_selfBarrierCoef,_limitCoef,_smoothnessCoef;
  bool _implicit,_useConvexHullObs,_useConvexHullSelf,_output,_newtonMethod;
  int _maxIter,_dimX,_dimY,_dimZ;
  T _gridSize;
  bool _initialized,_optimized;
};
}

#endif
