#include "ZMPPlanner.h"
#include <Optimizer/LIPDynamicsSequenceSplineZT.h>
#include <Optimizer/LIPDynamicsSequenceSplineXYZT.h>
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
template <typename T>
ZMPPlanner<T>::ZMPPlanner():PlannerBase<T>() {
  useIPOPT(true);
}
template <typename T>
ZMPPlanner<T>::ZMPPlanner(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize):PlannerBase<T>(env,path,normalize) {
  useIPOPT(true);
}
template <typename T>
ZMPPlanner<T>::ZMPPlanner(std::shared_ptr<Environment<T>> env,
                          std::shared_ptr<ArticulatedBody> body,
                          std::shared_ptr<ArticulatedBody> bodyVis,
                          const std::vector<std::shared_ptr<EndEffectorBounds>>& ees)
  :PlannerBase<T>(env,body,bodyVis,ees) {
  useIPOPT(true);
}
template <typename T>
void ZMPPlanner<T>::update(const std::vector<std::shared_ptr<FootStep<T>>>& steps) {
  _steps.resize(steps.size());
  for(int i=0; i<(int)steps.size(); i++)
    _steps[i]=*steps[i];
}
template <typename T>
std::vector<std::shared_ptr<FootStep<T>>> ZMPPlanner<T>::stepsSmartPtr() const {
  std::vector<std::shared_ptr<FootStep<T>>> ret;
  for(const FootStep<T>& step:steps())
    ret.push_back(std::shared_ptr<FootStep<T>>(new FootStep<T>(step)));
  return ret;
}
template <typename T>
const std::vector<FootStep<T>>& ZMPPlanner<T>::steps() const {
  return _steps;
}
template <typename T>
void ZMPPlanner<T>::clearProblem() {
  _obj=SQPObjectiveCompound<T>();
  _smoothBodyXY=NULL;
  _smoothBodyZT=NULL;
  _LIPDynamics=NULL;
  _smoothFoot.clear();
  _footsteps.clear();
}
template <typename T>
void ZMPPlanner<T>::buildProblem() {
  clearProblem();
  //footstep
  for(int eid=0; eid<(int)_ees.size(); eid++) {
    const std::shared_ptr<EndEffectorBounds>& ee=_ees[eid];
    _footsteps.push_back(std::shared_ptr<FootStepSequence<T>>(new FootStepSequence<T>(_obj,ee->name(),ee->isEven(),eid,ee->_phi0,doubleHorizon(),singleHorizon(),_env,_steps)));
    _footsteps[eid]->init(_obj,_steps);
    _footsteps[eid]->fixInitialStep(_obj);
    if(footStepCoef()!=0)
      _footsteps[eid]->setTargetEnergy(_obj,footStepCoef());
    _obj.addComponent(_footsteps.back());
    //smoothness of foot
    _smoothFoot.push_back(std::shared_ptr<LaplaceObjective<T,2>>(new LaplaceObjective<T,2>(_obj,"EndEffector"+std::to_string(eid),_footsteps.back()->getVar(),_footsteps.back()->getDiffVar(),laplaceCoef())));
    _obj.addComponent(_smoothFoot.back());
  }
  //linear inverted pendulum
  buildSimplifiedBody();
  if(splineXY()>0 && splineZT()>0) {
    std::shared_ptr<PhaseSequence<T>> phaseXY(new PhaseSequence<T>(_obj,"",0.1f,dt()*_footsteps[0]->nrTimestep(),-splineXY(),1));
    std::shared_ptr<PhaseSequence<T>> phaseZT(new PhaseSequence<T>(_obj,"",0.1f,dt()*_footsteps[0]->nrTimestep(),-splineZT(),1));
    _LIPDynamics.reset(new LIPDynamicsSequenceSplineXYZT<T>(_obj,"",dt(),g(),bodyHeightTarget(),_bodySimplified,_footsteps,isBoundingCircle(),phaseXY,phaseZT,_ees));
  } else if(splineXY()<=0 && splineZT()>0) {
    std::shared_ptr<PhaseSequence<T>> phase(new PhaseSequence<T>(_obj,"",0.1f,dt()*_footsteps[0]->nrTimestep(),-splineZT(),1));
    _LIPDynamics.reset(new LIPDynamicsSequenceSplineZT<T>(_obj,"",dt(),g(),bodyHeightTarget(),_bodySimplified,_footsteps,isBoundingCircle(),phase,_ees));
  } else _LIPDynamics.reset(new LIPDynamicsSequence<T>(_obj,"",dt(),g(),bodyHeightTarget(),_bodySimplified,_footsteps,isBoundingCircle(),_ees));
  _LIPDynamics->initDOF(_obj,doubleHorizon(),singleHorizon(),_steps);
  _LIPDynamics->initSameLambda(_obj);
  _LIPDynamics->fixInitialPose(_obj,false);
  _LIPDynamics->setTargetHeightEnergy(_obj,bodyHeightCoef(),lambdaCoef());
  _obj.addComponent(_LIPDynamics);
  //smoothness of body
  _smoothBodyXY.reset(new LaplaceObjective<T,2>(_obj,"BodyPoseXY",_LIPDynamics->DOFVarXY(),_LIPDynamics->DOFDiffVarXY(),laplaceCoef()));
  _obj.addComponent(_smoothBodyXY);
  _smoothBodyZT.reset(new LaplaceObjective<T,2>(_obj,"BodyPoseZT",_LIPDynamics->DOFVarZT(),_LIPDynamics->DOFDiffVarZT(),laplaceCoef()));
  _obj.addComponent(_smoothBodyZT);
}
template <typename T>
bool ZMPPlanner<T>::plan() {
  buildProblem();

  bool ret=true;
  if(!useIPOPT())
    ret=solveKnitro(_obj,_x,&_fvec);
  else ret=solveIpopt(_obj,_x);

  _obj.checkViolation(&_x);
  _xPoses=_LIPDynamics->getPoses(*_body,*_env,_x);
  return ret;
}
//property
template <typename T>
void ZMPPlanner<T>::singleHorizon(int N) {
  put<int>(_pt,"singleHorizon",N);
}
template <typename T>
int ZMPPlanner<T>::singleHorizon() const {
  return get<int>(_pt,"singleHorizon",10);
}
template <typename T>
void ZMPPlanner<T>::doubleHorizon(int N) {
  put<int>(_pt,"doubleHorizon",N);
}
template <typename T>
int ZMPPlanner<T>::doubleHorizon() const {
  return get<int>(_pt,"doubleHorizon",10);
}
template <typename T>
void ZMPPlanner<T>::lambdaCoef(T coef) {
  put<T>(_pt,"lambdaCoef",coef);
}
template <typename T>
T ZMPPlanner<T>::lambdaCoef() const {
  return get<T>(_pt,"lambdaCoef",1e-2f);
}
template <typename T>
void ZMPPlanner<T>::laplaceCoef(T coef) {
  put<T>(_pt,"laplaceCoef",coef);
}
template <typename T>
T ZMPPlanner<T>::laplaceCoef() const {
  return get<T>(_pt,"laplaceCoef",1e-2f);
}
template <typename T>
void ZMPPlanner<T>::footStepCoef(T coef) {
  put<T>(_pt,"footStepCoef",coef);
}
template <typename T>
T ZMPPlanner<T>::footStepCoef() const {
  return get<T>(_pt,"footStepCoef",-1.);
}
template <typename T>
void ZMPPlanner<T>::splineXY(int spline) {
  put<bool>(_pt,"splineXY",spline);
}
template <typename T>
int ZMPPlanner<T>::splineXY() const {
  return get<bool>(_pt,"splineXY",0);
}
template <typename T>
void ZMPPlanner<T>::splineZT(int spline) {
  put<bool>(_pt,"splineZT",spline);
}
template <typename T>
int ZMPPlanner<T>::splineZT() const {
  return get<bool>(_pt,"splineZT",1);
}
template <typename T>
void ZMPPlanner<T>::isBoundingCircle(bool circle) {
  put<bool>(_pt,"isBoundingCircle",circle);
}
template <typename T>
bool ZMPPlanner<T>::isBoundingCircle() const {
  return get<bool>(_pt,"isBoundingCircle",false);
}
template class ZMPPlanner<FLOAT>;
}
