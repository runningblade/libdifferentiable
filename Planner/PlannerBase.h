#ifndef PLANNER_BASE_H
#define PLANNER_BASE_H

#include <Optimizer/SQPObjective.h>
#include <Articulated/ArticulatedBody.h>
#include <Articulated/EndEffectorInfo.h>
#include <Environment/Environment.h>
#include <tinyxml2.h>

namespace PHYSICSMOTION {
template <typename T>
class GUROBIInterface;
template <typename T>
struct FootStep {
  DECL_MAT_VEC_MAP_TYPES_T
  static FootStep random(int nrFoot,bool isEven);
  Vec6T _bodyPose;//trans+rot
  Mat3XT _eePose; //3x#EE
  bool _isEven;
};
template <typename T>
class PlannerBase : public SerializableBase {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  PlannerBase();
  PlannerBase(std::shared_ptr<Environment<T>> env,const std::string& path,T normalize);
  PlannerBase(std::shared_ptr<Environment<T>> env,
              std::shared_ptr<ArticulatedBody> body,
              std::shared_ptr<ArticulatedBody> bodyVis,
              const std::vector<std::shared_ptr<EndEffectorBounds>>& ees);
  virtual ~PlannerBase();
  bool read(std::istream& is,IOData* dat) override;
  bool write(std::ostream& os,IOData* dat) const override;
  std::string type() const override;
  void applyEdit(const typename ArticulatedBody::Vec& x,const std::vector<char>& excluded,const std::vector<char>& markFoot);
  void detectEndEffector(const std::set<std::string>& fixNames= {},T errThresEE=.5f);
  void computeReachableRegion(bool containLocalPos=false,bool centerLocalPos=false,int mask=0,T errThresReachable=1.f);
  void replaceJoint(const typename ArticulatedBody::Vec& x,int jid,const Mat3XT& axes);
  void alignDirectionZ();
  void flipLeftRight();
  void normalize(T normalize);
  std::vector<std::shared_ptr<EndEffectorBounds>>& getEEs();
  const std::vector<std::shared_ptr<EndEffectorBounds>>& getEEs() const;
  std::shared_ptr<Environment<T>>& getEnv();
  void setEnv(std::shared_ptr<Environment<T>> env);
  const std::shared_ptr<Environment<T>>& getEnv() const;
  std::shared_ptr<ArticulatedBody> getBodyVis() const;
  std::shared_ptr<ArticulatedBody>& getBodyVis();
  std::shared_ptr<ArticulatedBody> getBody() const;
  std::shared_ptr<ArticulatedBody>& getBody();
  Vec getPose(T t,bool& warp) const;
  void setPoses(const MatT& poses);
  const MatT& getPoses() const;
  Vec getFirstPose() const;
  Vec getLastPose() const;
  Vec stepToDOF(const FootStep<T>& s) const;
  Vec stepToDOF(std::shared_ptr<FootStep<T>> s) const;
  Vec stepToDOF(const FootStep<T>& s0,const FootStep<T>& s1,T alpha) const;
  Vec stepToDOF(std::shared_ptr<FootStep<T>> s0,std::shared_ptr<FootStep<T>> s1,T alpha) const;
  std::shared_ptr<FootStep<T>> DOFToStep(const Vec& DOF,bool isEven) const;
  //property
  const tinyxml2::XMLDocument& getPt() const;
  void parsePtree(int argc,char** argv);
  void mu(T mu);
  T mu() const;
  void dt(T dt);
  T dt() const;
  void dtPlayback(T dt);
  T dtPlayback() const;
  void g(const Vec3T& g);
  Vec3T g() const;
  void bodyHeightCoef(T hCoef);
  T bodyHeightCoef() const;
  void bodyHeightTarget(T hTarget);
  T bodyHeightTarget() const;
  void callback(int callback);
  int callback() const;
  void ftol(T ftol);
  T ftol() const;
  void xtol(T xtol);
  T xtol() const;
  void tol(T xtol);
  T tol() const;
  void useIPOPT(bool useIPOPT);
  bool useIPOPT() const;
  void derivCheck(T derivCheck);
  T derivCheck() const;
  void muInit(T muInit);
  T muInit() const;
  void maxIter(int maxIter);
  int maxIter() const;
  void maxNode(int nNode);
  int maxNode() const;
  void maxTime(int nTime);
  int maxTime() const;
  void resolve(bool resolve);
  bool resolve() const;
  void useDirectOrCG(bool direct);
  bool useDirectOrCG() const;
  void transformFromSourceFile(const Mat3X4T& trans);
  Mat3X4T transformFromSourceFile() const;
 protected:
#ifndef SWIG
  bool solveKnitro(SQPObjectiveCompound<T>& obj,Vec& x,Vec* fvec=NULL);
  bool solveIpopt(SQPObjectiveCompound<T>& obj,Vec& x);
  bool solveGurobi(SQPObjectiveCompound<T>& obj,Vec& x);
  void buildSimplifiedBody();
  //data
  tinyxml2::XMLDocument _pt;
#endif
  std::shared_ptr<Environment<T>> _env;
  std::shared_ptr<ArticulatedBody> _body,_bodyVis,_bodySimplified;
  std::vector<std::shared_ptr<EndEffectorBounds>> _ees;
  //solver data
  MatT _xPoses;
 private:
#ifdef GUROBI_SUPPORT
  std::shared_ptr<GUROBIInterface<T>> _gurobiSolver;
#endif
};
}

#endif
