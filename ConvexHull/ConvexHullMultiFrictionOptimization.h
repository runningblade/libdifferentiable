#ifndef CONVEX_HULL_MULTI_FRICTION_OPTIMIZATION_H
#define CONVEX_HULL_MULTI_FRICTION_OPTIMIZATION_H

#include "ConvexHullFrictionOptimization.h"

namespace PHYSICSMOTION {
//Use Woodbury matrix identity to solve the system of linear equations
template <typename T,typename TH=typename HigherPrecisionTraits<T>::TH>
struct WoodburyHessianHierarchy {
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Matrix<TH,-1,1> VecTH;
  typedef Eigen::Matrix<TH,-1,-1> MatTH;
  typedef Eigen::Matrix<TH,3,3> Mat3TH;
  void reset(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>>& terms);
  void compute(TH alpha);
  template <typename LHS,typename RHS>
  void solve(Eigen::MatrixBase<LHS>& D,const Eigen::MatrixBase<RHS>& G,TH alpha) {
    compute(alpha);
    MatTH DBlk;
    D.resize(G.rows(),G.cols());
    _UTInvAG.setZero(_Hd.size(),G.cols());
    //compute A^{-1}*G, UTInvAG, and Schur
    for(int i=0,off=0; i<(int)_Ass.size(); i++) {
      DBlk=D.block(off,0,_Ass[i].rows(),G.cols());
      _Ass[i].solve(DBlk,G.block(off,0,_Ass[i].rows(),G.cols()),alpha);
      D.block(off,0,_Ass[i].rows(),G.cols())=DBlk;
      //Schur-related computation
      _UTInvAG+=_dbdfpss[i]->template cast<TH>()*D.block(off,0,_Ass[i].rows(),G.cols());
      off+=_Ass[i].rows();
    }
    //compute Schur.inverse()*UTInvAG
    _UTInvAG=_invSchur.solve(_UTInvAG);
    //finish computation
    for(int i=0,off=0; i<(int)_Ass.size(); i++) {
      _delta=_dbdfpss[i]->template cast<TH>().transpose()*_UTInvAG;
      _invADelta.resize(_delta.rows(),_delta.cols());
      _Ass[i].solve(_invADelta,_delta,alpha);
      D.block(off,0,_Ass[i].rows(),G.cols())-=_invADelta;
      off+=_Ass[i].rows();
    }
  }
  void swap(WoodburyHessianHierarchy& H);
  int rows() const;
  std::vector<WoodburyHessian<T,TH>> _Ass;
  std::vector<const MatT*> _dbdfpss;
  VecTH _Hd;
 private:
  TH _alpha=0;
  bool _initialized=false;
  Eigen::LDLT<MatTH> _invSchur;
  MatTH _UTInvAG,_delta,_invADelta;
  MatTH _Schur;
};
template <typename T,typename TH>
class CCBarrierMultiFrictionOptimization {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef typename CCBarrierFrictionOptimization<T,TH>::PFunc PFunc;
  typedef typename CCBarrierFrictionOptimization<T,TH>::Barrier Barrier;
  typedef Eigen::Matrix<TH,-1,1> VecTH;
  typedef Eigen::Matrix<TH,-1,-1> MatTH;
  typedef Eigen::Matrix<TH,3,-1> Mat3XTH;
  typedef Eigen::Matrix<TH,4,-1> Mat4XTH;
  typedef Eigen::Matrix<TH,2,1> Vec2TH;
  typedef Eigen::Matrix<TH,3,1> Vec3TH;
  typedef Eigen::Matrix<TH,4,1> Vec4TH;
  typedef Eigen::Matrix<TH,3,3> Mat3TH;
  typedef Eigen::Matrix<TH,4,4> Mat4TH;
 public:
  CCBarrierMultiFrictionOptimization(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> fpTerm,T eps,T coef);
  static void debugEnergy(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> fpTerm,T eps);
  static void debugEnergyDerivative(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> fpTerm,T eps);
  bool eval(const Vec& b,bool updateFp,VecTH& fp,T* E,Vec* dEdTheta,const std::vector<std::array<Mat3X4T,4>>* DNDX);
  bool evalNormalBarrier(std::vector<std::array<Mat3X4T,4>>* DNDX);
  const ArticulatedBody& body() const;
  const CollisionGradInfo<T>& grad() const;
  CollisionGradInfo<T>& grad();
  void setB(const Vec& b);
  int size() const;
 protected:
  //evaluate energy for physics value function
  //energy 5: \sum_i P(eps-(b_i + \sum_j dx_{1j}/d\theta_i * fp1_j))+P(eps+(b_i + \sum_j dx_{2j}/d\theta_i * fp2_j))
  bool energySoftLinear(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessianHierarchy<T,TH>* Hw) const;
  void energySoftLinearDeriv(const VecTH& fp,Vec& dEdTheta) const;
  //optimize for best friction force
  bool energy(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessianHierarchy<T,TH>* Hw) const;
  void prepareEnergy();
  bool optimize(VecTH& fp,TH& E,VecTH& G,MatTH& H,bool output=false);
  bool optimizeFast(VecTH& fp,TH& E,VecTH& G,WoodburyHessianHierarchy<T,TH>& H,bool output=false);
  //data
  Vec _b;
  std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> _fpTerm;
  const T _eps,_coef;
  PFunc _p;
};
}
#endif
