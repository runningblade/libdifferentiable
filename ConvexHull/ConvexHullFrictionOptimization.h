#ifndef CONVEX_HULL_FRICTION_OPTIMIZATION_H
#define CONVEX_HULL_FRICTION_OPTIMIZATION_H

#include "ConvexHullDistanceConvexEnergy.h"

namespace PHYSICSMOTION {
//Use Woodbury matrix identity to solve the system of linear equations
template <typename T,typename TH=typename HigherPrecisionTraits<T>::TH>
struct WoodburyHessian {
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Matrix<TH,-1,1> VecTH;
  typedef Eigen::Matrix<TH,-1,-1> MatTH;
  typedef Eigen::Matrix<TH,3,3> Mat3TH;
  void reset(int rows,const MatT& dbLAdfp);
  void compute(TH alpha);
  template <typename LHS,typename RHS>
  void solve(Eigen::MatrixBase<LHS>& D,const Eigen::MatrixBase<RHS>& G,TH alpha) {
    compute(alpha);
    D.resize(G.rows(),G.cols());
    if(_dbLAdfp->rows()>0)
      _UTInvAG.setZero(_Hd.size(),G.cols());
    //compute A^{-1}*G, UTInvAG, and Schur
    for(int i=0,j=0; i<(int)_H.size(); i++,j+=3) {
      D.block(j,0,3,G.cols())=_invH[i]*G.block(j,0,3,G.cols());
      //Schur-related computation
      if(_dbLAdfp->rows()>0) {
        auto dbLAdfpBlk=_dbLAdfp->block(0,j,_dbLAdfp->rows(),3).template cast<TH>();
        _UTInvAG+=dbLAdfpBlk*D.block(j,0,3,G.cols());
      }
    }
    if(_dbLAdfp->rows()>0) {
      //compute Schur.inverse()*UTInvAG
      _UTInvAG=_invSchur.solve(_UTInvAG);
      //finish computation
      for(int i=0,j=0; i<(int)_H.size(); i++,j+=3) {
        auto dbLAdfpBlk=_dbLAdfp->block(0,j,_dbLAdfp->rows(),3).template cast<TH>();
        D.block(j,0,3,G.cols())-=_invH[i]*dbLAdfpBlk.transpose()*_UTInvAG;
      }
    }
  }
  void swap(WoodburyHessian& H);
  int rows() const {
    return _H.size()*3;
  }
  const MatT* _dbLAdfp=NULL;
  std::vector<Mat3TH> _H;
  VecTH _Hd;
 private:
  TH _alpha=0;
  bool _initialized=false;
  std::vector<Mat3TH> _invH;
  Eigen::LDLT<MatTH> _invSchur;
  MatTH _UTInvAG;
  MatTH _Schur;
};
template <typename T,typename TH=typename HigherPrecisionTraits<T>::TH>
class CCBarrierMultiFrictionOptimization;
template <typename T,typename TH=typename HigherPrecisionTraits<T>::TH>
class CCBarrierFrictionOptimization {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Logx PFunc;
  typedef CCBarrierConvexEnergy<T,PFunc,TH> Barrier;
  friend class CCBarrierMultiFrictionOptimization<T,TH>;
  typedef Eigen::Matrix<TH,-1,1> VecTH;
  typedef Eigen::Matrix<TH,-1,-1> MatTH;
  typedef Eigen::Matrix<TH,3,-1> Mat3XTH;
  typedef Eigen::Matrix<TH,4,-1> Mat4XTH;
  typedef Eigen::Matrix<TH,2,1> Vec2TH;
  typedef Eigen::Matrix<TH,3,1> Vec3TH;
  typedef Eigen::Matrix<TH,4,1> Vec4TH;
  typedef Eigen::Matrix<TH,3,3> Mat3TH;
  typedef Eigen::Matrix<TH,4,4> Mat4TH;
 public:
  CCBarrierFrictionOptimization(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,const Barrier& barrier,T fri,T eps,T coef);
  static void debugJacobian(const ArticulatedBody& body,int JID,int JID2,T fri,T eps);
  static void debugEnergy(const ArticulatedBody& body,int JID,int JID2,T fri,T eps);
  static void debugEnergyDerivative(const ArticulatedBody& body,const GJKPolytope<T>& p,int JID,T fri,T eps);
  static void debugEnergyDerivative(const ArticulatedBody& body,int JID,int JID2,T fri,T eps);
  bool eval(const Vec& b,bool updateFp,VecTH& fp,T* E,Vec* dEdTheta,const std::array<Mat3X4T,4>* DNDX);
  const ArticulatedBody& body() const;
  const CollisionGradInfo<T>& grad() const;
  const MatT& dbdfp() const;
  const MatT& dbLAdfp() const;
  void setB(const Vec& b);
  int size() const;
 protected:
  //evaluate energy for each barrier term, these two energys (combined) are strictly convex
  //energy 1: P(eps-<n,f_{x,//}^i>)+P(eps+<n,f_{x,//}^i)
  //energy 2: P(sqrt(4*eps^2+fri^2*<fn,fn>)-sqrt(eps^2+<f_x^i,f_x^i>))
  bool energyPerForce(T fnSqr,const Vec3TH& fp,TH& E,Vec3TH* Gf,Mat3TH* Hf) const;
  bool energyPerForce(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const;
  void energyPerForceDeriv(const Vec3T& fn,const Vec3T& fp,Vec4T& dEdp,Vec3T& dEdfn) const;
  void energyPerForceDeriv(const VecTH& fp,Vec4T& dEdp,Vec3T& w1,Vec3T& t1,Vec3T& w2,Vec3T& t2) const;
  //evaluate energy to ensure linear/angular momentum conservation
  //energy 3: P(eps - (\sum_j fp1_i - \sum_j fp2_j)) + P(eps + (\sum_j fp1_i - \sum_j fp2_j))
  //energy 4: P(eps - n.(\sum_j [x1_i]fp1_i - \sum_j [x2_i]fp2_j)) + P(eps + n.(\sum_j [x1_i]fp1_i - \sum_j [x2_i]fp2_j))
  bool energySoftLinear(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const;
  void energySoftLinearDeriv(const VecTH& fp,Vec& dEdTheta,Vec4T& dEdp,Vec3T& w1,Vec3T& t1,Vec3T& w2,Vec3T& t2) const;
  void energySoftLinearDeriv(const VecTH& fp,const Vec& Gb,Mat3XT& G) const;    //for multi-friction
  void prepareEnergyBalance();
  //evaluate energy for physics value function
  //energy 5: \sum_i P(eps-(b_i + \sum_j dx_{1j}/d\theta_i * fp1_j))+P(eps+(b_i + \sum_j dx_{2j}/d\theta_i * fp2_j))
  void prepareEnergyPhysics();
  //optimize for best friction force
  virtual bool energy(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const;
  virtual void prepareEnergy();
  virtual bool optimize(VecTH& fp,TH& E,VecTH& G,MatTH& H,bool output=false);
  virtual bool optimizeFast(VecTH& fp,TH& E,VecTH& G,WoodburyHessian<T,TH>& H,bool output=false);
  //user provided physics violation without friction is stored in _b
  //_bLA is the lumped physics violation and linear/angular momentum violation
  Vec _b,_bLA,_fnSqr;
  MatT _dbdfp,_dbLAdfp;
  bool _multiFriction=false;
  int _torqueBalanceRow=-1;
  //persistent data
  const ArticulatedBody& _body;
  const CollisionGradInfo<T>& _grad;
  const Barrier& _barrier;
  const T _fri,_eps,_coef;
  PFunc _p;
};
}
#endif
