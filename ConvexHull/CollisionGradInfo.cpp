#include "CollisionGradInfo.h"
#include "ConvexHullDistanceEnergy.h"
#include <Environment/MeshExact.h>

namespace PHYSICSMOTION {
//CollisionGradInfo
template <typename T>
CollisionGradInfo<T>::CollisionGradInfo() {}
template <typename T>
CollisionGradInfo<T>::CollisionGradInfo(const ArticulatedBody& body,const Vec& theta) {
  reset(body,theta);
}
template <typename T>
void CollisionGradInfo<T>::reset(const ArticulatedBody& body,const Vec& theta) {
  _HTheta.setZero(body.nrDOF(),body.nrDOF());
  _HThetaL.setZero(body.nrDOF(),body.nrDOF());
  _HThetaLL.setZero(body.nrDOF(),body.nrDOF());
  _HThetaPTarget.setZero(body.nrDOF(),body.nrDOF());
  _HThetaDTarget.setZero(body.nrDOF(),body.nrDOF());
  _HPos.setZero(body.nrDOF(),3);
  _HThetaN=MatX4T::Zero(body.nrDOF(),4);
  _HThetaU=MatX3T::Zero(body.nrDOF(),3);
  _DTG.setZero(3,4*body.nrJ());
  _info.reset(body,theta);
  //data
  if((int)_polytopes.size()!=body.nrJ())
    _polytopes.resize(body.nrJ());
  Eigen::Matrix<int,2,1> off(0,0);
  for(int i=0; i<body.nrJ(); i++) {
    std::shared_ptr<MeshExact> m=std::dynamic_pointer_cast<MeshExact>(body.joint(i)._mesh);
    if(!m)
      _polytopes[i]=GJKPolytope<T>();
    else {
      if(_polytopes[i].jid()==-1)
        _polytopes[i]=GJKPolytope<T>(i,body,*this);
      else _polytopes[i].resetGlobalVss(this);
      off[1]=off[0]+(int)m->vss().size();
      _polytopes[i].setVertexId(off);
      off[0]+=m->vss().size();
    }
  }
  _nrVex=off[0];
  _HThetaD.setZero(body.nrDOF(),_nrVex*3);
}
//instance
template struct CollisionGradInfo<FLOAT>;

}
