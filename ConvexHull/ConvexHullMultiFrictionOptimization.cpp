#include "ConvexHullMultiFrictionOptimization.h"

namespace PHYSICSMOTION {
//Use hierarchical Woodbury matrix identity to solve the system of linear equations
template <typename T,typename TH>
void WoodburyHessianHierarchy<T,TH>::reset(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>>& terms) {
  _Ass.resize(terms.size());
  _dbdfpss.resize(terms.size());
  for(int i=0; i<(int)terms.size(); i++) {
    _Ass[i].reset(terms[i]->size(),terms[i]->dbLAdfp());
    _dbdfpss[i]=&(terms[i]->dbdfp());
  }
  _Hd.setZero(terms[0]->body().nrDOF());
  _initialized=false;
}
template <typename T,typename TH>
void WoodburyHessianHierarchy<T,TH>::compute(TH alpha) {
  if(_initialized && _alpha==alpha)
    return;
  //compute invA and Schur
  MatTH invA_dbdfpT;
  _Schur.setZero(_Hd.size(),_Hd.size());
  _Schur.diagonal().array()=_Hd.array().inverse();
  for(int i=0; i<(int)_Ass.size(); i++) {
    invA_dbdfpT.resize(_dbdfpss[i]->cols(),_dbdfpss[i]->rows());
    _Ass[i].solve(invA_dbdfpT,_dbdfpss[i]->transpose().template cast<TH>(),alpha);
    _Schur+=_dbdfpss[i]->template cast<TH>()*invA_dbdfpT;
  }
  //compute Schur.inverse()
  _invSchur.compute(_Schur);
  _alpha=alpha;
  _initialized=true;
}
template <typename T,typename TH>
void WoodburyHessianHierarchy<T,TH>::swap(WoodburyHessianHierarchy& H) {
  ASSERT(_Ass.size()==H._Ass.size())
  for(int i=0; i<(int)_Ass.size(); i++)
    _Ass[i].swap(H._Ass[i]);
  _Hd.swap(H._Hd);
  _initialized=false;
}
template <typename T,typename TH>
int WoodburyHessianHierarchy<T,TH>::rows() const {
  int off=0;
  for(int i=0; i<(int)_Ass.size(); i++)
    off+=_Ass[i].rows();
  return off;
}
//CCBarrierMultiFrictionOptimization
template <typename T,typename TH>
CCBarrierMultiFrictionOptimization<T,TH>::CCBarrierMultiFrictionOptimization(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> fpTerm,T eps,T coef)
  :_fpTerm(fpTerm),_eps(eps),_coef(coef) {}
template <typename T,typename TH>
void CCBarrierMultiFrictionOptimization<T,TH>::debugEnergy(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> fpTerm,T eps) {
  TH Ef,Ef2;
  VecTH fp,fp0,dfp,G,G2,D,DRef;
  MatTH H;
  Vec x,dx;
  T coef=0.4,alpha=0.1f;
  WoodburyHessianHierarchy<T,TH> Hw;
  DEFINE_NUMERIC_DELTA_T(TH)
  while(true) {
    //construct energy
    CCBarrierMultiFrictionOptimization<T,TH> Et(fpTerm,eps,coef);
    Et.setB(Vec::Random(Et.body().nrDOF())*eps*0.5f);
    x=Vec::Random(Et.body().nrDOF());
    dx=Vec::Random(Et.body().nrDOF());
    Et.grad().reset(Et.body(),x);
    if(!Et.evalNormalBarrier(NULL))
      continue;
    //prepare the energies for friction computation
    Et.prepareEnergy();
    fp=fp0=VecTH::Random(Et.size())*(TH)eps*0.1f;
    dfp.setRandom(Et.size());
    if(!Et.energy(fp,Ef,&G,&H,NULL))
      continue;
    //Woodbury matrix inversion
    Et.energy(fp,Ef,&G,NULL,&Hw);
    D.resize(G.size());
    Hw.solve(D,G,(TH)alpha);
    DRef=(H+MatTH::Identity(G.size(),G.size())*(TH)alpha).inverse()*G;
    DEBUG_GRADIENT("Woodbury-D",DRef.norm(),(D-DRef).norm())
    //debug gradient
    Et.energy(fp+dfp*DELTA,Ef2,&G2,NULL,NULL);
    DEBUG_GRADIENT("G",dfp.dot(G),(dfp.dot(G)-(Ef2-Ef)/DELTA))
    DEBUG_GRADIENT("H",(H*dfp).norm(),(H*dfp-(G2-G)/DELTA).norm())
    //optimize
    Et.optimize(fp=fp0,Ef,G,H,true);
    Et.optimizeFast(fp=fp0,Ef,G,Hw,true);
    break;
  }
}
template <typename T,typename TH>
void CCBarrierMultiFrictionOptimization<T,TH>::debugEnergyDerivative(const std::vector<std::shared_ptr<CCBarrierFrictionOptimization<T,TH>>> fpTerm,T eps) {
  Vec x,dx,dEdTheta,b;
  VecTH fp;
  T coef=0.4,E,E2;
  std::vector<std::array<Mat3X4T,4>> DNDX;
  CollisionGradInfo<T> info;
  DEFINE_NUMERIC_DELTA_T(T)
  while(true) {
    //construct energy
    CCBarrierMultiFrictionOptimization<T,TH> Et(fpTerm,eps,coef);
    x=Vec::Random(Et.body().nrDOF());
    dx=Vec::Random(Et.body().nrDOF());
    b=Vec::Random(Et.body().nrDOF())*eps*0.5f;
    {
      Et.grad().reset(Et.body(),x);
      if(!Et.evalNormalBarrier(&DNDX))
        continue;
      if(!Et.eval(b,true,fp,&E,&dEdTheta,&DNDX))
        continue;
    }
    //debug derivative
    {
      Et.grad().reset(Et.body(),x+dx*DELTA);
      if(!Et.evalNormalBarrier(NULL))
        continue;
      if(!Et.eval(b,true,fp,&E2,NULL,NULL))
        continue;
    }
    DEBUG_GRADIENT("G",dEdTheta.dot(dx),dEdTheta.dot(dx)-(E2-E)/DELTA)
    break;
  }
}
template <typename T,typename TH>
bool CCBarrierMultiFrictionOptimization<T,TH>::eval(const Vec& b,bool updateFp,VecTH& fp,T* E,Vec* dEdTheta,const std::vector<std::array<Mat3X4T,4>>* DNDX) {
  _b=b;
  //comptue fp
  TH E2;
  if(fp.size()!=size())
    fp.setZero(size());
  if(updateFp) {
    VecTH G2;
    WoodburyHessianHierarchy<T,TH> H2;
    if(!optimizeFast(fp,E2,G2,H2))
      return false;
  } else {
    prepareEnergy();
    energy(fp,E2,NULL,NULL,NULL);
  }
  if(E)
    *E=(T)E2;

  //compute derivative
  if(dEdTheta && DNDX) {
    VecTH fpi;
    Mat3XT wss=Mat3XT::Zero(3,body().nrJ());
    Mat3XT tss=Mat3XT::Zero(3,body().nrJ());
    dEdTheta->setZero(body().nrDOF());
    for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
      Vec4T dEdp=Vec4T::Zero();
      Vec3T w1=Vec3T::Zero(),t1=Vec3T::Zero();
      Vec3T w2=Vec3T::Zero(),t2=Vec3T::Zero();
      fpi=fp.segment(off,_fpTerm[i]->size());
      _fpTerm[i]->energyPerForceDeriv(fpi,dEdp,w1,t1,w2,t2);
      _fpTerm[i]->energySoftLinearDeriv(fpi,*dEdTheta,dEdp,w1,t1,w2,t2);
      //p1
      if(_fpTerm[i]->_barrier._p1.jid()>=0) {
        w1+=(*DNDX)[i][0]*dEdp/_fpTerm[i]->_barrier.coef();
        t1+=(*DNDX)[i][1]*dEdp/_fpTerm[i]->_barrier.coef();
        wss.col(_fpTerm[i]->_barrier._p1.jid())+=w1;
        tss.col(_fpTerm[i]->_barrier._p1.jid())+=t1;
      }
      //p2
      if(_fpTerm[i]->_barrier._p2.jid()>=0) {
        w2+=(*DNDX)[i][2]*dEdp/_fpTerm[i]->_barrier.coef();
        t2+=(*DNDX)[i][3]*dEdp/_fpTerm[i]->_barrier.coef();
        wss.col(_fpTerm[i]->_barrier._p2.jid())+=w2;
        tss.col(_fpTerm[i]->_barrier._p2.jid())+=t2;
      }
      off+=_fpTerm[i]->size();
    }
    //physics
    energySoftLinearDeriv(fp,*dEdTheta);
    //assemble
    for(int jid=0; jid<body().nrJ(); jid++) {
      grad()._info.JRSparse(body(),jid,[&](int c,const Vec3T& d) {
        dEdTheta->coeffRef(c)+=d.dot(wss.col(jid));
      });
      grad()._info.JCSparse(body(),jid,[&](int c,const Vec3T& d) {
        dEdTheta->coeffRef(c)+=d.dot(tss.col(jid));
      });
    }
  }
  return true;
}
template <typename T,typename TH>
bool CCBarrierMultiFrictionOptimization<T,TH>::evalNormalBarrier(std::vector<std::array<Mat3X4T,4>>* DNDX) {
  T Etmp;
  if(DNDX)
    DNDX->resize(_fpTerm.size());
  for(int i=0; i<(int)_fpTerm.size(); i++) {
    Barrier& barrier=const_cast<Barrier&>(_fpTerm[i]->_barrier);
    if(!barrier.eval(&Etmp,&(body()),&(grad()),DNDX?&(DNDX->at(i)):NULL,NULL,NULL))
      return false;
  }
  return true;
}
template <typename T,typename TH>
const ArticulatedBody& CCBarrierMultiFrictionOptimization<T,TH>::body() const {
  return _fpTerm[0]->body();
}
template <typename T,typename TH>
const CollisionGradInfo<T>& CCBarrierMultiFrictionOptimization<T,TH>::grad() const {
  return _fpTerm[0]->grad();
}
template <typename T,typename TH>
CollisionGradInfo<T>& CCBarrierMultiFrictionOptimization<T,TH>::grad() {
  return const_cast<CollisionGradInfo<T>&>(_fpTerm[0]->grad());
}
template <typename T,typename TH>
void CCBarrierMultiFrictionOptimization<T,TH>::setB(const Vec& b) {
  _b=b;
}
template <typename T,typename TH>
int CCBarrierMultiFrictionOptimization<T,TH>::size() const {
  int off=0;
  for(int i=0; i<(int)_fpTerm.size(); i++)
    off+=_fpTerm[i]->size();
  return off;
}
//evaluate energy for physics value function
//energy 5: \sum_i P(eps-(b_i + \sum_j dx_{1j}/d\theta_i * fp1_j))+P(eps+(b_i + \sum_j dx_{2j}/d\theta_i * fp2_j))
template <typename T,typename TH>
bool CCBarrierMultiFrictionOptimization<T,TH>::energySoftLinear(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessianHierarchy<T,TH>* Hw) const {
  if(_b.size()==0)
    return true;
  VecTH bf=_b.template cast<TH>();
  for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
    bf+=_fpTerm[i]->dbdfp().template cast<TH>()*fp.segment(off,_fpTerm[i]->size());
    off+=_fpTerm[i]->size();
  }
  //compute G/H in b
  VecTH Gb,Hb;
  TH D,DD;
  if(G)
    Gb.setZero(bf.size());
  if(H)
    Hb.setZero(bf.size());
  for(int i=0; i<bf.size(); i++) {
    E+=_p.template eval<TH>((TH)_eps-bf[i],G?&D:NULL,(H||Hw)?&DD:NULL,0,(TH)_coef);
    if(!isfinite(E))
      return false;
    if(G)
      Gb[i]-=D;
    if(H)
      Hb[i]+=DD;
    if(Hw)
      Hw->_Hd[i]+=DD;
    E+=_p.template eval<TH>((TH)_eps+bf[i],G?&D:NULL,(H||Hw)?&DD:NULL,0,(TH)_coef);
    if(!isfinite(E))
      return false;
    if(G)
      Gb[i]+=D;
    if(H)
      Hb[i]+=DD;
    if(Hw)
      Hw->_Hd[i]+=DD;
  }
  //comptue G/H in f
  if(G)
    for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
      G->segment(off,_fpTerm[i]->size())+=_fpTerm[i]->dbdfp().template cast<TH>().transpose()*Gb;
      off+=_fpTerm[i]->size();
    }
  if(H)
    for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
      for(int j=0,off2=0; j<(int)_fpTerm.size(); j++) {
        H->block(off,off2,_fpTerm[i]->size(),_fpTerm[j]->size())+=
          _fpTerm[i]->dbdfp().template cast<TH>().transpose()*
          Hb.asDiagonal()*
          _fpTerm[j]->dbdfp().template cast<TH>();
        off2+=_fpTerm[j]->size();
      }
      off+=_fpTerm[i]->size();
    }
  return true;
}
template <typename T,typename TH>
void CCBarrierMultiFrictionOptimization<T,TH>::energySoftLinearDeriv(const VecTH& fp,Vec& dEdTheta) const {
  if(_b.size()==0)
    return;
  T D;
  Mat3XT G;
  Vec bf=_b;
  for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
    bf+=_fpTerm[i]->dbdfp()*fp.segment(off,_fpTerm[i]->size()).template cast<T>();
    off+=_fpTerm[i]->size();
  }
  Vec Gb=Vec::Zero(bf.size());
  for(int i=0; i<bf.size(); i++) {
    _p.template eval<T>(_eps-bf[i],&D,NULL,0,_coef);
    Gb[i]-=D;
    _p.template eval<T>(_eps+bf[i],&D,NULL,0,_coef);
    Gb[i]+=D;
  }
  G.setZero(3,body().nrJ()*4);
  for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
    _fpTerm[i]->energySoftLinearDeriv(fp.segment(off,_fpTerm[i]->size()),Gb,G);
    off+=_fpTerm[i]->size();
  }
  grad()._info.toolB(body(),mapM(G),[&](int r,int c,T val) {
    dEdTheta[r]+=Gb[c]*val;
  });
}
//optimize for best friction force
template <typename T,typename TH>
bool CCBarrierMultiFrictionOptimization<T,TH>::energy(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessianHierarchy<T,TH>* Hw) const {
  E=0;
  if(G)
    G->setZero(size());
  if(H)
    H->setZero(size(),size());
  if(Hw) {
    Hw->reset(_fpTerm);
  }
  //per-term energy
  VecTH fpi,Gi;
  MatTH Hi;
  TH Ei;
  for(int i=0,off=0; i<(int)_fpTerm.size(); i++) {
    fpi=fp.segment(off,_fpTerm[i]->size());
    _fpTerm[i]->energy(fpi,Ei,G?&Gi:NULL,H?&Hi:NULL,Hw?&(Hw->_Ass[i]):NULL);
    E+=Ei;
    if(G)
      G->segment(off,_fpTerm[i]->size())+=Gi;
    if(H)
      H->block(off,off,_fpTerm[i]->size(),_fpTerm[i]->size())+=Hi;
    off+=_fpTerm[i]->size();
  }
  //physics energy
  if(!energySoftLinear(fp,E,G,H,Hw))
    return false;
  return true;
}
template <typename T,typename TH>
void CCBarrierMultiFrictionOptimization<T,TH>::prepareEnergy() {
  for(int i=0; i<(int)_fpTerm.size(); i++) {
    _fpTerm[i]->_multiFriction=true;
    _fpTerm[i]->prepareEnergyPhysics();
    _fpTerm[i]->prepareEnergyBalance();
  }
}
template <typename T,typename TH>
bool CCBarrierMultiFrictionOptimization<T,TH>::optimize(VecTH& fp,TH& E,VecTH& G,MatTH& H,bool output) {
  prepareEnergy();
  VecTH D,G2,fp2;
  Eigen::LDLT<MatTH> invH;
  MatTH id=MatTH::Identity(size(),size()),H2;
  TH E2,alpha=1,alphaDec=0.5,alphaInc=3.0,alphaMax=1e10;
  //assemble
  //debugEnergy(x);
  if(!energy(fp,E,&G,&H,NULL))
    return false;
  if(output)
    std::cout << "optimize" << std::endl;
  //main loop
  //  int i;
  //  for(i=0;i<1e5;++i){
  while(alpha<alphaMax) {
    if(E==0)
      break;
    //search direction
    invH.compute(H+id*alpha);
    D=invH.solve(G);
    //termination
    if(output)
      std::cout << "E=" << E << " G=" << G.cwiseAbs().maxCoeff() << " alpha=" << alpha << std::endl;
    if(G.cwiseAbs().maxCoeff()<=Epsilon<TH>::defaultEps())
      break;
    //test
    fp2=fp-D;
    if(energy(fp2,E2,&G2,&H2,NULL) && E2<E) {
      alpha=std::max<TH>(alpha*alphaDec,Epsilon<TH>::defaultEps());
      fp=fp2;
      E=E2;
      G.swap(G2);
      H.swap(H2);
    } else {
      alpha*=alphaInc;
    }
  }
  return true;
}
template <typename T,typename TH>
bool CCBarrierMultiFrictionOptimization<T,TH>::optimizeFast(VecTH& fp,TH& E,VecTH& G,WoodburyHessianHierarchy<T,TH>& H,bool output) {
  prepareEnergy();
  VecTH D,G2,fp2;
  WoodburyHessianHierarchy<T,TH> H2;
  TH E2,alpha=1,alphaDec=0.5,alphaInc=3.0,alphaMax=1e10;
  //assemble
  //debugEnergy(x);
  if(!energy(fp,E,&G,NULL,&H))
    return false;
  if(output)
    std::cout << "optimizeFast" << std::endl;
  //main loop
  //  int i;
  //  for(i=0;i<1e5;++i){
  while(alpha<alphaMax) {
    if(E==0)
      break;
    //search direction
    D.resize(G.size());
    H.solve(D,G,alpha);
    //termination
    if(output)
      std::cout << "E=" << E << " G=" << G.cwiseAbs().maxCoeff() << " alpha=" << alpha << std::endl;
    if(G.cwiseAbs().maxCoeff()<=Epsilon<TH>::defaultEps())
      break;
    //test
    fp2=fp-D;
    if(energy(fp2,E2,&G2,NULL,&H2) && E2<E) {
      alpha=std::max<TH>(alpha*alphaDec,Epsilon<TH>::defaultEps());
      fp=fp2;
      E=E2;
      G.swap(G2);
      H.swap(H2);
    } else {
      alpha*=alphaInc;
    }
  }
  return true;
}
//instance
template class WoodburyHessianHierarchy<FLOAT>;
template class CCBarrierMultiFrictionOptimization<FLOAT>;
}
