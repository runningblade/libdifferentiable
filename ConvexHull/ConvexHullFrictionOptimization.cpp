#include "ConvexHullFrictionOptimization.h"
#include <Utils/CrossSpatialUtils.h>

namespace PHYSICSMOTION {
//Use Woodbury matrix identity to solve the system of linear equations
template <typename T,typename TH>
void WoodburyHessian<T,TH>::reset(int rows,const MatT& dbLAdfp) {
  _dbLAdfp=&dbLAdfp;
  _H.assign(rows/3,Mat3TH::Zero());
  _Hd.setZero(dbLAdfp.rows());
  _initialized=false;
}
template <typename T,typename TH>
void WoodburyHessian<T,TH>::compute(TH alpha) {
  if(_initialized && _alpha==alpha)
    return;
  _invH.resize(_H.size());
  if(_dbLAdfp->rows()>0) {
    _Schur.setZero(_dbLAdfp->rows(),_dbLAdfp->rows());
    _Schur.diagonal().array()=_Hd.array().inverse();
  }
  //compute invH and Schur
  for(int i=0,j=0; i<(int)_H.size(); i++,j+=3) {
    _invH[i]=(_H[i]+Mat3TH::Identity()*alpha).inverse();
    if(_dbLAdfp->rows()>0) {
      auto dbLAdfpBlk=_dbLAdfp->block(0,j,_dbLAdfp->rows(),3).template cast<TH>();
      _Schur+=dbLAdfpBlk*_invH[i]*dbLAdfpBlk.transpose();
    }
  }
  //compute Schur.inverse()
  if(_dbLAdfp->rows()>0)
    _invSchur.compute(_Schur);
  _alpha=alpha;
  _initialized=true;
}
template <typename T,typename TH>
void WoodburyHessian<T,TH>::swap(WoodburyHessian& H) {
  _H.swap(H._H);
  _Hd.swap(H._Hd);
  _initialized=false;
}
//CCBarrierFrictionOptimization
template <typename T,typename TH>
CCBarrierFrictionOptimization<T,TH>::CCBarrierFrictionOptimization
(const ArticulatedBody& body,const CollisionGradInfo<T>& info,const Barrier& barrier,T fri,T eps,T coef)
  :_body(body),_grad(info),_barrier(barrier),_fri(fri),_eps(eps),_coef(coef) {}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::debugJacobian(const ArticulatedBody& body,int JID,int JID2,T fri,T eps) {
  Vec x,dx;
  T coef=0.5,E;
  PFunc barrier;
  Mat3XT vss1,vss2;
  Mat3XT vss1dx,vss2dx;
  CollisionGradInfo<T> info,info2;
  DEFINE_NUMERIC_DELTA_T(T)
  while(true) {
    x=Vec::Random(body.nrDOF());
    dx=Vec::Random(body.nrDOF());
    //construct energy
    info.reset(body,x);
    CCBarrierConvexEnergy<T,PFunc,TH> En(info._polytopes[JID],info._polytopes[JID2],barrier,0,&info,coef,true);
    CCBarrierFrictionOptimization<T,TH> Et(body,info,En,fri,eps,coef);
    Et.setB(Vec::Random(body.nrDOF())*eps*0.5f);
    //if energy does not evaluate to finite value, then return
    if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&E,&body,&info,NULL,NULL,NULL))
      continue;
    //prepare the energies for friction computation
    vss1=info._polytopes[JID].globalVss();
    vss2=info._polytopes[JID2].globalVss();
    Et.prepareEnergy();
    //debug that _dbdfp is the Jacobian
    int off=0;
    info2.reset(body,x+dx*DELTA);
    vss1dx=info2._polytopes[JID].globalVss();
    vss2dx=info2._polytopes[JID2].globalVss();
    for(int i=0; i<vss1dx.cols(); i++,off++) {
      auto JBlk=Et._dbdfp.block(0,off*3,Et._dbdfp.rows(),3).template cast<T>().transpose();
      DEBUG_GRADIENT("JacobianP1("+std::to_string(off)+"):",(JBlk*dx).norm(),(JBlk*dx-(vss1dx.col(i)-vss1.col(i))/DELTA).norm())
      std::cout << "fnP1(" << std::to_string(off) << ")=" << sqrt(Et._fnSqr[off]) << std::endl;
    }
    for(int i=0; i<vss2dx.cols(); i++,off++) {
      auto JBlk=Et._dbdfp.block(0,off*3,Et._dbdfp.rows(),3).template cast<T>().transpose();
      DEBUG_GRADIENT("JacobianP2("+std::to_string(off)+"):",(JBlk*dx).norm(),(JBlk*dx-(vss2dx.col(i)-vss2.col(i))/DELTA).norm())
      std::cout << "fnP2(" << std::to_string(off) << ")=" << sqrt(Et._fnSqr[off]) << std::endl;
    }
    break;
  }
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::debugEnergy(const ArticulatedBody& body,int JID,int JID2,T fri,T eps) {
  TH Ef,Ef2;
  VecTH fp,fp0,dfp,G,G2,D,DRef;
  MatTH H;
  Vec x,dx;
  T coef=0.5,E,alpha=0.1f;
  PFunc barrier;
  CollisionGradInfo<T> info;
  WoodburyHessian<T,TH> Hw;
  DEFINE_NUMERIC_DELTA_T(TH)
  while(true) {
    x=Vec::Random(body.nrDOF());
    dx=Vec::Random(body.nrDOF());
    //construct energy
    info.reset(body,x);
    CCBarrierConvexEnergy<T,PFunc,TH> En(info._polytopes[JID],info._polytopes[JID2],barrier,0,&info,coef,true);
    CCBarrierFrictionOptimization<T,TH> Et(body,info,En,fri,eps,coef);
    Et.setB(Vec::Random(body.nrDOF())*eps*0.5f);
    //if energy does not evaluate to finite value, then return
    if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&E,&body,&info,NULL,NULL,NULL))
      continue;
    //prepare the energies for friction computation
    Et.prepareEnergy();
    fp=fp0=VecTH::Random(Et._dbdfp.cols())*(TH)eps*0.1f;
    dfp.setRandom(Et._dbdfp.cols());
    if(!Et.energy(fp,Ef,&G,&H,NULL))
      continue;
    //Woodbury matrix inversion
    Et.energy(fp,Ef,&G,NULL,&Hw);
    D.resize(G.size());
    Hw.solve(D,G,(TH)alpha);
    DRef=(H+MatTH::Identity(G.size(),G.size())*(TH)alpha).inverse()*G;
    DEBUG_GRADIENT("Woodbury-D",DRef.norm(),(D-DRef).norm())
    //debug gradient
    Et.energy(fp+dfp*DELTA,Ef2,&G2,NULL,NULL);
    DEBUG_GRADIENT("G",dfp.dot(G),(dfp.dot(G)-(Ef2-Ef)/DELTA))
    DEBUG_GRADIENT("H",(H*dfp).norm(),(H*dfp-(G2-G)/DELTA).norm())
    //optimize
    Et.optimize(fp=fp0,Ef,G,H,true);
    Et.optimizeFast(fp=fp0,Ef,G,Hw,true);
    break;
  }
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::debugEnergyDerivative(const ArticulatedBody& body,const GJKPolytope<T>& p,int JID,T fri,T eps) {
  Vec x,dx,dEdTheta,b;
  VecTH fp;
  T coef=0.5,coefN=coef/2,E,E2,Etmp;
  PFunc barrier;
  std::array<Mat3X4T,4> DNDX;
  CollisionGradInfo<T> info;
  DEFINE_NUMERIC_DELTA_T(T)
  for(int pass=0; pass<2; pass++)
    while(true) {
      x=Vec::Random(body.nrDOF());
      dx=Vec::Random(body.nrDOF());
      b=Vec::Random(body.nrDOF())*eps*0.5f;
      {
        info.reset(body,x);
        GJKPolytope<T>& p2=info._polytopes[JID];
        CCBarrierConvexEnergy<T,PFunc,TH> En(pass?p:p2,pass?p2:p,barrier,0,&info,coefN,true);
        CCBarrierFrictionOptimization<T,TH> Et(body,info,En,fri,eps,coef);
        if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&Etmp,&body,&info,&DNDX,NULL,NULL))
          continue;
        if(!Et.eval(b,true,fp,&E,&dEdTheta,&DNDX))
          continue;
      }
      //debug derivative
      {
        info.reset(body,x+dx*DELTA);
        GJKPolytope<T>& p2=info._polytopes[JID];
        CCBarrierConvexEnergy<T,PFunc,TH> En(pass?p:p2,pass?p2:p,barrier,0,&info,coefN,true);
        CCBarrierFrictionOptimization<T,TH> Et(body,info,En,fri,eps,coef);
        if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&Etmp,&body,&info,NULL,NULL,NULL))
          continue;
        if(!Et.eval(b,true,fp,&E2,NULL,NULL))
          continue;
      }
      DEBUG_GRADIENT("DE",dEdTheta.dot(dx),dEdTheta.dot(dx)-(E2-E)/DELTA)
      break;
    }
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::debugEnergyDerivative(const ArticulatedBody& body,int JID,int JID2,T fri,T eps) {
  Vec x,dx,dEdTheta,b;
  VecTH fp;
  T coef=0.5,coefN=coef/2,E,E2,Etmp;
  PFunc barrier;
  std::array<Mat3X4T,4> DNDX;
  CollisionGradInfo<T> info;
  DEFINE_NUMERIC_DELTA_T(T)
  while(true) {
    x=Vec::Random(body.nrDOF());
    dx=Vec::Random(body.nrDOF());
    b=Vec::Random(body.nrDOF())*eps*0.5f;
    {
      info.reset(body,x);
      CCBarrierConvexEnergy<T,PFunc,TH> En(info._polytopes[JID],info._polytopes[JID2],barrier,0,&info,coefN,true);
      CCBarrierFrictionOptimization<T,TH> Et(body,info,En,fri,eps,coef);
      if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&Etmp,&body,&info,&DNDX,NULL,NULL))
        continue;
      if(!Et.eval(b,true,fp,&E,&dEdTheta,&DNDX))
        continue;
    }
    //debug derivative
    {
      info.reset(body,x+dx*DELTA);
      CCBarrierConvexEnergy<T,PFunc,TH> En(info._polytopes[JID],info._polytopes[JID2],barrier,0,&info,coefN,true);
      CCBarrierFrictionOptimization<T,TH> Et(body,info,En,fri,eps,coef);
      if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&Etmp,&body,&info,NULL,NULL,NULL))
        continue;
      if(!Et.eval(b,true,fp,&E2,NULL,NULL))
        continue;
    }
    DEBUG_GRADIENT("DE",dEdTheta.dot(dx),dEdTheta.dot(dx)-(E2-E)/DELTA)
    break;
  }
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::eval(const Vec& b,bool updateFp,VecTH& fp,T* E,Vec* dEdTheta,const std::array<Mat3X4T,4>* DNDX) {
  _b=b;
  //comptue fp
  TH E2;
  if(fp.size()!=size())
    fp.setZero(size());
  if(updateFp) {
    VecTH G2;
    WoodburyHessian<T,TH> H2;
    if(!optimizeFast(fp,E2,G2,H2))
      return false;
  } else {
    prepareEnergy();
    energy(fp,E2,NULL,NULL,NULL);
  }
  if(E)
    *E=(T)E2;

  //compute derivative
  if(dEdTheta && DNDX) {
    dEdTheta->setZero(_body.nrDOF());
    Vec4T dEdp=Vec4T::Zero();
    Vec3T w1=Vec3T::Zero(),t1=Vec3T::Zero();
    Vec3T w2=Vec3T::Zero(),t2=Vec3T::Zero();
    energyPerForceDeriv(fp,dEdp,w1,t1,w2,t2);
    energySoftLinearDeriv(fp,*dEdTheta,dEdp,w1,t1,w2,t2);
    //p1
    if(_barrier._p1.jid()>=0) {
      w1+=DNDX->at(0)*dEdp/_barrier.coef();
      t1+=DNDX->at(1)*dEdp/_barrier.coef();
      _grad._info.JRSparse(_body,_barrier._p1.jid(),[&](int c,const Vec3T& d) {
        dEdTheta->coeffRef(c)+=d.dot(w1);
      });
      _grad._info.JCSparse(_body,_barrier._p1.jid(),[&](int c,const Vec3T& d) {
        dEdTheta->coeffRef(c)+=d.dot(t1);
      });
    }
    //p2
    if(_barrier._p2.jid()>=0) {
      w2+=DNDX->at(2)*dEdp/_barrier.coef();
      t2+=DNDX->at(3)*dEdp/_barrier.coef();
      _grad._info.JRSparse(_body,_barrier._p2.jid(),[&](int c,const Vec3T& d) {
        dEdTheta->coeffRef(c)+=d.dot(w2);
      });
      _grad._info.JCSparse(_body,_barrier._p2.jid(),[&](int c,const Vec3T& d) {
        dEdTheta->coeffRef(c)+=d.dot(t2);
      });
    }
  }
  return true;
}
template <typename T,typename TH>
const ArticulatedBody& CCBarrierFrictionOptimization<T,TH>::body() const {
  return _body;
}
template <typename T,typename TH>
const CollisionGradInfo<T>& CCBarrierFrictionOptimization<T,TH>::grad() const {
  return _grad;
}
template <typename T,typename TH>
const typename CCBarrierFrictionOptimization<T,TH>::MatT& CCBarrierFrictionOptimization<T,TH>::dbdfp() const {
  return _dbdfp;
}
template <typename T,typename TH>
const typename CCBarrierFrictionOptimization<T,TH>::MatT& CCBarrierFrictionOptimization<T,TH>::dbLAdfp() const {
  return _dbLAdfp;
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::setB(const Vec& b) {
  _b=b;
}
template <typename T,typename TH>
int CCBarrierFrictionOptimization<T,TH>::size() const {
  return (_barrier._p1.jid()>=0?_barrier._p1.globalVss().size():0)+
         (_barrier._p2.jid()>=0?_barrier._p2.globalVss().size():0);
}
//evaluate energy for each barrier term, these two energys (combined) are strictly convex
//energy 1: P(eps-<n,f_{x,//}^i>)-log(eps+<n,f_{x,//}^i)
//energy 2: P(sqrt(4*eps^2+fri^2*<fn,fn>)-sqrt(eps^2+<f_x^i,f_x^i>))
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::energyPerForce(T fnSqr,const Vec3TH& fp,TH& E,Vec3TH* Gf,Mat3TH* Hf) const {
  TH D,DD;
  Vec3TH n=_barrier.getXH().template segment<3>(0);
  if(Gf)
    Gf->setZero();
  if(Hf)
    Hf->setZero();
  //energy 1.1
  E+=_p.template eval<TH>((TH)_eps-n.dot(fp),Gf?&D:NULL,Hf?&DD:NULL,0,(TH)_coef);
  if(!isfinite(E))
    return false;
  if(Gf)
    *Gf-=D*n;
  if(Hf)
    *Hf+=DD*n*n.transpose();
  //energy 1.2
  E+=_p.template eval<TH>((TH)_eps+n.dot(fp),Gf?&D:NULL,Hf?&DD:NULL,0,(TH)_coef);
  if(!isfinite(E))
    return false;
  if(Gf)
    *Gf+=D*n;
  if(Hf)
    *Hf+=DD*n*n.transpose();
  //energy 2
  T LHSSqr=4*_eps*_eps+_fri*_fri*fnSqr,LHS=sqrt(LHSSqr);
  TH RHSSqr=(TH)(_eps*_eps)+fp.squaredNorm(),RHS=sqrt(RHSSqr);
  E+=_p.template eval<TH>((TH)LHS-RHS,Gf?&D:NULL,Hf?&DD:NULL,0,(TH)_coef);
  if(!isfinite(E))
    return false;
  if(Gf)
    *Gf-=fp*D/RHS;
  if(Hf)
    *Hf+=-(Mat3TH::Identity()-fp*fp.transpose()/RHSSqr)*D/RHS+fp*fp.transpose()*DD/RHSSqr;
  return true;
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::energyPerForce(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const {
  Vec3TH Gf;
  Mat3TH Hf;
  for(int i=0,j=0; i<size(); i+=3,j++) {
    if(!energyPerForce(_fnSqr[j],fp.template segment<3>(i),E,G?&Gf:NULL,(H||Hw)?&Hf:NULL))
      return false;
    if(G)
      G->template segment<3>(i)+=Gf;
    if(H)
      H->template block<3,3>(i,i)+=Hf;
    if(Hw)
      Hw->_H[j]=Hf;
  }
  return true;
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::energyPerForceDeriv(const Vec3T& fn,const Vec3T& fp,Vec4T& dEdp,Vec3T& dEdfn) const {
  T D;
  Vec3T n=_barrier.getX().template segment<3>(0);
  //energy 1.1
  _p.template eval<T>(_eps-n.dot(fp),&D,NULL,0,_coef);
  dEdp.template segment<3>(0)-=D*fp;
  //energy 1.2
  _p.template eval<T>(_eps+n.dot(fp),&D,NULL,0,_coef);
  dEdp.template segment<3>(0)+=D*fp;
  //energy 2
  T LHSSqr=4*_eps*_eps+_fri*_fri*fn.squaredNorm(),LHS=sqrt(LHSSqr);
  T RHSSqr=_eps*_eps+fp.squaredNorm(),RHS=sqrt(RHSSqr);
  _p.template eval<T>(LHS-RHS,&D,NULL,0,_coef);
  dEdfn=D*_fri*_fri*fn/LHS;
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::energyPerForceDeriv(const VecTH& fp,Vec4T& dEdp,Vec3T& w1,Vec3T& t1,Vec3T& w2,Vec3T& t2) const {
  int off=0;
  Vec4T x=_barrier.getX();
  //p1
  if(_barrier._p1.jid()>=0)
    for(int i=0; i<(int)_barrier._p1.globalVss().cols(); i++,off+=3) {
      Vec3T p=_barrier._p1.globalVss().col(i);
      T D,DD,d=p.dot(x.template segment<3>(0))-x[3];
      _barrier._p.template eval<T>(d,&D,&DD,(T)_barrier._d0Half,1);
      Vec3T fn=D*x.template segment<3>(0),dEdfn;
      energyPerForceDeriv(fn,fp.template segment<3>(off).template cast<T>(),dEdp,dEdfn);
      //accumulate dEdp
      dEdp.template segment<3>(0)+=D*dEdfn;
      dEdp+=x.template segment<3>(0).dot(dEdfn)*DD*Vec4T(p[0],p[1],p[2],-1);
      //accumulate dEdpos
      Vec3T dEdpos=x.template segment<3>(0).dot(dEdfn)*DD*x.template segment<3>(0);
      w1+=(p-CTRI(_grad._info._TM,_barrier._p1.jid())).cross(dEdpos);
      t1+=dEdpos;
    }
  //p2
  if(_barrier._p2.jid()>=0)
    for(int i=0; i<(int)_barrier._p2.globalVss().cols(); i++,off+=3) {
      Vec3T p=_barrier._p2.globalVss().col(i);
      T D,DD,d=x[3]-p.dot(x.template segment<3>(0));
      _barrier._p.template eval<T>(d,&D,&DD,(T)_barrier._d0Half,1);
      Vec3T fn=-D*x.template segment<3>(0),dEdfn;
      energyPerForceDeriv(fn,fp.template segment<3>(off).template cast<T>(),dEdp,dEdfn);
      //accumulate dEdp
      dEdp.template segment<3>(0)-=D*dEdfn;
      dEdp+=x.template segment<3>(0).dot(dEdfn)*DD*Vec4T(p[0],p[1],p[2],-1);
      //accumulate dEdpos
      Vec3T dEdpos=x.template segment<3>(0).dot(dEdfn)*DD*x.template segment<3>(0);
      w2+=(p-CTRI(_grad._info._TM,_barrier._p2.jid())).cross(dEdpos);
      t2+=dEdpos;
    }
}
//evaluate energy to ensure linear/angular momentum conservation
//energy 3: P(eps - (\sum_j fp1_i + \sum_j fp2_j)) + P(eps + (\sum_j fp1_i + \sum_j fp2_j))
//energy 4: P(eps - n.(\sum_j [x1_i]fp1_i + \sum_j [x2_i]fp2_j)) + P(eps + n.(\sum_j [x1_i]fp1_i + \sum_j [x2_i]fp2_j))
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::energySoftLinear(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const {
  if(_bLA.size()==0)
    return true;
  VecTH bf=_bLA.template cast<TH>()+_dbLAdfp.template cast<TH>()*fp;
  //compute G/H in b
  VecTH Gb,Hb;
  TH D,DD;
  if(G)
    Gb.setZero(bf.size());
  if(H)
    Hb.setZero(bf.size());
  for(int i=0; i<bf.size(); i++) {
    E+=_p.template eval<TH>((TH)_eps-bf[i],G?&D:NULL,(H||Hw)?&DD:NULL,0,(TH)_coef);
    if(!isfinite(E))
      return false;
    if(G)
      Gb[i]-=D;
    if(H)
      Hb[i]+=DD;
    if(Hw)
      Hw->_Hd[i]+=DD;
    E+=_p.template eval<TH>((TH)_eps+bf[i],G?&D:NULL,(H||Hw)?&DD:NULL,0,(TH)_coef);
    if(!isfinite(E))
      return false;
    if(G)
      Gb[i]+=D;
    if(H)
      Hb[i]+=DD;
    if(Hw)
      Hw->_Hd[i]+=DD;
  }
  //comptue G/H in f
  if(G)
    *G+=_dbLAdfp.template cast<TH>().transpose()*Gb;
  if(H)
    *H+=_dbLAdfp.template cast<TH>().transpose()*Hb.asDiagonal()*_dbLAdfp.template cast<TH>();
  return true;
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::energySoftLinearDeriv(const VecTH& fp,Vec& dEdTheta,Vec4T& dEdp,Vec3T& w1,Vec3T& t1,Vec3T& w2,Vec3T& t2) const {
  if(_bLA.size()==0)
    return;
  T D;
  Mat3XT G;
  int off=0;
  Vec3T n=_barrier.getX().template segment<3>(0);
  Vec bf=_bLA+_dbLAdfp*fp.template cast<T>();
  Vec Gb=Vec::Zero(bf.size());
  for(int i=0; i<bf.size(); i++) {
    _p.template eval<T>(_eps-bf[i],&D,NULL,0,_coef);
    Gb[i]-=D;
    _p.template eval<T>(_eps+bf[i],&D,NULL,0,_coef);
    Gb[i]+=D;
  }
  //p1
  if(_barrier._p1.jid()>=0) {
    G.setZero(3,_body.nrJ()*4);
    for(int i=0; i<(int)_barrier._p1.globalVss().cols(); i++,off+=3) {
      Vec3T p=_barrier._p1.globalVss().col(i);
      Vec3T p0=_barrier._p1.mesh()->vss()[i].template cast<T>();
      Vec3T fpi=fp.template segment<3>(off).template cast<T>();
      //physics
      if(!_multiFriction) {
        ROTI(G,_barrier._p1.jid())+=fpi*p0.transpose();
        CTRI(G,_barrier._p1.jid())+=fpi;
      }
      //torque balance
      if(_torqueBalanceRow>=0) {
        dEdp.template segment<3>(0)+=Gb[_torqueBalanceRow]*p.cross(fpi);
        w1+=Gb[_torqueBalanceRow]*(p-CTRI(_grad._info._TM,_barrier._p1.jid())).cross(fpi.cross(n));
        t1+=Gb[_torqueBalanceRow]*fpi.cross(n);
      }
    }
    if(!_multiFriction)
      _grad._info.toolB(_barrier._p1.jid(),_body,mapM(G),[&](int r,int c,T val) {
      dEdTheta[r]+=Gb[c]*val;
    });
  }
  //p2
  if(_barrier._p2.jid()>=0) {
    G.setZero(3,_body.nrJ()*4);
    for(int i=0; i<(int)_barrier._p2.globalVss().cols(); i++,off+=3) {
      Vec3T p=_barrier._p2.globalVss().col(i);
      Vec3T p0=_barrier._p2.mesh()->vss()[i].template cast<T>();
      Vec3T fpi=fp.template segment<3>(off).template cast<T>();
      //physics
      if(!_multiFriction) {
        ROTI(G,_barrier._p2.jid())+=fpi*p0.transpose();
        CTRI(G,_barrier._p2.jid())+=fpi;
      }
      //torque balance
      if(_torqueBalanceRow>=0) {
        dEdp.template segment<3>(0)+=Gb[_torqueBalanceRow]*p.cross(fpi);
        w2+=Gb[_torqueBalanceRow]*(p-CTRI(_grad._info._TM,_barrier._p2.jid())).cross(fpi.cross(n));
        t2+=Gb[_torqueBalanceRow]*fpi.cross(n);
      }
    }
    if(!_multiFriction)
      _grad._info.toolB(_barrier._p2.jid(),_body,mapM(G),[&](int r,int c,T val) {
      dEdTheta[r]+=Gb[c]*val;
    });
  }
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::energySoftLinearDeriv(const VecTH& fp,const Vec& Gb,Mat3XT& G) const {
  ASSERT(_multiFriction)
  int off=0;
  //p1
  if(_barrier._p1.jid()>=0) {
    for(int i=0; i<(int)_barrier._p1.globalVss().cols(); i++,off+=3) {
      Vec3T p0=_barrier._p1.mesh()->vss()[i].template cast<T>();
      Vec3T fpi=fp.template segment<3>(off).template cast<T>();
      //physics
      ROTI(G,_barrier._p1.jid())+=fpi*p0.transpose();
      CTRI(G,_barrier._p1.jid())+=fpi;
    }
  }
  //p2
  if(_barrier._p2.jid()>=0) {
    for(int i=0; i<(int)_barrier._p2.globalVss().cols(); i++,off+=3) {
      Vec3T p0=_barrier._p2.mesh()->vss()[i].template cast<T>();
      Vec3T fpi=fp.template segment<3>(off).template cast<T>();
      //physics
      ROTI(G,_barrier._p2.jid())+=fpi*p0.transpose();
      CTRI(G,_barrier._p2.jid())+=fpi;
    }
  }
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::prepareEnergyBalance() {
  _torqueBalanceRow=-1;
  if(_multiFriction) {
    //multiple frictions are optimzied together
    if(_barrier._p1.jid()>=0 && _barrier._p2.jid()>=0) {
      _bLA.setZero(4);
      _torqueBalanceRow=_bLA.size()-1;
      int off=0;
      Vec3T n=_barrier.getX().template segment<3>(0);
      _dbLAdfp.setZero(4,_dbdfp.cols());
      for(int i=0; i<_barrier._p1.globalVss().cols(); i++,off+=3) {
        _dbLAdfp.template block<3,3>(0,off).setIdentity();
        _dbLAdfp.template block<1,3>(3,off)=n.cross(_barrier._p1.globalVss().col(i));
      }
      for(int i=0; i<_barrier._p2.globalVss().cols(); i++,off+=3) {
        _dbLAdfp.template block<3,3>(0,off).setIdentity();
        _dbLAdfp.template block<1,3>(3,off)=n.cross(_barrier._p2.globalVss().col(i));
      }
    } else {
      _bLA.setZero(0);
      _dbLAdfp.setZero(0,_dbdfp.cols());
    }
  } else {
    //only a single friction term is optimized
    if(_barrier._p1.jid()>=0 && _barrier._p2.jid()>=0) {
      _bLA.setZero(_dbdfp.rows()+4);
      _bLA.segment(0,_dbdfp.rows())=_b;
      _torqueBalanceRow=_bLA.size()-1;
      int off=0;
      Vec3T n=_barrier.getX().template segment<3>(0);
      _dbLAdfp.setZero(_dbdfp.rows()+4,_dbdfp.cols());
      _dbLAdfp.template block(0,0,_dbdfp.rows(),_dbdfp.cols())=_dbdfp;
      for(int i=0; i<_barrier._p1.globalVss().cols(); i++,off+=3) {
        _dbLAdfp.template block<3,3>(0+_dbdfp.rows(),off).setIdentity();
        _dbLAdfp.template block<1,3>(3+_dbdfp.rows(),off)=n.cross(_barrier._p1.globalVss().col(i));
      }
      for(int i=0; i<_barrier._p2.globalVss().cols(); i++,off+=3) {
        _dbLAdfp.template block<3,3>(0+_dbdfp.rows(),off).setIdentity();
        _dbLAdfp.template block<1,3>(3+_dbdfp.rows(),off)=n.cross(_barrier._p2.globalVss().col(i));
      }
    } else {
      _bLA=_b;
      _dbLAdfp=_dbdfp;
    }
  }
}
//evaluate energy for physics value function
//energy 5: \sum_i P(eps-(b_i + \sum_j dx_{1j}/d\theta_i * fp1_j))+P(eps+(b_i + \sum_j dx_{2j}/d\theta_i * fp2_j))
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::prepareEnergyPhysics() {
  int off=0;
  Mat3XT JR,JC;
  Vec4T x=_barrier.getX();
  int nrDOF=_body.nrDOF();
  _fnSqr.resize(size());
  _dbdfp.resize(nrDOF,size());
  //p1: fill in _fnSqr and _dbdfp
  if(_barrier._p1.jid()>=0) {
    JR.setZero(3,nrDOF);
    JC.setZero(3,nrDOF);
    _grad._info.JRSparse(_body,_barrier._p1.jid(),[&](int c,const Vec3T& d) {
      JR.col(c)=d;
    });
    _grad._info.JCSparse(_body,_barrier._p1.jid(),[&](int c,const Vec3T& d) {
      JC.col(c)=d;
    });
    for(int i=0; i<(int)_barrier._p1.globalVss().cols(); i++,off++) {
      Vec3T vLast=_barrier._p1.globalVss().col(i);
      T D,d=vLast.dot(x.template segment<3>(0))-x[3];
      _barrier._p.template eval<T>(d,&D,NULL,_barrier._d0Half,1);
      //_fnSqr: squared norm of normal force
      _fnSqr[off]=(D*x.template segment<3>(0)).squaredNorm();
      //_dbdfp: Jacobian of vertices
      Vec3T Rx=vLast-CTRI(_grad._info._TM,_barrier._p1.jid());
      _dbdfp.block(0,off*3,nrDOF,3)=(-cross<T>(Rx)*JR+JC).transpose();
    }
  }
  //p2: fill in _fnSqr and _dbdfp
  if(_barrier._p2.jid()>=0) {
    JR.setZero(3,nrDOF);
    JC.setZero(3,nrDOF);
    _grad._info.JRSparse(_body,_barrier._p2.jid(),[&](int c,const Vec3T& d) {
      JR.col(c)=d;
    });
    _grad._info.JCSparse(_body,_barrier._p2.jid(),[&](int c,const Vec3T& d) {
      JC.col(c)=d;
    });
    for(int i=0; i<(int)_barrier._p2.globalVss().cols(); i++,off++) {
      Vec3T vLast=_barrier._p2.globalVss().col(i);
      T D,d=x[3]-vLast.dot(x.template segment<3>(0));
      _barrier._p.template eval<T>(d,&D,NULL,_barrier._d0Half,1);
      //_fnSqr: squared norm of normal force
      _fnSqr[off]=(D*x.template segment<3>(0)).squaredNorm();
      //_dbdfp: Jacobian of vertices
      Vec3T Rx=vLast-CTRI(_grad._info._TM,_barrier._p2.jid());
      _dbdfp.block(0,off*3,nrDOF,3)=(-cross<T>(Rx)*JR+JC).transpose();
    }
  }
}
//optimize for best friction force
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::energy(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const {
  E=0;
  if(G)
    G->setZero(size());
  if(H)
    H->setZero(size(),size());
  if(Hw)
    Hw->reset(size(),_dbLAdfp);
  if(!energyPerForce(fp,E,G,H,Hw))
    return false;
  if(!energySoftLinear(fp,E,G,H,Hw))
    return false;
  return true;
}
template <typename T,typename TH>
void CCBarrierFrictionOptimization<T,TH>::prepareEnergy() {
  prepareEnergyPhysics();
  prepareEnergyBalance();
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::optimize(VecTH& fp,TH& E,VecTH& G,MatTH& H,bool output) {
  prepareEnergy();
  VecTH D,G2,fp2;
  Eigen::LDLT<MatTH> invH;
  MatTH id=MatTH::Identity(size(),size()),H2;
  TH E2,alpha=1,alphaDec=0.5,alphaInc=3.0,alphaMax=1e10;
  //assemble
  //debugEnergy(x);
  if(!energy(fp,E,&G,&H,NULL))
    return false;
  if(output)
    std::cout << "optimize" << std::endl;
  //main loop
  //  int i;
  //  for(i=0;i<1e5;++i){
  while(alpha<alphaMax) {
    if(E==0)
      break;
    //search direction
    invH.compute(H+id*alpha);
    D=invH.solve(G);
    //termination
    if(output)
      std::cout << "E=" << E << " G=" << G.cwiseAbs().maxCoeff() << " alpha=" << alpha << std::endl;
    if(G.cwiseAbs().maxCoeff()<=Epsilon<TH>::defaultEps())
      break;
    //test
    fp2=fp-D;
    if(energy(fp2,E2,&G2,&H2,NULL) && E2<E) {
      alpha=std::max<TH>(alpha*alphaDec,Epsilon<TH>::defaultEps());
      fp=fp2;
      E=E2;
      G.swap(G2);
      H.swap(H2);
    } else {
      alpha*=alphaInc;
    }
  }
  return true;
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimization<T,TH>::optimizeFast(VecTH& fp,TH& E,VecTH& G,WoodburyHessian<T,TH>& H,bool output) {
  prepareEnergy();
  VecTH D,G2,fp2;
  WoodburyHessian<T,TH> H2;
  TH E2,alpha=1,alphaDec=0.5,alphaInc=3.0,alphaMax=1e10;
  //assemble
  //debugEnergy(x);
  if(!energy(fp,E,&G,NULL,&H))
    return false;
  if(output)
    std::cout << "optimizeFast" << std::endl;
  //main loop
  //  int i;
  //  for(i=0;i<1e5;++i){
  while(alpha<alphaMax) {
    if(E==0)
      break;
    //search direction
    D.resize(G.size());
    H.solve(D,G,alpha);
    //termination
    if(output)
      std::cout << "E=" << E << " G=" << G.cwiseAbs().maxCoeff() << " alpha=" << alpha << std::endl;
    if(G.cwiseAbs().maxCoeff()<=Epsilon<TH>::defaultEps())
      break;
    //test
    fp2=fp-D;
    if(energy(fp2,E2,&G2,NULL,&H2) && E2<E) {
      alpha=std::max<TH>(alpha*alphaDec,Epsilon<TH>::defaultEps());
      fp=fp2;
      E=E2;
      G.swap(G2);
      H.swap(H2);
    } else {
      alpha*=alphaInc;
    }
  }
  return true;
}
//instance
template class WoodburyHessian<FLOAT>;
template class CCBarrierFrictionOptimization<FLOAT>;
}
