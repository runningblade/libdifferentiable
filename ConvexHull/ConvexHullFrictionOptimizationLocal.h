#ifndef CONVEX_HULL_FRICTION_OPTIMIZATION_LOCAL_H
#define CONVEX_HULL_FRICTION_OPTIMIZATION_LOCAL_H

#include "ConvexHullFrictionOptimization.h"

namespace PHYSICSMOTION {
template <typename T,typename TH=typename HigherPrecisionTraits<T>::TH>
class CCBarrierFrictionOptimizationLocal : public CCBarrierFrictionOptimization<T,TH> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  typedef Logx PFunc;
  typedef CCBarrierConvexEnergy<T,PFunc,TH> Barrier;
  using CCBarrierFrictionOptimization<T,TH>::size;
  using CCBarrierFrictionOptimization<T,TH>::energyPerForce;
  using CCBarrierFrictionOptimization<T,TH>::energySoftLinear;
  using CCBarrierFrictionOptimization<T,TH>::prepareEnergyPhysics;
  using CCBarrierFrictionOptimization<T,TH>::prepareEnergyBalance;
  using CCBarrierFrictionOptimization<T,TH>::_dbLAdfp;
  using CCBarrierFrictionOptimization<T,TH>::_multiFriction;
  typedef Eigen::Matrix<TH,-1,1> VecTH;
  typedef Eigen::Matrix<TH,-1,-1> MatTH;
  typedef Eigen::Matrix<TH,3,-1> Mat3XTH;
  typedef Eigen::Matrix<TH,4,-1> Mat4XTH;
  typedef Eigen::Matrix<TH,2,1> Vec2TH;
  typedef Eigen::Matrix<TH,3,1> Vec3TH;
  typedef Eigen::Matrix<TH,4,1> Vec4TH;
  typedef Eigen::Matrix<TH,3,3> Mat3TH;
  typedef Eigen::Matrix<TH,4,4> Mat4TH;
 public:
  CCBarrierFrictionOptimizationLocal(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,const Barrier& barrier,T fri,T eps,T coef);
  static void debugEnergy(const ArticulatedBody& body,int JID,int JID2,T fri,T eps);
  bool energyMatch(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const;
 protected:
  //optimize for best friction force
  bool energy(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const override;
  void prepareEnergy() override;
  bool optimize(VecTH& fp,TH& E,VecTH& G,MatTH& H,bool output=false) override;
  bool optimizeFast(VecTH& fp,TH& E,VecTH& G,WoodburyHessian<T,TH>& H,bool output=false) override;
  VecTH _fp0;
};
}
#endif
