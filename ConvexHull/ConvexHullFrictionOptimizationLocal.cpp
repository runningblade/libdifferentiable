#include "ConvexHullFrictionOptimizationLocal.h"

namespace PHYSICSMOTION {
//optimize for best friction force
template <typename T,typename TH>
CCBarrierFrictionOptimizationLocal<T,TH>::CCBarrierFrictionOptimizationLocal(const ArticulatedBody& body,const CollisionGradInfo<T>& grad,const Barrier& barrier,T fri,T eps,T coef)
  :CCBarrierFrictionOptimization<T,TH>(body,grad,barrier,fri,eps,coef) {}
template <typename T,typename TH>
void CCBarrierFrictionOptimizationLocal<T,TH>::debugEnergy(const ArticulatedBody& body,int JID,int JID2,T fri,T eps) {
  TH Ef,Ef2;
  VecTH fp,fp0,dfp,G,G2,D,DRef;
  MatTH H;
  Vec x,dx;
  T coef=0.5,E,alpha=0.1f;
  PFunc barrier;
  CollisionGradInfo<T> info;
  WoodburyHessian<T,TH> Hw;
  DEFINE_NUMERIC_DELTA_T(TH)
  while(true) {
    x=Vec::Random(body.nrDOF());
    dx=Vec::Random(body.nrDOF());
    //construct energy
    info.reset(body,x);
    CCBarrierConvexEnergy<T,PFunc,TH> En(info._polytopes[JID],info._polytopes[JID2],barrier,0,&info,coef,true);
    CCBarrierFrictionOptimizationLocal<T,TH> Et(body,info,En,fri,eps,coef);
    Et.setB(Vec::Random(body.nrDOF())*eps*0.5f);
    //if energy does not evaluate to finite value, then return
    if(!En.CCBarrierEnergy<T,PFunc,TH>::eval(&E,&body,&info,NULL,NULL,NULL))
      continue;
    //prepare the energies for friction computation
    Et.prepareEnergy();
    fp=fp0=VecTH::Random(Et._dbdfp.cols())*(TH)eps*0.1f;
    Et._fp0.setRandom(Et._dbdfp.cols());
    dfp.setRandom(Et._dbdfp.cols());
    if(!Et.energy(fp,Ef,&G,&H,NULL))
      continue;
    //Woodbury matrix inversion
    Et.energy(fp,Ef,&G,NULL,&Hw);
    D.resize(G.size());
    Hw.solve(D,G,(TH)alpha);
    DRef=(H+MatTH::Identity(G.size(),G.size())*(TH)alpha).inverse()*G;
    DEBUG_GRADIENT("Woodbury-D",DRef.norm(),(D-DRef).norm())
    //debug gradient
    Et.energy(fp+dfp*DELTA,Ef2,&G2,NULL,NULL);
    DEBUG_GRADIENT("G",dfp.dot(G),(dfp.dot(G)-(Ef2-Ef)/DELTA))
    DEBUG_GRADIENT("H",(H*dfp).norm(),(H*dfp-(G2-G)/DELTA).norm())
    //optimize
    Et.optimize(fp=fp0,Ef,G,H,true);
    Et.optimizeFast(fp=fp0,Ef,G,Hw,true);
    break;
  }
}

template <typename T,typename TH>
bool CCBarrierFrictionOptimizationLocal<T,TH>::energyMatch(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const {
  E+=(fp-_fp0).squaredNorm()/2;
  if(G)
    *G+=fp-_fp0;
  if(H)
    H->diagonal().array()+=1;
  //if(Hw) {}
  //If Hw!=NULL, we can just set alpha=alpha+1 to effectively add H
  return true;
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimizationLocal<T,TH>::energy(const VecTH& fp,TH& E,VecTH* G,MatTH* H,WoodburyHessian<T,TH>* Hw) const {
  E=0;
  if(G)
    G->setZero(size());
  if(H)
    H->setZero(size(),size());
  if(Hw)
    Hw->reset(size(),_dbLAdfp);
  if(!energyPerForce(fp,E,G,H,Hw))
    return false;
  if(!energySoftLinear(fp,E,G,H,Hw))
    return false;
  if(!energyMatch(fp,E,G,H,Hw))
    return false;
  return true;
}
template <typename T,typename TH>
void CCBarrierFrictionOptimizationLocal<T,TH>::prepareEnergy() {
  prepareEnergyPhysics();   //we only need this to calculate _fnSqr
  _multiFriction=true;
  prepareEnergyBalance();   //we need the force/torque balance constraint
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimizationLocal<T,TH>::optimize(VecTH& fp,TH& E,VecTH& G,MatTH& H,bool output) {
  _fp0=fp;  //let fp approach _fp0 as much as possible
  prepareEnergy();
  VecTH D,G2,fp2;
  Eigen::LDLT<MatTH> invH;
  MatTH id=MatTH::Identity(size(),size()),H2;
  TH E2,alpha=1,alphaDec=0.5,alphaInc=3.0,alphaMax=1e10;
  //assemble
  //debugEnergy(x);
  if(!energy(fp,E,&G,&H,NULL))
    return false;
  if(output)
    std::cout << "optimize" << std::endl;
  //main loop
  //  int i;
  //  for(i=0;i<1e5;++i){
  while(alpha<alphaMax) {
    if(E==0)
      break;
    //search direction
    invH.compute(H+id*alpha);
    D=invH.solve(G);
    //termination
    if(output)
      std::cout << "E=" << E << " G=" << G.cwiseAbs().maxCoeff() << " alpha=" << alpha << std::endl;
    if(G.cwiseAbs().maxCoeff()<=Epsilon<TH>::defaultEps())
      break;
    //test
    fp2=fp-D;
    if(energy(fp2,E2,&G2,&H2,NULL) && E2<E) {
      alpha=std::max<TH>(alpha*alphaDec,Epsilon<TH>::defaultEps());
      fp=fp2;
      E=E2;
      G.swap(G2);
      H.swap(H2);
    } else {
      alpha*=alphaInc;
    }
  }
  return true;
}
template <typename T,typename TH>
bool CCBarrierFrictionOptimizationLocal<T,TH>::optimizeFast(VecTH& fp,TH& E,VecTH& G,WoodburyHessian<T,TH>& H,bool output) {
  _fp0=fp;  //let fp approach _fp0 as much as possible
  prepareEnergy();
  VecTH D,G2,fp2;
  WoodburyHessian<T,TH> H2;
  TH E2,alpha=1,alphaDec=0.5,alphaInc=3.0,alphaMax=1e10;
  //assemble
  //debugEnergy(x);
  if(!energy(fp,E,&G,NULL,&H))
    return false;
  if(output)
    std::cout << "optimizeFast" << std::endl;
  //main loop
  //  int i;
  //  for(i=0;i<1e5;++i){
  while(alpha<alphaMax) {
    if(E==0)
      break;
    //search direction
    D.resize(G.size());
    H.solve(D,G,alpha+1);
    //termination
    if(output)
      std::cout << "E=" << E << " G=" << G.cwiseAbs().maxCoeff() << " alpha=" << alpha << std::endl;
    if(G.cwiseAbs().maxCoeff()<=Epsilon<TH>::defaultEps())
      break;
    //test
    fp2=fp-D;
    if(energy(fp2,E2,&G2,NULL,&H2) && E2<E) {
      alpha=std::max<TH>(alpha*alphaDec,Epsilon<TH>::defaultEps());
      fp=fp2;
      E=E2;
      G.swap(G2);
      H.swap(H2);
    } else {
      alpha*=alphaInc;
    }
  }
  return true;
}
//instance
template class CCBarrierFrictionOptimizationLocal<FLOAT>;
}
