#include "LIPDynamicsSequenceSplineXYZT.h"
#include <Articulated/NEArticulatedGradientInfo.h>
#include <Utils/RotationUtils.h>
#include <Utils/Interp.h>

namespace PHYSICSMOTION {
template <typename T>
LIPDynamicsSequenceSplineXYZT<T>::LIPDynamicsSequenceSplineXYZT
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec3T& G,T h0,
 std::shared_ptr<ArticulatedBody> bodySimplified,
 const std::vector<std::shared_ptr<FootStepSequence<T>>>& footSteps,bool isBoundingCircle,
 std::shared_ptr<PhaseSequence<T>> phaseXY,std::shared_ptr<PhaseSequence<T>> phaseZT,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int NCToFix,bool inherited)
  :LIPDynamicsSequence<T>(obj,inherited?name:name+"LIPDynamicsSequenceSplineXYZT",dt,G,h0,bodySimplified,footSteps,isBoundingCircle,ees,NCToFix,true) {
  if(inherited)
    return;
  ASSERT_MSG(_bodySimplified->nrDOF()==6,"NESimplifiedDynamicsSequence must have 6 degrees of freedom!")
  int N=footSteps[0]->nrTimestep();
  int NF=(int)_footSteps.size();
  _DOFVars.setConstant(4,N,-1);
  _XYSeq.reset(new VectorSequence<T,2>(obj,_name+"LIPXY",2,phaseXY));
  _ZTSeq.reset(new VectorSequence<T,2>(obj,_name+"LIPZT",2,phaseZT));
  buildStencil(obj);
  for(int c=_posConsStart; c<N; c++)
    //foot reachable constraint
    for(int f=0; f<NF; f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      EndEffectorBounds::Vec3T frameTCtr=ee->_frame.transpose()*ee->_ctrBB;
      if(_isBoundingCircle) {
        _gl.push_back(-SQPObjective<T>::infty()); //x,y
        _gu.push_back(pow(ee->getBBRadius(),2));
        _gl.push_back(ee->_bb[2]*ee->_res+frameTCtr[2]);  //z
        _gu.push_back(ee->_bb[5]*ee->_res+frameTCtr[2]);
      } else {
        for(int r=0; r<3; r++) {
          _gl.push_back(ee->_bb[r+0]*ee->_res+frameTCtr[r]);
          _gu.push_back(ee->_bb[r+3]*ee->_res+frameTCtr[r]);
        }
      }
    }
  for(int c=0; c<_xyInterp.rows(); c++) {
    //dynamics constraint
    _gl.push_back(0);
    _gu.push_back(0);
  }
  //lambda sum to one
  _lambdas.setConstant(NF,N,-1);
  for(int c=2; c<N; c++) {
    PolyXT sumLambda;
    for(int f=0; f<NF; f++)
      if(footSteps[f]->isStance(c)) {
        _lambdas(f,c)=obj.addVar(lambdaVar(f,c),0,1,SQPObjectiveCompound<T>::MUST_NEW)._id;
        sumLambda+=obj.addVarPoly(_lambdas(f,c));
      }
    //lambda sum to one
    SQPObjectiveComponent<T>::setPolynomialComponentConstraint((int)_gl.size(),sumLambda);
    _gl.push_back(1);
    _gu.push_back(1);
  }
}
template <typename T>
int LIPDynamicsSequenceSplineXYZT<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  std::vector<Vec> vss(_footSteps.size());
  for(int f=0; f<(int)vss.size(); f++)
    vss[f]=_footSteps[f]->pos(x);
  //reachable constraint
  int N=_footSteps[0]->nrTimestep();
  Vec DOFXY=_xyInterp.transpose()*x.segment(_rangeXY[0],_rangeXY[1]);
  Vec DOFZT=_ztInterp.transpose()*x.segment(_rangeZT[0],_rangeZT[1]);
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<N; c++) {
    Vec3T DRDZ,rel,ctrEE,relC,ctr=Vec3T(DOFXY[c*2+0],DOFXY[c*2+1],DOFZT[c*2+0]);
    Mat3T R=expWZ<T,Vec3T>(DOFZT[c*2+1],&DRDZ);
    for(int f=0; f<(int)_ees.size(); f++) {
      rel=vss[f].template segment<3>(c*3)-ctr;
      ctrEE=_ees[f]->getBBCenter().template cast<T>();
      relC=vss[f].template segment<3>(c*3)-ctr-R*ctrEE;
      int offset=_offset+((c-_posConsStart)*(int)_ees.size()+f)*(_isBoundingCircle?2:3);
      if(_isBoundingCircle) {
        fvec[offset]=relC.template segment<2>(0).squaredNorm();
        fvec[offset+1]=R.col(2).dot(rel);
        if(fjac) {
          for(int d=0; d<2; d++) {  //x,y
            for(typename SMatT::InnerIterator it(_xyInterp,c*2+d); it; ++it)
              addBlock(*fjac,offset,it.row()+_rangeXY[0],T(-2)*relC[d]*it.value());
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              addBlock(*fjac,offset,it.row(),T(2)*relC[d]*it.value());
          }
          T dR=-2*((cross(DRDZ)*R)*ctrEE).template segment<2>(0).dot(relC.template segment<2>(0));
          for(typename SMatT::InnerIterator it(_ztInterp,c*2+1); it; ++it)
            addBlock(*fjac,offset,it.row()+_rangeZT[0],T(dR*it.value()));
          for(int d=0; d<2; d++) {  //z
            for(typename SMatT::InnerIterator it(_xyInterp,c*2+d); it; ++it)
              fjac->push_back(STrip(offset+1,it.row()+_rangeXY[0],-R(d,2)*it.value()));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              fjac->push_back(STrip(offset+1,it.row(),R(d,2)*it.value()));
          }
          {
            for(typename SMatT::InnerIterator it(_ztInterp,c*2); it; ++it)
              fjac->push_back(STrip(offset+1,it.row()+_rangeZT[0],-R(2,2)*it.value()));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+2); it; ++it)
              fjac->push_back(STrip(offset+1,it.row(),R(2,2)*it.value()));
          }
          dR=(cross(DRDZ)*R).col(2).dot(rel);
          for(typename SMatT::InnerIterator it(_ztInterp,c*2+1); it; ++it)
            fjac->push_back(STrip(offset+1,it.row()+_rangeZT[0],dR*it.value()));
        }
      } else {
        fvec.template segment<3>(offset)=R.transpose()*rel;
        if(fjac) {
          for(int d=0; d<2; d++) {
            for(typename SMatT::InnerIterator it(_xyInterp,c*2+d); it; ++it)
              addBlock(*fjac,offset,it.row()+_rangeXY[0],-R.row(d).transpose()*it.value());
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              addBlock(*fjac,offset,it.row(),R.row(d).transpose()*it.value());
          }
          {
            for(typename SMatT::InnerIterator it(_ztInterp,c*2); it; ++it)
              addBlock(*fjac,offset,it.row()+_rangeZT[0],-R.row(2).transpose()*it.value());
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+2); it; ++it)
              addBlock(*fjac,offset,it.row(),R.row(2).transpose()*it.value());
          }
          Vec3T dR=(cross(DRDZ)*R).transpose()*rel;
          for(typename SMatT::InnerIterator it(_ztInterp,c*2+1); it; ++it)
            addBlock(*fjac,offset,it.row()+_rangeZT[0],dR*it.value());
        }
      }
    }
  }
  //dynamics constraint
  Vec fvecLIP=Vec::Zero(N*2);
  int NF=(int)_footSteps.size();
  T coefAccel=1/_dt/_dt;
  STrips fjacLIPTrips;
  OMP_PARALLEL_FOR_
  for(int c=2; c<N; c++) {
    int offset=c*2;
    for(int d=0; d<2; d++) {
      T u=0;
      for(int f=0; f<NF; f++)
        if(_lambdas(f,c)>=0)
          u+=vss[f](c*3+d)*x[_lambdas(f,c)];
      T accel=(DOFXY[c*2+d]-2*DOFXY[(c-1)*2+d]+DOFXY[(c-2)*2+d])*coefAccel;
      fvecLIP[offset+d]=accel-_w*(DOFXY[c*2+d]-u);
      if(fjac) {
        for(int f=0; f<NF; f++)
          if(_lambdas(f,c)>=0) {
            fjacLIPTrips.push_back(STrip(offset+d,_lambdas(f,c),vss[f](c*3+d)*_w));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              fjacLIPTrips.push_back(STrip(offset+d,it.row(),_w*x[_lambdas(f,c)]*it.value()));
          }
        for(typename SMatT::InnerIterator it(_xyInterp,c*2+d); it; ++it)
          fjacLIPTrips.push_back(STrip(offset+d,it.row()+_rangeXY[0],(coefAccel-_w)*it.value()));
        for(typename SMatT::InnerIterator it(_xyInterp,(c-1)*2+d); it; ++it)
          fjacLIPTrips.push_back(STrip(offset+d,it.row()+_rangeXY[0],-2*coefAccel*it.value()));
        for(typename SMatT::InnerIterator it(_xyInterp,(c-2)*2+d); it; ++it)
          fjacLIPTrips.push_back(STrip(offset+d,it.row()+_rangeXY[0],coefAccel*it.value()));
      }
    }
  }
  int offsetD=_offset+(N-_posConsStart)*(int)_ees.size()*(_isBoundingCircle?2:3);
  fvec.segment(offsetD,_xyInterp.rows())=_xyInterp*fvecLIP;
  if(fjac) {
    SMatT fjacLIP;
    fjacLIP.resize(N*2,x.size());
    fjacLIP.setFromTriplets(fjacLIPTrips.begin(),fjacLIPTrips.end());
    addBlock(*fjac,offsetD,0,SMatT(_xyInterp*fjacLIP));
  }
  return 0;
}
template <typename T>
Eigen::Matrix<int,-1,-1> LIPDynamicsSequenceSplineXYZT<T>::DOFVarXY() const {
  return _XYSeq->getVar();
}
template <typename T>
Eigen::Matrix<int,-1,-1> LIPDynamicsSequenceSplineXYZT<T>::DOFDiffVarXY() const {
  return _XYSeq->getDiffVar();
}
template <typename T>
Eigen::Matrix<int,-1,-1> LIPDynamicsSequenceSplineXYZT<T>::DOFVarZT() const {
  return _ZTSeq->getVar();
}
template <typename T>
Eigen::Matrix<int,-1,-1> LIPDynamicsSequenceSplineXYZT<T>::DOFDiffVarZT() const {
  return _ZTSeq->getDiffVar();
}
template <typename T>
void LIPDynamicsSequenceSplineXYZT<T>::fixInitialPose(SQPObjectiveCompound<T>& obj,bool fixVelocity) {
  for(int r=0; r<2; r++) {
    SQPVariable<T>& varXY=obj.addVar(_XYSeq->getVar()(r,0));
    varXY.fix(varXY._init);
    if(fixVelocity) {
      SQPVariable<T>& diffVarXY=obj.addVar(_XYSeq->getDiffVar()(r,0));
      diffVarXY.fix(diffVarXY._init);
    }
  }
  for(int r=0; r<2; r++) {
    SQPVariable<T>& varZT=obj.addVar(_ZTSeq->getVar()(r,0));
    varZT.fix(varZT._init);
    if(fixVelocity) {
      SQPVariable<T>& diffVarZT=obj.addVar(_ZTSeq->getDiffVar()(r,0));
      diffVarZT.fix(diffVarZT._init);
    }
  }
}
template <typename T>
void LIPDynamicsSequenceSplineXYZT<T>::initDOF(SQPObjectiveCompound<T>& obj,int doubleHorizon,int singleHorizon,const std::vector<FootStep<T>>& steps) {
  int N=_footSteps[0]->nrTimestep(),offXY=0,offZT=0;
  Vec DOFXY=Vec::Zero(N*2),DOFZT=Vec::Zero(N*2);
  for(int c=0,offStencil=0; c<(int)steps.size()-1; c++) {
    for(int i=0; i<singleHorizon; i++,offStencil++) {
      Vec6T DOF=interp1D(steps[c]._bodyPose,steps[c+1]._bodyPose,i/(T)singleHorizon);
      DOFXY[offXY++]=DOF[0];
      DOFXY[offXY++]=DOF[1];
      DOFZT[offZT++]=DOF[2];
      DOFZT[offZT++]=DOF[5];
    }
    for(int i=0; i<doubleHorizon; i++,offStencil++) {
      Vec6T DOF=steps[c+1]._bodyPose;
      DOFXY[offXY++]=DOF[0];
      DOFXY[offXY++]=DOF[1];
      DOFZT[offZT++]=DOF[2];
      DOFZT[offZT++]=DOF[5];
    }
  }
  {
    //minimize_x ||_xyInterp.transpose()*x-DOFXY||^2
    Vec x=Eigen::SimplicialLDLT<SMatT>(_xyInterp*_xyInterp.transpose()).solve(_xyInterp*DOFXY);
    const Eigen::Matrix<int,-1,-1>& var=_XYSeq->getVar();
    for(int r=0; r<var.rows(); r++)
      for(int c=0; c<var.cols(); c++)
        obj.addVar(var(r,c))._init=x[obj.addVar(var(r,c))._id-_rangeXY[0]];
    const Eigen::Matrix<int,-1,-1>& diffVar=_XYSeq->getDiffVar();
    for(int r=0; r<diffVar.rows(); r++)
      for(int c=0; c<diffVar.cols(); c++)
        obj.addVar(diffVar(r,c))._init=x[obj.addVar(diffVar(r,c))._id-_rangeXY[0]];
  }
  {
    //minimize_x ||_ztInterp.transpose()*x-DOFZT||^2
    Vec x=Eigen::SimplicialLDLT<SMatT>(_ztInterp*_ztInterp.transpose()).solve(_ztInterp*DOFZT);
    const Eigen::Matrix<int,-1,-1>& var=_ZTSeq->getVar();
    for(int r=0; r<var.rows(); r++)
      for(int c=0; c<var.cols(); c++)
        obj.addVar(var(r,c))._init=x[obj.addVar(var(r,c))._id-_rangeZT[0]];
    const Eigen::Matrix<int,-1,-1>& diffVar=_ZTSeq->getDiffVar();
    for(int r=0; r<diffVar.rows(); r++)
      for(int c=0; c<diffVar.cols(); c++)
        obj.addVar(diffVar(r,c))._init=x[obj.addVar(diffVar(r,c))._id-_rangeZT[0]];
  }
}
template <typename T>
typename LIPDynamicsSequenceSplineXYZT<T>::MatT LIPDynamicsSequenceSplineXYZT<T>::getPoses(const ArticulatedBody& body,const Environment<T>& env,const Vec& x) {
  std::vector<Vec> vss(_footSteps.size());
  for(int f=0; f<(int)vss.size(); f++)
    vss[f]=_footSteps[f]->pos(x);
  //compute full body pose
  MatT ret;
  int N=_footSteps[0]->nrTimestep();
  Vec DOFXY=_xyInterp.transpose()*x.segment(_rangeXY[0],_rangeXY[1]);
  Vec DOFZT=_ztInterp.transpose()*x.segment(_rangeZT[0],_rangeZT[1]);
  ret.resize(body.nrDOF(),N);
  OMP_PARALLEL_FOR_
  for(int c=0; c<N; c++) {
    //base
    Vec DOF=Vec::Zero(body.nrDOF());
    DOF.template segment<2>(0)=DOFXY.template segment<2>(c*2);
    DOF[2]=DOFZT[c*2+0];
    DOF[5]=DOFZT[c*2+1];
    NEArticulatedGradientInfo<T> info(*_bodySimplified,DOF.template segment<6>(0));
    Mat3X4T TJ=info.NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1);
    //xss
    for(int f=0; f<(int)_ees.size(); f++) {
      Vec3T pos=vss[f].template segment<3>(c*3);
      Vec3T nor=env.phiGrad(pos);
      pos=ROT(TJ).transpose()*(pos-CTR(TJ));
      nor=ROT(TJ).transpose()*nor;
      DOF+=_ees[f]->getDOFAll(body,pos.template cast<EndEffectorBounds::T>(),nor.template cast<EndEffectorBounds::T>()).template cast<T>();
    }
    ret.col(c)=DOF;
  }
  return ret;
}
template <typename T>
void LIPDynamicsSequenceSplineXYZT<T>::setTargetHeightEnergy(SQPObjectiveCompound<T>& obj,T hCoef,T coefLambdaEnergy) {
  PolyXT E;
  int N=_footSteps[0]->nrTimestep();
  int NF=(int)_footSteps.size();
  //objective: height should be as close to _h0 as possible
  if(hCoef>0)
    for(int c=_posConsStart; c<N; c++)
      for(int eid=0; eid<NF; eid++) {
        PolyXT heightDiff=getZTPoly(obj,c*2+0)-_footSteps[eid]->getPosPoly(obj,c*3+2)-PolyXT(_h0);
        E+=heightDiff*heightDiff*hCoef;
      }
  //objective: best lambda should evenly distribute robot's mass
  if(coefLambdaEnergy>0)
    for(int c=2; c<N; c++) {
      int NLambda=0;
      for(int f=0; f<NF; f++)
        if(_footSteps[f]->isStance(c))
          NLambda++;
      typename PolyXT::VECP lambdaPoly;
      lambdaPoly.resize(NLambda);
      for(int f=0,off=0; f<NF; f++)
        if(_footSteps[f]->isStance(c))
          lambdaPoly[off++]=obj.addVarPoly(_lambdas(f,c))-PolyXT(1./(T)NLambda);
      E+=lambdaPoly.squaredNorm()*coefLambdaEnergy;
    }
  SQPObjectiveComponent<T>::setPolynomialComponentObjective(E);
}
template <typename T>
typename LIPDynamicsSequenceSplineXYZT<T>::PolyXT LIPDynamicsSequenceSplineXYZT<T>::getXYPoly(SQPObjectiveCompound<T>& obj,int col) const {
  PolyXT ret;
  for(typename SMatT::InnerIterator it(_xyInterp,col); it; ++it)
    ret+=obj.addVarPoly(it.row()+_rangeXY[0])*PolyXT(it.value());
  return ret;
}
template <typename T>
typename LIPDynamicsSequenceSplineXYZT<T>::PolyXT LIPDynamicsSequenceSplineXYZT<T>::getZTPoly(SQPObjectiveCompound<T>& obj,int col) const {
  PolyXT ret;
  for(typename SMatT::InnerIterator it(_ztInterp,col); it; ++it)
    ret+=obj.addVarPoly(it.row()+_rangeZT[0])*PolyXT(it.value());
  return ret;
}
template <typename T>
void LIPDynamicsSequenceSplineXYZT<T>::buildStencil(const SQPObjectiveCompound<T>& obj) {
  int N=_footSteps[0]->nrTimestep();
  STrips trips;
  //XY
  trips.clear();
  _rangeXY[0]=std::min(_XYSeq->getVar().minCoeff(),_XYSeq->getDiffVar().minCoeff());
  _rangeXY[1]=_XYSeq->getVar().size()+_XYSeq->getDiffVar().size();
  for(int c=0; c<N; c++) {
    GRAD_INTERP dofGrads[2];
    _XYSeq->timeInterpolateStencil(obj.init(),c*_dt,dofGrads,NULL);
    for(int dof=0; dof<2; dof++)
      for(const std::pair<const int,T>& v:dofGrads[dof])
        trips.push_back(STrip(v.first-_rangeXY[0],c*2+dof,v.second));
  }
  _xyInterp.resize(_rangeXY[1],N*2);
  _xyInterp.setFromTriplets(trips.begin(),trips.end());
  //ZT
  trips.clear();
  _rangeZT[0]=std::min(_ZTSeq->getVar().minCoeff(),_ZTSeq->getDiffVar().minCoeff());
  _rangeZT[1]=_ZTSeq->getVar().size()+_ZTSeq->getDiffVar().size();
  for(int c=0; c<N; c++) {
    GRAD_INTERP dofGrads[2];
    _ZTSeq->timeInterpolateStencil(obj.init(),c*_dt,dofGrads,NULL);
    for(int dof=0; dof<2; dof++)
      for(const std::pair<const int,T>& v:dofGrads[dof])
        trips.push_back(STrip(v.first-_rangeZT[0],c*2+dof,v.second));
  }
  _ztInterp.resize(_rangeZT[1],N*2);
  _ztInterp.setFromTriplets(trips.begin(),trips.end());
}
template class LIPDynamicsSequenceSplineXYZT<FLOAT>;
}
