#include "InverseKinematics.h"

namespace PHYSICSMOTION {
//pos
template <typename T>
InverseKinematicsPosObj<T>::InverseKinematicsPosObj
(SQPObjectiveCompound<T>& obj,const std::string& name,const ArticulatedBody& body,const Vec3T& pos,
 const Vec3T& localPos,const std::vector<int>& DOFs,const Vec* init,bool limit,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"InverseKinematicsPosObj",true),_body(body),_localPos(localPos) {
  if(inherited)
    return;
  for(int i=0; i<(int)DOFs.size(); i++) {
    T l=limit?body.lowerLimit()[DOFs[i]]:-SQPObjective<T>::infty();
    T u=limit?body.upperLimit()[DOFs[i]]: SQPObjective<T>::infty();
    const SQPVariable<T>& var=obj.addVar("DOF"+std::to_string(DOFs[i]),l,u);
    if(init)
      obj.addVar(var._id)._init=init->coeff(i);
    _DOFToVid[DOFs[i]]=var;
  }
  for(int i=0; i<body.nrJ(); i++) {
    const Joint& J=body.joint(i);
    if(DOFs[0]>=J._offDOF && DOFs[0]<J._offDOF+J.nrDOF())
      _JID=i;
  }
  for(int i=0; i<3; i++) {
    _gl.push_back(pos[i]);
    _gu.push_back(pos[i]);
  }
}
template <typename T>
int InverseKinematicsPosObj<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  Vec DOF=Vec::Zero(_body.nrDOF());
  for(const auto& p:_DOFToVid)
    DOF[p.first]=x[p.second._id];
  //compute variable
  _info.reset(_body,DOF);
  fvec.template segment<3>(_offset)=ROTI(_info._TM,_JID)*_localPos+CTRI(_info._TM,_JID);
  if(fjac)
    for(int r=0; r<3; r++) {
      Mat3X4T GK=Mat3X4T::Zero();
      GK(r,3)=1;
      GK.template block<1,3>(r,0)=_localPos.transpose();
      _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
        typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
        if(it!=_DOFToVid.end())
          fjac->push_back(STrip(r,it->second._id,val));
      });
    }
  return 0;
}
template <typename T>
T InverseKinematicsPosObj<T>::operator()(const Vec& x,Vec* fgrad) {
  T E=0;
  for(const auto& p:_DOFToVid) {
    T diff=x[p.second._id]-p.second._init;
    if(fgrad)
      fgrad->coeffRef(p.second._id)+=diff;
    E+=diff*diff;
  }
  return E/2;
}
//pos/nor
template <typename T>
InverseKinematicsPosNorObj<T>::InverseKinematicsPosNorObj
(SQPObjectiveCompound<T>& obj,const std::string& name,const ArticulatedBody& body,const Vec3T& pos,const Vec3T& nor,
 const Vec3T& localPos,const Vec3T& localNor,const std::vector<int>& DOFs,T coef,const Vec* init,bool limit,bool inherited)
  :InverseKinematicsPosObj<T>(obj,inherited?name:name+"InverseKinematicsPosNorObj"+std::string(coef<=0?"Hard":"Soft"),body,pos,localPos,DOFs,init,limit,true),_localNor(localNor),_nor(nor),_coef(coef) {
  if(inherited)
    return;
  for(int i=0; i<(int)DOFs.size(); i++) {
    T l=limit?body.lowerLimit()[DOFs[i]]:-SQPObjective<T>::infty();
    T u=limit?body.upperLimit()[DOFs[i]]: SQPObjective<T>::infty();
    const SQPVariable<T>& var=obj.addVar("DOF"+std::to_string(DOFs[i]),l,u);
    if(init)
      obj.addVar(var._id)._init=init->coeff(i);
    _DOFToVid[DOFs[i]]=var;
  }
  for(int i=0; i<body.nrJ(); i++) {
    const Joint& J=body.joint(i);
    if(DOFs[0]>=J._offDOF && DOFs[0]<J._offDOF+J.nrDOF())
      _JID=i;
  }
  for(int i=0; i<3; i++) {
    _gl.push_back(pos[i]);
    _gu.push_back(pos[i]);
  }
  if(_coef<=0)
    for(int i=0; i<3; i++) {
      _gl.push_back(nor[i]);
      _gu.push_back(nor[i]);
    }
}
template <typename T>
int InverseKinematicsPosNorObj<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  Vec DOF=Vec::Zero(_body.nrDOF());
  for(const auto& p:_DOFToVid)
    DOF[p.first]=x[p.second._id];
  //compute variable
  _info.reset(_body,DOF);
  fvec.template segment<3>(_offset+0)=ROTI(_info._TM,_JID)*_localPos+CTRI(_info._TM,_JID);
  if(_coef<=0)
    fvec.template segment<3>(_offset+3)=ROTI(_info._TM,_JID)*_localNor;
  if(fjac)
    for(int r=0; r<3; r++) {
      Mat3X4T GK=Mat3X4T::Zero();
      //pos
      GK(r,3)=1;
      GK.template block<1,3>(r,0)=_localPos.transpose();
      _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
        typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
        if(it!=_DOFToVid.end())
          fjac->push_back(STrip(_offset+0+r,it->second._id,val));
      });
      //nor
      if(_coef<=0) {
        GK(r,3)=0;
        GK.template block<1,3>(r,0)=_localNor.transpose();
        _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
          typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
          if(it!=_DOFToVid.end())
            fjac->push_back(STrip(_offset+3+r,it->second._id,val));
        });
      }
    }
  return 0;
}
template <typename T>
T InverseKinematicsPosNorObj<T>::operator()(const Vec& x,Vec* fgrad) {
  T E=0;
  for(const auto& p:_DOFToVid) {
    T diff=x[p.second._id]-p.second._init;
    if(fgrad)
      fgrad->coeffRef(p.second._id)+=diff;
    E+=diff*diff;
  }
  if(_coef>0) {
    Vec DOF=Vec::Zero(_body.nrDOF());
    for(const auto& p:_DOFToVid)
      DOF[p.first]=x[p.second._id];
    _info.reset(_body,DOF);
    Vec3T N=ROTI(_info._TM,_JID)*_localNor-_nor;
    E+=N.squaredNorm();
    if(fgrad) {
      Mat3X4T GK=Mat3X4T::Zero();
      ROT(GK)=N*_localNor.transpose();
      _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
        typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
        if(it!=_DOFToVid.end())
          fgrad->coeffRef(it->second._id)+=val;
      });
    }
  }
  return E/2;
}
//pos/frame
template <typename T>
InverseKinematicsPosFrameObj<T>::InverseKinematicsPosFrameObj
(SQPObjectiveCompound<T>& obj,const std::string& name,const ArticulatedBody& body,const Vec3T& pos,const Mat3T& frame,
 const Vec3T& localPos,const Mat3T& localFrame,const std::vector<int>& DOFs,T coef,const Vec* init,bool limit,bool inherited)
  :InverseKinematicsPosObj<T>(obj,inherited?name:name+"InverseKinematicsPosFrameObj"+std::string(coef<=0?"Hard":"Soft"),body,pos,localPos,DOFs,init,limit,true),_localFrame(localFrame),_frame(frame),_coef(coef) {
  if(inherited)
    return;
  for(int i=0; i<(int)DOFs.size(); i++) {
    T l=limit?body.lowerLimit()[DOFs[i]]:-SQPObjective<T>::infty();
    T u=limit?body.upperLimit()[DOFs[i]]: SQPObjective<T>::infty();
    const SQPVariable<T>& var=obj.addVar("DOF"+std::to_string(DOFs[i]),l,u);
    if(init)
      obj.addVar(var._id)._init=init->coeff(i);
    _DOFToVid[DOFs[i]]=var;
  }
  for(int i=0; i<body.nrJ(); i++) {
    const Joint& J=body.joint(i);
    if(DOFs[0]>=J._offDOF && DOFs[0]<J._offDOF+J.nrDOF())
      _JID=i;
  }
  for(int i=0; i<3; i++) {
    _gl.push_back(pos[i]);
    _gu.push_back(pos[i]);
  }
  if(_coef<=0)
    for(int c=0; c<3; c++)
      for(int i=0; i<3; i++) {
        _gl.push_back(frame(i,c));
        _gu.push_back(frame(i,c));
      }
}
template <typename T>
int InverseKinematicsPosFrameObj<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  Vec DOF=Vec::Zero(_body.nrDOF());
  for(const auto& p:_DOFToVid)
    DOF[p.first]=x[p.second._id];
  //compute variable
  _info.reset(_body,DOF);
  fvec.template segment<3>(_offset+0)=ROTI(_info._TM,_JID)*_localPos+CTRI(_info._TM,_JID);
  if(_coef<=0)
    for(int c=0; c<3; c++)
      fvec.template segment<3>(_offset+3+3*c)=ROTI(_info._TM,_JID)*_localFrame.col(c);
  if(fjac) {
    for(int r=0; r<3; r++) {
      Mat3X4T GK=Mat3X4T::Zero();
      //pos
      GK(r,3)=1;
      GK.template block<1,3>(r,0)=_localPos.transpose();
      _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
        typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
        if(it!=_DOFToVid.end())
          fjac->push_back(STrip(_offset+0+r,it->second._id,val));
      });
    }
    if(_coef<=0)
      for(int c=0; c<3; c++)
        for(int r=0; r<3; r++) {
          Mat3X4T GK=Mat3X4T::Zero();
          //nor
          GK(r,3)=0;
          GK.template block<1,3>(r,0)=_localFrame.col(c).transpose();
          _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
            typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
            if(it!=_DOFToVid.end())
              fjac->push_back(STrip(_offset+3+3*c+r,it->second._id,val));
          });
        }
  }
  return 0;
}
template <typename T>
T InverseKinematicsPosFrameObj<T>::operator()(const Vec& x,Vec* fgrad) {
  T E=0;
  for(const auto& p:_DOFToVid) {
    T diff=x[p.second._id]-p.second._init;
    if(fgrad)
      fgrad->coeffRef(p.second._id)+=diff;
    E+=diff*diff;
  }
  if(_coef>0) {
    Vec DOF=Vec::Zero(_body.nrDOF());
    for(const auto& p:_DOFToVid)
      DOF[p.first]=x[p.second._id];
    _info.reset(_body,DOF);
    Mat3T F=ROTI(_info._TM,_JID)*_localFrame-_frame;
    E+=F.squaredNorm();
    if(fgrad) {
      Mat3X4T GK=Mat3X4T::Zero();
      ROT(GK)=F*_localFrame.transpose();
      _info.DTG(_JID,_body,GK,[&](int DOF,T val) {
        typename std::unordered_map<int,SQPVariable<T>>::const_iterator it=_DOFToVid.find(DOF);
        if(it!=_DOFToVid.end())
          fgrad->coeffRef(it->second._id)+=val;
      });
    }
  }
  return E/2;
}
template class InverseKinematicsPosObj<FLOAT>;
template class InverseKinematicsPosNorObj<FLOAT>;
template class InverseKinematicsPosFrameObj<FLOAT>;
#ifdef FORCE_ADD_DOUBLE_PRECISION
template struct InverseKinematicsPosObj<double>;
template struct InverseKinematicsPosNorObj<double>;
template struct InverseKinematicsPosFrameObj<double>;
#endif
}
