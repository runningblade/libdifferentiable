#include "PBCDMDynamicsSequenceSpline.h"
#include <Utils/CrossSpatialUtils.h>
#include <Utils/DebugGradient.h>
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
template <typename T>
PBCDMDynamicsSequenceSpline<T>::PBCDMDynamicsSequenceSpline
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,
 std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodySimplified,const Vec3T& G,
 std::shared_ptr<PhaseSequence<T>> phase,
 const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
 const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,bool inherited)
  :PBCDMDynamicsSequence<T>(obj,inherited?name:name+"PBCDMDynamicsSequenceSpline",dt,Vec2T::Zero(),body,bodySimplified,G,poses,forces,ees,0,true) {
  if(inherited)
    return;
  ASSERT_MSG(_bodySimplified->nrDOF()==6,"PBCDMDynamicsSequenceSpline can only handle 6-DOF torso!")
  //joints
  _joints.setConstant(1,_bodySimplified->nrJ()-1);
  //create DOF spline
  _DOFSeq.reset(new VectorSequence<T,6>(obj,"NESimplifiedDOF",2,phase));
  _DOFSeq->initConstant(Vec6T::Zero(),obj);
  //discrete test points
  int nrStep=round((double)phase->horizonRef()/(double)_dt)+1;
  _DOFVars.setConstant(6,nrStep,-1);
  //interpolation stencil
  _rangeX[0]=std::min(_DOFSeq->getVar().minCoeff(),_DOFSeq->getDiffVar().minCoeff());
  _rangeX[1]=_DOFSeq->getVar().size()+_DOFSeq->getDiffVar().size();
  STrips tripsQ;
  for(int c=0; c<nrStep; c++) {
    GRAD_INTERP dofGrads[6];
    _DOFSeq->timeInterpolateStencil(obj.init(),c*_dt,dofGrads,NULL);
    for(int dof=0; dof<6; dof++)
      for(const std::pair<const int,T>& v:dofGrads[dof])
        tripsQ.push_back(STrip(v.first-_rangeX[0],c*6+dof,v.second));
  }
  _qInterp.resize(_rangeX[1],nrStep*6);
  _qInterp.setFromTriplets(tripsQ.begin(),tripsQ.end());
  //position, dynamics constraints
  //(_frame.transpose()*(x+C-_ctrBB))[d] \in [minC,maxC]*_res
  //(_frame.transpose()*x)[d] \in [minC,maxC]*_res+(_frame.transpose()*(_ctrBB-C))[d]
  _infosPBCDM.resize(nrStep);
  for(int c=0; c<nrStep; c++)
    _infosPBCDM[c].init(*_bodySimplified,_ees.size(),G,_dt);
  for(int c=_posConsStart; c<nrStep; c++)
    for(int f=0; f<(int)_ees.size(); f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      EndEffectorBounds::Vec3T frameTCtr=ee->_frame.transpose()*ee->_ctrBB;
      for(int r=0; r<3; r++) {
        _gl.push_back(ee->_bb[r+0]*ee->_res+frameTCtr[r]);
        _gu.push_back(ee->_bb[r+3]*ee->_res+frameTCtr[r]);
      }
    }
  for(int c=0; c<_rangeX[1]; c++) {
    _gl.push_back(0);
    _gu.push_back(0);
  }
}
template <typename T>
int PBCDMDynamicsSequenceSpline<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  //position, dynamics constraint
  STrips DJac;
  Vec DCon=Vec::Zero((int)_infosPBCDM.size()*6);
  Vec q=_qInterp.transpose()*x.segment(_rangeX[0],_rangeX[1]);
  OMP_PARALLEL_FOR_
  for(int c=0; c<(int)_infosPBCDM.size(); c++)
    _infosPBCDM[c].reset(*_bodySimplified,q.template segment<6>(c*6));
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<(int)_infosPBCDM.size(); c++) {
    T time=c*_dt;
    std::vector<GRAD_INTERP> posGrads(_ees.size()*3),forceGrads(_ees.size()*3);
    Mat3X4T TJ=_infosPBCDM[c].getTrans(),GK;
    //position constraint
    Vec3T force,pos;
    int offset=_offset+(c-_posConsStart)*(int)_ees.size()*3;
    for(int f=0; f<(int)_ees.size(); f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      //position part
      pos=_poses[f]->timeInterpolateStencil(x,time,fjac?&posGrads[f*3]:NULL,NULL);
      Vec3T relPos=pos-CTR(TJ);
      Mat3T ROTFrame=ROT(TJ)*ee->_frame.template cast<T>();
      fvec.template segment<3>(offset).setZero()=ROTFrame.transpose()*relPos;
      if(fjac)
        for(int r=0; r<3; r++) {
          for(const std::pair<const int,T>& v:posGrads[f*3+r])
            addBlock(*fjac,offset,v.first,ROTFrame.row(r).transpose()*v.second);
          CTR(GK)=-ROTFrame.col(r);
          ROT(GK)=relPos*ee->_frame.col(r).template cast<T>().transpose();
          _infosPBCDM[c].DTG(GK,[&](int dof,T val) {
            for(typename SMatT::InnerIterator it(_qInterp,c*6+dof); it; ++it)
              fjac->push_back(STrip(offset+r,it.row()+_rangeX[0],it.value()*val));
          });
        }
      offset+=3;
      //dynamics part
      force=_forces[f]->timeInterpolateStencil(x,time,fjac?&forceGrads[f*3]:NULL,NULL);
      _infosPBCDM[c].setContactForce(f,pos,force);
    }
    if(c<2)
      continue;
    //set FD
    offset=c*6;
    if(!fjac) {
      DCon.template segment<6>(offset)=_infosPBCDM[c].C(_infosPBCDM[c-2],_infosPBCDM[c-1]);
    } else {
      DCon.template segment<6>(offset)=_infosPBCDM[c].C(_infosPBCDM[c-2],_infosPBCDM[c-1],
      [&](int row,int col,Vec3T blk) {//DCDINN
        for(int d=0; d<3; d++)
          for(typename SMatT::InnerIterator it(_qInterp,offset-12+col); it; ++it)
            DJac.push_back(STrip(offset+row+d,it.row()+_rangeX[0],blk[d]*it.value()));
      },[&](int row,int col,Vec3T blk) {//DCDIN
        for(int d=0; d<3; d++)
          for(typename SMatT::InnerIterator it(_qInterp,offset-6+col); it; ++it)
            DJac.push_back(STrip(offset+row+d,it.row()+_rangeX[0],blk[d]*it.value()));
      },[&](int row,int col,Vec3T blk) {//DCDI
        for(int d=0; d<3; d++)
          for(typename SMatT::InnerIterator it(_qInterp,offset+col); it; ++it)
            DJac.push_back(STrip(offset+row+d,it.row()+_rangeX[0],blk[d]*it.value()));
      },[&](int row,int col,Mat3T blk) {//DCDX
        for(int d=0; d<3; d++)
          for(int d2=0; d2<3; d2++)
            for(const std::pair<const int,T>& v:posGrads[col+d2])
              DJac.push_back(STrip(offset+row+d,v.first,blk(d,d2)*v.second));
      },[&](int row,int col,Mat3T blk) {//DCDF
        for(int d=0; d<3; d++)
          for(int d2=0; d2<3; d2++)
            for(const std::pair<const int,T>& v:forceGrads[col+d2])
              DJac.push_back(STrip(offset+row+d,v.first,blk(d,d2)*v.second));
      });
    }
  }
  //Galerkin projection
  int offset=_offset+((int)_infosPBCDM.size()-_posConsStart)*(int)_ees.size()*3;
  fvec.segment(offset,_rangeX[1])=_qInterp*DCon;
  if(fjac) {
    SMatT DJacM;
    DJacM.resize((int)_infosPBCDM.size()*6,x.size());
    DJacM.setFromTriplets(DJac.begin(),DJac.end());
    addBlock(*fjac,offset,0,SMatT(_qInterp*DJacM));
  }
  return 0;
}
template <typename T>
bool PBCDMDynamicsSequenceSpline<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  return SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef);
}
template <typename T>
const typename PBCDMDynamicsSequenceSpline<T>::Vec& PBCDMDynamicsSequenceSpline<T>::controlCoef() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlCoef();
}
template <typename T>
typename PBCDMDynamicsSequenceSpline<T>::Vec PBCDMDynamicsSequenceSpline<T>::residualToControl() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::residualToControl();
}
template <typename T>
typename PBCDMDynamicsSequenceSpline<T>::Vec PBCDMDynamicsSequenceSpline<T>::controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(keepFunc,consSet);
}
template <typename T>
typename PBCDMDynamicsSequenceSpline<T>::SMatT PBCDMDynamicsSequenceSpline<T>::controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(keepFunc,consSet);
}
template <typename T>
typename PBCDMDynamicsSequenceSpline<T>::Vec PBCDMDynamicsSequenceSpline<T>::controlledDOFs(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(constrained,consSet);
}
template <typename T>
typename PBCDMDynamicsSequenceSpline<T>::SMatT PBCDMDynamicsSequenceSpline<T>::controlledDOFsProj(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(constrained,consSet);
}
template <typename T>
const Eigen::Matrix<int,-1,-1>& PBCDMDynamicsSequenceSpline<T>::DOFVar() const {
  return _DOFSeq->getVar();
}
template <typename T>
typename PBCDMDynamicsSequenceSpline<T>::MatT PBCDMDynamicsSequenceSpline<T>::getPoses(const Vec& x) {
  MatT ret;
  ret.resize(_body->nrDOF(),_DOFVars.cols());
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    T time=c*_dt;
    //base
    Vec DOF=Vec::Zero(_body->nrDOF());
    PBCentroidBodyDynamicsGradientInfo<T>& info=_infosPBCDM[c];
    info.reset(*_bodySimplified,_DOFSeq->timeInterpolateStencil(x,time,NULL,NULL));
    DOF.template segment<6>(0)=_infosPBCDM[c].getDOF();
    Mat3X4T TJ=info.getTrans();
    //xss
    for(int f=0; f<(int)_ees.size(); f++) {
      Vec3T pos=_poses[f]->timeInterpolateStencil(x,time,NULL,NULL);
      Vec3T nor=_poses[f]->getEnv()->phiGrad(pos);
      pos=ROT(TJ).transpose()*(pos-CTR(TJ));
      nor=ROT(TJ).transpose()*nor;
      DOF+=_ees[f]->getDOFAll(*_body,pos.template cast<EndEffectorBounds::T>(),nor.template cast<EndEffectorBounds::T>()).template cast<T>();
    }
    ret.col(c)=DOF;
  }
  return ret;
}
template <typename T>
std::shared_ptr<VectorSequence<T,6>> PBCDMDynamicsSequenceSpline<T>::DOFSeq() const {
  return _DOFSeq;
}
template class PBCDMDynamicsSequenceSpline<FLOAT>;
}
