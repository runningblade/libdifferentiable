#ifndef LIP_DYNAMICS_SEQUENCE_H
#define LIP_DYNAMICS_SEQUENCE_H

#include "SQPObjective.h"
#include "FootStepSequence.h"
#include <Articulated/EndEffectorInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class LIPDynamicsSequence : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  LIPDynamicsSequence(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec3T& G,T h0,
                      std::shared_ptr<ArticulatedBody> bodySimplified,
                      const std::vector<std::shared_ptr<FootStepSequence<T>>>& footSteps,bool isBoundingCircle,
                      const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int NCToFix=2,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual std::string DOFVar(int r,int stage) const;
  virtual std::string lambdaVar(int r,int stage) const;
  virtual Eigen::Matrix<int,-1,-1> DOFVarXY() const;
  virtual Eigen::Matrix<int,-1,-1> DOFDiffVarXY() const;
  virtual Eigen::Matrix<int,-1,-1> DOFVarZT() const;
  virtual Eigen::Matrix<int,-1,-1> DOFDiffVarZT() const;
  virtual void initSameLambda(SQPObjectiveCompound<T>& obj);
  virtual void fixInitialPose(SQPObjectiveCompound<T>& obj,bool fixVelocity);
  virtual void initDOF(SQPObjectiveCompound<T>& obj,int doubleHorizon,int singleHorizon,const std::vector<FootStep<T>>& steps);
  virtual MatT getPoses(const ArticulatedBody& body,const Environment<T>& env,const Vec& x);
  virtual void setTargetHeightEnergy(SQPObjectiveCompound<T>& obj,T hCoef,T coefLambdaEnergy);
 protected:
  std::shared_ptr<ArticulatedBody> _bodySimplified;
  std::vector<std::shared_ptr<FootStepSequence<T>>> _footSteps;
  std::vector<std::shared_ptr<EndEffectorBounds>> _ees;
  //bounding box or circle
  bool _isBoundingCircle;
  //variables
  int _posConsStart;
  T _w,_h0,_dt;
  Eigen::Matrix<int,-1,-1> _DOFVars;
  Eigen::Matrix<int,-1,-1> _lambdas;
};
}

#endif
