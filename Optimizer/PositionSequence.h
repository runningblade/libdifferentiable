#ifndef POSITION_SEQUENCE_H
#define POSITION_SEQUENCE_H

#include "VectorSequence.h"
#include <Environment/Environment.h>

namespace PHYSICSMOTION {
template <typename T>
class DeformedEnvironment;
template <typename T>
class PositionSequence : public VectorSequence<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
#ifndef SWIG
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using typename VectorSequence<T>::GRAD_INTERP;
  using typename VectorSequence<T>::VecDT;
  using VectorSequence<T>::_name;
  using VectorSequence<T>::_gl;
  using VectorSequence<T>::_gu;
  using VectorSequence<T>::_offset;
  using VectorSequence<T>::_even;
  using VectorSequence<T>::_var;
  using VectorSequence<T>::_diffVar;
  using VectorSequence<T>::_phase;
  using VectorSequence<T>::debugGradientConditional;
  using VectorSequence<T>::debugGradientConditionalVID;
  //using VectorSequence<T>::timeInterpolateStencil;
  //using VectorSequence<T>::diffInterpolateStencil;
  //using VectorSequence<T>::ddiffInterpolateStencil;
  using VectorSequence<T>::diffVar;
  using VectorSequence<T>::var;
  using VectorSequence<T>::removeNonZero;
  using VectorSequence<T>::initConstant;
#endif
 public:
  PositionSequence(SQPObjectiveCompound<T>& obj,const std::string& name,
                   int positionAboveGroundStencilSize,T phi0,bool even,
                   std::shared_ptr<PhaseSequence<T>> phase,
                   std::shared_ptr<Environment<T>> env,bool inherited=false);
  void setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv=NULL);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual VecDT timeInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan=true) const override;
  virtual VecDT diffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan=true) const override;
  virtual VecDT ddiffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan=true) const override;
  virtual void initConstant(const VecDT& pos,SQPObjectiveCompound<T>& obj) override;
  virtual void initLineSegment(const VecDT& a,const VecDT& b,SQPObjectiveCompound<T>& obj,int N=0) override;
  virtual void initLineSegments(const std::vector<std::pair<VecDT,VecDT>>& segments,SQPObjectiveCompound<T>& obj) override;
  virtual void initRegularInterval(const MatT& DOFs,T interval,SQPObjectiveCompound<T>& obj,bool fixInitVar,bool fixInitDiffVar) override;
  std::vector<Vec3T> getPositionAboveGroundStencil(const Vec& x) const;
  std::vector<Vec3T> getPositionOnGroundStencil(const Vec& x) const;
  std::shared_ptr<Environment<T>> getEnv() const;
  T getPhi0() const;
 protected:
  const T _phi0;
  std::shared_ptr<Environment<T>> _env;
  std::vector<GRAD_INTERP> _positionAboveGroundStencil;
  std::shared_ptr<DeformedEnvironment<T>> _DEnv;
};
}

#endif
