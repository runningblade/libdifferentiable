﻿#include "NEDynamicsSequence.h"
#include <Utils/CrossSpatialUtils.h>
#include <Utils/DebugGradient.h>
#include <Utils/SparseUtils.h>

namespace PHYSICSMOTION {
//DynamicsSequence
template <typename T>
NEDynamicsSequence<T>::NEDynamicsSequence
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
 std::shared_ptr<ArticulatedBody> body,const Vec3T& G,
 const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
 const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart,bool inherited)
  :PBDDynamicsSequence<T>(obj,inherited?name:name+"NEDynamicsSequence",dt,period,body,G,poses,forces,ees,posConsStart,true) {
  if(inherited)
    return;
  int offPeriod=(int)round(period[0]/_dt),nrPeriod=(int)round(period[1]/_dt)+1,nrStep=3;
  if(!poses.empty() && poses[0]->getPhase())
    nrStep=round((double)poses[0]->getPhase()->horizonRef()/(double)_dt)+1;
  Vec l=_body->lowerLimit(SQPObjective<typename ArticulatedBody::T>::infty()).template cast<T>();
  Vec u=_body->upperLimit(SQPObjective<typename ArticulatedBody::T>::infty()).template cast<T>();
  _DOFVars.resize(body->nrDOF(),nrStep);
  for(int jid=0; jid<body->nrJ(); jid++) {
    const Joint& J=body->joint(jid);
    for(int r=J._offDOF; r<J._offDOF+J.nrDOF(); r++)
      if(J.isRoot(*_body))
        for(int c=0; c<nrStep; c++)
          _DOFVars(r,c)=obj.addVar(DOFVar(r,c),l[r],u[r],SQPObjectiveCompound<T>::MUST_NEW)._id;
      else
        for(int c=0; c<nrStep; c++)
          _DOFVars(r,c)=obj.addVar(DOFVar(r,c<offPeriod?c:offPeriod+((c-offPeriod)%nrPeriod)),l[r],u[r],c<offPeriod+nrPeriod?SQPObjectiveCompound<T>::MUST_NEW:SQPObjectiveCompound<T>::MUST_EXIST)._id;
  }
  //G
  _a0=concat<Vec>(Vec3T::Zero(),-G);
  //joints
  _joints.resize((int)forces.size());
  for(int i=0; i<(int)forces.size(); i++)
    _joints[i]=ees[i]->jointId();
  //position constraints
  _C=_body->control().template cast<T>();
  _proj=controlledDOFsProj(true,NULL);
  for(int c=posConsStart; c<_DOFVars.cols(); c++)
    for(int f=0; f<(int)_forces.size(); f++)
      for(int r=0; r<3; r++) {
        _gl.push_back(0);
        _gu.push_back(0);
      }
  //dynamics constraints
  for(int c=2; c<_DOFVars.cols(); c++)
    for(int r=0; r<_proj.rows(); r++) {
      _gl.push_back(0);
      _gu.push_back(0);
    }
  _infosNE.resize(nrStep);
}
template <typename T>
int NEDynamicsSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    _infosNE[c].reset(*_body,xc);
  }
  //position constraints
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<_DOFVars.cols(); c++)
    for(int f=0; f<(int)_ees.size(); f++) {
      GRAD_INTERP posGrads[3];
      const std::shared_ptr<EndEffectorBounds>& ef=_ees[f];
      int offset=_offset+((c-_posConsStart)*(int)_ees.size()+f)*3;
      Mat3X4T TJ=_infosNE[c].NEArticulatedGradientInfoMap<T>::getTrans(ef->jointId());
      Vec3T pos=ROT(TJ)*ef->_localPos.template cast<T>()+CTR(TJ);
      Vec3T posRef=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?posGrads:NULL,NULL);
      fvec.template segment<3>(offset)=pos-posRef;
      if(fjac)
        for(int r=0; r<3; r++) {
          Mat3X4T GK=Mat3X4T::Zero();
          GK(r,3)=1;
          GK.template block<1,3>(r,0)=ef->_localPos.transpose().template cast<T>();
          _infosNE[c].NEArticulatedGradientInfoMap<T>::DTG(ef->jointId(),*_body,GK,[&](int DOF,T val) {
            fjac->push_back(STrip(offset+r,_DOFVars(DOF,c),val));
          });
          for(const std::pair<const int,T>& v:posGrads[r])
            fjac->push_back(STrip(offset+r,v.first,-v.second));
        }
    }
  //dynamics constraints
  OMP_PARALLEL_FOR_
  for(int c=2; c<_DOFVars.cols(); c++) {
    //set forces
    MatT Sc,ScCross;
    std::vector<GRAD_INTERP> forceGrads((int)_ees.size()*3);
    std::vector<GRAD_INTERP> posGrads((int)_ees.size()*3);
    Mat6XT forces=Mat6XT::Zero(6,_body->nrJ());
    Mat3XT poses=Mat3XT::Zero(3,(int)_ees.size());
    for(int f=0; f<(int)_ees.size(); f++) {
      Eigen::Block<Mat6XT,6,1> forceI=forces.template block<6,1>(0,_ees[f]->jointId());
      forceI.template segment<3>(3)=_forces[f]->timeInterpolateStencil(x,c*_dt,fjac?&forceGrads[f*3]:NULL,NULL);
      poses.col(f)=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?&posGrads[f*3]:NULL,NULL);
      forceI.template segment<3>(0)=poses.col(f).cross(forceI.template segment<3>(3));
    }
    //set FD
    _infosNE[c]._dqM=(_infosNE[c]._qM-_infosNE[c-1]._qM)/_dt;
    _infosNE[c]._ddqM=(_infosNE[c]._qM-2*_infosNE[c-1]._qM+_infosNE[c-2]._qM)/(_dt*_dt);
    if(fjac)
      _infosNE[c].RNEADerivatives(*_body,mapCV(_a0),mapCM(forces));
    else _infosNE[c].RNEA(*_body,mapCV(_a0),mapCM(forces));
    //fill
    int offset=_offset+(_DOFVars.cols()-_posConsStart)*(int)_ees.size()*3+(c-2)*_proj.rows();
    fvec.segment(offset,_proj.rows())=_proj*_infosNE[c]._tauM;
    if(fjac) {
      _infosNE[c].calcH(*_body);
      MatT tmp=_proj*(_infosNE[c]._DtauDqM+_infosNE[c]._DtauDdqM/_dt+_infosNE[c]._HM/(_dt*_dt));
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offset,_DOFVars(d,c  ),tmp.col(d));
      tmp=_proj*(-_infosNE[c]._DtauDdqM/_dt-2*_infosNE[c]._HM/(_dt*_dt));
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offset,_DOFVars(d,c-1),tmp.col(d));
      tmp=_proj*_infosNE[c]._HM/(_dt*_dt);
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offset,_DOFVars(d,c-2),tmp.col(d));
      //Sc
      Sc.resize((int)_ees.size()*6,_infosNE[c]._qM.size());
      _infosNE[c].Sc(*_body,mapCV(_joints),mapM(Sc));
      Sc=(-_proj*Sc.transpose()).eval();
      for(int f=0; f<(int)_ees.size(); f++) {
        //fjac
        ScCross=Sc.block(0,f*6,_proj.rows(),3)*cross<T>(poses.col(f));
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:forceGrads[f*3+r])
            addBlock(*fjac,offset,v.first,(ScCross.col(r)+Sc.col(f*6+3+r))*v.second);
        //pjac
        Eigen::Block<Mat6XT,6,1> forceI=forces.template block<6,1>(0,_ees[f]->jointId());
        ScCross=Sc.block(0,f*6,_proj.rows(),3)*cross<T>(-forceI.template segment<3>(3));
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:posGrads[f*3+r])
            addBlock(*fjac,offset,v.first,ScCross.col(r)*v.second);
      }
    }
  }
  return 0;
}
template <typename T>
bool NEDynamicsSequence<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  return SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef);
}
template <typename T>
void NEDynamicsSequence<T>::resetInfo(int id,const Vec& x) {
  Vec xId=Vec::Zero(_DOFVars.rows());
  for(int i=0; i<xId.size(); i++)
    xId[i]=x[_DOFVars(i,id)];
  if(_infosNE[id]._qM.size()!=xId.size() || _infosNE[id]._qM!=xId)
    _infosNE[id].reset(*_body,xId);
}
template <typename T>
typename NEDynamicsSequence<T>::Mat3X4T NEDynamicsSequence<T>::getJointTrans(int id,int JID) const {
  return fromSpatial<T>(spatialInv<T>(TRANSI6(_infosNE[id]._invTM,JID)));
}
template <typename T>
void NEDynamicsSequence<T>::DTG(int id,int JID,Mat3X4T GK,std::function<void(int,T)> DTG) const {
  _infosNE[id].DTG(JID,*_body,GK,DTG);
}
template class NEDynamicsSequence<FLOAT>;
}
