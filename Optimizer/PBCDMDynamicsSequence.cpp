#include "PBCDMDynamicsSequence.h"
#include <Utils/CrossSpatialUtils.h>
#include <Utils/DebugGradient.h>

namespace PHYSICSMOTION {
template <typename T>
PBCDMDynamicsSequence<T>::PBCDMDynamicsSequence
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
 std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodySimplified,const Vec3T& G,
 const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
 const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart,bool inherited)
  :NEDynamicsSequence<T>(obj,inherited?name:name+"PBCDMDynamicsSequence",dt,period,body,G,poses,forces,ees,posConsStart,true),_bodySimplified(bodySimplified) {
  if(inherited)
    return;
  int offPeriod=(int)round(period[0]/_dt),nrPeriod=(int)round(period[1]/_dt)+1,nrStep=3;
  if(!poses.empty() && poses[0]->getPhase())
    nrStep=round((double)poses[0]->getPhase()->horizonRef()/(double)_dt)+1;
  ASSERT_MSG(_bodySimplified->nrDOF()==6,"PBCDMDynamicsSequence must have 6 degrees of freedom!")
  //joints
  _joints.setConstant(1,_bodySimplified->nrJ()-1);
  //base
  _DOFVars.resize(6,nrStep);
  for(int r=0; r<6; r++)
    for(int c=0; c<nrStep; c++) {
      T l=-SQPObjective<T>::infty();
      T u=SQPObjective<T>::infty();
      if(r<3)
        _DOFVars(r,c)=obj.addVar(DOFVar(r,c),l,u,SQPObjectiveCompound<T>::MUST_NEW)._id;
      else
        _DOFVars(r,c)=obj.addVar(DOFVar(r,c<offPeriod?c:offPeriod+((c-offPeriod)%nrPeriod)),l,u,c<offPeriod+nrPeriod?SQPObjectiveCompound<T>::MUST_NEW:SQPObjectiveCompound<T>::MUST_EXIST)._id;
    }
  //position constraints
  //(_frame.transpose()*(x-_ctrBB))[d] \in [minC,maxC]*_res
  //(_frame.transpose()*x)[d] \in [minC,maxC]*_res+(_frame.transpose()*_ctrBB)[d]
  for(int c=posConsStart; c<nrStep; c++)
    for(int f=0; f<(int)_ees.size(); f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      EndEffectorBounds::Vec3T frameTCtr=ee->_frame.transpose()*ee->_ctrBB;
      for(int r=0; r<3; r++) {
        _gl.push_back(ee->_bb[r+0]*ee->_res+frameTCtr[r]);
        _gu.push_back(ee->_bb[r+3]*ee->_res+frameTCtr[r]);
      }
    }
  //dynamics constraints
  _infosPBCDM.resize(nrStep);
  for(int c=0; c<nrStep; c++)
    _infosPBCDM[c].init(*_bodySimplified,_ees.size(),G,_dt);
  for(int c=2; c<nrStep; c++)
    for(int r=0; r<6; r++) {
      _gl.push_back(0);
      _gu.push_back(0);
    }
}
template <typename T>
void PBCDMDynamicsSequence<T>::setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv) {
  _DEnv=DEnv;
  for(auto& infoPBCDM:_infosPBCDM)
    infoPBCDM.setDeformedEnvironment(_DEnv);
}
template <typename T>
std::shared_ptr<DeformedEnvironment<T>> PBCDMDynamicsSequence<T>::getDeformedEnvironment() const {
  return _DEnv;
}
template <typename T>
int PBCDMDynamicsSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    _infosPBCDM[c].reset(*_bodySimplified,xc);
  }
  //position constraints
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<(int)_infosPBCDM.size(); c++) {
    GRAD_INTERP posGrads[3];
    Mat3X4T TJ=_infosPBCDM[c].getTrans(),GK;
    for(int f=0; f<(int)_ees.size(); f++) {
      int offset=_offset+((c-_posConsStart)*(int)_ees.size()+f)*3;
      Vec3T posRef=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?posGrads:NULL,NULL);
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      //relative pos constraint
      Vec3T relPos=posRef-CTR(TJ);
      Mat3T ROTFrame=ROT(TJ)*ee->_frame.template cast<T>();
      fvec.template segment<3>(offset)=ROTFrame.transpose()*relPos;
      if(fjac)
        for(int r=0; r<3; r++) {
          for(const std::pair<const int,T>& v:posGrads[r])
            addBlock(*fjac,offset,v.first,ROTFrame.row(r).transpose()*v.second);
          CTR(GK)=-ROTFrame.col(r);
          ROT(GK)=relPos*ee->_frame.col(r).template cast<T>().transpose();
          _infosPBCDM[c].DTG(GK,[&](int dof,T val) {
            fjac->push_back(STrip(offset+r,_DOFVars(dof,c),val));
          });
        }
    }
  }
  //dynamics constraints
  int offset=_offset+((int)_infosPBCDM.size()-_posConsStart)*(int)_ees.size()*3;
  OMP_PARALLEL_FOR_
  for(int c=2; c<_DOFVars.cols(); c++) {
    //set forces
    std::vector<GRAD_INTERP> forceGrads((int)_ees.size()*3);
    std::vector<GRAD_INTERP> posGrads((int)_ees.size()*3);
    Vec3T force,pos;
    for(int f=0; f<(int)_ees.size(); f++) {
      force=_forces[f]->timeInterpolateStencil(x,c*_dt,fjac?&forceGrads[f*3]:NULL,NULL);
      pos=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?&posGrads[f*3]:NULL,NULL);
      _infosPBCDM[c].setContactForce(f,pos,force);
    }
    //set FD
    int offsetc=offset+(c-2)*6;
    if(!fjac)
      fvec.segment(offsetc,6)=_infosPBCDM[c].C(_infosPBCDM[c-2],_infosPBCDM[c-1]);
    else {
      fvec.segment(offsetc,6)=_infosPBCDM[c].C(_infosPBCDM[c-2],_infosPBCDM[c-1],
      [&](int row,int col,Vec3T blk) {//DCDINN
        for(int d=0; d<3; d++)
          fjac->push_back(STrip(offsetc+row+d,_DOFVars(col,c-2),blk[d]));
      },[&](int row,int col,Vec3T blk) {//DCDIN
        for(int d=0; d<3; d++)
          fjac->push_back(STrip(offsetc+row+d,_DOFVars(col,c-1),blk[d]));
      },[&](int row,int col,Vec3T blk) {//DCDI
        for(int d=0; d<3; d++)
          fjac->push_back(STrip(offsetc+row+d,_DOFVars(col,c),blk[d]));
      },[&](int row,int col,Mat3T blk) {//DCDX
        for(int d=0; d<3; d++)
          for(int d2=0; d2<3; d2++)
            for(const std::pair<const int,T>& v:posGrads[col+d2])
              fjac->push_back(STrip(offsetc+row+d,v.first,blk(d,d2)*v.second));
      },[&](int row,int col,Mat3T blk) {//DCDF
        for(int d=0; d<3; d++)
          for(int d2=0; d2<3; d2++)
            for(const std::pair<const int,T>& v:forceGrads[col+d2])
              fjac->push_back(STrip(offsetc+row+d,v.first,blk(d,d2)*v.second));
      });
    }
  }
  return 0;
}
template <typename T>
bool PBCDMDynamicsSequence<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  return SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef);
}
template <typename T>
const typename PBCDMDynamicsSequence<T>::Vec& PBCDMDynamicsSequence<T>::controlCoef() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlCoef();
}
template <typename T>
typename PBCDMDynamicsSequence<T>::Vec PBCDMDynamicsSequence<T>::residualToControl() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::residualToControl();
}
template <typename T>
typename PBCDMDynamicsSequence<T>::Vec PBCDMDynamicsSequence<T>::controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(keepFunc,consSet);
}
template <typename T>
typename PBCDMDynamicsSequence<T>::SMatT PBCDMDynamicsSequence<T>::controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(keepFunc,consSet);
}
template <typename T>
typename PBCDMDynamicsSequence<T>::Vec PBCDMDynamicsSequence<T>::controlledDOFs(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(constrained,consSet);
}
template <typename T>
typename PBCDMDynamicsSequence<T>::SMatT PBCDMDynamicsSequence<T>::controlledDOFsProj(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(constrained,consSet);
}
template <typename T>
typename PBCDMDynamicsSequence<T>::MatT PBCDMDynamicsSequence<T>::getPoses(const Vec& x) {
  MatT ret;
  ret.resize(_body->nrDOF(),_DOFVars.cols());
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    _infosPBCDM[c].reset(*_bodySimplified,xc);
  }
  OMP_PARALLEL_FOR_
  for(int c=0; c<(int)_infosPBCDM.size(); c++) {
    //base
    Vec DOF=Vec::Zero(_body->nrDOF());
    DOF.template segment<6>(0)=_infosPBCDM[c].getDOF();
    Mat3X4T TJ=_infosPBCDM[c].getTrans();
    //xss
    for(int f=0; f<(int)_ees.size(); f++) {
      Vec3T pos=_poses[f]->timeInterpolateStencil(x,c*_dt,NULL,NULL);
      Vec3T nor=_poses[f]->getEnv()->phiGrad(pos);
      pos=ROT(TJ).transpose()*(pos-CTR(TJ));
      nor=ROT(TJ).transpose()*nor;
      DOF+=_ees[f]->getDOFAll(*_body,pos.template cast<EndEffectorBounds::T>(),nor.template cast<EndEffectorBounds::T>()).template cast<T>();
    }
    ret.col(c)=DOF;
  }
  return ret;
}
template class PBCDMDynamicsSequence<FLOAT>;
}
