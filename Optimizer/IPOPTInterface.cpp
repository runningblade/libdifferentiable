#ifdef IPOPT_SUPPORT
#include "IPOPTInterface.h"
#include <coin/IpIpoptApplication.hpp>
#include <Utils/Timing.h>
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
template <typename T>
IPOPTInterface<T>::IPOPTInterface(SQPObjective<T>& obj,bool timing)
  :_timing(timing),_obj(obj),_x(obj.init().template cast<double>()) {
  SQPObjectiveCompound<T>* objC=dynamic_cast<SQPObjectiveCompound<T>*>(&obj);
  if(objC)
    objC->assemblePoly();
}
template <typename T>
void IPOPTInterface<T>::set_xlu(const SQPObjective<double>::Vec& xl,const SQPObjective<double>::Vec& xu) {
  if(xl.size()==0)
    _xl=NULL;
  else {
    _xl.reset(new SQPObjective<double>::Vec(xl));
    ASSERT(xl.size()==_obj.inputs())
  }
  if(xu.size()==0)
    _xu=NULL;
  else {
    _xu.reset(new SQPObjective<double>::Vec(xu));
    ASSERT(xu.size()==_obj.inputs())
  }
}
template <typename T>
void IPOPTInterface<T>::set_glu(const SQPObjective<double>::Vec& gl,const SQPObjective<double>::Vec& gu) {
  if(gl.size()==0)
    _gl=NULL;
  else {
    _gl.reset(new SQPObjective<double>::Vec(gl));
    ASSERT(gl.size()==_obj.values())
  }
  if(gu.size()==0)
    _gu=NULL;
  else {
    _gu.reset(new SQPObjective<double>::Vec(gu));
    ASSERT(gu.size()==_obj.values());
  }
}
template <typename T>
bool IPOPTInterface<T>::get_nlp_info(Index& n,Index& m,Index& nnz_jac_g,
                                     Index& nnz_h_lag,IndexStyleEnum& index_style) {
  n=_obj.inputs();
  m=_obj.values();
  updateX(_x);
  nnz_jac_g=_gJacS.nonZeros();
  nnz_h_lag=_fHessS.nonZeros();
  index_style=TNLP::C_STYLE;
  return true;
}
template <typename T>
bool IPOPTInterface<T>::get_bounds_info(Index n,Number* x_l,Number* x_u,
                                        Index m,Number* g_l,Number* g_u) {
  ASSERT(n==_obj.inputs() && m==_obj.values())
  if(x_l)
    Eigen::Map<Eigen::Matrix<Number,-1,1> >(x_l,n)=(_xl?*_xl:SQPObjective<double>::Vec::Constant(n,-getInfty())).template cast<Number>();
  if(x_u)
    Eigen::Map<Eigen::Matrix<Number,-1,1> >(x_u,n)=(_xu?*_xu:SQPObjective<double>::Vec::Constant(n, getInfty())).template cast<Number>();
  if(g_l)
    Eigen::Map<Eigen::Matrix<Number,-1,1> >(g_l,m)=(_gl?*_gl:SQPObjective<double>::Vec::Constant(m,0)).template cast<Number>();
  if(g_u)
    Eigen::Map<Eigen::Matrix<Number,-1,1> >(g_u,m)=(_gu?*_gu:SQPObjective<double>::Vec::Constant(m,0)).template cast<Number>();
  return true;
}
template <typename T>
bool IPOPTInterface<T>::get_starting_point(Index n,bool init_x,Number* x,
    bool init_z,Number* z_L,Number* z_U,
    Index m,bool init_lambda,
    Number* lambda) {
  if(init_x) {
    ASSERT(n==_x.size())
    Eigen::Map<Eigen::Matrix<Number,-1,1>>(x,n)=_x.cast<Number>();
  }
  if(init_z) {
    Eigen::Map<Eigen::Matrix<Number,-1,1>>(z_L,n).setZero();
    Eigen::Map<Eigen::Matrix<Number,-1,1>>(z_U,n).setZero();
  }
  if(init_lambda) {
    Eigen::Map<Eigen::Matrix<Number,-1,1>>(lambda,m).setZero();
  }
  return true;
}
template <typename T>
bool IPOPTInterface<T>::eval_f(Index n,const Number* x,bool new_x,Number& obj_value) {
  if(new_x && x)
    updateX(Eigen::Map<const Eigen::Matrix<Number,-1,1>>(x,n).cast<double>());
  obj_value=_E;
  return true;
}
template <typename T>
bool IPOPTInterface<T>::eval_grad_f(Index n,const Number* x,bool new_x,Number* grad_f) {
  if(new_x && x)
    updateX(Eigen::Map<const Eigen::Matrix<Number,-1,1>>(x,n).cast<double>());
  if(grad_f)
    Eigen::Map<Eigen::Matrix<Number,-1,1>>(grad_f,n)=_fGrad.cast<Number>();
  return true;
}
template <typename T>
bool IPOPTInterface<T>::eval_g(Index n,const Number* x,bool new_x,Index m,Number* g) {
  if(new_x && x)
    updateX(Eigen::Map<const Eigen::Matrix<Number,-1,1>>(x,n).cast<double>());
  if(g)
    Eigen::Map<Eigen::Matrix<Number,-1,1>>(g,m)=_gVec.cast<Number>();
  return true;
}
template <typename T>
bool IPOPTInterface<T>::eval_jac_g(Index n,const Number* x,bool new_x,
                                   Index,Index nele_jac,Index* iRow,Index *jCol,
                                   Number* values) {
  if(new_x && x)
    updateX(Eigen::Map<const Eigen::Matrix<Number,-1,1> >(x,n).cast<double>());
  int nnz=0;
  for(int k=0; k<_gJacS.outerSize(); ++k)
    for(SQPObjective<double>::SMatT::InnerIterator it(_gJacS,k); it; ++it) {
      if(iRow) {
        *iRow=it.row();
        iRow++;
      }
      if(jCol) {
        *jCol=it.col();
        jCol++;
      }
      if(values) {
        *values=it.value();
        values++;
      }
      nnz++;
    }
  ASSERT_MSGV(nnz == nele_jac,"NNZ in hessian(%d) does not match that declared in get_nlp_info(%d)!",nnz,nele_jac)
  return true;
}
template <typename T>
void IPOPTInterface<T>::finalize_solution(SolverReturn,
    Index n,const Number* x,const Number*,const Number*,
    Index,const Number*,const Number*,
    Number,const IpoptData*,
    IpoptCalculatedQuantities*) {
  _x=Eigen::Map<const Eigen::Matrix<Number,-1,1>>(x,n).cast<double>();
}
template <typename T>
void IPOPTInterface<T>::updateInitial() {
  _x=_obj.init().template cast<double>();
}
template <typename T>
bool IPOPTInterface<T>::optimize(SQPObjective<T>& obj,int callback,double derivCheck,double tolG,int maxIter,double muInit,typename SQPObjective<T>::Vec& x) {
  Ipopt::SmartPtr<IPOPTInterface<T>> mynlp=new IPOPTInterface<T>(obj);
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app=IpoptApplicationFactory();
  mynlp->set_xlu(obj.lb().template cast<double>(),obj.ub().template cast<double>());
  mynlp->set_glu(obj.gl().template cast<double>(),obj.gu().template cast<double>());
  app->RethrowNonIpoptException(true);
  if(derivCheck>0) {
    app->Options()->SetStringValue("derivative_test","first-order");
    app->Options()->SetNumericValue("derivative_test_tol",derivCheck);
  } else app->Options()->SetStringValue("derivative_test","none");
  app->Options()->SetStringValue("fixed_variable_treatment","make_parameter");
  //app->Options()->SetStringValue("linear_solver","ma77");
  app->Options()->SetNumericValue("tol",tolG);
  app->Options()->SetNumericValue("constr_viol_tol",1e-3f);
  if(muInit>0)
    app->Options()->SetNumericValue("mu_init",muInit);
  app->Options()->SetIntegerValue("max_iter",maxIter);
  app->Options()->SetIntegerValue("print_level",callback<0?0:callback>12?12:callback);
  app->Options()->SetStringValue("hessian_approximation","limited-memory");
  if(x.size()==obj.inputs())
    app->Options()->SetStringValue("warm_start_init_point","yes");
  //app->Options()->SetStringValue("mu_strategy","adaptive");
  app->Options()->SetStringValue("mu_strategy","monotone");
  Ipopt::ApplicationReturnStatus status=app->Initialize();
  status=app->OptimizeTNLP(mynlp);
  x=mynlp->_x.template cast<T>();
  //ASSERT_MSG(status==Ipopt::Solve_Succeeded,"Error in Ipopt optimization!")
  return status==Ipopt::Solve_Succeeded;
}
template <typename T>
void IPOPTInterface<T>::updateX(const SQPObjective<double>::Vec& x) {
  if(_timing)
    TBEG("IPOPTInterface<T>::updateX");
  typename SQPObjective<T>::SMatT gJacS;
  typename SQPObjective<T>::Vec gVec,fGrad;
  _obj(x.template cast<T>(),gVec,&gJacS);
  _gVec=gVec.template cast<double>();
  _gJacS=gJacS.template cast<double>();
  _E=(double)_obj(x.template cast<T>(),&fGrad);
  _fGrad=fGrad.template cast<double>();
  if(_timing)
    TEND();
}
template class IPOPTInterface<FLOAT>;
#ifdef FORCE_ADD_DOUBLE_PRECISION
template struct IPOPTInterface<double>;
#endif
}

#endif
