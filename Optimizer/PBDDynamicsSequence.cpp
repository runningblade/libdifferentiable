#include "PBDDynamicsSequence.h"
#include <Utils/RotationUtils.h>
#include <Utils/DebugGradient.h>
#include <Utils/SparseUtils.h>

namespace PHYSICSMOTION {
//DynamicsSequence
template <typename T>
PBDDynamicsSequence<T>::PBDDynamicsSequence
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
 std::shared_ptr<ArticulatedBody> body,const Vec3T& G,
 const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
 const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"PBDDynamicsSequence"),_body(body),_poses(poses),_forces(forces),_ees(ees),_dt(dt),_posConsStart(posConsStart) {
  if(inherited)
    return;
  int offPeriod=(int)round(period[0]/_dt),nrPeriod=(int)round(period[1]/_dt)+1,nrStep=3;
  if(!poses.empty() && poses[0]->getPhase())
    nrStep=round((double)poses[0]->getPhase()->horizonRef()/(double)_dt)+1;
  Vec l=_body->lowerLimit(SQPObjective<typename ArticulatedBody::T>::infty()).template cast<T>();
  Vec u=_body->upperLimit(SQPObjective<typename ArticulatedBody::T>::infty()).template cast<T>();
  _DOFVars.resize(body->nrDOF(),nrStep);
  for(int jid=0; jid<body->nrJ(); jid++) {
    const Joint& J=body->joint(jid);
    for(int r=J._offDOF; r<J._offDOF+J.nrDOF(); r++)
      if(J.isRoot(*_body))
        for(int c=0; c<nrStep; c++)
          _DOFVars(r,c)=obj.addVar(DOFVar(r,c),l[r],u[r],SQPObjectiveCompound<T>::MUST_NEW)._id;
      else
        for(int c=0; c<nrStep; c++)
          _DOFVars(r,c)=obj.addVar(DOFVar(r,c<offPeriod?c:offPeriod+((c-offPeriod)%nrPeriod)),l[r],u[r],c<offPeriod+nrPeriod?SQPObjectiveCompound<T>::MUST_NEW:SQPObjectiveCompound<T>::MUST_EXIST)._id;
  }
  //G
  _JRCF.setZero(3,_body->nrJ()*4);
  for(int i=0; i<(int)_body->nrJ(); i++)
    if(_body->joint(i)._M>0) {
      ROTI(_JRCF,i)=-G*_body->joint(i)._MC.transpose().template cast<T>();
      CTRI(_JRCF,i)=-_body->joint(i)._M*G;
    }
  //position constraints
  _C=_body->control().template cast<T>();
  _proj=controlledDOFsProj(true,NULL);
  for(int c=posConsStart; c<_DOFVars.cols(); c++)
    for(int f=0; f<(int)_forces.size(); f++)
      for(int r=0; r<3; r++) {
        _gl.push_back(0);
        _gu.push_back(0);
      }
  //dynamics constraints
  for(int c=2; c<_DOFVars.cols(); c++)
    for(int r=0; r<_proj.rows(); r++) {
      _gl.push_back(0);
      _gu.push_back(0);
    }
  _infos.resize(nrStep);
}
template <typename T>
int PBDDynamicsSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    _infos[c].reset(*_body,xc);
  }
  //position constraints
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<_DOFVars.cols(); c++)
    for(int f=0; f<(int)_ees.size(); f++) {
      GRAD_INTERP posGrads[3];
      const std::shared_ptr<EndEffectorBounds>& ef=_ees[f];
      int offset=_offset+((c-_posConsStart)*(int)_ees.size()+f)*3;
      Vec3T pos=ROTI(_infos[c]._TM,ef->jointId())*ef->_localPos.template cast<T>()+CTRI(_infos[c]._TM,ef->jointId());
      Vec3T posRef=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?posGrads:NULL,NULL);
      fvec.template segment<3>(offset)=pos-posRef;
      if(fjac)
        for(int r=0; r<3; r++) {
          Mat3X4T GK=Mat3X4T::Zero();
          GK(r,3)=1;
          GK.template block<1,3>(r,0)=ef->_localPos.transpose().template cast<T>();
          _infos[c].DTG(ef->jointId(),*_body,GK,[&](int DOF,T val) {
            fjac->push_back(STrip(offset+r,_DOFVars(DOF,c),val));
          });
          for(const std::pair<const int,T>& v:posGrads[r])
            fjac->push_back(STrip(offset+r,v.first,-v.second));
        }
    }
  //dynamics constraints
  OMP_PARALLEL_FOR_
  for(int c=2; c<_DOFVars.cols(); c++) {
    //set forces
    std::vector<GRAD_INTERP> forceGrads((int)_ees.size()*3);
    Mat3XT forces=Mat3XT::Zero(3,(int)_ees.size());
    for(int f=0; f<(int)_ees.size(); f++)
      forces.col(f)=_forces[f]->timeInterpolateStencil(x,c*_dt,fjac?&forceGrads[f*3]:NULL,NULL);
    //set FD
    Vec FD=Vec::Zero(_body->nrDOF());
    MatT DForces=MatT::Zero(_body->nrDOF(),_ees.size()*3);
    MatT DFDDXI_0=MatT::Zero(_body->nrDOF(),_body->nrDOF());
    MatT DFDDXI_1=MatT::Zero(_body->nrDOF(),_body->nrDOF());
    MatT DFDDXI_2=MatT::Zero(_body->nrDOF(),_body->nrDOF());
    Mat3XT G=Mat3XT::Zero(3,_body->nrJ()*4);
    Mat3XT MRR=Mat3XT::Zero(3,_body->nrJ()*3);
    Mat3XT MRt=Mat3XT::Zero(3,_body->nrJ()*3);
    Mat3XT MtR=Mat3XT::Zero(3,_body->nrJ()*3);
    Mat3XT Mtt=Mat3XT::Zero(3,_body->nrJ()*3);
    Mat3XT GB=Mat3XT::Zero(3,_body->nrJ()*4);
    buildSystem(_infos[c],_infos[c-1],_infos[c-2],
                true,true,mapCM(forces),mapM(DForces),
                NULL,mapV(FD),mapM(DFDDXI_0),mapM(DFDDXI_1),mapM(DFDDXI_2),mapM(G),
                mapM(MRR),mapM(MRt),mapM(MtR),mapM(Mtt),mapM(GB));
    //fill
    int offset=_offset+(_DOFVars.cols()-_posConsStart)*(int)_ees.size()*3+(c-2)*_proj.rows();
    fvec.segment(offset,_proj.rows())=_proj*FD;
    if(fjac) {
      MatT tmp=_proj*DFDDXI_0;
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offset,_DOFVars(d,c  ),tmp.col(d));
      tmp=_proj*DFDDXI_1;
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offset,_DOFVars(d,c-1),tmp.col(d));
      tmp=_proj*DFDDXI_2;
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offset,_DOFVars(d,c-2),tmp.col(d));
      MatT PDForces=_proj*DForces;
      for(int f=0; f<(int)_ees.size(); f++)
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:forceGrads[f*3+r])
            addBlock(*fjac,offset,v.first,PDForces.block(0,f*3+r,_proj.rows(),1)*v.second);
    }
  }
  return 0;
}
template <typename T>
bool PBDDynamicsSequence<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  if(!SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef))
    return false;
  DEFINE_NUMERIC_DELTA_T(T)
  if(DELTARef>0)
    DELTA=DELTARef;
  for(int i=0; i<nrTrial; i++) {
    Vec g,x=makeValid(Vec::Random(inputs));
    Vec delta=Vec(Vec::Random(inputs));
    Vec x2=x+delta*DELTA;
    //fvec
    MatT tmp;
    Vec xc=Vec::Zero(_DOFVars.rows());
    Vec FD=Vec::Zero(_body->nrDOF());
    //pre-allocate memory
    T E=0,E2=0;
    Mat3XT G,MRR,MRt,MtR,Mtt,GB,forces;
    G.resize(3,_body->nrJ()*4);
    MRR.resize(3,_body->nrJ()*3);
    MRt.resize(3,_body->nrJ()*3);
    MtR.resize(3,_body->nrJ()*3);
    Mtt.resize(3,_body->nrJ()*3);
    GB.resize(3,_body->nrJ()*4);
    forces.resize(3,_ees.size());
    //dynamic constraints
    for(int c=0; c<3; c++) {
      for(int r=0; r<xc.size(); r++)
        xc[r]=x[_DOFVars(r,c)];
      _infos[c].reset(*_body,xc);
    }
    for(int f=0; f<(int)_ees.size(); f++)
      forces.col(f)=_poses[f]->timeInterpolateStencil(x,2*_dt,NULL,NULL);
    buildSystem(_infos[2],_infos[1],_infos[0],
                true,true,mapCM(forces),mapM((MatT*)NULL),
                &E,mapV(FD),mapM((MatT*)NULL),mapM((MatT*)NULL),mapM((MatT*)NULL),mapM(G),mapM(MRR),mapM(MRt),mapM(MtR),mapM(Mtt),mapM(GB));
    //dynamic constraints2
    for(int r=0; r<xc.size(); r++)
      xc[r]=x2[_DOFVars(r,2)];
    _infos[2].reset(*_body,xc);
    buildSystem(_infos[2],_infos[1],_infos[0],
                true,true,mapCM(forces),mapM((MatT*)NULL),
                &E2,mapV((Vec*)NULL),mapM((MatT*)NULL),mapM((MatT*)NULL),mapM((MatT*)NULL),mapM(G),mapM(MRR),mapM(MRt),mapM(MtR),mapM(Mtt),mapM(GB));
    //test gradient
    T ref=0;
    for(int r=0; r<_DOFVars.rows(); r++)
      ref+=FD[r]*delta[_DOFVars(r,2)];
    T err=ref-(E2-E)/DELTA;
    DEBUG_GRADIENT(_name+"DEDX",ref,err)
  }
  return true;
}
template <typename T>
typename PBDDynamicsSequence<T>::Vec PBDDynamicsSequence<T>::makeValid(const Vec& x) const {
  Vec ret=x;
  for(int i=0; i<(int)_poses.size(); i++)
    ret=_poses[i]->makeValid(ret);
  return ret;
}
template <typename T>
const typename PBDDynamicsSequence<T>::Vec& PBDDynamicsSequence<T>::controlCoef() const {
  return _C;
}
template <typename T>
typename PBDDynamicsSequence<T>::Vec PBDDynamicsSequence<T>::residualToControl() const {
  Vec c=controlCoef();
  Vec ret=Vec::Zero(c.size());
  for(int i=0; i<c.size(); i++)
    if(c[i] == 0)
      ret[i]=0;
    else ret[i]=-1/c[i];
  return ret;
}
template <typename T>
typename PBDDynamicsSequence<T>::Vec PBDDynamicsSequence<T>::controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  Vec c=controlCoef();
  Vec ret=Vec::Zero(c.size());
  for(int i=0; i<c.size(); i++)
    if(!keepFunc(c[i]))
      ret[i]=0;
    else {
      ret[i]=1;
      if(consSet)
        consSet->insert(i);
    }
  return ret;
}
template <typename T>
typename PBDDynamicsSequence<T>::SMatT PBDDynamicsSequence<T>::controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  int off=0;
  Vec c=controlCoef();
  STrips trips;
  if(consSet)
    consSet->clear();
  for(int i=0; i<c.size(); i++)
    if(keepFunc(c[i])) {
      if(consSet)
        consSet->insert(i);
      trips.push_back(STrip(off++,i,1));
    }
  //assemble
  SMatT ret;
  ret.resize(off,c.size());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T>
typename PBDDynamicsSequence<T>::Vec PBDDynamicsSequence<T>::controlledDOFs(bool constrained,std::set<int>* consSet) const {
  return controlledDOFs([&](T val) {
    if(constrained)return val==0;
    else return val!=0;
  },consSet);
}
template <typename T>
typename PBDDynamicsSequence<T>::SMatT PBDDynamicsSequence<T>::controlledDOFsProj(bool constrained,std::set<int>* consSet) const {
  return controlledDOFsProj([&](T val) {
    if(constrained)return val==0;
    else return val!=0;
  },consSet);
}
template <typename T>
std::string PBDDynamicsSequence<T>::DOFVar(int r,int stage) const {
  return _name+"["+std::to_string(stage)+"]["+std::to_string(r)+"]";
}
template <typename T>
const Eigen::Matrix<int,-1,-1>& PBDDynamicsSequence<T>::DOFVar() const {
  return _DOFVars;
}
template <typename T>
const ArticulatedBody& PBDDynamicsSequence<T>::body() const {
  return *_body;
}
template <typename T>
void PBDDynamicsSequence<T>::lockFrame(SQPObjectiveCompound<T>& obj,int fid) {
  ASSERT(fid>=0 && fid<_DOFVars.cols())
  for(int r=0; r<_DOFVars.rows(); r++) {
    SQPVariable<T>& var=obj.addVar(_DOFVars(r,fid));
    var._l=var._u=var._init;
  }
}
template <typename T>
typename PBDDynamicsSequence<T>::MatT PBDDynamicsSequence<T>::getPoses(const Vec& x) {
  MatT mat;
  mat.resize(_DOFVars.rows(),_DOFVars.cols());
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    mat.col(c)=xc;
  }
  return mat;
}
template <typename T>
void PBDDynamicsSequence<T>::resetInfo(int id,const Vec& x) {
  Vec xId=Vec::Zero(_DOFVars.rows());
  for(int i=0; i<xId.size(); i++)
    xId[i]=x[_DOFVars(i,id)];
  if(_infos[id]._xM.size()!=xId.size() || _infos[id]._xM!=xId)
    _infos[id].reset(*_body,xId);
}
template <typename T>
typename PBDDynamicsSequence<T>::Mat3X4T PBDDynamicsSequence<T>::getJointTrans(int id,int JID) const {
  return TRANSI(_infos[id]._TM,JID);
}
template <typename T>
void PBDDynamicsSequence<T>::DTG(int id,int JID,Mat3X4T GK,std::function<void(int,T)> DTG) const {
  _infos[id].DTG(JID,*_body,GK,DTG);
}
template <typename T>
T PBDDynamicsSequence<T>::dt() const {
  return _dt;
}
template <typename T>
void PBDDynamicsSequence<T>::buildSystem
(PBDArticulatedGradientInfoMap<T>& XI,const PBDArticulatedGradientInfoMap<T>& XI_1,const PBDArticulatedGradientInfoMap<T>& XI_2,
 bool addMass,bool addForce,const Mat3XTCM& forces,MatTM DForces,
 T* E,VecM FD,MatTM DFDDXI_0,MatTM DFDDXI_1,MatTM DFDDXI_2,
 Mat3XTM G,Mat3XTM MRR,Mat3XTM MRt,Mat3XTM MtR,Mat3XTM Mtt,Mat3XTM GB) {
  Mat3X4T A,LA,LB;
  T coef=1/(_dt*_dt);
  //compute G
  int nrJ=_body->nrJ();
  if(FD.data()) {
    G.setZero();
    for(int k=0; k<nrJ; k++) {
      const Joint& J=_body->joint(k);
      const Mat3T PPT=J._MCCT.template cast<T>();
      const Vec3T P=J._MC.template cast<T>();
      if(addMass) {
        A=(TRANSI(XI._TM,k)-2*TRANSI(XI_1._TM,k)+TRANSI(XI_2._TM,k))*coef;
        ROTI(G,k)+=ROT(A)*PPT+CTR(A)*P.transpose();
        CTRI(G,k)+=CTR(A)*J._M+ROT(A)*P;
      }
      if(addForce)
        TRANSI(G,k)+=TRANSI(_JRCF,k);
    }
    if(addForce)
      for(int c=0; c<forces.cols(); c++) {
        ROTI(G,_ees[c]->jointId())-=forces.col(c)*_ees[c]->_localPos.transpose().template cast<T>();
        CTRI(G,_ees[c]->jointId())-=forces.col(c);
      }
  }
  //compute E
  if(E) {
    for(int k=0,k4=0; k<nrJ; k++,k4+=4) {
      const Joint& J=_body->joint(k);
      const Mat3T PPT=J._MCCT.template cast<T>();
      const Vec3T P=J._MC.template cast<T>();
      if(addMass) {
        A=TRANSI(XI._TM,k)-2*TRANSI(XI_1._TM,k)+TRANSI(XI_2._TM,k);
        *E+=(ROT(A)*PPT*ROT(A).transpose()+2*CTR(A)*P.transpose()*ROT(A).transpose()+CTR(A)*CTR(A).transpose()*J._M).trace()*coef/2;
      }
      if(addForce)
        *E+=(TRANSI(XI._TM,k)*TRANSI(_JRCF,k).transpose()).trace();
    }
    if(addForce)
      for(int c=0; c<forces.cols(); c++)
        *E-=(ROTI(XI._TM,_ees[c]->jointId())*_ees[c]->_localPos.template cast<T>()+CTRI(XI._TM,_ees[c]->jointId())).dot(forces.col(c));
  }
  //compute FD
  if(FD.data())
    XI.DTG(*_body,GB=G,FD);
  if(DForces.data())
    for(int c=0; c<forces.cols(); c++)
      for(int d=0; d<3; d++) {
        A.setZero();
        A.template block<1,3>(d,0)=-_ees[c]->_localPos.transpose().template cast<T>();
        A(d,3)=-1;
        XI.DTG(_ees[c]->jointId(),*_body,A,[&](int jid,T coef) {
          DForces(jid,c*3+d)+=coef;
        });
      }
  //compute DFDDXI
  if(DFDDXI_0.data()) {
    toolAContractTensorM(addMass,XI,XI,MRR,MRt,MtR,Mtt,coef);
    XI.toolAB(*_body,MRR,MRt,MtR,Mtt,GB=G,[&](int row,int col,T val) {
      DFDDXI_0(row,col)+=val;
    });
  }
  //compute DFDDXI_1
  if(DFDDXI_1.data()) {
    toolAContractTensorM(addMass,XI,XI_1,MRR,MRt,MtR,Mtt,-2*coef);
    XI.toolA(*_body,XI_1,MRR,MRt,MtR,Mtt,[&](int row,int col,T val) {
      DFDDXI_1(row,col)+=val;
    });
  }
  //compute DFDDXI_2
  if(DFDDXI_2.data() && addMass) {
    toolAContractTensorM(addMass,XI,XI_2,MRR,MRt,MtR,Mtt,coef);
    XI.toolA(*_body,XI_2,MRR,MRt,MtR,Mtt,[&](int row,int col,T val) {
      DFDDXI_2(row,col)+=val;
    });
  }
}
template <typename T>
void PBDDynamicsSequence<T>::toolAContractTensorM
(bool addMass,PBDArticulatedGradientInfoMap<T>& XIL,const PBDArticulatedGradientInfoMap<T>& XIR,
 Mat3XTM MRR,Mat3XTM MRt,Mat3XTM MtR,Mat3XTM Mtt,T coef) const {
  if(!addMass) {
    MRR.setZero();
    MRt.setZero();
    MtR.setZero();
    Mtt.setZero();
  } else {
    int nrJ=_body->nrJ();
    for(int k=0; k<nrJ; k++) {
      const Joint& J=_body->joint(k);
      MRR.template block<3,3>(0,k*3)=-invDoubleCrossMatTrace<T>(ROTI(XIL._TM,k)*J._MCCT.template cast<T>()*ROTI(XIR._TM,k).transpose())*coef;
      MRt.template block<3,3>(0,k*3)=cross<T>(ROTI(XIL._TM,k)*J._MC.template cast<T>())*coef;
      MtR.template block<3,3>(0,k*3)=-cross<T>(ROTI(XIR._TM,k)*J._MC.template cast<T>())*coef;
      Mtt.template block<3,3>(0,k*3)=Mat3T::Identity()*J._M*coef;
    }
  }
}
template class PBDDynamicsSequence<FLOAT>;
}
