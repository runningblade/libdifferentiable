#ifndef PBD_DYNAMICS_SEQUENCE_H
#define PBD_DYNAMICS_SEQUENCE_H

#include <Articulated/ArticulatedBody.h>
#include <Articulated/EndEffectorInfo.h>
#include <Articulated/PBDArticulatedGradientInfo.h>
#include "PositionSequence.h"
#include "ForceSequence.h"

namespace PHYSICSMOTION {
template <typename T>
class PBDDynamicsSequence : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  PBDDynamicsSequence(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
                      std::shared_ptr<ArticulatedBody> body,const Vec3T& G,
                      const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
                      const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
                      const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart=0,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0) override;
  virtual Vec makeValid(const Vec& x) const override;
  virtual const Vec& controlCoef() const;
  virtual Vec residualToControl() const;
  virtual Vec controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet=NULL) const;
  virtual SMatT controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet=NULL) const;
  virtual Vec controlledDOFs(bool constrained,std::set<int>* consSet=NULL) const;
  virtual SMatT controlledDOFsProj(bool constrained,std::set<int>* consSet=NULL) const;
  virtual std::string DOFVar(int r,int stage) const;
  virtual const Eigen::Matrix<int,-1,-1>& DOFVar() const;
  virtual const ArticulatedBody& body() const;
  virtual void lockFrame(SQPObjectiveCompound<T>& obj,int fid);
  virtual MatT getPoses(const Vec& x);
  virtual void resetInfo(int id,const Vec& x);
  virtual Mat3X4T getJointTrans(int id,int JID) const;
  virtual void DTG(int id,int JID,Mat3X4T GK,std::function<void(int,T)> DTG) const;
  T dt() const;
  void buildSystem(PBDArticulatedGradientInfoMap<T>& XI,const PBDArticulatedGradientInfoMap<T>& XI_1,const PBDArticulatedGradientInfoMap<T>& XI_2,
                   bool addMass,bool addForce,const Mat3XTCM& forces,MatTM DForces,
                   T* E,VecM FD,MatTM DFDDXI_0,MatTM DFDDXI_1,MatTM DFDDXI_2,
                   Mat3XTM G,Mat3XTM MRR,Mat3XTM MRt,Mat3XTM MtR,Mat3XTM Mtt,Mat3XTM GB);
  void toolAContractTensorM(bool addMass,PBDArticulatedGradientInfoMap<T>& XIL,const PBDArticulatedGradientInfoMap<T>& XIR,
                            Mat3XTM MRR,Mat3XTM MRt,Mat3XTM MtR,Mat3XTM Mtt,T coef) const;
 protected:
  std::shared_ptr<ArticulatedBody> _body;
  std::vector<std::shared_ptr<PositionSequence<T>>> _poses;
  std::vector<std::shared_ptr<ForceSequence<T>>> _forces;
  std::vector<std::shared_ptr<EndEffectorBounds>> _ees;
  Mat3XT _JRCF;
  T _dt;
  //variables
  Vec _C;
  MatT _proj;
  int _posConsStart;
  std::vector<PBDArticulatedGradientInfo<T>> _infos;
  Eigen::Matrix<int,-1,-1> _DOFVars;
};
}

#endif
