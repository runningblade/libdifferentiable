#ifndef MASS_SPRING_OBJECTIVE_H
#define MASS_SPRING_OBJECTIVE_H

#include "SQPObjective.h"

namespace PHYSICSMOTION {
template <typename T,int DIM>
struct MassSpringObjective : public SQPObjectiveComponent<T> {
 public:
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  typedef std::tuple<int,int,T> SPRING;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  MassSpringObjective(SQPObjectiveCompound<T>& obj,const std::vector<SPRING>& springs,const Eigen::Matrix<T,DIM,1>& g,T coef);
  void addConstraintInCircle(SQPObjectiveCompound<T>& obj,const Eigen::Matrix<T,DIM,1>& ctr,T rad);
  void fixNode(SQPObjectiveCompound<T>& obj,int id,const Eigen::Matrix<T,DIM,1>& pos);
 private:
  const std::vector<SPRING>& _springs;
  int _nNode;
};
template <typename T>
void visualizeMassSpringChain2D(int argc,char** argv,int N,const Eigen::Matrix<T,2,1>& g,T coef,bool cons,bool useIPOPT=false);
template <typename T>
void visualizeMassSpringChain3D(int argc,char** argv,int N,const Eigen::Matrix<T,3,1>& g,T coef,bool cons,bool useIPOPT=false);
}

#endif
