#ifndef LAPLACE_OBJECTIVE_H
#define LAPLACE_OBJECTIVE_H

#include "PolynomialObjective.h"

namespace PHYSICSMOTION {
template <typename T,int DIM=3>
class LaplaceObjective : public PolynomialObjective<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  typedef typename SQPObjectiveCompound<T>::TermXT TermXT;
  using SQPObjectiveComponent<T>::_name;
  LaplaceObjective(SQPObjectiveCompound<T>& obj,const std::string& name,int stage,T coef,bool cubic=false,bool dirichletOrLaplace=false);
  LaplaceObjective(SQPObjectiveCompound<T>& obj,const std::string& name,const Eigen::Matrix<int,-1,-1>& var,const Eigen::Matrix<int,-1,-1>& diffVar,T coef,bool dirichletOrLaplace=false);
  std::string diffVar(int r,int stage) const;
  std::string var(int r,int stage) const;
  const Eigen::Matrix<int,-1,-1>& getVar() const;
  const Eigen::Matrix<int,-1,-1>& getDiffVar() const;
  static Mat4T hermiteDirichlet();
  static Mat4T hermiteLaplace();
  static Mat2T finiteDiffDirichlet();
  static Mat3T finiteDiffLaplace();
  static PolyXT polyContract(const TermXT* terms,const MatT& H);
 private:
  void reset(SQPObjectiveCompound<T>& obj,bool dirichletOrLaplace);
  T _coef;
  Eigen::Matrix<int,-1,-1> _var;
  Eigen::Matrix<int,-1,-1> _diffVar;
};
}

#endif
