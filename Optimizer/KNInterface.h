#ifndef KNITRO_INTERFACE_H
#define KNITRO_INTERFACE_H
#ifdef KNITRO_SUPPORT

#include <knitro.h>
#include "SQPObjective.h"

namespace PHYSICSMOTION {
template <typename T>
class KNInterface {
 public:
  typedef typename SQPObjective<T>::Vec Vec;
  typedef typename SQPObjective<T>::MatT MatT;
  typedef typename SQPObjective<T>::SMatT SMatT;
  typedef typename SQPObjective<T>::STrip STrip;
  typedef typename SQPObjective<T>::STrips STrips;
  KNInterface(SQPObjective<T>& obj,bool timing=false);
  ~KNInterface();
  void updateInitial();
  bool optimize(bool callback,double checkDeriv,double ftol,double xtol,bool useDirect,int maxIter,double muInit,Vec& x,Vec* fvec=NULL);
 private:
  KNInterface(const KNInterface& other);
  KNInterface& operator=(const KNInterface& other);
  static int callbackEvalFC(KN_context_ptr,CB_context_ptr,
                            KN_eval_request_ptr const evalRequest,
                            KN_eval_result_ptr const evalResult,
                            void* const userParams);
  static int callbackEvalGA(KN_context_ptr,CB_context_ptr,
                            KN_eval_request_ptr const evalRequest,
                            KN_eval_result_ptr const evalResult,
                            void* const userParams);
  //data
  std::unordered_map<int,typename SQPObjectiveComponent<T>::PolyXT> _polyCompCons;
  std::shared_ptr<typename SQPObjectiveComponent<T>::PolyXT> _polyCompObj;
  SQPObjective<T>& _obj;
  KN_context* _kc;
  CB_context* _cb;
  bool _timing;
  //tmp eval
  int _inputs,_values,_jacNonZeros;
  Eigen::Matrix<double,-1,1> _x;
  Vec _grad,_fvec;
  SMatT _fjac;
  T _f;
};
}

#endif
#endif
