#include "SQPObjective.h"
#include <Utils/DebugGradient.h>
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
//KTRObjective
template <typename T>
T SQPObjective<T>::infty() {
  return 1E6;
}
template <typename T>
typename SQPObjective<T>::Vec SQPObjective<T>::lb() const {
  return Vec::Constant(inputs(), -infty());
}
template <typename T>
typename SQPObjective<T>::Vec SQPObjective<T>::ub() const {
  return Vec::Constant(inputs(), infty());
}
template <typename T>
typename SQPObjective<T>::Vec SQPObjective<T>::gl() const {
  return Vec::Constant(values(), 0);
}
template <typename T>
typename SQPObjective<T>::Vec SQPObjective<T>::gu() const {
  return Vec::Constant(values(), infty());
}
template <typename T>
typename SQPObjective<T>::Vec SQPObjective<T>::init() const {
  return Vec::Zero(inputs());
}
template <typename T>
int SQPObjective<T>::nnzJ() {
  Vec fvec;
  SMatT fjac;
  Vec ret = Vec::Zero(inputs());
  operator()(ret, fvec, &fjac);
  //count nnz
  int off = 0;
  for (int k = 0; k < fjac.outerSize(); ++k)
    for (typename SMatT::InnerIterator it(fjac, k); it; ++it, off++);
  return off;
}
//variable
template <typename T>
SQPVariable<T>::SQPVariable():_isBinary(false),_id(-1) {}
template <typename T>
void SQPVariable<T>::fix(T val) {
  _l=_u=_init=val;
}
template <typename T>
bool SQPVariable<T>::isFixed() const {
  return _l==_u;
}
//constraint
template <typename T>
int SQPObjective<T>::operator()(const Vec& x,Vec& fvec,MatT* fjac) {
  int ret=operator()(x,fvec,fjac?&_tmpFjacs:NULL);
  if(fjac)
    *fjac=_tmpFjacs.toDense();
  return ret;
}
template <typename T>
int SQPObjective<T>::operator()(const Vec& x,Vec& fvec,SMatT* fjac) {
  if(fjac)  //to remember number of entries
    _tmp.clear();
  int ret=operator()(x,fvec,fjac?&_tmp:NULL);
  if(fjac) {
    fjac->resize(values(),inputs());
    if(!_tmp.getVector().empty())
      fjac->setFromTriplets(_tmp.begin(),_tmp.end());
  }
  return ret;
}
template <typename T>
int SQPObjective<T>::operator()(const Vec&,Vec&,STrips*) {
  ASSERT_MSG(false,"Not Implemented: Sparse Constraint Function!")
  return -1;
}
//objective
template <typename T>
T SQPObjective<T>::operator()(const Vec& x,Vec* fgrad) {
  return operator()(x,fgrad,(STrips*)NULL);
}
template <typename T>
T SQPObjective<T>::operator()(const Vec& x,Vec* fgrad,MatT* fhess) {
  T ret=operator()(x,fgrad,fhess?&_tmpFjacs:NULL);
  if(fhess)
    *fhess=_tmpFjacs.toDense();
  return ret;
}
template <typename T>
T SQPObjective<T>::operator()(const Vec& x,Vec* fgrad,SMatT* fhess) {
  if(fhess)  //to remember number of entries
    _tmp.clear();
  T ret=operator()(x,fgrad,fhess?&_tmp:NULL);
  if(fhess) {
    fhess->resize(inputs(),inputs());
    if(!_tmp.getVector().empty())
      fhess->setFromTriplets(_tmp.begin(),_tmp.end());
  }
  return ret;
}
template <typename T>
T SQPObjective<T>::operator()(const Vec&,Vec*,STrips*) {
  ASSERT_MSG(false,"Not Implemented: Sparse Objective Function!")
  return -1;
}
//problem size
template <typename T>
int SQPObjective<T>::inputs() const {
  return 0;
}
template <typename T>
int SQPObjective<T>::values() const {
  return 0;
}
template <typename T>
bool SQPObjective<T>::debug(int nrTrial,T thresErr,Vec* x0,bool hess,T DELTARef) {
  DEFINE_NUMERIC_DELTA_T(T)
  if(DELTARef>0)
    DELTA=DELTARef;
  for(int i=0; i<nrTrial; i++)
    for(int vid=thresErr!=0?0:-1; vid<int(thresErr!=0?inputs():0); vid++) {
      T FX,FX2;
      SMatT fjac,fhess;
      Vec x=Vec::Random(inputs()),grad,grad2;
      if(x0)
        x.segment(0,x0->size())=*x0;
      Vec delta=thresErr>0?Vec(Vec::Unit(inputs(),vid)):Vec(Vec::Random(inputs()));
      //objective
      grad.setZero(inputs());
      grad2.setZero(inputs());
      if(hess) {
        FX=operator()(x,&grad,&fhess);
        FX2=operator()(x+delta*DELTA,&grad2,(SMatT*)NULL);
        fhess=sparseBlk(fhess,0,0,inputs(),inputs());
      } else {
        FX=operator()(x,&grad);
        FX2=operator()(x+delta*DELTA,NULL);
      }
      if(thresErr!=0) {
        std::string entryStr="grad["+(vid<0?"-":std::to_string(vid))+"]";
        T ref=grad.dot(delta),err=grad.dot(delta)-(FX2-FX)/DELTA;
        if(ref==0)
          continue;
        T thresRel=thresErr>0?thresErr:std::max<T>(-thresErr,-thresErr*fabs(ref));
        if(fabs(err)>thresRel) {
          DEBUG_GRADIENT(entryStr,ref,err)
          return false;
        }
        if(hess) {
          std::string entryStr="hess["+(vid<0?"-":std::to_string(vid))+"]";
          ref=(hess*delta).norm(),err=(hess*delta-(grad2-grad)/DELTA).norm();
          if(ref==0)
            continue;
          T thresRel=thresErr>0?thresErr:std::max<T>(-thresErr,-thresErr*fabs(ref));
          if(fabs(err)>thresRel) {
            DEBUG_GRADIENT(entryStr,ref,err)
            return false;
          }
        }
      } else {
        std::string entryStr="grad["+(vid<0?"-":std::to_string(vid))+"]";
        T ref=grad.dot(delta),err=grad.dot(delta)-(FX2-FX)/DELTA;
        if(ref==0)
          continue;
        DEBUG_GRADIENT(entryStr,ref,err)
        if(hess) {
          std::string entryStr="hess["+(vid<0?"-":std::to_string(vid))+"]";
          ref=(fhess*delta).norm(),err=(fhess*delta-(grad2-grad)/DELTA).norm();
          if(ref==0)
            continue;
          DEBUG_GRADIENT(entryStr,ref,err)
        }
      }
    }
  if(values()<=0)
    return true;
  for(int i=0; i<nrTrial; i++)
    for(int vid=thresErr!=0?0:-1; vid<int(thresErr!=0?inputs():0); vid++) {
      SMatT fjac;
      Vec x=Vec::Random(inputs()),grad,grad2;
      if(x0)
        x.segment(0,x0->size())=*x0;
      Vec delta=thresErr>0?Vec(Vec::Unit(inputs(),vid)):Vec(Vec::Random(inputs()));
      //constraint
      Vec fvec=Vec::Zero(values());
      Vec fvec2=Vec::Zero(values());
      fjac.resize(values(),inputs());
      operator()(x,fvec,(SMatT*)&fjac);
      operator()(x+delta*DELTA,fvec2,(SMatT*)NULL);
      if(thresErr!=0) {
        for(int r=0; r<fvec.size(); r++) {
          std::string entryStr="fJac("+std::to_string(r)+","+(vid<0?"-":std::to_string(vid))+")";
          T ref=(fjac*delta)[r],err=(fjac*delta-(fvec2-fvec)/DELTA)[r];
          if(ref==0 && fvec2==fvec)
            continue;
          T thresRel=thresErr>0?thresErr:std::max<T>(-thresErr,-thresErr*fabs(ref));
          if(fabs(err)>thresRel) {
            DEBUG_GRADIENT(entryStr,ref,err)
            return false;
          }
        }
      } else {
        std::string entryStr="fJac(-,"+(vid<0?"-":std::to_string(vid))+")";
        T ref=(fjac*delta).norm(),err=(fjac*delta-(fvec2-fvec)/DELTA).norm();
        if(ref==0 && fvec2==fvec)
          continue;
        DEBUG_GRADIENT(entryStr,ref,err)
      }
    }
  return true;
}
//KTRObjectiveComponent
template <typename T>
SQPObjectiveComponent<T>::SQPObjectiveComponent(SQPObjectiveCompound<T>& obj,const std::string& name,bool force):_vars(&(obj.vars())),_offset(0) {
  _name=name;
  if(force) {
    if(obj.components().find(_name)!=obj.components().end()) {
      int dupId=0;
      do {
        dupId++;
        _name=name+"[Duplicate"+std::to_string(dupId)+"]";
      } while(obj.components().find(_name)!=obj.components().end());
    }
  }
}
template <typename T>
SQPObjectiveComponent<T>::~SQPObjectiveComponent() {}
template <typename T>
int SQPObjectiveComponent<T>::inputs() const {
  return _inputs;
}
template <typename T>
int SQPObjectiveComponent<T>::values() const {
  ASSERT(_gl.size()==_gu.size())
  return (int)_gl.size();
}
//whether modifying objective function expression is allowed
template <typename T>
void SQPObjectiveComponent<T>::setUpdateCache(const Vec&,bool) {}
//constraint
template <typename T>
int SQPObjectiveComponent<T>::operator()(const Vec&,Vec&,STrips*) {
  return 0;
}
//objective
template <typename T>
T SQPObjectiveComponent<T>::operator()(const Vec&,Vec*) {
  //default to pure constraints
  return 0;
}
//variable manipulation
template <typename T>
std::shared_ptr<typename SQPObjectiveComponent<T>::PolyXT>& SQPObjectiveComponent<T>::getPolynomialComponentObjective() {
  return _polyCompObj;
}
template <typename T>
std::unordered_map<int,typename SQPObjectiveComponent<T>::PolyXT>& SQPObjectiveComponent<T>::getPolynomialComponentConstraint() {
  return _polyCompCons;
}
template <typename T>
void SQPObjectiveComponent<T>::addPolynomialComponentObjective(const PolyXT& polyComp) {
  if(!_polyCompObj)
    _polyCompObj.reset(new PolyXT);
  *_polyCompObj+=polyComp;
}
template <typename T>
void SQPObjectiveComponent<T>::addPolynomialComponentConstraint(int consId,const PolyXT& polyComp) {
  _polyCompCons[consId]+=polyComp;
}
template <typename T>
void SQPObjectiveComponent<T>::setPolynomialComponentObjective(const PolyXT& polyComp) {
  if(!_polyCompObj)
    _polyCompObj.reset(new PolyXT);
  *_polyCompObj=polyComp;
}
template <typename T>
void SQPObjectiveComponent<T>::setPolynomialComponentConstraint(int consId,const PolyXT& polyComp) {
  _polyCompCons[consId]=polyComp;
}
template <typename T>
void SQPObjectiveComponent<T>::addSOS1(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<int>& ids) {
  //tree
  int nrVar=1;
  std::vector<int> branch;
  std::vector<int> branchFactor;
  while((int)ids.size()>nrVar) {
    nrVar*=2;
    branch.push_back(obj.addBinaryVar(name+"SOS1Aux["+std::to_string(branch.size())+"]",0,1,SQPObjectiveCompound<T>::MUST_NEW)._id);
    branchFactor.push_back(nrVar);
  }
  //bound
  int cid=1;
  for(int i=0; i<(int)ids.size(); i++) {
    for(int j=0; j<(int)branchFactor.size(); j++) {
      int rem=i%branchFactor[j];
      if(rem<branchFactor[j]/2) {
        addPolynomialComponentConstraint((int)_gl.size(),obj.addVarPoly(ids[i])-obj.addVarPoly(branch[j]));
        _gl.push_back(-SQPObjective<T>::infty());
        _gu.push_back(0);
      } else {
        addPolynomialComponentConstraint((int)_gl.size(),obj.addVarPoly(ids[i])+obj.addVarPoly(branch[j]));
        _gl.push_back(-SQPObjective<T>::infty());
        _gu.push_back(1);
        cid++;
      }
    }
  }
}
template <typename T>
void SQPObjectiveComponent<T>::addSOS1SumToOne(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<int>& ids) {
  addSOS1(obj,name,ids);
  PolyXT sumId;
  for(int i=0; i<(int)ids.size(); i++)
    sumId+=obj.addVarPoly(ids[i]);
  addPolynomialComponentConstraint((int)_gl.size(),sumId);
  _gl.push_back(1);
  _gu.push_back(1);
}
template <typename T>
void SQPObjectiveComponent<T>::addSOS2SumToOne(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<int>& ids) {
  std::vector<int> idsSOS1;
  for(int i=0; i<(int)ids.size()-1; i++)
    idsSOS1.push_back(obj.addVar(name+"SOS1["+std::to_string(i)+"]",0,1,SQPObjectiveCompound<T>::MUST_NEW)._id);
  //SOS1 Helper
  addSOS1(obj,name+"SOS1",idsSOS1);
  //build SOS2SumToOne from SOS1
  PolyXT sumId,id;
  for(int i=0; i<(int)ids.size(); i++) {
    id=obj.addVarPoly(ids[i]);
    sumId+=id;
    if(i==0)
      addPolynomialComponentConstraint((int)_gl.size(),id-obj.addVarPoly(idsSOS1[i]));
    else if(i==(int)ids.size()-1)
      addPolynomialComponentConstraint((int)_gl.size(),id-obj.addVarPoly(idsSOS1[i-1]));
    else addPolynomialComponentConstraint((int)_gl.size(),id-obj.addVarPoly(idsSOS1[i])-obj.addVarPoly(idsSOS1[i-1]));
    _gl.push_back(-SQPObjective<T>::infty());
    _gu.push_back(0);
  }
  addPolynomialComponentConstraint((int)_gl.size(),sumId);
  _gl.push_back(1);
  _gu.push_back(1);
}
template <typename T>
bool SQPObjectiveComponent<T>::debug(int inputs,int nrTrial,T thresErr,Vec* x0,bool hess,T DELTARef) {
  int offset=_offset;
  _inputs=inputs;
  _offset=0;
  DEFINE_NUMERIC_DELTA_T(T)
  if(DELTARef>0)
    DELTA=DELTARef;
  for(int i=0; i<nrTrial; i++)
    for(int vid=thresErr!=0?0:-1; vid<int(thresErr!=0?inputs:0); vid++) {
      T FX,FX2;
      SMatT fjac,fhess;
      Vec x=makeValid(Vec::Random(inputs)),grad,grad2;
      if(x0)
        x.segment(0,x0->size())=*x0;
      Vec delta=thresErr>0?Vec(Vec::Unit(inputs,vid)):Vec(Vec::Random(inputs));
      //objective
      grad.setZero(inputs);
      grad2.setZero(inputs);
      if(hess) {
        setUpdateCache(x,true);
        FX=operator()(x,&grad,&fhess);
        setUpdateCache(x+delta*DELTA,false);
        FX2=operator()(x+delta*DELTA,&grad2,(SMatT*)NULL);
        fhess=sparseBlk(fhess,0,0,inputs,inputs);
      } else {
        setUpdateCache(x,true);
        FX=operator()(x,&grad);
        setUpdateCache(x+delta*DELTA,false);
        FX2=operator()(x+delta*DELTA,&grad2);
      }
      if(thresErr!=0) {
        std::string entryStr=_name+":grad["+(vid<0?"-":std::to_string(vid))+"]";
        T ref=grad.dot(delta),err=grad.dot(delta)-(FX2-FX)/DELTA;
        if(ref==0)
          continue;
        T thresRel=thresErr>0?thresErr:std::max<T>(-thresErr,-thresErr*fabs(ref));
        if(fabs(err)>thresRel) {
          DEBUG_GRADIENT(entryStr,ref,err)
          return false;
        }
        if(hess) {
          std::string entryStr=_name+":hess["+(vid<0?"-":std::to_string(vid))+"]";
          ref=(hess*delta).norm(),err=(hess*delta-(grad2-grad)/DELTA).norm();
          if(ref==0)
            continue;
          T thresRel=thresErr>0?thresErr:std::max<T>(-thresErr,-thresErr*fabs(ref));
          if(fabs(err)>thresRel) {
            DEBUG_GRADIENT(entryStr,ref,err)
            return false;
          }
        }
      } else {
        std::string entryStr=_name+":grad["+(vid<0?"-":std::to_string(vid))+"]";
        T ref=grad.dot(delta),err=grad.dot(delta)-(FX2-FX)/DELTA;
        if(ref==0)
          continue;
        DEBUG_GRADIENT(entryStr,ref,err)
        if(hess) {
          std::string entryStr=_name+":hess["+(vid<0?"-":std::to_string(vid))+"]";
          ref=(fhess*delta).norm(),err=(fhess*delta-(grad2-grad)/DELTA).norm();
          if(ref==0)
            continue;
          DEBUG_GRADIENT(entryStr,ref,err)
        }
      }
    }
  _offset=offset;
  if(values()<=0)
    return true;

  _offset=0;
  for(int i=0; i<nrTrial; i++)
    for(int vid=thresErr!=0?0:-1; vid<int(thresErr!=0?inputs:0); vid++) {
      SMatT fjac;
      Vec x=makeValid(Vec::Random(inputs)),grad,grad2;
      if(x0)
        x.segment(0,x0->size())=*x0;
      Vec delta=thresErr>0?Vec(Vec::Unit(inputs,vid)):Vec(Vec::Random(inputs));
      //constraint
      Vec fvec=Vec::Zero(values());
      Vec fvec2=Vec::Zero(values());
      fjac.resize(values(),inputs);
      setUpdateCache(x,true);
      operator()(x,fvec,(SMatT*)&fjac);
      setUpdateCache(x+delta*DELTA,false);
      operator()(x+delta*DELTA,fvec2,(SMatT*)NULL);
      if(thresErr!=0) {
        for(int r=0; r<fvec.size(); r++) {
          std::string entryStr=_name+":fJac("+std::to_string(r)+","+(vid<0?"-":std::to_string(vid))+")";
          T ref=(fjac*delta)[r],err=(fjac*delta-(fvec2-fvec)/DELTA)[r];
          if(ref==0 && fvec2==fvec)
            continue;
          T thresRel=thresErr>0?thresErr:std::max<T>(-thresErr,-thresErr*fabs(ref));
          if(fabs(err)>thresRel) {
            DEBUG_GRADIENT(entryStr,ref,err)
            return false;
          }
        }
      } else {
        std::string entryStr=_name+":fJac(-,"+(vid<0?"-":std::to_string(vid))+")";
        T ref=(fjac*delta).norm(),err=(fjac*delta-(fvec2-fvec)/DELTA).norm();
        if(ref==0 && fvec2==fvec)
          continue;
        DEBUG_GRADIENT(entryStr,ref,err)
      }
    }
  _offset=offset;
  return true;
}
template <typename T>
bool SQPObjectiveComponent<T>::debug(int nrTrial,T thresErr,Vec* x0,bool hess,T DELTARef) {
  int inputs=0;
  for(const std::pair<const std::string,SQPVariable<T>>& v:*_vars)
    inputs=std::max(inputs,v.second._id+1);
  return debug(inputs,nrTrial,thresErr,x0,hess,DELTARef);
}
template <typename T>
typename SQPObjectiveComponent<T>::Vec SQPObjectiveComponent<T>::makeValid(const Vec& x) const {
  return x;
}
template <typename T>
bool SQPObjectiveComponent<T>::debugGradientConditional(const std::string& entryStr,T ref,T err,T thres) const {
  if(ref==0 && err==0)
    return true;
  std::string entryStrName=_name+":"+entryStr;
  DEFINE_NUMERIC_DELTA_T(T)
  if(thres!=0) {
    T thresRel=thres>0?thres:std::max<T>(-thres,-thres*fabs(ref));
    if(fabs(err)>thresRel) {
      DEBUG_GRADIENT(entryStrName,ref,err)
      return false;
    }
  } else {
    DEBUG_GRADIENT(entryStrName,ref,err)
  }
  return true;
}
template <typename T>
bool SQPObjectiveComponent<T>::debugGradientConditionalVID(const std::string& entryStr,int vid,T ref,T err,T thres) const {
  if(vid==-1)
    return debugGradientConditional(entryStr,ref,err,thres);
  else return debugGradientConditional(entryStr+"("+std::to_string(vid)+")",ref,err,thres);
}
template <typename T>
void SQPObjectiveComponent<T>::setOffset(int offset) {
  _offset=offset;
}
template <typename T>
void SQPObjectiveComponent<T>::clear() {
  _gl.clear();
  _gu.clear();
  _polyCompObj=NULL;
  _polyCompCons.clear();
}
//KTRObjectiveCompound
template <typename T>
typename SQPObjectiveCompound<T>::Vec SQPObjectiveCompound<T>::lb() const {
  Vec ret=Vec::Zero(_vars.size());
  for(const std::pair<const std::string,SQPVariable<T>>& v:_vars)
    ret[v.second._id]=v.second._l;
  return ret;
}
template <typename T>
typename SQPObjectiveCompound<T>::Vec SQPObjectiveCompound<T>::ub() const {
  Vec ret=Vec::Zero(_vars.size());
  for(const std::pair<const std::string,SQPVariable<T>>& v:_vars)
    ret[v.second._id]=v.second._u;
  return ret;
}
template <typename T>
typename SQPObjectiveCompound<T>::Vec SQPObjectiveCompound<T>::gl() const {
  Vec ret=Vec::Zero(values());
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components)
    ret.segment(v.second->_offset,v.second->values())=VecCM(&(v.second->_gl[0]),v.second->_gl.size());
  return ret;
}
template <typename T>
typename SQPObjectiveCompound<T>::Vec SQPObjectiveCompound<T>::gu() const {
  Vec ret=Vec::Zero(values());
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components)
    ret.segment(v.second->_offset,v.second->values())=VecCM(&(v.second->_gu[0]),v.second->_gu.size());
  return ret;
}
template <typename T>
typename SQPObjectiveCompound<T>::Vec SQPObjectiveCompound<T>::init() const {
  Vec ret=Vec::Zero(_vars.size());
  for(const std::pair<const std::string,SQPVariable<T>>& v:_vars)
    ret[v.second._id]=v.second._init;
  return ret;
}
template <typename T>
int SQPObjectiveCompound<T>::inputs() const {
  return _vars.size();
}
template <typename T>
int SQPObjectiveCompound<T>::values() const {
  int ret=0;
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components)
    ret+=v.second->values();
  return ret;
}
//constraint
template <typename T>
int SQPObjectiveCompound<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  fvec.setZero(values());
  if(fjac)
    fjac->clear();
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components)
    v.second->operator()(x,fvec,fjac);
  for(const std::pair<const int,PolyXT>& p:_polyCompCons)
    fvec[p.first]+=p.second.evalJac(p.first,x,fjac);
  return 0;
}
//objective
template <typename T>
T SQPObjectiveCompound<T>::operator()(const Vec& x,Vec* fgrad) {
  T FX=0;
  if(fgrad)
    fgrad->setZero(inputs());
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components)
    FX+=v.second->operator()(x,fgrad);
  if(_polyCompObj)
    FX+=_polyCompObj->eval(x,fgrad);
  return FX;
}
//variable manipulation
template <typename T>
void SQPObjectiveCompound<T>::assemblePoly() {
  _polyCompObj=NULL;
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components)
    if(v.second->getPolynomialComponentObjective()) {
      if(!_polyCompObj)
        _polyCompObj.reset(new PolyXT);
      *_polyCompObj+=*(v.second->getPolynomialComponentObjective());
    }
  int offset=0;
  _polyCompCons.clear();
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components) {
    v.second->_offset=offset;
    for(const std::pair<const int,PolyXT>& polyC:v.second->getPolynomialComponentConstraint())
      _polyCompCons[polyC.first+v.second->_offset]+=polyC.second;
    offset+=v.second->values();
  }
}
template <typename T>
std::shared_ptr<typename SQPObjectiveCompound<T>::PolyXT>& SQPObjectiveCompound<T>::getPolynomialComponentObjective() {
  return _polyCompObj;
}
template <typename T>
std::unordered_map<int,typename SQPObjectiveCompound<T>::PolyXT>& SQPObjectiveCompound<T>::getPolynomialComponentConstraint() {
  return _polyCompCons;
}
template <typename T>
const SQPVariable<T>& SQPObjectiveCompound<T>::addBinaryVar(const std::string& name,T l,T u,VARIABLE_OP op) {
  const SQPVariable<T>& var=addVar(name,l,u,op);
  const_cast<SQPVariable<T>&>(var)._isBinary=true;
  return var;
}
template <typename T>
const SQPVariable<T>& SQPObjectiveCompound<T>::addVar(const std::string& name,T l,T u,VARIABLE_OP op) {
  typename VARMAP::iterator it=_vars.find(name);
  if(op==MUST_EXIST) {
    ASSERT_MSGV(it!=_vars.end(),"Variable: %s does not exist!",name.c_str())
  } else {
    if(op==MUST_NEW) {
      ASSERT_MSGV(it==_vars.end(),"Variable: %s already exists!",name.c_str())
    }
    if(it==_vars.end()) {
      _vars[name]=SQPVariable<T>();
      _vars[name]._id=(int)_vars.size()-1;
      _varsInv[(int)_vars.size()-1]=name;
      it=_vars.find(name);
    }
  }
  it->second._l=l;
  it->second._u=u;
  return it->second;
}
template <typename T>
const SQPVariable<T>& SQPObjectiveCompound<T>::addVar(const std::string& name,VARIABLE_OP op) {
  typename VARMAP::iterator it=_vars.find(name);
  if(op==MUST_EXIST) {
    ASSERT_MSGV(it!=_vars.end(),"Variable: %s does not exist!",name.c_str())
  } else {
    if(op==MUST_NEW) {
      ASSERT_MSGV(it==_vars.end(),"Variable: %s already exists!",name.c_str())
    }
    if(it==_vars.end()) {
      _vars[name]=SQPVariable<T>();
      _vars[name]._id=(int)_vars.size()-1;
      _varsInv[(int)_vars.size()-1]=name;
      it=_vars.find(name);
    }
  }
  return it->second;
}
template <typename T>
int SQPObjectiveCompound<T>::getVarId(const std::string& name) const {
  return const_cast<SQPObjectiveCompound<T>&>(*this).addVar(name,MUST_EXIST)._id;
}
template <typename T>
const SQPVariable<T>& SQPObjectiveCompound<T>::addVar(int id) const {
  ASSERT_MSGV(_varsInv.find(id)!=_varsInv.end(),"Variable id: %d already exists!",id)
  return _vars.find(_varsInv.find(id)->second)->second;
}
template <typename T>
SQPVariable<T>& SQPObjectiveCompound<T>::addVar(int id) {
  ASSERT_MSGV(_varsInv.find(id)!=_varsInv.end(),"Variable id: %d already exists!",id)
  return _vars.find(_varsInv.find(id)->second)->second;
}
template <typename T>
typename SQPObjectiveCompound<T>::PolyXT SQPObjectiveCompound<T>::addVarPoly(int id) const {
  return SOSTerm<T,'x'>(1,id,1);
}
template <typename T>
typename SQPObjectiveCompound<T>::TermXT SQPObjectiveCompound<T>::addVarTerm(int id) const {
  return SOSTerm<T,'x'>(1,id,1);
}
template <typename T>
const typename SQPObjectiveCompound<T>::VARMAP& SQPObjectiveCompound<T>::vars() const {
  return _vars;
}
template <typename T>
void SQPObjectiveCompound<T>::setVarInit(const std::string& name,T init) {
  typename VARMAP::iterator it=_vars.find(name);
  ASSERT_MSGV(it!=_vars.end(),"Variable: %s does not exist!",name.c_str())
  it->second._init=init;
}
template <typename T>
void SQPObjectiveCompound<T>::setVarInit(int id,T init) {
  ASSERT(_varsInv.find(id)!=_varsInv.end())
  _vars.find(_varsInv.find(id)->second)->second._init=init;
}
template <typename T>
void SQPObjectiveCompound<T>::checkViolation(const Vec* at) {
  assemblePoly();
  Vec fvec;
  Vec l=gl();
  Vec u=gu();
  STrips fjac;
  int index;
  Vec x=at?*at:init();
  operator()(x,fvec,&fjac);
  for(const std::pair<const std::string,std::shared_ptr<SQPObjectiveComponent<T>>>& v:_components) {
    T maxVioBelow,maxVioAbove;
    Vec grad=Vec::Zero(inputs()),vioMin,vioMax;
    T obj=v.second->operator()(x,&grad);
    if(v.second->values()==0) {
      maxVioBelow=maxVioAbove=0;
    } else {
      vioMin=(fvec-l).segment(v.second->_offset,v.second->values());
      maxVioBelow=std::min<T>(vioMin.minCoeff(&index),0);
      vioMax=(fvec-u).segment(v.second->_offset,v.second->values());
      maxVioAbove=std::max<T>(vioMax.maxCoeff(&index),0);
    }
    std::cout << "Component " << v.second->_name << ": violation=(below: " << maxVioBelow << " above: " << maxVioAbove << ") objective=" << obj << "!" << std::endl;
  }
}
template <typename T>
const typename SQPObjectiveCompound<T>::CONSMAP& SQPObjectiveCompound<T>::components() const {
  return _components;
}
template <typename T>
void SQPObjectiveCompound<T>::addComponent(std::shared_ptr<SQPObjectiveComponent<T>> c) {
  ASSERT_MSGV(_components.find(c->_name)==_components.end(),"Constraint %s already exists!",c->_name.c_str())
  c->setOffset(values());
  _components[c->_name]=c;
}
template <typename T>
bool SQPObjectiveCompound<T>::debug(const std::string& str,int nrTrial,T thresErr,T DELTARef) {
  ASSERT_MSGV(_components.find(str)!=_components.end(),"Cannot find component: %s!",str.c_str())
  std::shared_ptr<SQPObjectiveComponent<T>> comp=_components.find(str)->second;
  int offset=comp->_offset;
  comp->setOffset(0);
  bool ret=comp->debug(inputs(),nrTrial,thresErr,NULL,false,DELTARef);
  comp->setOffset(offset);
  return ret;
}
template struct SQPObjective<FLOAT>;
template struct SQPVariable<FLOAT>;
template struct SQPObjectiveComponent<FLOAT>;
template struct SQPObjectiveCompound<FLOAT>;
#ifdef FORCE_ADD_DOUBLE_PRECISION
template struct SQPObjective<double>;
template struct SQPVariable<double>;
template struct SQPObjectiveComponent<double>;
template struct SQPObjectiveCompound<double>;
#endif
}
