#include "NESimplifiedDynamicsSequence.h"
#include <Utils/CrossSpatialUtils.h>
#include <Utils/DebugGradient.h>

namespace PHYSICSMOTION {
template <typename T>
NESimplifiedDynamicsSequence<T>::NESimplifiedDynamicsSequence
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
 std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodySimplified,const Vec3T& G,
 const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
 const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart,bool inherited)
  :NEDynamicsSequence<T>(obj,inherited?name:name+"NESimplifiedDynamicsSequence",dt,period,body,G,poses,forces,ees,posConsStart,true),_bodySimplified(bodySimplified) {
  if(inherited)
    return;
  int offPeriod=(int)round(period[0]/_dt),nrPeriod=(int)round(period[1]/_dt)+1,nrStep=3;
  if(!poses.empty() && poses[0]->getPhase())
    nrStep=round((double)poses[0]->getPhase()->horizonRef()/(double)_dt)+1;
  ASSERT_MSG(_bodySimplified->nrDOF()==6,"NESimplifiedDynamicsSequence must have 6 degrees of freedom!")
  //G
  _a0=concat<Vec>(Vec3T::Zero(),-G);
  //joints
  _joints.setConstant(1,_bodySimplified->nrJ()-1);
  //base
  _DOFVars.resize(6,nrStep);
  for(int r=0; r<6; r++)
    for(int c=0; c<nrStep; c++) {
      T l=-SQPObjective<T>::infty();
      T u=SQPObjective<T>::infty();
      if(r<3)
        _DOFVars(r,c)=obj.addVar(DOFVar(r,c),l,u,SQPObjectiveCompound<T>::MUST_NEW)._id;
      else
        _DOFVars(r,c)=obj.addVar(DOFVar(r,c<offPeriod?c:offPeriod+((c-offPeriod)%nrPeriod)),l,u,c<offPeriod+nrPeriod?SQPObjectiveCompound<T>::MUST_NEW:SQPObjectiveCompound<T>::MUST_EXIST)._id;
    }
  //position constraints
  //(_frame.transpose()*(x-_ctrBB))[d] \in [minC,maxC]*_res
  //(_frame.transpose()*x)[d] \in [minC,maxC]*_res+(_frame.transpose()*_ctrBB)[d]
  for(int c=posConsStart; c<nrStep; c++)
    for(int f=0; f<(int)_ees.size(); f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      EndEffectorBounds::Vec3T frameTCtr=ee->_frame.transpose()*ee->_ctrBB;
      for(int r=0; r<3; r++) {
        _gl.push_back(ee->_bb[r+0]*ee->_res+frameTCtr[r]);
        _gu.push_back(ee->_bb[r+3]*ee->_res+frameTCtr[r]);
      }
    }
  //dynamics constraints
  for(int c=2; c<nrStep; c++)
    for(int r=0; r<6; r++) {
      _gl.push_back(0);
      _gu.push_back(0);
    }
  _infosNE.resize(nrStep);
}
template <typename T>
int NESimplifiedDynamicsSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    _infosNE[c].reset(*_bodySimplified,xc);
  }
  //position constraints
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<(int)_infosNE.size(); c++) {
    GRAD_INTERP posGrads[3];
    Mat3X4T TJ=_infosNE[c].NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1),GK;
    for(int f=0; f<(int)_ees.size(); f++) {
      int offset=_offset+((c-_posConsStart)*(int)_ees.size()+f)*3;
      Vec3T posRef=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?posGrads:NULL,NULL);
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      //relative pos constraint
      Vec3T relPos=posRef-CTR(TJ);
      Mat3T ROTFrame=ROT(TJ)*ee->_frame.template cast<T>();
      fvec.template segment<3>(offset)=ROTFrame.transpose()*relPos;
      if(fjac)
        for(int r=0; r<3; r++) {
          for(const std::pair<const int,T>& v:posGrads[r])
            addBlock(*fjac,offset,v.first,ROTFrame.row(r).transpose()*v.second);
          CTR(GK)=-ROTFrame.col(r);
          ROT(GK)=relPos*ee->_frame.col(r).template cast<T>().transpose();
          _infosNE[c].DTG(_bodySimplified->nrJ()-1,*_bodySimplified,GK,[&](int dof,T val) {
            fjac->push_back(STrip(offset+r,_DOFVars(dof,c),val));
          });
        }
    }
  }
  //dynamics constraints
  int offset=_offset+((int)_infosNE.size()-_posConsStart)*(int)_ees.size()*3;
  OMP_PARALLEL_FOR_
  for(int c=2; c<_DOFVars.cols(); c++) {
    //set forces
    MatT Sc,ScCross;
    std::vector<GRAD_INTERP> forceGrads((int)_ees.size()*3);
    std::vector<GRAD_INTERP> posGrads((int)_ees.size()*3);
    Mat6XT forces=Mat6XT::Zero(6,_bodySimplified->nrJ());
    Mat3XT forceIfs=Mat3XT::Zero(3,(int)_ees.size());
    Mat3XT poses=Mat3XT::Zero(3,(int)_ees.size());
    for(int f=0; f<(int)_ees.size(); f++) {
      Eigen::Block<Mat6XT,6,1> forceI=forces.template block<6,1>(0,_bodySimplified->nrJ()-1);
      forceIfs.col(f)=_forces[f]->timeInterpolateStencil(x,c*_dt,fjac?&forceGrads[f*3]:NULL,NULL);
      poses.col(f)=_poses[f]->timeInterpolateStencil(x,c*_dt,fjac?&posGrads[f*3]:NULL,NULL);
      forceI.template segment<3>(3)+=forceIfs.col(f);
      forceI.template segment<3>(0)+=poses.col(f).cross(forceIfs.col(f));
    }
    //set FD
    _infosNE[c]._dqM=(_infosNE[c]._qM-_infosNE[c-1]._qM)/_dt;
    _infosNE[c]._ddqM=(_infosNE[c]._qM-2*_infosNE[c-1]._qM+_infosNE[c-2]._qM)/(_dt*_dt);
    if(fjac)
      _infosNE[c].RNEADerivatives(*_bodySimplified,mapCV(_a0),mapCM(forces));
    else _infosNE[c].RNEA(*_bodySimplified,mapCV(_a0),mapCM(forces));
    //fill
    int offsetc=offset+(c-2)*6;
    fvec.segment(offsetc,6)=_infosNE[c]._tauM;
    if(fjac) {
      _infosNE[c].calcH(*_bodySimplified);
      MatT tmp=_infosNE[c]._DtauDqM+_infosNE[c]._DtauDdqM/_dt+_infosNE[c]._HM/(_dt*_dt);
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offsetc,_DOFVars(d,c  ),tmp.col(d));
      tmp=-_infosNE[c]._DtauDdqM/_dt-2*_infosNE[c]._HM/(_dt*_dt);
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offsetc,_DOFVars(d,c-1),tmp.col(d));
      tmp=_infosNE[c]._HM/(_dt*_dt);
      for(int d=0; d<tmp.cols(); d++)
        addBlock(*fjac,offsetc,_DOFVars(d,c-2),tmp.col(d));
      //Sc
      Sc.resize(6,_infosNE[c]._qM.size());
      _infosNE[c].Sc(*_bodySimplified,mapCV(_joints),mapM(Sc));
      Sc=(-Sc.transpose()).eval();
      for(int f=0; f<(int)_ees.size(); f++) {
        //fjac
        ScCross=Sc.template block<6,3>(0,0)*cross<T>(poses.col(f));
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:forceGrads[f*3+r])
            addBlock(*fjac,offsetc,v.first,(ScCross.col(r)+Sc.col(3+r))*v.second);
        //pjac
        ScCross=Sc.template block<6,3>(0,0)*cross<T>(-forceIfs.col(f));
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:posGrads[f*3+r])
            addBlock(*fjac,offsetc,v.first,ScCross.col(r)*v.second);
      }
    }
  }
  return 0;
}
template <typename T>
bool NESimplifiedDynamicsSequence<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  return SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef);
}
template <typename T>
const typename NESimplifiedDynamicsSequence<T>::Vec& NESimplifiedDynamicsSequence<T>::controlCoef() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlCoef();
}
template <typename T>
typename NESimplifiedDynamicsSequence<T>::Vec NESimplifiedDynamicsSequence<T>::residualToControl() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::residualToControl();
}
template <typename T>
typename NESimplifiedDynamicsSequence<T>::Vec NESimplifiedDynamicsSequence<T>::controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(keepFunc,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequence<T>::SMatT NESimplifiedDynamicsSequence<T>::controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(keepFunc,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequence<T>::Vec NESimplifiedDynamicsSequence<T>::controlledDOFs(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(constrained,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequence<T>::SMatT NESimplifiedDynamicsSequence<T>::controlledDOFsProj(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(constrained,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequence<T>::MatT NESimplifiedDynamicsSequence<T>::getPoses(const Vec& x) {
  MatT ret;
  ret.resize(_body->nrDOF(),_DOFVars.cols());
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    Vec xc=Vec::Zero(_DOFVars.rows());
    for(int r=0; r<xc.size(); r++)
      xc[r]=x[_DOFVars(r,c)];
    _infosNE[c].reset(*_bodySimplified,xc);
  }
  OMP_PARALLEL_FOR_
  for(int c=0; c<(int)_infosNE.size(); c++) {
    //base
    Vec DOF=Vec::Zero(_body->nrDOF());
    DOF.template segment<6>(0) << x[_DOFVars(0,c)],x[_DOFVars(1,c)],x[_DOFVars(2,c)],x[_DOFVars(3,c)],x[_DOFVars(4,c)],x[_DOFVars(5,c)];
    Mat3X4T TJ=_infosNE[c].NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1);
    //xss
    for(int f=0; f<(int)_ees.size(); f++) {
      Vec3T pos=_poses[f]->timeInterpolateStencil(x,c*_dt,NULL,NULL);
      Vec3T nor=_poses[f]->getEnv()->phiGrad(pos);
      pos=ROT(TJ).transpose()*(pos-CTR(TJ));
      nor=ROT(TJ).transpose()*nor;
      DOF+=_ees[f]->getDOFAll(*_body,pos.template cast<EndEffectorBounds::T>(),nor.template cast<EndEffectorBounds::T>()).template cast<T>();
    }
    ret.col(c)=DOF;
  }
  return ret;
}
template class NESimplifiedDynamicsSequence<FLOAT>;
}
