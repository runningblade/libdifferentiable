#include "GUROBIInterface.h"
#ifdef GUROBI_SUPPORT
#include "gurobi_c.h"

namespace PHYSICSMOTION {
//GUROBIInterface
#define GRB_SAFE_CALL(X) {err=X;ASSERT_MSGV(!err,"Error calling GUROBI function: %s,code=%d",STRINGIFY_OMP(X),err)}
template <typename T>
GUROBIInterface<T>::GUROBIInterface() {
  int err=0;
  GRB_SAFE_CALL(GRBemptyenv(&_env))
  GRB_SAFE_CALL(GRBstartenv(_env))
}
template <typename T>
GUROBIInterface<T>::~GUROBIInterface() {
  GRBfreeenv(_env);
}
template <typename T>
bool GUROBIInterface<T>::optimize(SQPObjectiveCompound<T>& obj,bool callback,double tol,int maxNode,int maxTime,Vec& x) {
  reset(obj,callback,tol,maxNode,maxTime);
  int err=0;
  int status;
  Eigen::Matrix<double,-1,1> xD;
  xD.resize(x.size());
  GRB_SAFE_CALL(GRBoptimize(_model))
  GRB_SAFE_CALL(GRBgetintattr(_model,GRB_INT_ATTR_STATUS,&status))
  GRB_SAFE_CALL(GRBgetdblattrarray(_model,GRB_DBL_ATTR_X,0,x.size(),xD.data()))
  bool succ=status==GRB_OPTIMAL||
            status==GRB_SUBOPTIMAL||
            status==GRB_INF_OR_UNBD||
            status==GRB_UNBOUNDED||
            status==GRB_NODE_LIMIT||
            status==GRB_TIME_LIMIT;
  if(succ)
    x=xD.template cast<T>();
  GRB_SAFE_CALL(GRBfreemodel(_model))
  return succ;
}
template <typename T>
void GUROBIInterface<T>::reset(SQPObjectiveCompound<T>& obj,bool callback,double tol,int maxNode,int maxTime) {
  _inputs=obj.inputs();
  _values=obj.values();
  obj.assemblePoly();
  int err=0;
  GRB_SAFE_CALL(GRBsetintparam(_env,"OutputFlag",callback))
  GRB_SAFE_CALL(GRBsetdblparam(_env,"OptimalityTol",tol))
  GRB_SAFE_CALL(GRBsetdblparam(_env,"NodeLimit",maxNode))
  GRB_SAFE_CALL(GRBsetdblparam(_env,"TimeLimit",maxTime))
  GRB_SAFE_CALL(GRBnewmodel(_env,&_model,"GUROBIModel",0,NULL,NULL,NULL,NULL,NULL))
  //add variables
  typedef char* charPtr;
  charPtr* names=new charPtr[_inputs];
  Eigen::Matrix<double,-1,1> lb=Eigen::Matrix<double,-1,1>::Zero(_inputs);
  Eigen::Matrix<double,-1,1> ub=Eigen::Matrix<double,-1,1>::Zero(_inputs);
  Eigen::Matrix<char,-1,1> vtype=Eigen::Matrix<char,-1,1>::Zero(_inputs);
  for(const std::pair<std::string,SQPVariable<T>>& v:obj.vars()) {
    names[v.second._id]=(char*)v.first.c_str();
    vtype[v.second._id]=v.second._isBinary?GRB_BINARY:GRB_CONTINUOUS;
    lb[v.second._id]=v.second._l==-SQPObjective<T>::infty()?-GRB_INFINITY:(double)v.second._l;
    ub[v.second._id]=v.second._u== SQPObjective<T>::infty()? GRB_INFINITY:(double)v.second._u;
  }
  GRB_SAFE_CALL(GRBaddvars(_model,_inputs,0,NULL,NULL,NULL,NULL,lb.data(),ub.data(),vtype.data(),names))
  delete [] names;
  //objective
  std::shared_ptr<typename SQPObjectiveComponent<T>::PolyXT> polyCompObj=obj.getPolynomialComponentObjective();
  Eigen::SparseMatrix<double,0,int> HD,GD;
  std::vector<int> rowsl,rowsq,colsq;
  std::vector<double> datal,dataq;
  if(polyCompObj) {
    //order0
    double c0=(double)(T)polyCompObj->retainOrder(0);
    //order1
    rowsl.clear();
    datal.clear();
    GD=polyCompObj->gradientSparse(1).template cast<T>().template cast<double>();
    for(typename Eigen::SparseMatrix<double,0,int>::InnerIterator it(GD,0); it; ++it) {
      rowsl.push_back(it.row());
      datal.push_back(it.value());
    }
    //order2
    rowsq.clear();
    colsq.clear();
    dataq.clear();
    HD=polyCompObj->hessianSparse(2).template cast<T>().template cast<double>();
    for(int k=0; k<HD.outerSize(); ++k)
      for(typename Eigen::SparseMatrix<double,0,int>::InnerIterator it(HD,k); it; ++it)
        if(it.row()>=it.col()) {
          rowsq.push_back(it.row());
          colsq.push_back(it.col());
          dataq.push_back(it.row()==it.col()?it.value()/2:it.value());
        }
    GRB_SAFE_CALL(GRBsetobjective(_model,GRB_MINIMIZE,c0,
                                  rowsl.size(),rowsl.data(),datal.data(),
                                  rowsq.size(),rowsq.data(),colsq.data(),dataq.data()))
    //remaining terms will be ignored
  }
  //constraints
  Eigen::Matrix<double,-1,1> gl=obj.gl().template cast<double>();
  Eigen::Matrix<double,-1,1> gu=obj.gu().template cast<double>();
  std::unordered_map<int,typename SQPObjectiveComponent<T>::PolyXT> polyCompCons=obj.getPolynomialComponentConstraint();
  ASSERT_MSGV(gl.size()==(int)polyCompCons.size(),"gl/gu.size()=%d!=%d=#PolynomialConstraints, GUROBI can only handle polynomial constraints!",(int)gl.size(),(int)polyCompCons.size())
  for(const std::pair<int,typename SQPObjectiveComponent<T>::PolyXT>& c:polyCompCons) {
    //order0
    double c0=(double)(T)c.second.retainOrder(0);
    //order1
    rowsl.clear();
    datal.clear();
    GD=c.second.gradientSparse(1).template cast<T>().template cast<double>();
    for(typename Eigen::SparseMatrix<double,0,int>::InnerIterator it(GD,0); it; ++it) {
      rowsl.push_back(it.row());
      datal.push_back(it.value());
    }
    //order2
    rowsq.clear();
    colsq.clear();
    dataq.clear();
    HD=c.second.hessianSparse(2).template cast<T>().template cast<double>();
    for(int k=0; k<HD.outerSize(); ++k)
      for(typename Eigen::SparseMatrix<double,0,int>::InnerIterator it(HD,k); it; ++it)
        if(it.row()>=it.col()) {
          rowsq.push_back(it.row());
          colsq.push_back(it.col());
          dataq.push_back(it.row()==it.col()?it.value()/2:it.value());
        }
    if(rowsq.empty()) {
      //this is a linear constraint
      if(gu[c.first]< SQPObjective<T>::infty()) {
        GRB_SAFE_CALL(GRBaddconstr(_model,rowsl.size(),rowsl.data(),datal.data(),GRB_LESS_EQUAL,   gu[c.first]-c0,NULL))
      }
      if(gl[c.first]>-SQPObjective<T>::infty()) {
        GRB_SAFE_CALL(GRBaddconstr(_model,rowsl.size(),rowsl.data(),datal.data(),GRB_GREATER_EQUAL,gl[c.first]-c0,NULL))
      }
    } else {
      if(gu[c.first]< SQPObjective<T>::infty() && gl[c.first]>-SQPObjective<T>::infty()) {
        ASSERT_MSG(false,"Two-sided quadratic constraint cannot be convex and cannot be handled by GUROBI!")
      } else if(gu[c.first]< SQPObjective<T>::infty()) {
        GRB_SAFE_CALL(GRBaddqconstr(_model,
                                    rowsl.size(),rowsl.data(),datal.data(),
                                    rowsq.size(),rowsq.data(),colsq.data(),dataq.data(),
                                    GRB_LESS_EQUAL,   gu[c.first]-c0,NULL))
      } else if(gl[c.first]>-SQPObjective<T>::infty()) {
        GRB_SAFE_CALL(GRBaddqconstr(_model,
                                    rowsl.size(),rowsl.data(),datal.data(),
                                    rowsq.size(),rowsq.data(),colsq.data(),dataq.data(),
                                    GRB_GREATER_EQUAL,gl[c.first]-c0,NULL))
      }
    }
    //remaining terms will be ignored
  }
}
template <typename T>
bool GUROBIInterface<T>::QPSolve(const MatT& Hn,const Vec& Gn,const Vec& Cn,const MatT& Jn,Vec& x,bool callback) const {
  int err=0;
  GRBmodel* model;
  Eigen::Matrix<double,-1,1> GnD=Gn.template cast<double>();
  GRB_SAFE_CALL(GRBsetintparam(_env,"OutputFlag",callback))
  GRB_SAFE_CALL(GRBnewmodel(_env,&model,"GUROBIModel",0,NULL,NULL,NULL,NULL,NULL))
  //objective
  std::vector<int> rowsq,colsq;
  std::vector<double> dataq;
  for(int r=0; r<Hn.rows(); r++)
    for(int c=0; c<=r; c++) {
      rowsq.push_back(r);
      colsq.push_back(c);
      dataq.push_back(double(r==c?Hn(r,c)/2:Hn(r,c)));
    }
  GRB_SAFE_CALL(GRBaddvars(model,Gn.size(),0,NULL,NULL,NULL,GnD.data(),NULL,NULL,NULL,NULL))
  GRB_SAFE_CALL(GRBaddqpterms(model,rowsq.size(),rowsq.data(),colsq.data(),dataq.data()))
  //constraint
  rowsq.clear();
  for(int c=0; c<Jn.cols(); c++)
    rowsq.push_back(c);
  for(int r=0; r<Jn.rows(); r++) {
    dataq.clear();
    for(int c=0; c<Jn.cols(); c++)
      dataq.push_back(double(Jn(r,c)));
    GRB_SAFE_CALL(GRBaddconstr(model,Jn.cols(),rowsq.data(),dataq.data(),GRB_GREATER_EQUAL,double(-Cn[r]),""))
  }
  //solve
  int status;
  Eigen::Matrix<double,-1,1> xD;
  xD.resize(x.size());
  GRB_SAFE_CALL(GRBoptimize(model))
  GRB_SAFE_CALL(GRBgetintattr(model,GRB_INT_ATTR_STATUS,&status))
  GRB_SAFE_CALL(GRBgetdblattrarray(model,GRB_DBL_ATTR_X,0,x.size(),xD.data()))
  bool succ=status==GRB_OPTIMAL||
            status==GRB_SUBOPTIMAL||
            status==GRB_INF_OR_UNBD||
            status==GRB_UNBOUNDED||
            status==GRB_NODE_LIMIT||
            status==GRB_TIME_LIMIT;
  if(succ)
    x=xD.template cast<T>();
  GRB_SAFE_CALL(GRBfreemodel(model))
  return succ;
}
template <typename T>
bool GUROBIInterface<T>::QCPSolve(const MatT& Ht,const Vec& Gt,const Vec& Bt,Vec& x,bool callback) const {
  int err=0;
  GRBmodel* model;
  Eigen::Matrix<double,-1,1> GtD=Gt.template cast<double>();
  GRB_SAFE_CALL(GRBsetintparam(_env,"OutputFlag",callback))
  GRB_SAFE_CALL(GRBnewmodel(_env,&model,"GUROBIModel",0,NULL,NULL,NULL,NULL,NULL))
  //objective
  std::vector<int> rowsq,colsq;
  std::vector<double> dataq;
  for(int r=0; r<Ht.rows(); r++)
    for(int c=0; c<=r; c++) {
      rowsq.push_back(r);
      colsq.push_back(c);
      dataq.push_back(double(r==c?Ht(r,c)/2:Ht(r,c)));
    }
  GRB_SAFE_CALL(GRBaddvars(model,Gt.size(),0,NULL,NULL,NULL,GtD.data(),NULL,NULL,NULL,NULL))
  GRB_SAFE_CALL(GRBaddqpterms(model,rowsq.size(),rowsq.data(),colsq.data(),dataq.data()))
  //constraint
  for(int r=0; r<Bt.rows(); r++) {
    rowsq.clear();
    rowsq.push_back(r*2+0);
    rowsq.push_back(r*2+1);
    colsq.clear();
    colsq.push_back(r*2+0);
    colsq.push_back(r*2+1);
    dataq.clear();
    dataq.push_back(1);
    dataq.push_back(1);
    GRB_SAFE_CALL(GRBaddqconstr(model,0,NULL,NULL,2,rowsq.data(),colsq.data(),dataq.data(),GRB_LESS_EQUAL,double(Bt[r]*Bt[r]),""))
  }
  //solve
  int status;
  Eigen::Matrix<double,-1,1> xD;
  xD.resize(x.size());
  GRB_SAFE_CALL(GRBoptimize(model))
  GRB_SAFE_CALL(GRBgetintattr(model,GRB_INT_ATTR_STATUS,&status))
  GRB_SAFE_CALL(GRBgetdblattrarray(model,GRB_DBL_ATTR_X,0,x.size(),xD.data()))
  bool succ=status==GRB_OPTIMAL||
            status==GRB_SUBOPTIMAL||
            status==GRB_INF_OR_UNBD||
            status==GRB_UNBOUNDED||
            status==GRB_NODE_LIMIT||
            status==GRB_TIME_LIMIT;
  if(succ)
    x=xD.template cast<T>();
  GRB_SAFE_CALL(GRBfreemodel(model))
  return succ;
}
template class GUROBIInterface<FLOAT>;
#ifdef FORCE_ADD_DOUBLE_PRECISION
template struct GUROBIInterface<double>;
#endif
}

#endif
