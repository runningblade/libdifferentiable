#include "PositionObj.h"
#include "PBCDMDynamicsSequence.h"
#include <Environment/DeformedEnvironment.h>

namespace PHYSICSMOTION {
template <typename T>
PositionObj<T>::PositionObj(SQPObjectiveCompound<T>& obj,const std::string& name,int frm,const Vec3T& target,const Eigen::Matrix<T,-1,3>& mask,T coef,std::shared_ptr<PBDDynamicsSequence<T>> dyn)
  :SQPObjectiveComponent<T>(obj,name+"PositionObj<"+std::to_string(frm)+">"),_mask(mask),_target(target),_t(-1),_coef(coef) {
  if(_coef<=0)
    for(int i=0; i<mask.rows(); i++) {
      _gl.push_back(target.dot(mask.row(i)));
      _gu.push_back(target.dot(mask.row(i)));
    }
  const Joint& J=dyn->body().joint(0);
  ASSERT_MSG(J._typeJoint==Joint::TRANS_2D||J._typeJoint==Joint::TRANS_3D,"Joint(0) must have type TRANS_2D or TRANS_3D in order to use PositionObj!")
  if(J._typeJoint==Joint::TRANS_2D) {
    _vars.resize(2);
    _vars[0]=dyn->DOFVar()(0,frm);
    _vars[1]=dyn->DOFVar()(1,frm);
  } else {
    _vars.resize(3);
    _vars[0]=dyn->DOFVar()(0,frm);
    _vars[1]=dyn->DOFVar()(1,frm);
    _vars[2]=dyn->DOFVar()(2,frm);
  }
  std::shared_ptr<PBCDMDynamicsSequence<T>> dynPBCDM=std::dynamic_pointer_cast<PBCDMDynamicsSequence<T>>(dyn);
  _DEnv=dynPBCDM->getDeformedEnvironment();
  ASSERT_MSG(!_DEnv || _vars.size()==3,"PositionObj with DeformedEnvironment can only work in 3D!")
}
template <typename T>
PositionObj<T>::PositionObj(SQPObjectiveCompound<T>& obj,const std::string& name,T t,const Vec3T& target,const Eigen::Matrix<T,-1,3>& mask,T coef,std::shared_ptr<PositionSequence<T>> pos)
  :SQPObjectiveComponent<T>(obj,name+"PositionObj"+pos->_name),_pos(pos),_mask(mask),_target(target),_t(t),_coef(coef) {
  ASSERT_MSG(t>0,"Time must be bigger than zero for PositionObj!")
  if(_coef<=0)
    for(int i=0; i<mask.rows(); i++) {
      _gl.push_back(target.dot(mask.row(i)));
      _gu.push_back(target.dot(mask.row(i)));
    }
}
template <typename T>
PositionObj<T>::PositionObj(SQPObjectiveCompound<T>& obj,const std::string& name,const Eigen::Matrix<int,3,1>& var,const Vec3T& target,const Eigen::Matrix<T,-1,3>& mask,T coef)
  :SQPObjectiveComponent<T>(obj,name+"PositionObj"),_mask(mask),_target(target),_t(-1),_coef(coef) {
  if(_coef<=0)
    for(int i=0; i<mask.rows(); i++) {
      _gl.push_back(target.dot(mask.row(i)));
      _gu.push_back(target.dot(mask.row(i)));
    }
  _vars=var;
}
template <typename T>
T PositionObj<T>::operator()(const Vec& x,Vec* fgrad) {
  if(_coef<=0)
    return 0;   //this is a constraint
  else if(_t<0) {
    if(_DEnv) {
      Mat3T J;
      ASSERT(_vars.size()==3)
      Vec3T diff=_DEnv->forward(Vec3T(x[_vars[0]],x[_vars[1]],x[_vars[2]]),fgrad?&J:NULL,NULL)-_target;
      Vec3T mDiff=_mask*diff,grad;
      if(fgrad) {
        grad=J.transpose()*_mask.transpose()*mDiff*_coef;
        for(int r=0; r<_vars.size(); r++)
          (*fgrad)[_vars[r]]+=grad[r];
      }
      return mDiff.squaredNorm()*_coef/2;
    } else {
      Vec3T diff=-_target,grad;
      for(int r=0; r<_vars.size(); r++)
        diff+=x[_vars[r]]*Vec3T::Unit(r);
      Vec mDiff=_mask*diff;
      if(fgrad) {
        grad=_mask.transpose()*mDiff*_coef;
        for(int r=0; r<_vars.size(); r++)
          (*fgrad)[_vars[r]]+=grad[r];
      }
      return mDiff.squaredNorm()*_coef/2;
    }
  } else {
    Vec3T gT,grad;
    GRAD_INTERP g[3];
    Vec3T diff=_pos->timeInterpolateStencil(x,_t,fgrad?g:NULL,fgrad?&gT:NULL)-_target;
    Vec mDiff=_mask*diff;
    if(fgrad) {
      grad=_mask.transpose()*mDiff*_coef;
      for(int c=0; c<3; c++)
        for(const std::pair<const int,T>& v:g[c])
          (*fgrad)[v.first]+=grad[c]*v.second;
    }
    return mDiff.squaredNorm()*_coef/2;
  }
}
template <typename T>
int PositionObj<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  if(_coef>0)
    return 0;   //this is an objective
  else if(_t<0) {
    if(_DEnv) {
      Mat3T J;
      ASSERT(_vars.size()==3)
      Vec3T diff=_DEnv->forward(Vec3T(x[_vars[0]],x[_vars[1]],x[_vars[2]]),fjac?&J:NULL,NULL);
      fvec.segment(_offset,_mask.rows())=_mask*diff;
      if(fjac) {
        MatT MJ=_mask*J;
        for(int r=0; r<_vars.size(); r++)
          addBlock(*fjac,_offset,_vars[r],MJ.col(r));
      }
    } else {
      Vec3T diff=Vec3T::Zero();
      for(int r=0; r<_vars.size(); r++)
        diff+=x[_vars[r]]*Vec3T::Unit(r);
      fvec.segment(_offset,_mask.rows())=_mask*diff;
      if(fjac)
        for(int r=0; r<_vars.size(); r++)
          addBlock(*fjac,_offset,_vars[r],_mask.col(r));
    }
  } else {
    Vec3T gT;
    GRAD_INTERP g[3];
    Vec3T diff=_pos->timeInterpolateStencil(x,_t,fjac?g:NULL,fjac?&gT:NULL);
    fvec.segment(_offset,_mask.rows())=_mask*diff;
    if(fjac)
      for(int c=0; c<3; c++)
        for(const std::pair<const int,T>& v:g[c])
          addBlock(*fjac,_offset,v.first,_mask.col(c)*v.second);
  }
  return 0;
}
template class PositionObj<FLOAT>;
}
