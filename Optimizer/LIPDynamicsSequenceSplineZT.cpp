#include "LIPDynamicsSequenceSplineZT.h"
#include <Articulated/NEArticulatedGradientInfo.h>
#include <Utils/RotationUtils.h>
#include <Utils/Interp.h>

namespace PHYSICSMOTION {
template <typename T>
LIPDynamicsSequenceSplineZT<T>::LIPDynamicsSequenceSplineZT
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec3T& G,T h0,
 std::shared_ptr<ArticulatedBody> bodySimplified,
 const std::vector<std::shared_ptr<FootStepSequence<T>>>& footSteps,bool isBoundingCircle,
 std::shared_ptr<PhaseSequence<T>> phase,const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int NCToFix,bool inherited)
  :LIPDynamicsSequence<T>(obj,inherited?name:name+"LIPDynamicsSequenceSplineZT",dt,G,h0,bodySimplified,footSteps,isBoundingCircle,ees,NCToFix,true) {
  if(inherited)
    return;
  ASSERT_MSG(_bodySimplified->nrDOF()==6,"NESimplifiedDynamicsSequence must have 6 degrees of freedom!")
  int N=footSteps[0]->nrTimestep();
  int NF=(int)_footSteps.size();
  _DOFVars.resize(2,N);
  for(int c=0; c<N; c++)
    for(int r=0; r<2; r++)  //x,y,z,theta
      _DOFVars(r,c)=obj.addVar(DOFVar(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  _ZTSeq.reset(new VectorSequence<T,2>(obj,_name+"LIPZT",2,phase));
  buildStencil(obj);
  for(int c=_posConsStart; c<N; c++)
    //foot reachable constraint
    for(int f=0; f<NF; f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      EndEffectorBounds::Vec3T frameTCtr=ee->_frame.transpose()*ee->_ctrBB;
      if(_isBoundingCircle) {
        _gl.push_back(-SQPObjective<T>::infty()); //x,y
        _gu.push_back(pow(ee->getBBRadius(),2));
        _gl.push_back(ee->_bb[2]*ee->_res+frameTCtr[2]);  //z
        _gu.push_back(ee->_bb[5]*ee->_res+frameTCtr[2]);
      } else {
        for(int r=0; r<3; r++) {
          _gl.push_back(ee->_bb[r+0]*ee->_res+frameTCtr[r]);
          _gu.push_back(ee->_bb[r+3]*ee->_res+frameTCtr[r]);
        }
      }
    }
  for(int c=2; c<N; c++)
    //dynamics constraint
    for(int r=0; r<2; r++) {
      _gl.push_back(0);
      _gu.push_back(0);
    }
  //lambda sum to one
  _lambdas.setConstant(NF,N,-1);
  for(int c=2; c<N; c++) {
    PolyXT sumLambda;
    for(int f=0; f<NF; f++)
      if(footSteps[f]->isStance(c)) {
        _lambdas(f,c)=obj.addVar(lambdaVar(f,c),0,1,SQPObjectiveCompound<T>::MUST_NEW)._id;
        sumLambda+=obj.addVarPoly(_lambdas(f,c));
      }
    //lambda sum to one
    SQPObjectiveComponent<T>::setPolynomialComponentConstraint((int)_gl.size(),sumLambda);
    _gl.push_back(1);
    _gu.push_back(1);
  }
}
template <typename T>
int LIPDynamicsSequenceSplineZT<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  std::vector<Vec> vss(_footSteps.size());
  for(int f=0; f<(int)vss.size(); f++)
    vss[f]=_footSteps[f]->pos(x);
  //reachable constraint
  int N=_footSteps[0]->nrTimestep();
  Vec DOFZT=_ztInterp.transpose()*x.segment(_rangeZT[0],_rangeZT[1]);
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<N; c++) {
    Vec3T DRDZ,rel,ctrEE,relC,ctr=Vec3T(x[_DOFVars(0,c)],x[_DOFVars(1,c)],DOFZT[c*2+0]);
    Mat3T R=expWZ<T,Vec3T>(DOFZT[c*2+1],&DRDZ);
    for(int f=0; f<(int)_ees.size(); f++) {
      rel=vss[f].template segment<3>(c*3)-ctr;
      ctrEE=_ees[f]->getBBCenter().template cast<T>();
      relC=vss[f].template segment<3>(c*3)-ctr-R*ctrEE;
      int offset=_offset+((c-_posConsStart)*(int)_ees.size()+f)*(_isBoundingCircle?2:3);
      if(_isBoundingCircle) {
        fvec[offset]=relC.template segment<2>(0).squaredNorm();
        fvec[offset+1]=R.col(2).dot(rel);
        if(fjac) {
          for(int d=0; d<2; d++) {  //x,y
            fjac->push_back(STrip(offset,_DOFVars(d,c),-2*relC[d]));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              addBlock(*fjac,offset,it.row(),T(2)*relC[d]*it.value());
          }
          T dR=-2*((cross(DRDZ)*R)*ctrEE).template segment<2>(0).dot(relC.template segment<2>(0));
          for(typename SMatT::InnerIterator it(_ztInterp,c*2+1); it; ++it)
            addBlock(*fjac,offset,it.row()+_rangeZT[0],T(dR*it.value()));
          for(int d=0; d<2; d++) {  //z
            fjac->push_back(STrip(offset+1,_DOFVars(d,c),-R(d,2)));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              fjac->push_back(STrip(offset+1,it.row(),R(d,2)*it.value()));
          }
          {
            for(typename SMatT::InnerIterator it(_ztInterp,c*2); it; ++it)
              fjac->push_back(STrip(offset+1,it.row()+_rangeZT[0],-R(2,2)*it.value()));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+2); it; ++it)
              fjac->push_back(STrip(offset+1,it.row(),R(2,2)*it.value()));
          }
          dR=(cross(DRDZ)*R).col(2).dot(rel);
          for(typename SMatT::InnerIterator it(_ztInterp,c*2+1); it; ++it)
            fjac->push_back(STrip(offset+1,it.row()+_rangeZT[0],dR*it.value()));
        }
      } else {
        fvec.template segment<3>(offset)=R.transpose()*rel;
        if(fjac) {
          for(int d=0; d<2; d++) {
            addBlock(*fjac,offset,_DOFVars(d,c),-R.row(d).transpose());
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              addBlock(*fjac,offset,it.row(),R.row(d).transpose()*it.value());
          }
          {
            for(typename SMatT::InnerIterator it(_ztInterp,c*2); it; ++it)
              addBlock(*fjac,offset,it.row()+_rangeZT[0],-R.row(2).transpose()*it.value());
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+2); it; ++it)
              addBlock(*fjac,offset,it.row(),R.row(2).transpose()*it.value());
          }
          Vec3T dR=(cross(DRDZ)*R).transpose()*rel;
          for(typename SMatT::InnerIterator it(_ztInterp,c*2+1); it; ++it)
            addBlock(*fjac,offset,it.row()+_rangeZT[0],dR*it.value());
        }
      }
    }
  }
  //dynamics constraint
  int offsetD=_offset+(N-_posConsStart)*(int)_ees.size()*(_isBoundingCircle?2:3);
  int NF=(int)_footSteps.size();
  T coefAccel=1/_dt/_dt;
  OMP_PARALLEL_FOR_
  for(int c=2; c<N; c++) {
    int offset=offsetD+(c-2)*2;
    for(int d=0; d<2; d++) {
      T u=0;
      for(int f=0; f<NF; f++)
        if(_lambdas(f,c)>=0)
          u+=vss[f](c*3+d)*x[_lambdas(f,c)];
      T accel=(x[_DOFVars(d,c)]-2*x[_DOFVars(d,c-1)]+x[_DOFVars(d,c-2)])*coefAccel;
      fvec[offset+d]=accel-_w*(x[_DOFVars(d,c)]-u);
      if(fjac) {
        for(int f=0; f<NF; f++)
          if(_lambdas(f,c)>=0) {
            fjac->push_back(STrip(offset+d,_lambdas(f,c),vss[f](c*3+d)*_w));
            for(typename SMatT::InnerIterator it(_footSteps[f]->getStencil(),c*3+d); it; ++it)
              fjac->push_back(STrip(offset+d,it.row(),_w*x[_lambdas(f,c)]*it.value()));
          }
        fjac->push_back(STrip(offset+d,_DOFVars(d,c),coefAccel-_w));
        fjac->push_back(STrip(offset+d,_DOFVars(d,c-1),-2*coefAccel));
        fjac->push_back(STrip(offset+d,_DOFVars(d,c-2),coefAccel));
      }
    }
  }
  return 0;
}
template <typename T>
Eigen::Matrix<int,-1,-1> LIPDynamicsSequenceSplineZT<T>::DOFVarZT() const {
  return _ZTSeq->getVar();
}
template <typename T>
Eigen::Matrix<int,-1,-1> LIPDynamicsSequenceSplineZT<T>::DOFDiffVarZT() const {
  return _ZTSeq->getDiffVar();
}
template <typename T>
void LIPDynamicsSequenceSplineZT<T>::fixInitialPose(SQPObjectiveCompound<T>& obj,bool fixVelocity) {
  for(int r=0; r<2; r++)
    for(int c=0; c<(fixVelocity?2:1); c++) {
      SQPVariable<T>& var=obj.addVar(_DOFVars(r,c));
      var.fix(var._init);
    }
  for(int r=0; r<2; r++) {
    SQPVariable<T>& varZT=obj.addVar(_ZTSeq->getVar()(r,0));
    varZT.fix(varZT._init);
    if(fixVelocity) {
      SQPVariable<T>& diffVarZT=obj.addVar(_ZTSeq->getDiffVar()(r,0));
      diffVarZT.fix(diffVarZT._init);
    }
  }
}
template <typename T>
void LIPDynamicsSequenceSplineZT<T>::initDOF(SQPObjectiveCompound<T>& obj,int doubleHorizon,int singleHorizon,const std::vector<FootStep<T>>& steps) {
  int N=_footSteps[0]->nrTimestep(),offZT=0;
  Vec DOFZT=Vec::Zero(N*2);
  for(int c=0,offStencil=0; c<(int)steps.size()-1; c++) {
    for(int i=0; i<singleHorizon; i++,offStencil++) {
      Vec6T DOF=interp1D(steps[c]._bodyPose,steps[c+1]._bodyPose,i/(T)singleHorizon);
      obj.addVar(_DOFVars(0,offStencil))._init=DOF[0];
      obj.addVar(_DOFVars(1,offStencil))._init=DOF[1];
      DOFZT[offZT++]=DOF[2];
      DOFZT[offZT++]=DOF[5];
    }
    for(int i=0; i<doubleHorizon; i++,offStencil++) {
      Vec6T DOF=steps[c+1]._bodyPose;
      obj.addVar(_DOFVars(0,offStencil))._init=DOF[0];
      obj.addVar(_DOFVars(1,offStencil))._init=DOF[1];
      DOFZT[offZT++]=DOF[2];
      DOFZT[offZT++]=DOF[5];
    }
  }
  {
    //minimize_x ||_ztInterp.transpose()*x-DOFZT||^2
    Vec x=Eigen::SimplicialLDLT<SMatT>(_ztInterp*_ztInterp.transpose()).solve(_ztInterp*DOFZT);
    const Eigen::Matrix<int,-1,-1>& var=_ZTSeq->getVar();
    for(int r=0; r<var.rows(); r++)
      for(int c=0; c<var.cols(); c++)
        obj.addVar(var(r,c))._init=x[obj.addVar(var(r,c))._id-_rangeZT[0]];
    const Eigen::Matrix<int,-1,-1>& diffVar=_ZTSeq->getDiffVar();
    for(int r=0; r<diffVar.rows(); r++)
      for(int c=0; c<diffVar.cols(); c++)
        obj.addVar(diffVar(r,c))._init=x[obj.addVar(diffVar(r,c))._id-_rangeZT[0]];
  }
}
template <typename T>
typename LIPDynamicsSequenceSplineZT<T>::MatT LIPDynamicsSequenceSplineZT<T>::getPoses(const ArticulatedBody& body,const Environment<T>& env,const Vec& x) {
  std::vector<Vec> vss(_footSteps.size());
  for(int f=0; f<(int)vss.size(); f++)
    vss[f]=_footSteps[f]->pos(x);
  //compute full body pose
  MatT ret;
  Vec DOFZT=_ztInterp.transpose()*x.segment(_rangeZT[0],_rangeZT[1]);
  int N=_footSteps[0]->nrTimestep();
  ret.resize(body.nrDOF(),N);
  OMP_PARALLEL_FOR_
  for(int c=0; c<N; c++) {
    //base
    Vec DOF=Vec::Zero(body.nrDOF());
    DOF[0]=x[_DOFVars(0,c)];
    DOF[1]=x[_DOFVars(1,c)];
    DOF[2]=DOFZT[c*2+0];
    DOF[5]=DOFZT[c*2+1];
    NEArticulatedGradientInfo<T> info(*_bodySimplified,DOF.template segment<6>(0));
    Mat3X4T TJ=info.NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1);
    //xss
    for(int f=0; f<(int)_ees.size(); f++) {
      Vec3T pos=vss[f].template segment<3>(c*3);
      Vec3T nor=env.phiGrad(pos);
      pos=ROT(TJ).transpose()*(pos-CTR(TJ));
      nor=ROT(TJ).transpose()*nor;
      DOF+=_ees[f]->getDOFAll(body,pos.template cast<EndEffectorBounds::T>(),nor.template cast<EndEffectorBounds::T>()).template cast<T>();
    }
    ret.col(c)=DOF;
  }
  return ret;
}
template <typename T>
void LIPDynamicsSequenceSplineZT<T>::setTargetHeightEnergy(SQPObjectiveCompound<T>& obj,T hCoef,T coefLambdaEnergy) {
  PolyXT E;
  int N=_footSteps[0]->nrTimestep();
  int NF=(int)_footSteps.size();
  //objective: height should be as close to _h0 as possible
  if(hCoef>0)
    for(int c=_posConsStart; c<N; c++)
      for(int eid=0; eid<NF; eid++) {
        PolyXT heightDiff=getZTPoly(obj,c*2+0)-_footSteps[eid]->getPosPoly(obj,c*3+2)-PolyXT(_h0);
        E+=heightDiff*heightDiff*hCoef;
      }
  //objective: best lambda should evenly distribute robot's mass
  if(coefLambdaEnergy>0)
    for(int c=2; c<N; c++) {
      int NLambda=0;
      for(int f=0; f<NF; f++)
        if(_footSteps[f]->isStance(c))
          NLambda++;
      typename PolyXT::VECP lambdaPoly;
      lambdaPoly.resize(NLambda);
      for(int f=0,off=0; f<NF; f++)
        if(_footSteps[f]->isStance(c))
          lambdaPoly[off++]=obj.addVarPoly(_lambdas(f,c))-PolyXT(1./(T)NLambda);
      E+=lambdaPoly.squaredNorm()*coefLambdaEnergy;
    }
  SQPObjectiveComponent<T>::setPolynomialComponentObjective(E);
}
template <typename T>
typename LIPDynamicsSequenceSplineZT<T>::PolyXT LIPDynamicsSequenceSplineZT<T>::getZTPoly(SQPObjectiveCompound<T>& obj,int col) const {
  PolyXT ret;
  for(typename SMatT::InnerIterator it(_ztInterp,col); it; ++it)
    ret+=obj.addVarPoly(it.row()+_rangeZT[0])*PolyXT(it.value());
  return ret;
}
template <typename T>
void LIPDynamicsSequenceSplineZT<T>::buildStencil(const SQPObjectiveCompound<T>& obj) {
  int N=_footSteps[0]->nrTimestep();
  STrips trips;
  //ZT
  trips.clear();
  _rangeZT[0]=std::min(_ZTSeq->getVar().minCoeff(),_ZTSeq->getDiffVar().minCoeff());
  _rangeZT[1]=_ZTSeq->getVar().size()+_ZTSeq->getDiffVar().size();
  for(int c=0; c<N; c++) {
    GRAD_INTERP dofGrads[2];
    _ZTSeq->timeInterpolateStencil(obj.init(),c*_dt,dofGrads,NULL);
    for(int dof=0; dof<2; dof++)
      for(const std::pair<const int,T>& v:dofGrads[dof])
        trips.push_back(STrip(v.first-_rangeZT[0],c*2+dof,v.second));
  }
  _ztInterp.resize(_rangeZT[1],N*2);
  _ztInterp.setFromTriplets(trips.begin(),trips.end());
}
template class LIPDynamicsSequenceSplineZT<FLOAT>;
}
