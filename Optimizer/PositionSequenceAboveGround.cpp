#include "PositionSequenceAboveGround.h"

namespace PHYSICSMOTION {
template <typename T>
PositionSequenceAboveGround<T>::PositionSequenceAboveGround(SQPObjectiveCompound<T>& obj,const std::string& name,bool above,T dt,std::shared_ptr<PositionSequence<T>> pos,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"PositionSequence"+(above?std::string("Above"):std::string("Below"))+"Ground"),_pos(pos),_dt(dt) {
  if(inherited)
    return;
  int nrStep=(int)floor(_pos->getPhase()->horizonRef()/_dt);
  for(int c=0; c<nrStep; c++)
    if(above) {
      _gl.push_back(_pos->getPhi0());
      _gu.push_back(SQPObjective<T>::infty());
    } else {
      _gl.push_back(-SQPObjective<T>::infty());
      _gu.push_back(_pos->getPhi0());
    }
}
template <typename T>
int PositionSequenceAboveGround<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  std::shared_ptr<Environment<T>> env=_pos->getEnv();
  OMP_PARALLEL_FOR_
  for(int c=0; c<(int)_gl.size(); c++) {
    Vec3T gradTime,g;
    GRAD_INTERP posGrad[3];
    Vec3T posRef=_pos->timeInterpolateStencil(x,c*_dt,fjac?posGrad:NULL,fjac?&gradTime:NULL);
    fvec[_offset+c]=env->phi(posRef,fjac?&g:NULL);
    if(fjac)
      for(int r=0; r<3; r++)
        for(const std::pair<const int,T>& v:posGrad[r])
          fjac->push_back(STrip(_offset+c,v.first,g[r]*v.second));
  }
  return 0;
}
template class PositionSequenceAboveGround<FLOAT>;
}
