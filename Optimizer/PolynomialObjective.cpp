#include "PolynomialObjective.h"

namespace PHYSICSMOTION {
template <typename T>
PolynomialObjective<T>::PolynomialObjective(SQPObjectiveCompound<T>& obj,const std::string& name)
  :SQPObjectiveComponent<T>(obj,name+"PolynomialObjective") {}
template <typename T>
PolynomialObjective<T>::PolynomialObjective(SQPObjectiveCompound<T>& obj,const std::string& name,const PolyXT& objPoly)
  :SQPObjectiveComponent<T>(obj,name) {
  SQPObjectiveComponent<T>::addPolynomialComponentObjective(objPoly);
}
template <typename T>
PolynomialObjective<T>::PolynomialObjective
(SQPObjectiveCompound<T>& obj,const std::string& name,const PolyXT& con,const Vec2T& bound)
  :SQPObjectiveComponent<T>(obj,name) {
  _gl.push_back(bound[0]);
  _gu.push_back(bound[1]);
  SQPObjectiveComponent<T>::addPolynomialComponentConstraint(0,con);
}
template <typename T>
PolynomialObjective<T>::PolynomialObjective
(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<PolyXT>& cons,
 const std::vector<Vec2T>& bounds)
  :SQPObjectiveComponent<T>(obj,name) {
  for(const Vec2T& bd:bounds) {
    _gl.push_back(bd[0]);
    _gu.push_back(bd[1]);
  }
  int off=0;
  for(const PolyXT& c:cons)
    SQPObjectiveComponent<T>::addPolynomialComponentConstraint(off++,c);
}
template class PolynomialObjective<FLOAT>;
}
