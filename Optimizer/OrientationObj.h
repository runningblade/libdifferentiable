#ifndef ORIENTATION_OBJ_H
#define ORIENTATION_OBJ_H

#include "PBDDynamicsSequence.h"
#include "NEDynamicsSequence.h"
#include <unordered_map>

namespace PHYSICSMOTION {
template <typename T>
class OrientationObj : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  OrientationObj(SQPObjectiveCompound<T>& obj,const std::string& name,int frm,const Mat3T& target,T coef,std::shared_ptr<PBDDynamicsSequence<T>> dyn);
  virtual T operator()(const Vec& x,Vec* fgrad) override;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
 protected:
  Eigen::Matrix<int,-1,1> _vars;
  Mat3T _target;
  bool _isEXP;
  T _coef;
};
}

#endif
