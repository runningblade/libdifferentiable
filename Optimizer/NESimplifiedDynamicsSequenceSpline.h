#ifndef NE_SIMPLIFIED_DYNAMICS_SEQUENCE_SPLINE_H
#define NE_SIMPLIFIED_DYNAMICS_SEQUENCE_SPLINE_H

#include "NEDynamicsSequence.h"
#include <Articulated/NEArticulatedGradientInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class NESimplifiedDynamicsSequenceSpline : public NEDynamicsSequence<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(PBDDynamicsSequence<T>)
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using PBDDynamicsSequence<T>::_body;
  using PBDDynamicsSequence<T>::_poses;
  using PBDDynamicsSequence<T>::_forces;
  using PBDDynamicsSequence<T>::_ees;
  using PBDDynamicsSequence<T>::_dt;
  using PBDDynamicsSequence<T>::_C;
  using PBDDynamicsSequence<T>::_JRCF;
  using PBDDynamicsSequence<T>::_DOFVars;
  using PBDDynamicsSequence<T>::_posConsStart;
  using PBDDynamicsSequence<T>::DOFVar;
  using NEDynamicsSequence<T>::_infosNE;
  using NEDynamicsSequence<T>::_joints;
  using NEDynamicsSequence<T>::_a0;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  NESimplifiedDynamicsSequenceSpline
  (SQPObjectiveCompound<T>& obj,const std::string& name,T dt,
   std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodySimplified,const Vec3T& G,
   std::shared_ptr<PhaseSequence<T>> phase,
   const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
   const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
   const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0) override;
  virtual const Vec& controlCoef() const override;
  virtual Vec residualToControl() const override;
  virtual Vec controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet=NULL) const override;
  virtual SMatT controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet=NULL) const override;
  virtual Vec controlledDOFs(bool constrained,std::set<int>* consSet=NULL) const override;
  virtual SMatT controlledDOFsProj(bool constrained,std::set<int>* consSet=NULL) const override;
  virtual const Eigen::Matrix<int,-1,-1>& DOFVar() const override;
  virtual MatT getPoses(const Vec& x) override;
  std::shared_ptr<VectorSequence<T,6>> DOFSeq() const;
 protected:
  std::shared_ptr<ArticulatedBody> _bodySimplified;
  std::shared_ptr<VectorSequence<T,6>> _DOFSeq;
  Eigen::Matrix<int,2,1> _rangeX;
  SMatT _qInterp;
};
}

#endif
