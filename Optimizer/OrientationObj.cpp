#include "OrientationObj.h"
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
template <typename T>
OrientationObj<T>::OrientationObj(SQPObjectiveCompound<T>& obj,const std::string& name,int frm,const Mat3T& target,T coef,std::shared_ptr<PBDDynamicsSequence<T>> dyn)
  :SQPObjectiveComponent<T>(obj,name+"OrientationObj"+std::to_string(frm)),_target(target),_coef(coef) {
  if(_coef<=0)
    for(int i=0; i<9; i++) {
      _gl.push_back(target.data()[i]);
      _gu.push_back(target.data()[i]);
    }
  const Joint& J=dyn->body().joint(1);
  ASSERT_MSG(J._typeJoint==Joint::ROT_3D_EXP||J._typeJoint==Joint::ROT_3D_XYZ,"Joint(1) must have type ROT_3D_EXP or ROT_3D_XYZ in order to use OrientationObj!")
  _vars.resize(3);
  _vars[0]=dyn->DOFVar()(3,frm);
  _vars[1]=dyn->DOFVar()(4,frm);
  _vars[2]=dyn->DOFVar()(5,frm);
  _isEXP=J._typeJoint==Joint::ROT_3D_EXP;
}
template <typename T>
T OrientationObj<T>::operator()(const Vec& x,Vec* fgrad) {
  if(_coef<=0)
    return 0;   //this is a constraint
  else {
    Vec3T diff=Vec3T::Zero(),w[3];
    for(int r=0; r<_vars.size(); r++)
      diff[r]=x[_vars[r]];
    Mat3T dR,R;
    if(_isEXP)
      R=expWGradV<T,Vec3T>(diff,fgrad?w:NULL);
    else R=eulerX1Y3Z2<T,Vec3T>(diff,fgrad?w:NULL,NULL);
    Mat3T diffR=R-_target;
    if(fgrad)
      for(int r=0; r<_vars.size(); r++) {
        dR=cross<T>(w[r])*R;
        fgrad->coeffRef(_vars[r])+=(dR*diffR.transpose()).trace();
      }
    return diffR.squaredNorm()*_coef/2;
  }
}
template <typename T>
int OrientationObj<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  if(_coef>0)
    return 0;   //this is an objective
  else {
    Vec3T diff=Vec3T::Zero(),w[3];
    for(int r=0; r<_vars.size(); r++)
      diff[r]=x[_vars[r]];
    Mat3T dR,R;
    if(_isEXP)
      R=expWGradV<T,Vec3T>(diff,fjac?w:NULL);
    else R=eulerX1Y3Z2<T,Vec3T>(diff,fjac?w:NULL,NULL);
    fvec.template segment<9>(_offset).array()=Eigen::Map<const Eigen::Matrix<T,9,1>>(R.data());
    if(fjac)
      for(int r=0; r<_vars.size(); r++) {
        dR=cross<T>(w[r])*R;
        addBlock(*fjac,_offset,_vars[r],Eigen::Map<const Eigen::Matrix<T,9,1>>(dR.data()));
      }
  }
  return 0;
}
template class OrientationObj<FLOAT>;
}
