#include "InnerSphereDetection.h"

namespace PHYSICSMOTION {
InnerSphereDetection::InnerSphereDetection(SQPObjectiveCompound<T>& obj,const ConvexHullExact& convex,const Mat3X4T& trans,const Vec3T& up,T errThres)
  :SQPObjectiveComponent<T>(obj,"InnerSphereDetection") {
  _sphere[0]=obj.addVar("x",-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::NEW_OR_EXIST)._id;
  _sphere[1]=obj.addVar("y",-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::NEW_OR_EXIST)._id;
  _sphere[2]=obj.addVar("z",-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::NEW_OR_EXIST)._id;
  _sphere[3]=obj.addVar("r",-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::NEW_OR_EXIST)._id;
  //inside convex hull
  int offset=0;
  PolyXT rad=obj.addVarPoly(_sphere[3]);
  PolyXT::VECP p;
  p.resize(3);
  p[0]=obj.addVarPoly(_sphere[0]);
  p[1]=obj.addVarPoly(_sphere[1]);
  p[2]=obj.addVarPoly(_sphere[2]);
  for(int i=0; i<convex.nrPlane(); i++) {
    Eigen::Matrix<T,4,1> plane=convex.plane(i).template cast<T>();
    plane/=plane.template segment<3>(0).norm();
    plane.template segment<3>(0)=ROT(trans)*plane.template segment<3>(0);
    plane[3]-=plane.template segment<3>(0).dot(CTR(trans));
    PolyXT d=p[0]*plane[0]+p[1]*plane[1]+p[2]*plane[2]+plane[3];
    addPolynomialComponentConstraint(offset,d+rad);
    _gl.push_back(-SQPObjective<T>::infty());
    _gu.push_back(0);
    offset++;
  }
  //cannot be too far from bottom
  std::vector<Eigen::Matrix<double,3,1>> vss;
  std::vector<Eigen::Matrix<int,3,1>> iss;
  convex.getMesh(vss,iss);
  T smallestZ=std::numeric_limits<T>::max();
  for(const Eigen::Matrix<T,3,1>& v:vss)
    smallestZ=std::min(smallestZ,(ROT(trans)*v+CTR(trans)).dot(up));
  addPolynomialComponentConstraint(offset,p.dot(up.template cast<PolyXT>())-PolyXT(smallestZ)-rad*errThres-rad);
  _gl.push_back(-SQPObjective<T>::infty());
  _gu.push_back(0);
  //maximize radius
  addPolynomialComponentObjective((rad*-1.)+(p.dot(up.template cast<PolyXT>())+p.dot(p))*1e-6); //add a small regularization
}
Eigen::Matrix<InnerSphereDetection::T,3,1> InnerSphereDetection::localPos(const Vec& x) const {
  return Eigen::Matrix<T,3,1>(x[_sphere[0]],x[_sphere[1]],x[_sphere[2]]);
}
InnerSphereDetection::T InnerSphereDetection::phi0(const Vec& x) const {
  return x[_sphere[3]];
}
}
