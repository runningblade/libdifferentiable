#ifndef INNER_SPHERE_DETECTION_H
#define INNER_SPHERE_DETECTION_H

#include "SQPObjective.h"
#include <Articulated/ArticulatedBody.h>
#include <Environment/ConvexHullExact.h>

namespace PHYSICSMOTION {
struct InnerSphereDetection : public SQPObjectiveComponent<ArticulatedBody::T> {
 public:
  typedef ArticulatedBody::T T;
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  InnerSphereDetection(SQPObjectiveCompound<T>& obj,const ConvexHullExact& convex,const Mat3X4T& trans,const Vec3T& up,T errThres);
  Eigen::Matrix<T,3,1> localPos(const Vec& x) const;
  T phi0(const Vec& x) const;
 private:
  Eigen::Matrix<int,4,1> _sphere;
};
}

#endif
