#ifndef POLYNOMIAL_OBJECTIVE_H
#define POLYNOMIAL_OBJECTIVE_H

#include "SQPObjective.h"

namespace PHYSICSMOTION {
template <typename T>
class PolynomialObjective : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  PolynomialObjective(SQPObjectiveCompound<T>& obj,const std::string& name);
  PolynomialObjective(SQPObjectiveCompound<T>& obj,const std::string& name,const PolyXT& objPoly);
  PolynomialObjective(SQPObjectiveCompound<T>& obj,const std::string& name,const PolyXT& con,const Vec2T& bound);
  PolynomialObjective(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<PolyXT>& cons,const std::vector<Vec2T>& bounds);
};
}

#endif
