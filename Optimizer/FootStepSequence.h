#ifndef FOOT_STEP_SEQUENCE_H
#define FOOT_STEP_SEQUENCE_H

#include "VectorSequence.h"
#include <Planner/PlannerBase.h>

namespace PHYSICSMOTION {
template <typename T>
class FootStepSequence : public VectorSequence<T,2> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using SQPObjectiveComponent<T>::_name;
  using VectorSequence<T,2>::_even;
  using VectorSequence<T,2>::_var;
  using VectorSequence<T,2>::_diffVar;
  using VectorSequence<T,2>::var;
  using VectorSequence<T,2>::diffVar;
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  FootStepSequence(SQPObjectiveCompound<T>& obj,const std::string& name,char even,int eeId,T phi0,int doubleHorizon,int singleHorizon,
                   std::shared_ptr<Environment<T>> env,const std::vector<FootStep<T>>& steps,bool inherited=false);
  void init(SQPObjectiveCompound<T>& obj,const std::vector<FootStep<T>>& steps);
  PolyXT getPosPoly(SQPObjectiveCompound<T>& obj,int col) const;
  void setTargetEnergy(SQPObjectiveCompound<T>& obj,T coef);
  void fixInitialStep(SQPObjectiveCompound<T>& obj);
  void printStencilInfo(int DIM=1) const;
  const SMatT& getStencil() const;
  Vec pos(const Vec& x) const;
  bool isStance(int i) const;
  int nrTimestep() const;
 protected:
  std::string findVarName(int id) const;
  Eigen::Matrix<bool,-1,1> _stance;
  SMatT _stencil;
  Vec _pos0;
  int _eeId;
};
}

#endif
