#include "MixedIntegerObstacle.h"
#include "Environment/EnvironmentUtils.h"
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
template <typename T>
MixedIntegerObstacle<T>::MixedIntegerObstacle(SQPObjectiveCompound<T>& obj,const std::string& name,std::shared_ptr<ShapeExact> obstacle,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"MixedIntegerObstacle"),_obstacle(obstacle) {
  if(inherited)
    return;
  //get mesh
  std::vector<Eigen::Matrix<int,3,1>> iss;
  obstacle->getMesh(_vss,iss);
  makeConvexProject(_vss);
}
template <typename T>
void MixedIntegerObstacle<T>::addConstraint(SQPObjectiveCompound<T>& obj,int frame,const Eigen::Matrix<int,2,1>& bodyPoseDOF,T clearance,T bigM) {
  //rotation by 90 degrees
  Eigen::Matrix<double,2,2> R;
  R << 0,1,-1,0;
  //build clearance
  std::vector<int> weights;
  typename PolyXT::VECP bodyXY;
  bodyXY.resize(2);
  for(int d=0; d<2; d++)
    bodyXY[d]=obj.addVarPoly(bodyPoseDOF[d]);
  for(int i=0; i<(int)_vss.size(); i++) {
    weights.push_back(obj.addVar(_name+"["+std::to_string(frame)+"]["+std::to_string(i)+"]",0,1,SQPObjectiveCompound<T>::MUST_NEW)._id);
    Eigen::Matrix<double,2,1> a=_vss[i].template segment<2>(0);
    Eigen::Matrix<double,2,1> b=_vss[(i+1)%(int)_vss.size()].template segment<2>(0);
    Eigen::Matrix<double,2,1> n=(R*(b-a)).normalized();
    //constraint: <x-a,n>>=clearance-(1-weight)*bigM
    PolyXT con=(bodyXY-a.template cast<PolyXT>()).dot(n.template cast<PolyXT>())+(PolyXT(1)-obj.addVarPoly(weights[i]))*PolyXT(bigM);
    SQPObjectiveComponent<T>::addPolynomialComponentConstraint((int)_gl.size(),con);
    _gl.push_back(clearance);
    _gu.push_back(SQPObjective<T>::infty());
  }
  //only one plane activates
  SQPObjectiveComponent<T>::addSOS1SumToOne(obj,_name+"["+std::to_string(frame)+"]",weights);
}
template <typename T>
bool MixedIntegerObstacle<T>::satisfyConstraint(const Vec& x,const Eigen::Matrix<int,2,1>& bodyPoseDOF,T clearance) const {
  //rotation by 90 degrees
  Eigen::Matrix<double,2,2> R;
  R << 0,1,-1,0;
  //build clearnace
  Vec2T XY(x[bodyPoseDOF[0]],x[bodyPoseDOF[1]]);
  for(int i=0; i<(int)_vss.size(); i++) {
    Eigen::Matrix<double,2,1> a=_vss[i].template segment<2>(0);
    Eigen::Matrix<double,2,1> b=_vss[(i+1)%(int)_vss.size()].template segment<2>(0);
    Eigen::Matrix<double,2,1> n=(R*(b-a)).normalized();
    if((XY.template cast<double>()-a).dot(n)>=(double)clearance)
      return true;
  }
  return false;
}
template <typename T>
std::shared_ptr<ShapeExact> MixedIntegerObstacle<T>::getShape() const {
  return _obstacle;
}
template class MixedIntegerObstacle<FLOAT>;
}
