#ifndef PBCDM_DYNAMICS_SEQUENCE_H
#define PBCDM_DYNAMICS_SEQUENCE_H

#include "NEDynamicsSequence.h"
#include <Articulated/PBCentroidBodyDynamicsGradientInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class PBCDMDynamicsSequence : public NEDynamicsSequence<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(PBDDynamicsSequence<T>)
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using PBDDynamicsSequence<T>::_body;
  using PBDDynamicsSequence<T>::_poses;
  using PBDDynamicsSequence<T>::_forces;
  using PBDDynamicsSequence<T>::_ees;
  using PBDDynamicsSequence<T>::_dt;
  using PBDDynamicsSequence<T>::_C;
  using PBDDynamicsSequence<T>::_JRCF;
  using PBDDynamicsSequence<T>::_proj;
  using PBDDynamicsSequence<T>::_DOFVars;
  using PBDDynamicsSequence<T>::_posConsStart;
  using PBDDynamicsSequence<T>::DOFVar;
  using NEDynamicsSequence<T>::_infosNE;
  using NEDynamicsSequence<T>::_joints;
  using NEDynamicsSequence<T>::_a0;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  PBCDMDynamicsSequence
  (SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
   std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodySimplified,const Vec3T& G,
   const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
   const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
   const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart=0,bool inherited=false);
  void setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv=NULL);
  std::shared_ptr<DeformedEnvironment<T>> getDeformedEnvironment() const;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0) override;
  virtual const Vec& controlCoef() const override;
  virtual Vec residualToControl() const override;
  virtual Vec controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet=NULL) const override;
  virtual SMatT controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet=NULL) const override;
  virtual Vec controlledDOFs(bool constrained,std::set<int>* consSet=NULL) const override;
  virtual SMatT controlledDOFsProj(bool constrained,std::set<int>* consSet=NULL) const override;
  virtual MatT getPoses(const Vec& x) override;
 protected:
  std::shared_ptr<ArticulatedBody> _bodySimplified;
  std::vector<PBCentroidBodyDynamicsGradientInfo<T>> _infosPBCDM;
  std::shared_ptr<DeformedEnvironment<T>> _DEnv;
};
}

#endif
