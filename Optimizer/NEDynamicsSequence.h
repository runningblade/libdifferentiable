#ifndef NE_DYNAMICS_SEQUENCE_H
#define NE_DYNAMICS_SEQUENCE_H

#include "PBDDynamicsSequence.h"
#include <Articulated/NEArticulatedGradientInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class NEDynamicsSequence : public PBDDynamicsSequence<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(PBDDynamicsSequence<T>)
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  using PBDDynamicsSequence<T>::_body;
  using PBDDynamicsSequence<T>::_poses;
  using PBDDynamicsSequence<T>::_forces;
  using PBDDynamicsSequence<T>::_ees;
  using PBDDynamicsSequence<T>::_dt;
  using PBDDynamicsSequence<T>::_C;
  using PBDDynamicsSequence<T>::_proj;
  using PBDDynamicsSequence<T>::_DOFVars;
  using PBDDynamicsSequence<T>::_posConsStart;
  using PBDDynamicsSequence<T>::makeValid;
  using PBDDynamicsSequence<T>::controlCoef;
  using PBDDynamicsSequence<T>::residualToControl;
  using PBDDynamicsSequence<T>::controlledDOFs;
  using PBDDynamicsSequence<T>::controlledDOFsProj;
  using PBDDynamicsSequence<T>::DOFVar;
  using PBDDynamicsSequence<T>::body;
 public:
  NEDynamicsSequence(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec2T& period,
                     std::shared_ptr<ArticulatedBody> body,const Vec3T& G,
                     const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
                     const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
                     const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int posConsStart=0,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0) override;
  virtual void resetInfo(int id,const Vec& x) override;
  virtual Mat3X4T getJointTrans(int id,int JID) const override;
  virtual void DTG(int id,int JID,Mat3X4T GK,std::function<void(int,T)> DTG) const override;
 protected:
  std::vector<NEArticulatedGradientInfo<T>> _infosNE;
  Eigen::Matrix<int,-1,1> _joints;
  Vec6T _a0;
};
}

#endif
