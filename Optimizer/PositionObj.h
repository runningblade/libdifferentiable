#ifndef POSITION_OBJ_H
#define POSITION_OBJ_H

#include "PBDDynamicsSequence.h"
#include "NEDynamicsSequence.h"
#include <unordered_map>

namespace PHYSICSMOTION {
template <typename T>
class PositionObj : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  PositionObj(SQPObjectiveCompound<T>& obj,const std::string& name,int frm,const Vec3T& target,const Eigen::Matrix<T,-1,3>& mask,T coef,std::shared_ptr<PBDDynamicsSequence<T>> dyn);
  PositionObj(SQPObjectiveCompound<T>& obj,const std::string& name,T t,const Vec3T& target,const Eigen::Matrix<T,-1,3>& mask,T coef,std::shared_ptr<PositionSequence<T>> pos);
  PositionObj(SQPObjectiveCompound<T>& obj,const std::string& name,const Eigen::Matrix<int,3,1>& var,const Vec3T& target,const Eigen::Matrix<T,-1,3>& mask,T coef);
  virtual T operator()(const Vec& x,Vec* fgrad) override;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
 protected:
  std::shared_ptr<DeformedEnvironment<T>> _DEnv;
  std::shared_ptr<PositionSequence<T>> _pos;
  Eigen::Matrix<int,-1,1> _vars;
  Eigen::Matrix<T,-1,3> _mask;
  Vec3T _target;
  T _t,_coef;
};
}

#endif
