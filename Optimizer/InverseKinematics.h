#ifndef INVERSE_KINEMATICS_H
#define INVERSE_KINEMATICS_H

#include "SQPObjective.h"
#include <Articulated/ArticulatedBody.h>
#include <Articulated/PBDArticulatedGradientInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class InverseKinematicsPosObj : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  InverseKinematicsPosObj(SQPObjectiveCompound<T>& obj,const std::string& name,const ArticulatedBody& body,const Vec3T& pos,
                          const Vec3T& localPos,const std::vector<int>& DOFs,const Vec* init=NULL,bool limit=true,bool inherited=false);
  int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  T operator()(const Vec& x,Vec* fgrad) override;
 protected:
  std::unordered_map<int,SQPVariable<T>> _DOFToVid;
  PBDArticulatedGradientInfo<T> _info;
  const ArticulatedBody& _body;
  Vec3T _localPos;
  int _JID;
};
template <typename T>
class InverseKinematicsPosNorObj : public InverseKinematicsPosObj<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  //parameters
  using InverseKinematicsPosObj<T>::_DOFToVid;
  using InverseKinematicsPosObj<T>::_info;
  using InverseKinematicsPosObj<T>::_body;
  using InverseKinematicsPosObj<T>::_localPos;
  using InverseKinematicsPosObj<T>::_JID;
  InverseKinematicsPosNorObj(SQPObjectiveCompound<T>& obj,const std::string& name,const ArticulatedBody& body,const Vec3T& pos,const Vec3T& nor,
                             const Vec3T& localPos,const Vec3T& localNor,const std::vector<int>& DOFs,T coef,const Vec* init=NULL,bool limit=true,bool inherited=false);
  int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  T operator()(const Vec& x,Vec* fgrad) override;
 protected:
  Vec3T _localNor,_nor;
  T _coef;
};
template <typename T>
class InverseKinematicsPosFrameObj : public InverseKinematicsPosObj<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  //parameters
  using InverseKinematicsPosObj<T>::_DOFToVid;
  using InverseKinematicsPosObj<T>::_info;
  using InverseKinematicsPosObj<T>::_body;
  using InverseKinematicsPosObj<T>::_localPos;
  using InverseKinematicsPosObj<T>::_JID;
  InverseKinematicsPosFrameObj(SQPObjectiveCompound<T>& obj,const std::string& name,const ArticulatedBody& body,const Vec3T& pos,const Mat3T& frame,
                               const Vec3T& localPos,const Mat3T& localFrame,const std::vector<int>& DOFs,T coef,const Vec* init=NULL,bool limit=true,bool inherited=false);
  int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  T operator()(const Vec& x,Vec* fgrad) override;
 protected:
  Mat3T _localFrame,_frame;
  T _coef;
};
}

#endif
