#include "PositionSequence.h"
#include <Environment/DeformedEnvironment.h>
#include <Utils/DebugGradient.h>
#include <random>

namespace PHYSICSMOTION {
template <typename T>
PositionSequence<T>::PositionSequence(SQPObjectiveCompound<T>& obj,const std::string& name,
                                      int positionAboveGroundStencilSize,T phi0,bool even,
                                      std::shared_ptr<PhaseSequence<T>> phase,
                                      std::shared_ptr<Environment<T>> env,bool inherited)
  :VectorSequence<T>(obj,inherited?name:name+"PositionSequence",even,phase,true),_phi0(phi0),_env(env) {
  if(inherited)
    return;
  //position (derivative)
  _var.setConstant(3,phase->nrColumn(even),-1);
  _diffVar.setConstant(3,phase->nrColumn(even),-1);
  for(int r=0; r<3; r++)
    for(int c=0; c<phase->nrColumn(even); c++) {
      if(!(phase->segmentOffset(c)==0 || phase->segmentOffset(c)==phase->subStage()))
        _diffVar(r,c)=obj.addVar(diffVar(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
      if(_phase->segmentId(c)>0 && _phase->segmentOffset(c)==0)
        _var(r,c)=obj.addVar(var(r,c-1),SQPObjectiveCompound<T>::MUST_EXIST)._id;
      else _var(r,c)=obj.addVar(var(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
    }
  //position above ground
  if(positionAboveGroundStencilSize>0) {
    T offTime=0,span;
    Vec init=obj.init();
    GRAD_INTERP grad[3];
    const Eigen::Matrix<int,-1,1>& timeVar=_phase->timeSpanVar();
    for(int i=0; i<timeVar.size(); i++) {
      span=init[timeVar[i]];
      if(i%2!=_even)
        for(int c=0; c<positionAboveGroundStencilSize+1; c++) {
          if(c==0 && i-2>=0)
            continue;
          T time=offTime+span*c/positionAboveGroundStencilSize;
          timeInterpolateStencil(init,time,grad,NULL,false);
          for(int r=0; r<3; r++)
            _positionAboveGroundStencil.push_back(removeNonZero(grad[r]));
          _gl.push_back(_phi0);
          _gu.push_back((c==0||c==positionAboveGroundStencilSize)?_phi0:SQPObjective<T>::infty());
        }
      offTime+=span;
    }
  }
  //trivial initialization
  initConstant(Vec3T::Zero(),obj);
}
template <typename T>
void PositionSequence<T>::setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv) {
  _DEnv=DEnv;
}
template <typename T>
int PositionSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  //position above ground
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_positionAboveGroundStencil.size(); i+=3) {
    int offset=_offset+i/3;
    Vec3T pos=Vec3T::Zero(),g;
    if(_DEnv) {
      //Constraint: pos[2]>=0
      for(std::pair<int,T> stencil:_positionAboveGroundStencil[i+2])
        pos[2]+=x[stencil.first]*stencil.second;
      fvec[offset]=pos[2];
      if(fjac)
        for(std::pair<int,T> stencil:_positionAboveGroundStencil[i+2])
          fjac->push_back(STrip(offset,stencil.first,stencil.second));
    } else {
      //Constraint: phi(pos)>=0
      for(int r=0; r<3; r++)
        for(std::pair<int,T> stencil:_positionAboveGroundStencil[i+r])
          pos[r]+=x[stencil.first]*stencil.second;
      fvec[offset]=_env->phi(pos,fjac?&g:NULL);
      if(fjac)
        for(int r=0; r<3; r++)
          for(std::pair<int,T> stencil:_positionAboveGroundStencil[i+r])
            fjac->push_back(STrip(offset,stencil.first,g[r]*stencil.second));
    }
  }
  return 0;
}
template <typename T>
typename PositionSequence<T>::VecDT PositionSequence<T>::timeInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan) const {
  if(_DEnv) {
    Mat3T J;
    GRAD_INTERP gradTmp[3];
    Vec3T a=VectorSequence<T>::timeInterpolateStencil(x,time,grad?gradTmp:NULL,gradTime,gradTimeSpan);
    Vec3T pos=_DEnv->forward(a,&J,NULL);
    if(grad) {
      //allocate
      for(int d2=0; d2<3; d2++)
        grad[d2].clear();
      for(int d=0; d<3; d++)
        for(const auto& g:gradTmp[d])
          for(int d2=0; d2<3; d2++)
            grad[d2][g.first]=0;
      //fill
      for(int d=0; d<3; d++)
        for(const auto& g:gradTmp[d])
          for(int d2=0; d2<3; d2++)
            grad[d2][g.first]+=J(d2,d)*g.second;
    }
    if(gradTime)
      *gradTime=J**gradTime;
    return pos;
  } else {
    return VectorSequence<T>::timeInterpolateStencil(x,time,grad,gradTime,gradTimeSpan);
  }
}
template <typename T>
typename PositionSequence<T>::VecDT PositionSequence<T>::diffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan) const {
  if(_DEnv) {
    FUNCTION_NOT_IMPLEMENTED
  } else return VectorSequence<T>::diffInterpolateStencil(x,time,grad,gradTime,gradTimeSpan);
}
template <typename T>
typename PositionSequence<T>::VecDT PositionSequence<T>::ddiffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan) const {
  if(_DEnv) {
    FUNCTION_NOT_IMPLEMENTED
  } else return VectorSequence<T>::ddiffInterpolateStencil(x,time,grad,gradTime,gradTimeSpan);
}
template <typename T>
void PositionSequence<T>::initConstant(const VecDT& pos,SQPObjectiveCompound<T>& obj) {
  if(_DEnv)
    VectorSequence<T>::initConstant(_DEnv->alpha(pos),obj);
  else VectorSequence<T>::initConstant(pos,obj);
}
template <typename T>
void PositionSequence<T>::initLineSegment(const VecDT& a,const VecDT& b,SQPObjectiveCompound<T>& obj,int N) {
  if(_DEnv)
    VectorSequence<T>::initLineSegment(_DEnv->alpha(a),_DEnv->alpha(b),obj,N);
  else VectorSequence<T>::initLineSegment(a,b,obj,N);
}
template <typename T>
void PositionSequence<T>::initLineSegments(const std::vector<std::pair<VecDT,VecDT>>& segments,SQPObjectiveCompound<T>& obj) {
  if(_DEnv) {
    std::vector<std::pair<VecDT,VecDT>> segmentsCopy;
    for(const auto& segment:segments)
      segmentsCopy.push_back(std::make_pair(_DEnv->alpha(segment.first),_DEnv->alpha(segment.second)));
    VectorSequence<T>::initLineSegments(segmentsCopy,obj);
  } else VectorSequence<T>::initLineSegments(segments,obj);
}
template <typename T>
void PositionSequence<T>::initRegularInterval(const MatT& DOFs,T interval,SQPObjectiveCompound<T>& obj,bool fixInitVar,bool fixInitDiffVar) {
  if(_DEnv) {
    FUNCTION_NOT_IMPLEMENTED
  } else initRegularInterval(DOFs,interval,obj,fixInitVar,fixInitDiffVar);
}
template <typename T>
std::vector<typename PositionSequence<T>::Vec3T> PositionSequence<T>::getPositionAboveGroundStencil(const Vec& x) const {
  std::vector<Vec3T> ret;
  for(int i=0; i<(int)_positionAboveGroundStencil.size(); i+=3)
    if(_gu[i/3]>_gl[i/3]) {
      Vec3T pos=Vec3T::Zero();
      for(int r=0; r<3; r++)
        for(std::pair<int,T> stencil:_positionAboveGroundStencil[i+r])
          pos[r]+=x[stencil.first]*stencil.second;
      ret.push_back(pos);
    }
  std::cout << "PositionSequence[" << _name << "] has " << ret.size() << " position-above-ground test points!" << std::endl;
  return ret;
}
template <typename T>
std::vector<typename PositionSequence<T>::Vec3T> PositionSequence<T>::getPositionOnGroundStencil(const Vec& x) const {
  std::vector<Vec3T> ret;
  for(int i=0; i<(int)_positionAboveGroundStencil.size(); i+=3)
    if(_gu[i/3]==_gl[i/3]) {
      Vec3T pos=Vec3T::Zero();
      for(int r=0; r<3; r++)
        for(std::pair<int,T> stencil:_positionAboveGroundStencil[i+r])
          pos[r]+=x[stencil.first]*stencil.second;
      ret.push_back(pos);
    }
  std::cout << "PositionSequence[" << _name << "] has " << ret.size() << " position-on-ground test points!" << std::endl;
  return ret;
}
template <typename T>
std::shared_ptr<Environment<T>> PositionSequence<T>::getEnv() const {
  return _env;
}
template <typename T>
T PositionSequence<T>::getPhi0() const {
  return _phi0;
}
template class PositionSequence<FLOAT>;
}
