#include "FootStepSequence.h"

namespace PHYSICSMOTION {
template <typename T>
FootStepSequence<T>::FootStepSequence(SQPObjectiveCompound<T>& obj,const std::string& name,char even,int eeId,T phi0,int doubleHorizon,int singleHorizon,
                                      std::shared_ptr<Environment<T>> env,const std::vector<FootStep<T>>& steps,bool inherited)
  :VectorSequence<T,2>(obj,inherited?name:name+"FootStepSequence"+std::string(even?"Even":"Odd"),even,NULL,true),_eeId(eeId) {
  int N=(int)steps.size();
  int nrSeg=(_even==steps[0]._isEven)?(N-1)/2:N/2;
  //initial var
  _var.resize(2,nrSeg+1);
  for(int c=0; c<_var.cols(); c++)
    for(int r=0; r<2; r++)
      _var(r,c)=obj.addVar(var(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  //initial diffVar
  _diffVar.resize(2,nrSeg*2);
  for(int c=0; c<_diffVar.cols(); c++)
    for(int r=0; r<2; r++)
      _diffVar(r,c)=obj.addVar(diffVar(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  //timestep
  int maxVarId=std::max(_var.maxCoeff(),_diffVar.maxCoeff())+1;
  int NT=(N-1)*(doubleHorizon+singleHorizon);
  _stance.resize(NT);
  STrips trips;
  Vec4T coef,dcdt,dcdt0,dcds;
  for(int c=0,offStencil=0,offStance=0; c<N-1; c++) {
    int segId=(_even==steps[0]._isEven)?c/2:(c+1)/2;
    if(_even==steps[c]._isEven) {
      //always stance
      for(int i=0; i<doubleHorizon+singleHorizon; i++) {
        for(int r=0; r<2; r++,offStencil++)
          trips.push_back(STrip(_var(r,segId),offStencil,1));
        _stance[offStance++]=true;
      }
    } else {
      //swing->stance
      for(int i=0; i<singleHorizon; i++) {
        PhaseSequence<T>::coeffHermite(1,i/(T)singleHorizon,0,coef,dcdt,dcdt0,dcds);
        for(int r=0; r<2; r++,offStencil++) {
          trips.push_back(STrip(_var(r,segId),offStencil,coef[0]));
          trips.push_back(STrip(_diffVar(r,segId*2),offStencil,coef[1]));
          trips.push_back(STrip(_var(r,segId+1),offStencil,coef[2]));
          trips.push_back(STrip(_diffVar(r,segId*2+1),offStencil,coef[3]));
        }
        _stance[offStance++]=false;
      }
      for(int i=0; i<doubleHorizon; i++) {
        for(int r=0; r<2; r++,offStencil++)
          trips.push_back(STrip(_var(r,segId+1),offStencil,1));
        _stance[offStance++]=true;
      }
    }
  }
  _stencil.resize(maxVarId,NT*2);
  _stencil.setFromTriplets(trips.begin(),trips.end());
  //environment
  Vec3T grad;
  trips.clear();
  SMatT stencilEnv;
  Eigen::Matrix<T,2,3> stencilZ;
  stencilZ.setIdentity();
  T phi=env->phi(Vec3T::Zero(),&grad);
  stencilZ(0,2)=-grad[0];
  stencilZ(1,2)=-grad[1];
  _pos0.resize(NT*3);
  for(int i=0; i<NT; i++) {
    _pos0.template segment<3>(i*3)=Vec3T(0,0,-phi+phi0);
    addBlock(trips,i*2,i*3,stencilZ);
  }
  stencilEnv.resize(NT*2,NT*3);
  stencilEnv.setFromTriplets(trips.begin(),trips.end());
  _stencil=_stencil*stencilEnv;
  _stencil.prune((T)1);
}
template <typename T>
void FootStepSequence<T>::init(SQPObjectiveCompound<T>& obj,const std::vector<FootStep<T>>& steps) {
  //initial var
  for(int c=0; c<_var.cols(); c++)
    for(int r=0; r<2; r++) {
      int segId=(_even==steps[0]._isEven)?(c*2):std::max(c*2-1,0);
      obj.addVar(_var(r,c))._init=steps[segId]._eePose(r,_eeId);
    }
  //initial diffVar
  for(int c=0; c<_diffVar.cols(); c++)
    for(int r=0; r<2; r++) {
      int segId=c/2;
      obj.addVar(_diffVar(r,c))._init=obj.addVar(_var(r,segId+1))._init-obj.addVar(_var(r,segId))._init;
    }
}
template <typename T>
typename FootStepSequence<T>::PolyXT FootStepSequence<T>::getPosPoly(SQPObjectiveCompound<T>& obj,int col) const {
  PolyXT ret;
  for(typename SMatT::InnerIterator it(_stencil,col); it; ++it)
    ret+=obj.addVarPoly(it.row())*PolyXT(it.value());
  return ret;
}
template <typename T>
void FootStepSequence<T>::setTargetEnergy(SQPObjectiveCompound<T>& obj,T coef) {
  PolyXT E;
  for(int c=0; c<_var.cols(); c++)
    for(int r=0; r<2; r++) {
      SQPVariable<T>& var=obj.addVar(_var(r,c));
      if(coef<0)
        var.fix(var._init);
      else {
        var._l=-SQPObjective<T>::infty();
        var._u= SQPObjective<T>::infty();
        E+=(obj.addVarPoly(_var(r,c))-var._init)*(obj.addVarPoly(_var(r,c))-var._init);
      }
    }
  if(coef>0)
    SQPObjectiveComponent<T>::setPolynomialComponentObjective(E*coef);
}
template <typename T>
void FootStepSequence<T>::fixInitialStep(SQPObjectiveCompound<T>& obj) {
  for(int d=0; d<2; d++) {
    SQPVariable<T>& var=obj.addVar(_var(d,0));
    var.fix(var._init);
  }
}
template <typename T>
void FootStepSequence<T>::printStencilInfo(int DIM) const {
  int nrSeg=_var.cols()-1,NT=_stance.size();
  std::cout << _name << " nrSeg=" << nrSeg << " nrTimestep=" << NT << std::endl;
  for(int i=0; i<NT; i++)
    for(int d=0; d<DIM; d++) {
      std::string stencil;
      std::cout << "Timestep[" << i << "][DIM=" << d << "][stance=" << _stance[i] << "]: ";
      for(typename SMatT::InnerIterator it(_stencil,i*3+d); it; ++it)
        stencil+=findVarName(it.row())+"*"+std::to_string((double)it.value())+"+";
      std::cout << stencil.substr(0,stencil.length()-1) << std::endl;
    }
}
template <typename T>
const typename FootStepSequence<T>::SMatT& FootStepSequence<T>::getStencil() const {
  return _stencil;
}
template <typename T>
typename FootStepSequence<T>::Vec FootStepSequence<T>::pos(const Vec& x) const {
  return _stencil.transpose()*x.segment(0,_stencil.rows())+_pos0;
}
template <typename T>
bool FootStepSequence<T>::isStance(int i) const {
  return _stance[i];
}
template <typename T>
int FootStepSequence<T>::nrTimestep() const {
  return _stencil.cols()/3;
}
template <typename T>
std::string FootStepSequence<T>::findVarName(int id) const {
  for(int r=0; r<_var.rows(); r++)
    for(int c=0; c<_var.cols(); c++)
      if(_var(r,c)==id)
        return "var("+std::to_string(r)+","+std::to_string(c)+")";
  for(int r=0; r<_diffVar.rows(); r++)
    for(int c=0; c<_diffVar.cols(); c++)
      if(_diffVar(r,c)==id)
        return "diffVar("+std::to_string(r)+","+std::to_string(c)+")";
  ASSERT(false)
  return "";
}
template class FootStepSequence<FLOAT>;
}
