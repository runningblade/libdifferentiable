#include "LaplaceObjective.h"

namespace PHYSICSMOTION {
template <typename T,int DIM>
LaplaceObjective<T,DIM>::LaplaceObjective(SQPObjectiveCompound<T>& obj,const std::string& name,int stage,T coef,bool cubic,bool dirichletOrLaplace)
  :PolynomialObjective<T>(obj,name),_coef(coef) {
  _diffVar.resize(DIM,cubic?stage:0);
  _var.resize(DIM,stage);
  for(int c=0; c<stage; c++)
    for(int r=0; r<DIM; r++) {
      if(cubic)
        _diffVar(r,c)=obj.addVar(diffVar(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
      _var(r,c)=obj.addVar(var(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
    }
  reset(obj,dirichletOrLaplace);
}
template <typename T,int DIM>
LaplaceObjective<T,DIM>::LaplaceObjective(SQPObjectiveCompound<T>& obj,const std::string& name,const Eigen::Matrix<int,-1,-1>& var,const Eigen::Matrix<int,-1,-1>& diffVar,T coef,bool dirichletOrLaplace)
  :PolynomialObjective<T>(obj,name+"LaplaceObjective<"+std::to_string(DIM)+">"),_coef(coef),_var(var),_diffVar(diffVar) {
  reset(obj,dirichletOrLaplace);
}
template <typename T,int DIM>
std::string LaplaceObjective<T,DIM>::diffVar(int r,int stage) const {
  return _name+"Diff["+std::to_string(stage)+"]["+std::to_string(r)+"]";
}
template <typename T,int DIM>
std::string LaplaceObjective<T,DIM>::var(int r,int stage) const {
  return _name+"["+std::to_string(stage)+"]["+std::to_string(r)+"]";
}
template <typename T,int DIM>
const Eigen::Matrix<int,-1,-1>& LaplaceObjective<T,DIM>::getVar() const {
  return _var;
}
template <typename T,int DIM>
const Eigen::Matrix<int,-1,-1>& LaplaceObjective<T,DIM>::getDiffVar() const {
  return _diffVar;
}
template <typename T,int DIM>
typename LaplaceObjective<T,DIM>::Mat4T LaplaceObjective<T,DIM>::hermiteDirichlet() {
  Mat4T ret;
  ret << 6/5,1/10,-6/5,1/10,
      1/10,2/15,-1/10,-1/30,
      -6/5,-1/10,6/5,-1/10,
      1/10,-1/30,-1/10,2/15;
  return ret;
}
template <typename T,int DIM>
typename LaplaceObjective<T,DIM>::Mat4T LaplaceObjective<T,DIM>::hermiteLaplace() {
  Mat4T ret;
  ret << 12,6,-12,6,
      6,4,-6,2,
      -12,-6,12,-6,
      6,2,-6,4;
  return ret;
}
template <typename T,int DIM>
typename LaplaceObjective<T,DIM>::Mat2T LaplaceObjective<T,DIM>::finiteDiffDirichlet() {
  Vec2T ret(1,-1);
  return ret*ret.transpose();
}
template <typename T,int DIM>
typename LaplaceObjective<T,DIM>::Mat3T LaplaceObjective<T,DIM>::finiteDiffLaplace() {
  Vec3T ret(1,-2,1);
  return ret*ret.transpose();
}
template <typename T,int DIM>
typename LaplaceObjective<T,DIM>::PolyXT LaplaceObjective<T,DIM>::polyContract(const TermXT* terms,const MatT& H) {
  PolyXT ret;
  for(int r=0; r<H.rows(); r++)
    for(int c=0; c<H.cols(); c++)
      if(H(r,c)!=0)
        ret+=terms[r]*terms[c]*H(r,c);
  return ret;
}
template <typename T,int DIM>
void LaplaceObjective<T,DIM>::reset(SQPObjectiveCompound<T>& obj,bool dirichletOrLaplace) {
  if(_coef<=0)
    return;
  PolyXT objPoly;
  TermXT param[4];
  bool cubic=_diffVar.cols()==_var.cols();
  for(int c=0; c<_var.cols(); c++)
    for(int r=0; r<DIM; r++) {
      if(cubic) {
        bool separateDiff=_diffVar.cols()>_var.cols();
        ASSERT_MSGV(_diffVar.cols()==_var.cols() || _diffVar.cols()==(_var.cols()-1)*2,
                    "Incorrect diffVar.size()=%ld and var.size()=%ld, we must ensure that: _diffVar.cols()==_var.cols() || _diffVar.cols()==(_var.cols()-1)*2",_diffVar.cols(),_var.cols())
        if(c>=_var.cols()-1)
          continue;
        if(_var(r,c)>=0)
          param[0]=obj.addVarTerm(_var(r,c));
        else param[0]=TermXT();
        if(_diffVar(r,separateDiff?c*2:c)>=0)
          param[1]=obj.addVarTerm(_diffVar(r,separateDiff?c*2:c));
        else param[1]=TermXT();
        if(_var(r,c+1)>=0)
          param[2]=obj.addVarTerm(_var(r,c+1));
        else param[2]=TermXT();
        if(_diffVar(r,separateDiff?c*2+1:c+1)>=0)
          param[3]=obj.addVarTerm(_diffVar(r,separateDiff?c*2+1:c+1));
        else param[3]=TermXT();
        if(dirichletOrLaplace)
          objPoly+=polyContract(param,hermiteDirichlet());
        else objPoly+=polyContract(param,hermiteLaplace());
      } else if(dirichletOrLaplace) {
        if(c>=_var.cols()-1)
          continue;
        param[0]=obj.addVarTerm(_var(r,c));
        param[1]=obj.addVarTerm(_var(r,c+1));
        objPoly+=polyContract(param,finiteDiffDirichlet());
      } else {
        if(c>=_var.cols()-2)
          continue;
        param[0]=obj.addVarTerm(_var(r,c));
        param[1]=obj.addVarTerm(_var(r,c+1));
        param[2]=obj.addVarTerm(_var(r,c+2));
        objPoly+=polyContract(param,finiteDiffLaplace());
      }
    }
  SQPObjectiveComponent<T>::addPolynomialComponentObjective(objPoly*PolyXT(_coef*0.5));
}
template class LaplaceObjective<FLOAT,2>;
template class LaplaceObjective<FLOAT,3>;
template class LaplaceObjective<FLOAT,4>;
}
