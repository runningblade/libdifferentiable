#ifndef MIXED_INTEGER_OBSTACLE_H
#define MIXED_INTEGER_OBSTACLE_H

#include "SQPObjective.h"
#include <Environment/Environment.h>
#include <Environment/ConvexHullExact.h>
#include <Articulated/EndEffectorInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class MixedIntegerObstacle : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
#ifndef SWIG
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
#endif
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  MixedIntegerObstacle(SQPObjectiveCompound<T>& obj,const std::string& name,std::shared_ptr<ShapeExact> obstacle,bool inherited=false);
  void addConstraint(SQPObjectiveCompound<T>& obj,int frame,const Eigen::Matrix<int,2,1>& bodyPoseDOF,T clearance,T bigM);
  bool satisfyConstraint(const Vec& x,const Eigen::Matrix<int,2,1>& bodyPoseDOF,T clearance) const;
  std::shared_ptr<ShapeExact> getShape() const;
 protected:
  std::shared_ptr<ShapeExact> _obstacle;
  std::vector<Eigen::Matrix<double,3,1>> _vss;
};
}

#endif
