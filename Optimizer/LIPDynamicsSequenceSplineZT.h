#ifndef LIP_DYNAMICS_SEQUENCE_SPLINE_ZT_H
#define LIP_DYNAMICS_SEQUENCE_SPLINE_ZT_H

#include "LIPDynamicsSequence.h"

namespace PHYSICSMOTION {
template <typename T>
class LIPDynamicsSequenceSplineZT : public LIPDynamicsSequence<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  REUSE_MAP_FUNCS_T(LIPDynamicsSequence<T>)
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using LIPDynamicsSequence<T>::_bodySimplified;
  using LIPDynamicsSequence<T>::_footSteps;
  using LIPDynamicsSequence<T>::_ees;
  using LIPDynamicsSequence<T>::_isBoundingCircle;
  using LIPDynamicsSequence<T>::_posConsStart;
  using LIPDynamicsSequence<T>::_w;
  using LIPDynamicsSequence<T>::_h0;
  using LIPDynamicsSequence<T>::_dt;
  using LIPDynamicsSequence<T>::_DOFVars;
  using LIPDynamicsSequence<T>::_lambdas;
  using LIPDynamicsSequence<T>::DOFVar;
  using LIPDynamicsSequence<T>::lambdaVar;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  LIPDynamicsSequenceSplineZT(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec3T& G,T h0,
                              std::shared_ptr<ArticulatedBody> bodySimplified,
                              const std::vector<std::shared_ptr<FootStepSequence<T>>>& footSteps,bool isBoundingCircle,
                              std::shared_ptr<PhaseSequence<T>> phase,const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,int NCToFix=2,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual Eigen::Matrix<int,-1,-1> DOFVarZT() const override;
  virtual Eigen::Matrix<int,-1,-1> DOFDiffVarZT() const override;
  virtual void fixInitialPose(SQPObjectiveCompound<T>& obj,bool fixVelocity) override;
  virtual void initDOF(SQPObjectiveCompound<T>& obj,int doubleHorizon,int singleHorizon,const std::vector<FootStep<T>>& steps) override;
  virtual MatT getPoses(const ArticulatedBody& body,const Environment<T>& env,const Vec& x) override;
  virtual void setTargetHeightEnergy(SQPObjectiveCompound<T>& obj,T hCoef,T coefLambdaEnergy) override;
 protected:
  PolyXT getZTPoly(SQPObjectiveCompound<T>& obj,int col) const;
  void buildStencil(const SQPObjectiveCompound<T>& obj);
  std::shared_ptr<VectorSequence<T,2>> _ZTSeq;
  Eigen::Matrix<int,2,1> _rangeZT;
  SMatT _ztInterp;
};
}

#endif
