#include "MixedIntegerReachable.h"
#include <Articulated/NEArticulatedGradientInfo.h>
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
template <typename T>
MixedIntegerReachable<T>::MixedIntegerReachable
(SQPObjectiveCompound<T>& obj,const std::string& name,int res,T ref,std::shared_ptr<Environment<T>> env,bool linearizeEnv,
 const Eigen::Matrix<int,3,1>& bodyPoseDOF,const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,
 const std::vector<Eigen::Matrix<int,2,1>>& eesDOF,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"MixedIntegerReachable"),_linearizeEnv(linearizeEnv),_bodyPoseDOF(bodyPoseDOF),_ees(ees),_env(env) {
  if(inherited)
    return;
  _eesDOF.resize(eesDOF.size());
  _bodyZ=obj.addVar(name+"TorsoZ",-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  for(int eid=0; eid<(int)ees.size(); eid++) {
    _eesDOF[eid][0]=eesDOF[eid][0];
    _eesDOF[eid][1]=eesDOF[eid][1];
    _eesDOF[eid][2]=obj.addVar(name+"["+std::to_string(eid)+"].z",-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
  }
  //environment
  if(env)
    for(int eid=0; eid<(int)ees.size(); eid++) {
      if(linearizeEnv) {
        Vec3T grad;
        PolyXT phi=PolyXT(_env->phi(Vec3T::Zero(),&grad));
        for(int d=0; d<3; d++)
          phi+=PolyXT(grad[d])*obj.addVarPoly(_eesDOF[eid][d]);
        SQPObjectiveComponent<T>::addPolynomialComponentConstraint((int)_gl.size(),phi);
      }
      _gl.push_back(ees[eid]->_phi0);
      _gu.push_back(ees[eid]->_phi0);
    }
  //Z-constraint
  for(int eid=0; eid<(int)ees.size(); eid++) {
    T lbZ=ees[eid]->_ctrBB[2]+ees[eid]->_bb[2]*ees[eid]->_res;
    T ubZ=ees[eid]->_ctrBB[2]+ees[eid]->_bb[5]*ees[eid]->_res;
    ASSERT_MSG(ees[eid]->_frame.col(2)==EndEffectorBounds::Vec3T::UnitZ(),"Direction-Z must be separable!")
    SQPObjectiveComponent<T>::addPolynomialComponentConstraint((int)_gl.size(),obj.addVarPoly(_eesDOF[eid][2])-obj.addVarPoly(_bodyZ));
    _gl.push_back(lbZ);
    _gu.push_back(ubZ);
  }
  //rotation
  typename PolyXT::MATP R=getRot(obj,res,ref,bodyPoseDOF[2]);
  //in-circle
  for(int eid=0; eid<(int)ees.size(); eid++) {
    typename PolyXT::VECP ee,ctr=ees[eid]->getBBCenter().template segment<2>(0).template cast<PolyXT>(),t;
    ee.resize(2);
    ee[0]=obj.addVarPoly(_eesDOF[eid][0]);
    ee[1]=obj.addVarPoly(_eesDOF[eid][1]);
    //add constraint
    t.resize(2);
    t[0]=obj.addVarPoly(bodyPoseDOF[0]);
    t[1]=obj.addVarPoly(bodyPoseDOF[1]);
    ctr=R*ctr+t;
    SQPObjectiveComponent<T>::addPolynomialComponentConstraint((int)_gl.size(),(ee-ctr).squaredNorm());
    _gl.push_back(-SQPObjective<T>::infty());
    _gu.push_back(pow(ees[eid]->getBBRadius(),2));
  }
}
template <typename T>
int MixedIntegerReachable<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  if(_env && !_linearizeEnv)
    for(int eid=0; eid<(int)_ees.size(); eid++) {
      Vec3T pos(x[_eesDOF[eid][0]],x[_eesDOF[eid][1]],x[_eesDOF[eid][2]]),grad;
      fvec[_offset+eid]=_env->phi(pos,fjac?&grad:NULL);
      if(fjac)
        for(int d=0; d<3; d++)
          fjac->push_back(STrip(_offset+eid,_eesDOF[eid][d],grad[d]));
    }
  return 0;
}
template <typename T>
void MixedIntegerReachable<T>::getEEs(const Vec& x,std::vector<Vec3T>& ees) const {
  ees.resize(_eesDOF.size());
  for(int i=0; i<(int)ees.size(); i++)
    for(int d=0; d<3; d++)
      ees[i][d]=x[_eesDOF[i][d]];
}
template <typename T>
void MixedIntegerReachable<T>::getEEs(const Vec& x,Mat3XT& ees) const {
  ees.resize(3,_eesDOF.size());
  for(int i=0; i<(int)ees.cols(); i++)
    for(int d=0; d<3; d++)
      ees(d,i)=x[_eesDOF[i][d]];
}
template <typename T>
typename MixedIntegerReachable<T>::Vec MixedIntegerReachable<T>::getBodyPose(const Vec& x,const ArticulatedBody& body) const {
  ArticulatedBody simplified;
  for(int i=0; i<body.nrJ(); i++)
    if(body.joint(i)._M>0) {
      simplified=body.resizeJoints(i+1);
      break;
    }
  Vec ret=Vec::Zero(body.nrDOF());
  ret.template segment<6>(0)=getTorsoPose(x);
  NEArticulatedGradientInfo<T> info(simplified,ret.template segment<6>(0));
  Mat3X4T TJ=info.NEArticulatedGradientInfoMap<T>::getTrans(simplified.nrJ()-1);
  //get global EE pos
  std::vector<Vec3T> ees;
  getEEs(x,ees);
  //get full body DOF
  for(int i=0; i<(int)ees.size(); i++) {
    Vec3T eeLocal=ROT(TJ).transpose()*(ees[i]-CTR(TJ));
    Vec3T nor=_env->phiGrad(ees[i]);
    nor=ROT(TJ).transpose()*nor;
    ret+=_ees[i]->getDOFAll(body,eeLocal.template cast<ArticulatedBody::T>(),nor.template cast<ArticulatedBody::T>()).template cast<T>();
  }
  return ret;
}
template <typename T>
typename MixedIntegerReachable<T>::Vec MixedIntegerReachable<T>::getBodyPose(const Vec& x,std::shared_ptr<ArticulatedBody> body) const {
  return getBodyPose(x,*body);
}
template <typename T>
typename MixedIntegerReachable<T>::Vec6T MixedIntegerReachable<T>::getTorsoPose(const Vec& x) const {
  Vec6T ret;
  ret.setZero();
  ret[0]=x[_bodyPoseDOF[0]];
  ret[1]=x[_bodyPoseDOF[1]];
  ret[2]=x[_bodyZ];
  ret[5]=x[_bodyPoseDOF[2]];
  return ret;
}
template <typename T>
int MixedIntegerReachable<T>::getBodyZ() const {
  return _bodyZ;
}
template <typename T>
void MixedIntegerReachable<T>::setTargetHeightEnergy(SQPObjectiveCompound<T>& obj,T hTarget,T hCoef) {
  PolyXT objPoly,heightDiff;
  for(int eid=0; eid<(int)_eesDOF.size(); eid++) {
    heightDiff=obj.addVarPoly(_bodyZ)-obj.addVarPoly(_eesDOF[eid][2])-PolyXT(hTarget);
    objPoly+=heightDiff*heightDiff*hCoef;
  }
  SQPObjectiveComponent<T>::setPolynomialComponentObjective(objPoly);
}
template <typename T>
typename MixedIntegerReachable<T>::PolyXT::MATP MixedIntegerReachable<T>::getRot(SQPObjectiveCompound<T>& obj,int res,T ref,int theta) {
  typename PolyXT::MATP ret,R;
  std::vector<int> weight;
  PolyXT equalTheta;
  ret.setZero(2,2);
  for(int i=0; i<res; i++) {
    T angle=i/(T)(res-1);
    angle=(ref-M_PI)*(1-angle)+(ref+M_PI)*angle;  //we use reference value to allow the largest local, optimizable variation
    weight.push_back(obj.addVar(_name+"Weight"+std::to_string(i),0,1,SQPObjectiveCompound<T>::MUST_NEW)._id);
    equalTheta+=PolyXT(angle)*obj.addVarPoly(weight.back());
    R=expWZ<T,Vec3T>(angle,NULL).template block<2,2>(0,0).template cast<PolyXT>();
    ret+=R*obj.addVarPoly(weight.back());
  }
  SQPObjectiveComponent<T>::addSOS2SumToOne(obj,_name,weight);
  SQPObjectiveComponent<T>::addPolynomialComponentConstraint((int)_gl.size(),equalTheta-obj.addVarPoly(theta));
  _gl.push_back(0);
  _gu.push_back(0);
  return ret;
}
template class MixedIntegerReachable<FLOAT>;
}
