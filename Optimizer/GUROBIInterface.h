#ifndef GUROBI_INTERFACE_H
#define GUROBI_INTERFACE_H
#ifdef GUROBI_SUPPORT

#include "SQPObjective.h"

typedef struct _GRBmodel GRBmodel;
typedef struct _GRBenv GRBenv;
namespace PHYSICSMOTION {
template <typename T>
class GUROBIInterface {
 public:
  typedef typename SQPObjective<T>::Vec Vec;
  typedef typename SQPObjective<T>::MatT MatT;
  typedef typename SQPObjective<T>::SMatT SMatT;
  typedef typename SQPObjective<T>::STrip STrip;
  typedef typename SQPObjective<T>::STrips STrips;
  GUROBIInterface();
  ~GUROBIInterface();
  bool optimize(SQPObjectiveCompound<T>& obj,bool callback,double tol,int maxNode,int maxTime,Vec& x);
  bool QPSolve(const MatT& Hn,const Vec& Gn,const Vec& Cn,const MatT& Jn,Vec& x,bool callback=false) const;
  bool QCPSolve(const MatT& Ht,const Vec& Gt,const Vec& Bt,Vec& x,bool callback=false) const;
 protected:
  void reset(SQPObjectiveCompound<T>& obj,bool callback,double tol,int maxNode,int maxTime);
  GRBmodel* _model;
  GRBenv* _env;
  //tmp eval
  int _inputs,_values;
};
}

#endif
#endif
