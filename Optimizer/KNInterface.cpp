#include "KNInterface.h"
#ifdef KNITRO_SUPPORT
#include <Utils/Timing.h>
#include <Utils/DebugGradient.h>

namespace PHYSICSMOTION {
//KTRInterface
#define KN_SAFE_CALL(X) {err=X;ASSERT_MSGV(!err,"Error calling knitro function: %s,code=%d",STRINGIFY_OMP(X),err)}
template <typename T>
KNInterface<T>::KNInterface(SQPObjective<T>& obj,bool timing)
  :_obj(obj),_cb(NULL),_timing(timing),_inputs(obj.inputs()),_values(obj.values()) {
  SQPObjectiveCompound<T>* objC=dynamic_cast<SQPObjectiveCompound<T>*>(&obj);
  if(objC)
    objC->assemblePoly();
  //assemble Knitro
  int err=0;
  KN_SAFE_CALL(KN_new(&_kc))
  ASSERT_MSG(_kc!=NULL,"Cannot initialize Knitro!")
  KN_SAFE_CALL(KN_add_vars(_kc,_inputs,NULL))
  KN_SAFE_CALL(KN_add_cons(_kc,_values,NULL))
  //set primal initial value
  updateInitial();
  //set bounds
  Eigen::Matrix<double,-1,1> lbs=obj.lb().template cast<double>();
  Eigen::Matrix<double,-1,1> ubs=obj.ub().template cast<double>();
  KN_SAFE_CALL(KN_set_var_lobnds_all(_kc,lbs.data()))
  KN_SAFE_CALL(KN_set_var_upbnds_all(_kc,ubs.data()))
  Eigen::Matrix<double,-1,1> gls=obj.gl().template cast<double>();
  Eigen::Matrix<double,-1,1> gus=obj.gu().template cast<double>();
  KN_SAFE_CALL(KN_set_con_lobnds_all(_kc,gls.data()))
  KN_SAFE_CALL(KN_set_con_upbnds_all(_kc,gus.data()))
  //set types
  Eigen::Matrix<int,-1,1> types;
  types.setConstant(_inputs,KN_VARTYPE_CONTINUOUS);
  for(int i=0; i<_inputs; i++)
    if(objC->addVar(i)._isBinary)
      types[i]=KN_VARTYPE_BINARY;
  KN_SAFE_CALL(KN_set_var_types_all(_kc,types.data()))
  //analyze linear/quadratic struct
  Eigen::SparseMatrix<double,0,int> HD,GD;
  std::vector<int> rows,cols;
  std::vector<double> data;
  if(objC) {
    //objective
    _polyCompObj=objC->getPolynomialComponentObjective();
    if(_polyCompObj) {
      //order0
      T c0=(T)_polyCompObj->retainOrder(0);
      KN_SAFE_CALL(KN_add_obj_constant(_kc,(double)c0))
      //order1
      GD=_polyCompObj->gradientSparse(1).template cast<T>().template cast<double>();
      KN_SAFE_CALL(KN_add_obj_linear_struct(_kc,GD.nonZeros(),GD.innerIndexPtr(),GD.valuePtr()))
      //order2
      rows.clear();
      cols.clear();
      data.clear();
      HD=_polyCompObj->hessianSparse(2).template cast<T>().template cast<double>();
      for(int k=0; k<HD.outerSize(); ++k)
        for(typename Eigen::SparseMatrix<double,0,int>::InnerIterator it(HD,k); it; ++it)
          if(it.row()>=it.col()) {
            rows.push_back(it.row());
            cols.push_back(it.col());
            data.push_back(it.row()==it.col()?it.value()/2:it.value());
          }
      KN_SAFE_CALL(KN_add_obj_quadratic_struct(_kc,rows.size(),&rows[0],&cols[0],&data[0]))
      //retain remaining terms
      *(objC->getPolynomialComponentObjective())=objC->getPolynomialComponentObjective()->pruneOrder(2);
      if(objC->getPolynomialComponentObjective()->order()==0)
        objC->getPolynomialComponentObjective()=NULL;
    }
    //constraint
    _polyCompCons=objC->getPolynomialComponentConstraint();
    for(const std::pair<int,typename SQPObjectiveComponent<T>::PolyXT>& c:_polyCompCons) {
      //order0
      T c0=(T)c.second.retainOrder(0);
      KN_SAFE_CALL(KN_add_con_constant(_kc,c.first,(double)c0))
      //order1
      GD=c.second.gradientSparse(1).template cast<T>().template cast<double>();
      KN_SAFE_CALL(KN_add_con_linear_struct_one(_kc,GD.nonZeros(),c.first,GD.innerIndexPtr(),GD.valuePtr()))
      //order2
      rows.clear();
      cols.clear();
      data.clear();
      HD=c.second.hessianSparse(2).template cast<T>().template cast<double>();
      for(int k=0; k<HD.outerSize(); ++k)
        for(typename Eigen::SparseMatrix<double,0,int>::InnerIterator it(HD,k); it; ++it)
          if(it.row()>=it.col()) {
            rows.push_back(it.row());
            cols.push_back(it.col());
            data.push_back(it.row()==it.col()?it.value()/2:it.value());
          }
      KN_SAFE_CALL(KN_add_con_quadratic_struct_one(_kc,rows.size(),c.first,&rows[0],&cols[0],&data[0]))
      //retain remaining terms
      objC->getPolynomialComponentConstraint()[c.first]=c.second.pruneOrder(2);
      if(objC->getPolynomialComponentConstraint()[c.first].order()==0)
        objC->getPolynomialComponentConstraint().erase(c.first);
    }
  }
  //callbacks
  rows.clear();
  for(int i=0; i<_obj.values(); i++)
    rows.push_back(i);
  KN_SAFE_CALL(KN_add_eval_callback(_kc,true,rows.size(),&rows[0],&callbackEvalFC,&_cb))
  _obj(_obj.init(),_fvec,&_fjac);
  rows.clear();
  cols.clear();
  rows.reserve(_fjac.nonZeros());
  cols.reserve(_fjac.nonZeros());
  _jacNonZeros=_fjac.nonZeros();
  for(int k=0; k<_fjac.outerSize(); ++k)
    for(typename SMatT::InnerIterator it(_fjac,k); it; ++it) {
      rows.push_back(it.row());
      cols.push_back(it.col());
    }
  KN_SAFE_CALL(KN_set_cb_grad(_kc,_cb,KN_DENSE,NULL,_fjac.nonZeros(),&rows[0],&cols[0],&callbackEvalGA))
  KN_SAFE_CALL(KN_set_cb_user_params(_kc,_cb,(void*)this))
}
template <typename T>
KNInterface<T>::~KNInterface() {
  int err=0;
  KN_SAFE_CALL(KN_free(&_kc))
  SQPObjectiveCompound<T>* objC=dynamic_cast<SQPObjectiveCompound<T>*>(&_obj);
  if(objC) {
    objC->getPolynomialComponentObjective()=_polyCompObj;
    objC->getPolynomialComponentConstraint()=_polyCompCons;
  }
}
template <typename T>
void KNInterface<T>::updateInitial() {
  int err=0;
  _x=_obj.init().template cast<double>();
  for(int i=0; i<_obj.inputs(); i++) {
    KN_SAFE_CALL(KN_set_var_primal_init_value(_kc,i,_x[i]))
  }
  _x.resize(0);
}
template <typename T>
bool KNInterface<T>::optimize(bool callback,double checkDeriv,double ftol,double xtol,bool useDirect,int maxIter,double muInit,Vec& x,Vec* fvec) {
  int err=0;
  KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_ALGORITHM,useDirect?KN_ALG_BAR_DIRECT:KN_ALG_BAR_CG))
  //KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_ALGORITHM,useDirect?KN_ALG_ACT_SQP:KN_ALG_ACT_CG))
  KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_BAR_FEASIBLE,KN_BAR_FEASIBLE_GET_STAY))
  KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_OUTMODE,callback?KN_OUTMODE_SCREEN:KN_OUTMODE_FILE))
  KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_CG_MAXIT,50))
  //KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_CG_PRECOND,KN_CG_PRECOND_CHOL))
  KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_OUTLEV,KN_OUTLEV_ITER))
  if(checkDeriv>0) {
    KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_DERIVCHECK,KN_DERIVCHECK_FIRST))
    KN_SAFE_CALL(KN_set_double_param(_kc,KN_PARAM_DERIVCHECK_TOL,(double)checkDeriv))
  }
  if(ftol>0) {
    KN_SAFE_CALL(KN_set_double_param(_kc,KN_PARAM_FTOL,(double)ftol))
  }
  if(xtol>0) {
    KN_SAFE_CALL(KN_set_double_param(_kc,KN_PARAM_XTOL,(double)xtol))
  }
  KN_SAFE_CALL(KN_set_double_param(_kc,KN_PARAM_FEASTOL,(double)1e-3f))
  if(maxIter>0) {
    KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_MAXIT,(int)maxIter))
  }
  if(muInit>0) {
    KN_SAFE_CALL(KN_set_int_param(_kc,KN_PARAM_BAR_PENRULE,KN_BAR_PENRULE_SINGLE))
    KN_SAFE_CALL(KN_set_double_param(_kc,KN_PARAM_INITPENALTY,(double)muInit))
  }

  //solve
  KN_solve(_kc);

  //fetch result
  double objSol=0;
  int solveStatus=0;
  Eigen::Matrix<double,-1,1> primal;
  primal.resize(_obj.inputs());
  KN_SAFE_CALL(KN_get_solution(_kc,&solveStatus,&objSol,primal.data(),NULL))
  bool succ=(-103<=solveStatus&&solveStatus<=-100) || (-406<=solveStatus&&solveStatus<=-400) || (solveStatus>=0);
  x=primal.template cast<T>();
  if(fvec)
    _obj.operator()(x,*fvec,(STrips*)NULL);
  return succ;
}
template <typename T>
KNInterface<T>::KNInterface(const KNInterface& other):_obj(other._obj) {}
template <typename T>
KNInterface<T>& KNInterface<T>::operator=(const KNInterface&) {
  return *this;
}
template <typename T>
int KNInterface<T>::callbackEvalFC(KN_context_ptr,CB_context_ptr,
                                   KN_eval_request_ptr const evalRequest,
                                   KN_eval_result_ptr const evalResult,
                                   void* const userParams) {
  ASSERT(evalRequest->type==KN_RC_EVALFC)
  const double* x=evalRequest->x;
  double* obj=evalResult->obj;
  double* c=evalResult->c;
  KNInterface<T>* ptr=(KNInterface<T>*)userParams;
  if(ptr->_x.size()!=ptr->_inputs || ptr->_x!=Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,ptr->_inputs)) {
    ptr->_x=Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,ptr->_inputs);
    ptr->_f=ptr->_obj(ptr->_x.template cast<T>(),&(ptr->_grad));
    ptr->_obj(ptr->_x.template cast<T>(),ptr->_fvec,&(ptr->_fjac));
  }
  *obj=(double)ptr->_f;
  Eigen::Map<Eigen::Matrix<double,-1,1>>(c,ptr->_values)=ptr->_fvec.template cast<double>();
  return(0);
}
template <typename T>
int KNInterface<T>::callbackEvalGA(KN_context_ptr,CB_context_ptr,
                                   KN_eval_request_ptr const evalRequest,
                                   KN_eval_result_ptr const evalResult,
                                   void* const userParams) {
  ASSERT(evalRequest->type==KN_RC_EVALGA)
  const double* x=evalRequest->x;
  double* objGrad=evalResult->objGrad;
  double* jac=evalResult->jac;
  KNInterface<T>* ptr=(KNInterface<T>*)userParams;
  if(ptr->_x.size()!=ptr->_inputs || ptr->_x!=Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,ptr->_inputs)) {
    ptr->_x=Eigen::Map<const Eigen::Matrix<double,-1,1>>(x,ptr->_inputs);
    ptr->_f=ptr->_obj(ptr->_x.template cast<T>(),&(ptr->_grad));
    ptr->_obj(ptr->_x.template cast<T>(),ptr->_fvec,&(ptr->_fjac));
  }
  ASSERT(ptr->_jacNonZeros==ptr->_fjac.nonZeros())
  Eigen::Map<Eigen::Matrix<double,-1,1>>(objGrad,ptr->_inputs)=ptr->_grad.template cast<double>();
  Eigen::Map<Eigen::Matrix<double,-1,1>>(jac,ptr->_jacNonZeros)=Eigen::Map<Vec>(ptr->_fjac.valuePtr(),ptr->_jacNonZeros).template cast<double>();
  return(0);
}
template class KNInterface<FLOAT>;
#ifdef FORCE_ADD_DOUBLE_PRECISION
template struct KNInterface<double>;
#endif
}

#endif
