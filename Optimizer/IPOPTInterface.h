#ifndef IPOPT_INTERFACE_H
#define IPOPT_INTERFACE_H
#ifdef IPOPT_SUPPORT

#include <coin/IpTNLP.hpp>
#include "SQPObjective.h"

namespace PHYSICSMOTION {
template <typename T>
class IPOPTInterface : public Ipopt::TNLP {
  typedef Ipopt::IpoptCalculatedQuantities IpoptCalculatedQuantities;
  typedef Ipopt::SolverReturn SolverReturn;
  typedef Ipopt::IpoptData IpoptData;
  typedef Ipopt::Number Number;
  typedef Ipopt::Index Index;
 public:
  IPOPTInterface(SQPObjective<T>& obj,bool timing=false);
  virtual void set_xlu(const SQPObjective<double>::Vec& xl,const SQPObjective<double>::Vec& xu);
  virtual void set_glu(const SQPObjective<double>::Vec& gl,const SQPObjective<double>::Vec& gu);
  virtual bool get_nlp_info(Index& n,Index& m,Index& nnz_jac_g,
                            Index& nnz_h_lag,IndexStyleEnum& index_style) override;
  virtual bool get_bounds_info(Index n,Number* x_l,Number* x_u,
                               Index m,Number* g_l,Number* g_u) override;
  virtual bool get_starting_point(Index n,bool init_x,Number* x,
                                  bool init_z,Number* z_L,Number* z_U,
                                  Index,bool init_lambda,
                                  Number* lambda) override;
  virtual bool eval_f(Index n,const Number* x,bool new_x,Number& obj_value) override;
  virtual bool eval_grad_f(Index n,const Number* x,bool new_x,Number* grad_f) override;
  virtual bool eval_g(Index n,const Number* x,bool new_x,Index m,Number* g) override;
  virtual bool eval_jac_g(Index n,const Number* x,bool new_x,
                          Index,Index nele_jac,Index* iRow,Index *jCol,
                          Number* values) override;
  virtual void finalize_solution(SolverReturn,
                                 Index n,const Number* x,const Number*,const Number*,
                                 Index,const Number*,const Number*,
                                 Number,const IpoptData*,
                                 IpoptCalculatedQuantities*) override;
  void updateInitial();
  static bool optimize(SQPObjective<T>& obj,int callback,double derivCheck,double tolG,int maxIter,double muInit,typename SQPObjective<T>::Vec& x);
  static double getInfty() {
    return 2e19;
  }
 protected:
  void updateX(const SQPObjective<double>::Vec& x);
  bool _timing;
  SQPObjective<T>& _obj;
  SQPObjective<double>::SMatT _gJacS,_fHessS;
  std::shared_ptr<SQPObjective<double>::Vec> _xl,_xu;
  std::shared_ptr<SQPObjective<double>::Vec> _gl,_gu;
  SQPObjective<double>::Vec _gVec,_fGrad;
  SQPObjective<double>::Vec _x;
  double _E;
};
}

#endif
#endif
