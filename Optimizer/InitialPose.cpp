#include "InitialPose.h"

namespace PHYSICSMOTION {
template <typename T>
InitialPose<T>::InitialPose(SQPObjectiveCompound<T>& obj,const std::string& name,
                            const Mat3T& frame,const Mat3T& frameTarget,
                            std::shared_ptr<Environment<T>> env,
                            std::shared_ptr<ArticulatedBody> body,bool rootOnly,Vec* xInit,
                            const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"InitialPose"),_env(env),_body(body),_ees(ees),_frame(frame),_frameTarget(frameTarget) {
  if(inherited)
    return;
  //DOF
  _DOFVars.resize(_body->nrDOF());
  Vec l=_body->lowerLimit(SQPObjective<typename ArticulatedBody::T>::infty()).template cast<T>();
  Vec u=_body->upperLimit(SQPObjective<typename ArticulatedBody::T>::infty()).template cast<T>();
  if(rootOnly) {
    const Joint& J=_body->joint(_body->rootJointId());
    int off=J._offDOF+J.nrDOF();
    for(int d=off; d<_body->nrDOF(); d++)
      l[d]=u[d]=xInit&&d<xInit->size()?(*xInit)[d]:0;
  }
  for(int i=0; i<_body->nrDOF(); i++)
    _DOFVars[i]=obj.addVar("DOF"+std::to_string(i),l[i],u[i],SQPObjectiveCompound<T>::NEW_OR_EXIST)._id;
  //frame constraint
  for(int c=0; c<3; c++)
    for(int r=0; r<3; r++) {
      _gl.push_back(0);
      _gu.push_back(0);
    }
  //position constraint: X/Y axes
  for(int d=0; d<2; d++) {
    _gl.push_back(xInit&&d<xInit->size()?(*xInit)[d]:0);
    _gu.push_back(xInit&&d<xInit->size()?(*xInit)[d]:0);
  }
}
template <typename T>
int InitialPose<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  Vec xc=Vec::Zero(_DOFVars.rows());
  for(int r=0; r<xc.size(); r++)
    xc[r]=x[_DOFVars[r]];
  _info.reset(*_body,xc);
  //frame constraint
  Mat3T R0=ROTI(_info._TM,_body->rootJointId());
  Mat3T RDiff=R0*_frame-_frameTarget;
  for(int c=0; c<3; c++)
    fvec.template segment<3>(_offset+c*3)=Eigen::Map<Vec3T>(RDiff.data()+c*3);
  if(fjac)
    _info.JRMSparse(*_body,_body->rootJointId(),[&](int DOF,const Mat3T& dR) {
    for(int c=0; c<3; c++)
      for(int r=0; r<3; r++)
        fjac->push_back(STrip(_offset+r+c*3,DOF,dR.row(r).dot(_frame.col(c))));
  });
  //position constraint: X/Y axes
  Vec3T C0=CTRI(_info._TM,_body->rootJointId());
  fvec.template segment<2>(_offset+9)=C0.template segment<2>(0);
  if(fjac)
    for(int r=0; r<2; r++) {
      Mat3X4T GK=Mat3X4T::Zero();
      GK(r,3)=1;
      _info.DTG(_body->rootJointId(),*_body,GK,[&](int DOF,T val) {
        fjac->push_back(STrip(_offset+r+9,DOF,val));
      });
    }
  return 0;
}
template <typename T>
T InitialPose<T>::operator()(const Vec& x,Vec* fgrad) {
  Vec xc=Vec::Zero(_DOFVars.rows());
  for(int r=0; r<xc.size(); r++)
    xc[r]=x[_DOFVars[r]];

  T ret=0;
  _info.reset(*_body,xc);
  for(std::shared_ptr<EndEffectorBounds>& ef:_ees) {
    Vec3T g,pos=ROTI(_info._TM,ef->jointId())*ef->_localPos.template cast<T>()+CTRI(_info._TM,ef->jointId());
    T phi=_env->phi(pos,fgrad?&g:NULL);
    ret+=(phi-ef->_phi0)*(phi-ef->_phi0);
    if(fgrad) {
      g*=(phi-ef->_phi0);
      Mat3X4T GK=Mat3X4T::Zero();
      GK.col(0)=g*ef->_localPos[0];
      GK.col(1)=g*ef->_localPos[1];
      GK.col(2)=g*ef->_localPos[2];
      GK.col(3)=g;
      _info.DTG(ef->jointId(),*_body,GK,[&](int DOF,T val) {
        fgrad->coeffRef(DOF)+=val;
      });
    }
  }
  return ret/2;
}
template <typename T>
const Eigen::Matrix<int,-1,1>& InitialPose<T>::DOFVars() const {
  return _DOFVars;
}
template class InitialPose<FLOAT>;
}
