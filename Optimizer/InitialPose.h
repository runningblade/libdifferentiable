#ifndef INITIAL_POSE_H
#define INITIAL_POSE_H

#include <Articulated/ArticulatedBody.h>
#include <Articulated/EndEffectorInfo.h>
#include <Articulated/PBDArticulatedGradientInfo.h>
#include <Environment/Environment.h>
#include <Optimizer/SQPObjective.h>

namespace PHYSICSMOTION {
template <typename T>
class InitialPose : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  DECL_MAP_FUNCS
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  InitialPose(SQPObjectiveCompound<T>& obj,const std::string& name,
              const Mat3T& frame,const Mat3T& frameTarget,
              std::shared_ptr<Environment<T>> env,
              std::shared_ptr<ArticulatedBody> body,bool rootOnly,Vec* xInit,
              const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,bool inherited=false);
  int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  T operator()(const Vec& x,Vec* fgrad) override;
  virtual const Eigen::Matrix<int,-1,1>& DOFVars() const;
 protected:
  std::shared_ptr<Environment<T>> _env;
  std::shared_ptr<ArticulatedBody> _body;
  std::vector<std::shared_ptr<EndEffectorBounds>> _ees;
  Mat3T _frame,_frameTarget;
  Eigen::Matrix<int,-1,1> _DOFVars;
  PBDArticulatedGradientInfo<T> _info;
};
}

#endif
