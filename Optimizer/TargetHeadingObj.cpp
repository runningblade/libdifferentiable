#include "TargetHeadingObj.h"
#include "LaplaceObjective.h"
#include "PBDDynamicsSequence.h"
#include "NEDynamicsSequence.h"
#include "NESimplifiedDynamicsSequence.h"
#include "NESimplifiedDynamicsSequenceSpline.h"

namespace PHYSICSMOTION {
template <typename T>
TargetHeadingObj<T>::TargetHeadingObj(SQPObjectiveCompound<T>& obj,const std::string& name,const Vec3T& target,T coef,std::shared_ptr<PBDDynamicsSequence<T>> seq,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"TargetHeadingObj"),_target(target),_coef(coef) {
  if(inherited)
    return;
  if(std::dynamic_pointer_cast<NESimplifiedDynamicsSequenceSpline<T>>(seq))
    resetSpline(obj,target,std::dynamic_pointer_cast<NESimplifiedDynamicsSequenceSpline<T>>(seq)->DOFSeq());
  else if(std::dynamic_pointer_cast<PBDDynamicsSequence<T>>(seq))
    resetDiscrete(obj,target,seq);
  else if(std::dynamic_pointer_cast<NEDynamicsSequence<T>>(seq))
    resetDiscrete(obj,target,seq);
  else if(std::dynamic_pointer_cast<NESimplifiedDynamicsSequence<T>>(seq))
    resetDiscrete(obj,target,seq);
  else {
    ASSERT_MSG(false,"Unsupported dynamics sequence type!")
  }
}
template <typename T>
TargetHeadingObj<T>::TargetHeadingObj(SQPObjectiveCompound<T>& obj,const std::string& name,const Vec3T& target,T coef,std::shared_ptr<VectorSequence<T,6>> seq,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"TargetHeadingObj"),_target(target),_coef(coef) {
  if(inherited)
    return;
  resetSpline(obj,target,seq);
}
template <typename T>
const typename TargetHeadingObj<T>::PolyXT& TargetHeadingObj<T>::objPoly() const {
  return _objPoly;
}
template <typename T>
const typename TargetHeadingObj<T>::Vec3T& TargetHeadingObj<T>::target() const {
  return _target;
}
template <typename T>
void TargetHeadingObj<T>::resetDiscrete(SQPObjectiveCompound<T>& obj,const Vec3T& target,std::shared_ptr<PBDDynamicsSequence<T>> seq) {
  TermXT param[2];
  _objPoly=PolyXT();
  // \int_{t=0}^H ||heading(t)-target||^2/2 dt
  const Eigen::Matrix<int,-1,1>& DOFVars=seq->DOFVar();
  for(int c=0; DOFVars.cols(); c++)
    for(int d=0; d<3; d++) {
      param[0]=obj.addVarTerm(DOFVars(3+d,c));
      param[1]=T(-target[d]);
      _objPoly+=LaplaceObjective<T>::polyContract(param,Mat3T::Ones());
    }
  _objPoly=_coef/2*seq->dt();
  SQPObjectiveComponent<T>::addPolynomialComponentObjective(_objPoly);
}
template <typename T>
void TargetHeadingObj<T>::resetSpline(SQPObjectiveCompound<T>& obj,const Vec3T& target,std::shared_ptr<VectorSequence<T,6>> DOFSeq) {
  Eigen::Matrix<T,5,5> coef;
  TermXT params[5];
  _objPoly=PolyXT();
  for(int s=0,c=0; s<std::abs(DOFSeq->getPhase()->stage()); s++) {
    for(int ss=0; ss<DOFSeq->getPhase()->subStage(); ss++,c++) {
      T span=DOFSeq->getPhase()->timeSpanVar(obj.init(),s)/DOFSeq->getPhase()->subStage();
      coef(0,0)=(13.0/35.0)*span;
      coef(0,1)=(11.0/210.0)*pow(span,2);
      coef(0,2)=(9.0/70.0)*span;
      coef(0,3)=-13.0/420.0*pow(span,2);
      coef(0,4)=-1.0/2.0*span;
      coef(1,0)=coef(0,1);
      coef(1,1)=(1.0/105.0)*pow(span,3);
      coef(1,2)=(13.0/420.0)*pow(span,2);
      coef(1,3)=-1.0/140.0*pow(span,3);
      coef(1,4)=-1.0/12.0*pow(span,2);
      coef(2,0)=coef(0,2);
      coef(2,1)=coef(1,2);
      coef(2,2)=(13.0/35.0)*span;
      coef(2,3)=-11.0/210.0*pow(span,2);
      coef(2,4)=-1.0/2.0*span;
      coef(3,0)=coef(0,3);
      coef(3,1)=coef(1,3);
      coef(3,2)=coef(2,3);
      coef(3,3)=(1.0/105.0)*pow(span,3);
      coef(3,4)=(1.0/12.0)*pow(span,2);
      coef(4,0)=coef(0,4);
      coef(4,1)=coef(1,4);
      coef(4,2)=coef(2,4);
      coef(4,3)=coef(3,4);
      coef(4,4)=span;
      for(int d=0; d<3; d++) {
        params[0]=obj.addVarTerm(DOFSeq->getVar()(3+d,c));
        params[1]=obj.addVarTerm(DOFSeq->getDiffVar()(3+d,c));
        params[2]=obj.addVarTerm(DOFSeq->getVar()(3+d,c+1));
        params[3]=obj.addVarTerm(DOFSeq->getDiffVar()(3+d,c+1));
        params[4]=target[d];
        _objPoly+=LaplaceObjective<T>::polyContract(params,coef);
      }
    }
  }
  _objPoly*=_coef/2;
  SQPObjectiveComponent<T>::addPolynomialComponentObjective(_objPoly);
}
template class TargetHeadingObj<FLOAT>;
}
