#include "ForceSequence.h"
#include <Environment/DeformedEnvironment.h>
#include <Utils/DebugGradient.h>

namespace PHYSICSMOTION {
template <typename T>
ForceSequence<T>::ForceSequence(SQPObjectiveCompound<T>& obj,const std::string& name,
                                int forceInConeStencilSize,T mu,bool even,const Vec3T& n0,
                                std::shared_ptr<PositionSequence<T>> position,bool inherited)
  :VectorSequence<T>(obj,inherited?name:name+"ForceSequence",even,position->getPhase(),true),_mu(mu),_n0(n0),_position(position) {
  if(inherited)
    return;
  if(!_n0.isZero())
    _n0.normalize();
  //force (derivative)
  std::shared_ptr<PhaseSequence<T>> phase=position->getPhase();
  _var.setConstant(3,phase->nrColumn(even),-1);
  _diffVar.setConstant(3,phase->nrColumn(even),-1);
  for(int r=0; r<3; r++)
    for(int c=0; c<phase->nrColumn(even); c++) {
      if(phase->segmentOffset(c)==0 || phase->segmentOffset(c)==phase->subStage())
        continue;
      _diffVar(r,c)=obj.addVar(diffVar(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
      _var(r,c)=obj.addVar(var(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
    }
  //position above ground
  T eps=1e-6f;
  T offTime=0,span;
  Vec init=obj.init();
  GRAD_INTERP grad[3];
  bool constantNormal=!_n0.isZero();
  const Eigen::Matrix<int,-1,1>& timeVar=_phase->timeSpanVar();
  for(int i=0; i<timeVar.size(); i++) {
    span=init[timeVar[i]];
    if(i%2!=_even)
      for(int c=0; c<forceInConeStencilSize+1; c++) {
        T time=offTime+span*c/forceInConeStencilSize;
        if(c==0 || c==forceInConeStencilSize)
          continue;
        //force
        timeInterpolateStencil(init,time,grad,NULL,false);
        for(int r=0; r<3; r++)
          _forceInConeStencil.push_back(removeNonZero(grad[r]));
        //position
        if(!constantNormal) {
          _position->timeInterpolateStencil(init,time,grad,NULL,false);
          for(int r=0; r<3; r++)
            _forceInConePositionStencil.push_back(removeNonZero(grad[r]));
        }
        //force.dot(n)>=||f-fn*n||
        _gl.push_back(-sqrt(eps));
        _gu.push_back( SQPObjective<T>::infty());
      }
    offTime+=span;
  }
  //trivial initialization
  initConstant(Vec3T::Zero(),obj);
}
template <typename T>
void ForceSequence<T>::setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv) {
  _DEnv=DEnv;
}
template <typename T>
int ForceSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  bool constantNormal=!_n0.isZero();
  std::shared_ptr<Environment<T>> env=constantNormal?NULL:_position->getEnv();
  T eps=1e-6f;
  OMP_PARALLEL_FOR_
  for(int i=0; i<(int)_forceInConeStencil.size(); i+=3) {
    Mat3T dndp;
    Vec3T pos=Vec3T::Zero(),f=Vec3T::Zero(),n,gf,gp;
    //compute f
    for(int r=0; r<3; r++)
      for(std::pair<int,T> stencil:_forceInConeStencil[i+r])
        f[r]+=x[stencil.first]*stencil.second;
    //force.dot(n)>=||f-fn*n||
    //compute n
    if(_DEnv)
      n=Vec3T::UnitZ();
    else if(env) {
      for(int r=0; r<3; r++)
        for(std::pair<int,T> stencil:_forceInConePositionStencil[i+r])
          pos[r]+=x[stencil.first]*stencil.second;
      n=env->phiGrad(pos,&dndp);
    } else n=_n0;
    //compute constraint
    int offset=_offset+i/3;
    T fn=n.dot(f);
    T ft=sqrt(f.squaredNorm()-fn*fn+eps);
    fvec[offset]=fn*getMu()-ft;
    gf=n*getMu()-(f-fn*n)/ft;
    if(fjac)
      for(int r=0; r<3; r++)
        for(std::pair<int,T> stencil:_forceInConeStencil[i+r])
          fjac->push_back(STrip(offset,stencil.first,gf[r]*stencil.second));
    if(!_DEnv && env) {
      gp=(getMu()+fn/ft)*(dndp.transpose()*f);
      if(fjac)
        for(int r=0; r<3; r++)
          for(std::pair<int,T> stencil:_forceInConePositionStencil[i+r])
            fjac->push_back(STrip(offset,stencil.first,gp[r]*stencil.second));
    }
  }
  return 0;
}
template <typename T>
typename ForceSequence<T>::VecDT ForceSequence<T>::timeInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan) const {
  if(_DEnv) {
    Mat3T J,H[3];
    Vec3T gradATime,gradFTime;
    GRAD_INTERP gradF[3],gradA[3];
    Vec3T f=VectorSequence<T>::timeInterpolateStencil(x,time,grad?gradF:NULL,gradTime?&gradFTime:NULL,gradTimeSpan);
    Vec3T a=_position->VectorSequence<T>::timeInterpolateStencil(x,time,grad?gradA:NULL,gradTime?&gradATime:NULL,gradTimeSpan);
    _DEnv->forward(a,&J,H);
    if(grad) {
      //allocate
      for(int d2=0; d2<3; d2++)
        grad[d2].clear();
      for(int d=0; d<3; d++) {
        for(const auto& g:gradF[d])
          for(int d2=0; d2<3; d2++)
            grad[d2][g.first]=0;
        for(const auto& g:gradA[d])
          for(int d2=0; d2<3; d2++)
            grad[d2][g.first]=0;
      }
      //fill
      for(int d=0; d<3; d++) {
        Vec3T dfda=H[d]*f;
        for(const auto& g:gradA[d])
          for(int d2=0; d2<3; d2++)
            grad[d2][g.first]+=dfda[d2]*g.second;
        for(const auto& g:gradF[d])
          for(int d2=0; d2<3; d2++)
            grad[d2][g.first]+=J(d2,d)*g.second;
      }
    }
    if(gradTime)
      *gradTime=J*gradFTime+(H[0]*gradATime[0]+H[1]*gradATime[1]+H[2]*gradATime[2])*f;
    return J*f;
  } else {
    return VectorSequence<T>::timeInterpolateStencil(x,time,grad,gradTime,gradTimeSpan);
  }
}
template <typename T>
typename ForceSequence<T>::VecDT ForceSequence<T>::diffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan) const {
  if(_DEnv) {
    FUNCTION_NOT_IMPLEMENTED
  } else return VectorSequence<T>::diffInterpolateStencil(x,time,grad,gradTime,gradTimeSpan);
}
template <typename T>
typename ForceSequence<T>::VecDT ForceSequence<T>::ddiffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan) const {
  if(_DEnv) {
    FUNCTION_NOT_IMPLEMENTED
  } else return VectorSequence<T>::ddiffInterpolateStencil(x,time,grad,gradTime,gradTimeSpan);
}
template <typename T>
std::vector<typename ForceSequence<T>::Vec3T> ForceSequence<T>::getForceInConeStencil(const Vec& x) const {
  std::vector<Vec3T> ret;
  for(int i=0; i<(int)_forceInConeStencil.size(); i+=3) {
    Vec3T pos=Vec3T::Zero();
    for(int r=0; r<3; r++)
      for(std::pair<int,T> stencil:_forceInConeStencil[i+r])
        pos[r]+=x[stencil.first]*stencil.second;
    ret.push_back(pos);
  }
  std::cout << "ForceSequence[" << _name << "] has " << ret.size() << " force-in-cone test points!" << std::endl;
  return ret;
}
template <typename T>
T ForceSequence<T>::getMu() const {
  return _mu;
}
template class ForceSequence<FLOAT>;
}
