#ifndef TARGET_VELOCITY_OBJ_H
#define TARGET_VELOCITY_OBJ_H

#include "PBDDynamicsSequence.h"

namespace PHYSICSMOTION {
template <typename T>
class TargetVelocityObj : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  typedef typename SQPObjectiveCompound<T>::TermXT TermXT;
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  TargetVelocityObj(SQPObjectiveCompound<T>& obj,const std::string& name,const Vec3T& target,T coef,std::shared_ptr<PBDDynamicsSequence<T>> seq,bool inherited=false);
  TargetVelocityObj(SQPObjectiveCompound<T>& obj,const std::string& name,const Vec3T& target,T coef,std::shared_ptr<VectorSequence<T,6>> seq,bool inherited=false);
  const PolyXT& objPoly() const;
  const Vec3T& target() const;
 private:
  virtual void resetDiscrete(SQPObjectiveCompound<T>& obj,const Vec3T& target,std::shared_ptr<PBDDynamicsSequence<T>> seq);
  virtual void resetSpline(SQPObjectiveCompound<T>& obj,const Vec3T& target,std::shared_ptr<VectorSequence<T,6>> DOFSeq);
  PolyXT _objPoly;
  Vec3T _target;
  T _coef;
};
}
#endif
