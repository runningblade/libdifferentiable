#ifndef MIXED_INTEGER_REACHABLE_H
#define MIXED_INTEGER_REACHABLE_H

#include "SQPObjective.h"
#include <Environment/Environment.h>
#include <Articulated/EndEffectorInfo.h>

namespace PHYSICSMOTION {
template <typename T>
class MixedIntegerReachable : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  typedef typename SQPObjectiveCompound<T>::PolyXT PolyXT;
  MixedIntegerReachable(SQPObjectiveCompound<T>& obj,const std::string& name,int res,T ref,std::shared_ptr<Environment<T>> env,bool linearizeEnv,
                        const Eigen::Matrix<int,3,1>& bodyPoseDOF,const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,
                        const std::vector<Eigen::Matrix<int,2,1>>& eesDOF,bool inherited=false);
  int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  void getEEs(const Vec& x,std::vector<Vec3T>& ees) const;
  void getEEs(const Vec& x,Mat3XT& ees) const;
  Vec getBodyPose(const Vec& x,const ArticulatedBody& body) const;
  Vec getBodyPose(const Vec& x,std::shared_ptr<ArticulatedBody> body) const;
  Vec6T getTorsoPose(const Vec& x) const;
  int getBodyZ() const;
  void setTargetHeightEnergy(SQPObjectiveCompound<T>& obj,T hTarget,T hCoef);
  typename PolyXT::MATP getRot(SQPObjectiveCompound<T>& obj,int res,T ref,int theta);
 protected:
  //data
  bool _linearizeEnv;
  const Eigen::Matrix<int,3,1> _bodyPoseDOF;
  std::vector<Eigen::Matrix<int,3,1>> _eesDOF;
  const std::vector<std::shared_ptr<EndEffectorBounds>>& _ees;
  std::shared_ptr<Environment<T>> _env;
  typename PolyXT::MATP _R;
  int _bodyZ;
};
}

#endif
