#ifndef FORCE_SEQUENCE_H
#define FORCE_SEQUENCE_H

#include "VectorSequence.h"
#include "PositionSequence.h"

namespace PHYSICSMOTION {
template <typename T>
class ForceSequence : public VectorSequence<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
#ifndef SWIG
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using typename VectorSequence<T>::GRAD_INTERP;
  using typename VectorSequence<T>::VecDT;
  using VectorSequence<T>::_name;
  using VectorSequence<T>::_gl;
  using VectorSequence<T>::_gu;
  using VectorSequence<T>::_offset;
  using VectorSequence<T>::_even;
  using VectorSequence<T>::_var;
  using VectorSequence<T>::_diffVar;
  using VectorSequence<T>::_phase;
  using VectorSequence<T>::debugGradientConditional;
  using VectorSequence<T>::debugGradientConditionalVID;
  using VectorSequence<T>::timeInterpolateStencil;
  using VectorSequence<T>::diffInterpolateStencil;
  using VectorSequence<T>::ddiffInterpolateStencil;
  using VectorSequence<T>::diffVar;
  using VectorSequence<T>::var;
  using VectorSequence<T>::removeNonZero;
  using VectorSequence<T>::initConstant;
#endif
 public:
  ForceSequence(SQPObjectiveCompound<T>& obj,const std::string& name,
                int forceInConeStencilSize,T mu,bool even,const Vec3T& n0,
                std::shared_ptr<PositionSequence<T>> position,bool inherited=false);
  void setDeformedEnvironment(std::shared_ptr<DeformedEnvironment<T>> DEnv=NULL);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  virtual VecDT timeInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan=true) const override;
  virtual VecDT diffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan=true) const override;
  virtual VecDT ddiffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[3],VecDT* gradTime,bool gradTimeSpan=true) const override;
  std::vector<Vec3T> getForceInConeStencil(const Vec& x) const;
  T getMu() const;
 protected:
  T _mu;
  Vec3T _n0;
  std::shared_ptr<PositionSequence<T>> _position;
  std::vector<GRAD_INTERP> _forceInConeStencil;
  std::vector<GRAD_INTERP> _forceInConePositionStencil;
  std::shared_ptr<DeformedEnvironment<T>> _DEnv;
};
}

#endif
