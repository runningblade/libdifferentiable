#include "PhaseSequence.h"
#include <Utils/DebugGradient.h>
#include <random>

namespace PHYSICSMOTION {
#define USE_POLYNOMIAL_OBJ_CONS
template <typename T>
PhaseSequence<T>::PhaseSequence(SQPObjectiveCompound<T>& obj,const std::string& name,T eps,T horizon,int stage,int subStage,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:name+"PhaseSequence"),_eps(eps),_horizon(horizon),_stage(std::abs(stage)),_subStage(subStage) {
  if(inherited)
    return;
  //declare timeSpanVar
  if(stage<0)
    _timeSpanVar.resize(0);
  else {
    _timeSpanVar.resize(stage);
    for(int i=0; i<stage; i++)
      _timeSpanVar[i]=obj.addVar(timeSpanVar(i),_eps,SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
    //initSameSpan(obj);
    //time-span constraint
    _gl.push_back(horizon);
    _gu.push_back(horizon);
#ifdef USE_POLYNOMIAL_OBJ_CONS
    PolyXT sumTime;
    for(int i=0; i<stage; i++)
      sumTime+=obj.addVarTerm(_timeSpanVar[i]);
    SQPObjectiveComponent<T>::addPolynomialComponentConstraint(0,sumTime);
#endif
  }
  initSameSpan(obj);
}
template <typename T>
int PhaseSequence<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  if(_timeSpanVar.size()>0) {
#ifdef USE_POLYNOMIAL_OBJ_CONS
#else
    for(int i=0; i<_timeSpanVar.size(); i++) {
      fvec[_offset]+=x[_timeSpanVar[i]];
      if(fjac)
        fjac->push_back(STrip(_offset,_timeSpanVar[i],1));
    }
#endif
  }
  return 0;
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::interpTpl(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad,HERMITE h,HERMITE hFirst,HERMITE hLast) const {
  stage=0;
  offset=0;
  Vec4T c,dcdt,dcdt0,dcds;
  T t0=even?0:timeSpanVar(x,0),t0First=t0,span=0;
  for(stage=even?0:1; stage<_stage; stage++) {
    //test whether this is a static phase or a swing phase
    int subStageAll=(stage%2==1)==even?1:_subStage;
    span=timeSpanVar(x,stage)/subStageAll;
    for(subStage=0; subStage<subStageAll; subStage++) {
      //test whether time falls into this span
      bool found=false;
      if(time<t0First) {
        hFirst(span,time,t0,c,dcdt,dcdt0,dcds);
        found=true;
      } else if(time>=t0 && time<t0+span) {
        h(span,time,t0,c,dcdt,dcdt0,dcds);
        found=true;
      } else if(offset==nrColumn(even)-2) {
        hLast(span,time,t0,c,dcdt,dcdt0,dcds);
        found=true;
      }
      //compute gradient and return
      if(found) {
        alpha=(time-t0)/span;
        if(grad) {
          grad->setZero(4,_stage+1);
          for(int j=0; j<stage; j++)
            grad->col(j)=dcdt0;
          grad->col(stage)=(dcdt0*subStage+dcds)/subStageAll;
          grad->col(_stage)=dcdt;
        }
        return c;
      }
      //update offset
      offset++;
      t0+=span;
    }
  }
  ASSERT(false)
  return c;
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::timeInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad) const {
  return interpTpl(x,time,alpha,stage,subStage,offset,even,grad,coeffHermite,coeffHermiteFirst,coeffHermiteLast);
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::diffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad) const {
  return interpTpl(x,time,alpha,stage,subStage,offset,even,grad,diffCoeffHermite,diffCoeffHermiteFirst,diffCoeffHermiteLast);
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::ddiffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad) const {
  return interpTpl(x,time,alpha,stage,subStage,offset,even,grad,ddiffCoeffHermite,ddiffCoeffHermiteFirst,ddiffCoeffHermiteLast);
}
template <typename T>
T PhaseSequence<T>::timeInterpolate(const Vec& x,T alpha,int& stage,int& subStage,bool even) const {
  T t0=0,span=0;
  for(int s=0; s<stage; s++)
    t0+=timeSpanVar(x,s);
  span=timeSpanVar(x,stage);
  int subStageAll=(stage%2==1)==even?1:_subStage;
  return t0+span*(subStage+alpha)/subStageAll;
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::interpTpl(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad,HERMITE h,HERMITE hFirst,HERMITE hLast) const {
  stage=0;
  offset=0;
  Vec4T c,dcdt,dcdt0,dcds;
  T t0=0,t0First=t0,span=0;
  for(stage=0; stage<_stage; stage++) {
    //test whether this is a static phase or a swing phase
    span=timeSpanVar(x,stage)/_subStage;
    for(subStage=0; subStage<_subStage; subStage++) {
      //test whether time falls into this span
      bool found=false;
      if(time<t0First) {
        hFirst(span,time,t0,c,dcdt,dcdt0,dcds);
        found=true;
      } else if(time>=t0 && time<t0+span) {
        h(span,time,t0,c,dcdt,dcdt0,dcds);
        found=true;
      } else if(offset==nrColumn()-2) {
        hLast(span,time,t0,c,dcdt,dcdt0,dcds);
        found=true;
      }
      //compute gradient and return
      if(found) {
        alpha=(time-t0)/span;
        if(grad) {
          grad->setZero(4,_stage+1);
          for(int j=0; j<stage; j++)
            grad->col(j)=dcdt0;
          grad->col(stage)=(dcdt0*subStage+dcds)/_subStage;
          grad->col(_stage)=dcdt;
        }
        return c;
      }
      //update offset
      offset++;
      t0+=span;
    }
  }
  ASSERT(false)
  return c;
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::timeInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad) const {
  return interpTpl(x,time,alpha,stage,subStage,offset,grad,coeffHermite,coeffHermiteFirst,coeffHermiteLast);
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::diffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad) const {
  return interpTpl(x,time,alpha,stage,subStage,offset,grad,diffCoeffHermite,diffCoeffHermiteFirst,diffCoeffHermiteLast);
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::ddiffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad) const {
  return interpTpl(x,time,alpha,stage,subStage,offset,grad,ddiffCoeffHermite,ddiffCoeffHermiteFirst,ddiffCoeffHermiteLast);
}
template <typename T>
T PhaseSequence<T>::timeInterpolate(const Vec& x,T alpha,int& stage,int& subStage) const {
  T t0=0,span=0;
  for(int s=0; s<stage; s++)
    t0+=timeSpanVar(x,s);
  span=timeSpanVar(x,stage);
  return t0+span*(subStage+alpha)/_subStage;
}
template <typename T>
std::string PhaseSequence<T>::timeSpanVar(int stage) const {
  return _name+"["+std::to_string(stage)+"]";
}
template <typename T>
T PhaseSequence<T>::timeSpanVar(const Vec& x,int stage) const {
  if(_timeSpanVar.size()>0)
    return x[_timeSpanVar[stage]];
  else return horizonRef()/_stage;
}
template <typename T>
bool PhaseSequence<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  if(!SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef))
    return false;
  DEFINE_NUMERIC_DELTA_T(T)
  if(DELTARef>0)
    DELTA=DELTARef;
  std::random_device rd;
  std::mt19937 gen(rd());
  for(int i=0; i<nrTrial; i++)
    for(int vid=thres!=0?0:-1; vid<int(thres!=0?inputs:0); vid++) {
      bool even=i>nrTrial/2;
      Mat4XT g,gRef;
      int stage,subStage,offset;
      Vec x=makeValid(Vec::Random(inputs));
      Vec delta=thres>0?Vec(Vec::Unit(inputs,vid)):Vec(Vec::Random(inputs));
      T t=std::uniform_real_distribution<>(-(double)horizon(x)*0.5,(double)horizon(x)*1.5)(gen),alpha,alpha2;

      //interpolation mode: even-odd
      std::string evenStr=even?"Even":"Odd";
      Vec4T c=timeInterpolateStencil(x,t,alpha,stage,subStage,offset,even,&g);
      Vec4T c2=timeInterpolateStencil(x+delta*DELTA,t,alpha2,stage,subStage,offset,even,NULL);
      T t2=timeInterpolate(x,alpha,stage,subStage,even);
      if(!debugGradientConditionalVID("timeRef"+evenStr,vid,t,t-t2,thres))
        return false;
      Vec4T dTdSpan=dotGrad(delta,g);
      if(timeSpanVar().size()>0 && !debugGradientConditionalVID("dTdSpan"+evenStr,vid,dTdSpan.norm(),(dTdSpan-(c2-c)/DELTA).norm(),thres))
        return false;
      c2=timeInterpolateStencil(x,t+DELTA,alpha2,stage,subStage,offset,even,NULL);
      if(!debugGradientConditionalVID("dTdTime"+evenStr,vid,g.col(_stage).norm(),(g.col(_stage)-(c2-c)/DELTA).norm(),thres))
        return false;

      //diff
      gRef=g.col(_stage);
      c=diffInterpolateStencil(x,t,alpha,stage,subStage,offset,even,&g);
      c2=diffInterpolateStencil(x+delta*DELTA,t,alpha2,stage,subStage,offset,even,NULL);
      if(!debugGradientConditionalVID("DTRef"+evenStr,vid,gRef.norm(),(gRef-c).norm(),thres))
        return false;
      dTdSpan=dotGrad(delta,g);
      if(timeSpanVar().size()>0 && !debugGradientConditionalVID("dDTdSpan"+evenStr,vid,dTdSpan.norm(),(dTdSpan-(c2-c)/DELTA).norm(),thres))
        return false;
      c2=diffInterpolateStencil(x,t+DELTA,alpha2,stage,subStage,offset,even,NULL);
      if(!debugGradientConditionalVID("dDTdTime"+evenStr,vid,g.col(_stage).norm(),(g.col(_stage)-(c2-c)/DELTA).norm(),thres))
        return false;

      //ddiff
      gRef=g.col(_stage);
      c=ddiffInterpolateStencil(x,t,alpha,stage,subStage,offset,even,&g);
      c2=ddiffInterpolateStencil(x+delta*DELTA,t,alpha2,stage,subStage,offset,even,NULL);
      if(!debugGradientConditionalVID("DDTRef"+evenStr,vid,gRef.norm(),(gRef-c).norm(),thres))
        return false;
      dTdSpan=dotGrad(delta,g);
      if(timeSpanVar().size()>0 && !debugGradientConditionalVID("dDDTdSpan"+evenStr,vid,dTdSpan.norm(),(dTdSpan-(c2-c)/DELTA).norm(),thres))
        return false;
      c2=ddiffInterpolateStencil(x,t+DELTA,alpha2,stage,subStage,offset,even,NULL);
      if(!debugGradientConditionalVID("dDDTdTime"+evenStr,vid,g.col(_stage).norm(),(g.col(_stage)-(c2-c)/DELTA).norm(),thres))
        return false;

      //interpolate mode: all
      evenStr="All";
      c=timeInterpolateStencil(x,t,alpha,stage,subStage,offset,&g);
      c2=timeInterpolateStencil(x+delta*DELTA,t,alpha2,stage,subStage,offset,NULL);
      t2=timeInterpolate(x,alpha,stage,subStage);
      if(!debugGradientConditionalVID("timeRef"+evenStr,vid,t,t-t2,thres))
        return false;
      dTdSpan=dotGrad(delta,g);
      if(timeSpanVar().size()>0 && !debugGradientConditionalVID("dTdSpan"+evenStr,vid,dTdSpan.norm(),(dTdSpan-(c2-c)/DELTA).norm(),thres))
        return false;
      c2=timeInterpolateStencil(x,t+DELTA,alpha2,stage,subStage,offset,NULL);
      if(!debugGradientConditionalVID("dTdTime"+evenStr,vid,g.col(_stage).norm(),(g.col(_stage)-(c2-c)/DELTA).norm(),thres))
        return false;

      //diff
      gRef=g.col(_stage);
      c=diffInterpolateStencil(x,t,alpha,stage,subStage,offset,&g);
      c2=diffInterpolateStencil(x+delta*DELTA,t,alpha2,stage,subStage,offset,NULL);
      if(!debugGradientConditionalVID("DTRef"+evenStr,vid,gRef.norm(),(gRef-c).norm(),thres))
        return false;
      dTdSpan=dotGrad(delta,g);
      if(timeSpanVar().size()>0 && !debugGradientConditionalVID("dDTdSpan"+evenStr,vid,dTdSpan.norm(),(dTdSpan-(c2-c)/DELTA).norm(),thres))
        return false;
      c2=diffInterpolateStencil(x,t+DELTA,alpha2,stage,subStage,offset,NULL);
      if(!debugGradientConditionalVID("dDTdTime"+evenStr,vid,g.col(_stage).norm(),(g.col(_stage)-(c2-c)/DELTA).norm(),thres))
        return false;

      //ddiff
      gRef=g.col(_stage);
      c=ddiffInterpolateStencil(x,t,alpha,stage,subStage,offset,&g);
      c2=ddiffInterpolateStencil(x+delta*DELTA,t,alpha2,stage,subStage,offset,NULL);
      if(!debugGradientConditionalVID("DDTRef"+evenStr,vid,gRef.norm(),(gRef-c).norm(),thres))
        return false;
      dTdSpan=dotGrad(delta,g);
      if(timeSpanVar().size()>0 && !debugGradientConditionalVID("dDDTdSpan"+evenStr,vid,dTdSpan.norm(),(dTdSpan-(c2-c)/DELTA).norm(),thres))
        return false;
      c2=ddiffInterpolateStencil(x,t+DELTA,alpha2,stage,subStage,offset,NULL);
      if(!debugGradientConditionalVID("dDDTdTime"+evenStr,vid,g.col(_stage).norm(),(g.col(_stage)-(c2-c)/DELTA).norm(),thres))
        return false;
    }
  return true;
}
template <typename T>
typename PhaseSequence<T>::Vec PhaseSequence<T>::makeValid(const Vec& x) const {
  Vec ret=x;
  if(_timeSpanVar.size()>0) {
    for(int i=0; i<_timeSpanVar.size(); i++)
      ret[_timeSpanVar[i]]=ret[_timeSpanVar[i]]*0.5+0.5+_eps;
  }
  return ret;
}
template <typename T>
T PhaseSequence<T>::horizon(const Vec& x) const {
  if(_timeSpanVar.size()>0) {
    T t0=0,span=0;
    for(int s=0; s<_stage; s++) {
      span=x[_timeSpanVar[s]];
      t0+=span;
    }
    return t0;
  } else return horizonRef();
}
template <typename T>
T PhaseSequence<T>::horizonRef() const {
  return _horizon;
}
template <typename T>
T PhaseSequence<T>::eps() const {
  return _eps;
}
template <typename T>
int PhaseSequence<T>::stage() const {
  return _stage;
}
template <typename T>
int PhaseSequence<T>::subStage() const {
  return _subStage;
}
template <typename T>
const Eigen::Matrix<int,-1,1>& PhaseSequence<T>::timeSpanVar() const {
  return _timeSpanVar;
}
template <typename T>
int PhaseSequence<T>::segmentId(int column) const {
  return column/(_subStage+1);
}
template <typename T>
int PhaseSequence<T>::segmentOffset(int column) const {
  return column%(_subStage+1);
}
template <typename T>
int PhaseSequence<T>::nrSegment(bool even) const {
  return (_stage+(even?1:0))/2;
}
template <typename T>
int PhaseSequence<T>::nrColumn(bool even) const {
  return nrSegment(even)*(_subStage+1);
}
template <typename T>
int PhaseSequence<T>::nrColumn() const {
  return _stage*_subStage+1;
}
template <typename T>
typename PhaseSequence<T>::Vec4T PhaseSequence<T>::dotGrad(const Vec& x,const Mat4XT& gTime) const {
  Vec4T ret=Vec4T::Zero();
  for(int i=0; i<_stage; i++)
    ret+=timeSpanVar(x,i)*gTime.col(i);
  return ret;
}
template <typename T>
void PhaseSequence<T>::initSameSpan(SQPObjectiveCompound<T>& obj,char even,T staticToSwing) {
  if(_timeSpanVar.size()==0)
    return;
  T nrUnit=(staticToSwing+1)*(_stage/2)+(staticToSwing-1)/2;
  T timeUnit=_horizon/nrUnit;
  if(even>1) {
    for(int i=0; i<_stage; i++)
      obj.setVarInit(_timeSpanVar[i*2+1],_horizon/_stage);
  } else if(even==1) {
    ASSERT_MSGV(staticToSwing>1,"staticToSwing (%f) must >1!",(double)staticToSwing)
    ASSERT_MSG(_stage%2==1,"initSameSpan can only take odd number of stages!")
    for(int i=0; i<_stage/2; i++) {
      obj.setVarInit(_timeSpanVar[i*2+0],timeUnit*staticToSwing);
      obj.setVarInit(_timeSpanVar[i*2+1],timeUnit);
    }
    obj.setVarInit(_timeSpanVar[_stage-1],timeUnit*(staticToSwing-1)/2);
  } else {
    ASSERT_MSGV(staticToSwing>1,"staticToSwing (%f) must >1!",(double)staticToSwing)
    ASSERT_MSG(_stage%2==1,"initSameSpan can only take odd number of stages!")
    obj.setVarInit(_timeSpanVar[0],timeUnit*(staticToSwing-1)/2);
    for(int i=0; i<_stage/2; i++) {
      obj.setVarInit(_timeSpanVar[1+i*2+0],timeUnit);
      obj.setVarInit(_timeSpanVar[1+i*2+1],timeUnit*staticToSwing);
    }
  }
  T totalTime=0;
  std::cout << "initSameSpan: ";
  for(int i=0; i<_stage; i++) {
    totalTime+=obj.addVar(_timeSpanVar[i])._init;
    std::cout << obj.addVar(_timeSpanVar[i])._init << " ";
  }
  std::cout << "totalTime=" << totalTime << " horizon=" << _horizon << std::endl;
}
template <typename T>
void PhaseSequence<T>::initSpanSequence(SQPObjectiveCompound<T>& obj,const std::vector<T>& spans) {
  ASSERT_MSG((int)spans.size()==_timeSpanVar.size(),"spans.size()!=timeSpanVar.size()!")
  for(int i=0; i<(int)spans.size(); i++)
    obj.setVarInit(_timeSpanVar[i],spans[i]);
}
template <typename T>
void PhaseSequence<T>::initRandomSpan(SQPObjectiveCompound<T>& obj) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis((double)eps(),(double)eps()*10);
  if(_timeSpanVar.size()>0) {
    T spanSum=0;
    std::vector<T> spans;
    for(int i=0; i<_stage; i++) {
      spans.push_back(dis(gen));
      spanSum+=spans.back();
    }
    for(int i=0; i<_stage; i++)
      spans[i]*=horizonRef()/spanSum;
    for(int i=0; i<_stage; i++)
      obj.setVarInit(_timeSpanVar[i],spans[i]);
  }
}
template <typename T>
void PhaseSequence<T>::coeffHermite(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  //input
  //T s;
  //T t;
  //T t0;

  //temp
  T tt1;
  T tt2;
  T tt3;
  T tt4;
  T tt5;
  T tt6;
  T tt7;
  T tt8;
  T tt9;
  T tt10;
  T tt11;
  T tt12;
  T tt13;
  T tt14;
  T tt15;
  T tt16;
  T tt17;
  T tt18;

  tt1=1/pow(s,(T)2);
  tt2=t-t0;
  tt3=pow(tt2,(T)2);
  tt4=1/pow(s,(T)3);
  tt5=pow(tt2,(T)3);
  tt6=-2*tt1*tt3;
  tt7=tt4*tt5;
  tt8=1/s;
  tt9=tt8*tt2;
  tt10=-tt1*tt3;
  tt11=6*tt4*tt3;
  tt12=tt11-6*tt1*tt2;
  tt13=3*tt4*tt3;
  tt14=-6*tt4*tt3;
  tt15=6*tt1*tt2+tt14;
  tt16=-3*tt4*tt3;
  tt17=1/pow(s,(T)4);
  tt18=-3*tt17*tt5;
  c[0]=2*tt4*tt5-3*tt1*tt3+1;
  c[1]=s*(tt9+tt7+tt6);
  c[2]=3*tt1*tt3-2*tt4*tt5;
  c[3]=s*(tt7+tt10);
  dcdt[0]=tt12;
  dcdt[1]=s*((-4*tt1*tt2)+tt13+tt8);
  dcdt[2]=tt15;
  dcdt[3]=s*(tt13-2*tt1*tt2);
  dcdt0[0]=tt15;
  dcdt0[1]=s*(4*tt1*tt2+tt16-tt8);
  dcdt0[2]=tt12;
  dcdt0[3]=s*(2*tt1*tt2+tt16);
  dcds[0]=tt11-6*tt17*tt5;
  dcds[1]=tt9+s*(4*tt4*tt3+tt18-tt1*tt2)+tt7+tt6;
  dcds[2]=6*tt17*tt5+tt14;
  dcds[3]=s*(2*tt4*tt3+tt18)+tt7+tt10;
}
template <typename T>
void PhaseSequence<T>::coeffHermiteFirst(T,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  c=Vec4T(1,t-t0,0,0);
  dcdt=Vec4T(0,1,0,0);
  dcdt0=Vec4T(0,-1,0,0);
  dcds.setZero();
}
template <typename T>
void PhaseSequence<T>::coeffHermiteLast(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  c=Vec4T(0,0,1,t-t0-s);
  dcdt=Vec4T(0,0,0,1);
  dcdt0=Vec4T(0,0,0,-1);
  dcds=Vec4T(0,0,0,-1);
}
template <typename T>
void PhaseSequence<T>::diffCoeffHermite(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  //input
  //T s;
  //T t;
  //T t0;

  //temp
  T tt1;
  T tt2;
  T tt3;
  T tt4;
  T tt5;
  T tt6;
  T tt7;
  T tt8;
  T tt9;
  T tt10;
  T tt11;
  T tt12;
  T tt13;
  T tt14;
  T tt15;
  T tt16;

  tt1=1/pow(s,(T)3);
  tt2=t-t0;
  tt3=pow(tt2,(T)2);
  tt4=1/pow(s,(T)2);
  tt5=1/s;
  tt6=3*tt1*tt3;
  tt7=-4*tt4*tt2;
  tt8=-2*tt4*tt2;
  tt9=12*tt1*tt2;
  tt10=tt9-6*tt4;
  tt11=6*tt1*tt2;
  tt12=-12*tt1*tt2;
  tt13=tt12+6*tt4;
  tt14=-6*tt1*tt2;
  tt15=1/pow(s,(T)4);
  tt16=-9*tt15*tt3;
  c[0]=6*tt1*tt3-6*tt4*tt2;
  c[1]=s*(tt7+tt6+tt5);
  c[2]=6*tt4*tt2-6*tt1*tt3;
  c[3]=s*(tt8+tt6);
  dcdt[0]=tt10;
  dcdt[1]=s*(tt11-4*tt4);
  dcdt[2]=tt13;
  dcdt[3]=s*(tt11-2*tt4);
  dcdt0[0]=tt13;
  dcdt0[1]=s*(tt14+4*tt4);
  dcdt0[2]=tt10;
  dcdt0[3]=s*(tt14+2*tt4);
  dcds[0]=tt9-18*tt15*tt3;
  dcds[1]=tt7+s*((-tt4)+tt16+8*tt1*tt2)+tt6+tt5;
  dcds[2]=tt12+18*tt15*tt3;
  dcds[3]=tt8+s*(tt16+4*tt1*tt2)+tt6;
}
template <typename T>
void PhaseSequence<T>::diffCoeffHermiteFirst(T,T,T,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  c.setZero();
  c[1]=1;
  dcdt.setZero();
  dcdt0.setZero();
  dcds.setZero();
}
template <typename T>
void PhaseSequence<T>::diffCoeffHermiteLast(T,T,T,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  c.setZero();
  c[3]=1;
  dcdt.setZero();
  dcdt0.setZero();
  dcds.setZero();
}
template <typename T>
void PhaseSequence<T>::ddiffCoeffHermite(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  //input
  //T s;
  //T t;
  //T t0;

  //temp
  T tt1;
  T tt2;
  T tt3;
  T tt4;
  T tt5;
  T tt6;
  T tt7;
  T tt8;
  T tt9;
  T tt10;
  T tt11;
  T tt12;

  tt1=1/pow(s,2);
  tt2=-6*tt1;
  tt3=1/pow(s,3);
  tt4=t-t0;
  tt5=-4*tt1;
  tt6=6*tt3*tt4;
  tt7=6*tt1;
  tt8=-2*tt1;
  tt9=12*tt3;
  tt10=-12*tt3;
  tt11=1/pow(s,4);
  tt12=-18*tt11*tt4;
  c[0]=12*tt3*tt4+tt2;
  c[1]=s*(tt6+tt5);
  c[2]=tt7-12*tt3*tt4;
  c[3]=s*(tt6+tt8);
  dcdt[0]=tt9;
  dcdt[1]=tt7;
  dcdt[2]=tt10;
  dcdt[3]=tt7;
  dcdt0[0]=tt10;
  dcdt0[1]=tt2;
  dcdt0[2]=tt9;
  dcdt0[3]=tt2;
  dcds[0]=tt9-36*tt11*tt4;
  dcds[1]=tt6+s*(8*tt3+tt12)+tt5;
  dcds[2]=36*tt11*tt4+tt10;
  dcds[3]=tt6+s*(4*tt3+tt12)+tt8;
}
template <typename T>
void PhaseSequence<T>::ddiffCoeffHermiteFirst(T,T,T,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  c=dcdt=dcdt0=dcds=Vec4T::Zero();
}
template <typename T>
void PhaseSequence<T>::ddiffCoeffHermiteLast(T,T,T,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds) {
  c=dcdt=dcdt0=dcds=Vec4T::Zero();
}
template class PhaseSequence<FLOAT>;
}
