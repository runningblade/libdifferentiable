#include "NESimplifiedDynamicsSequenceSpline.h"
#include <Utils/CrossSpatialUtils.h>
#include <Utils/DebugGradient.h>
#include <Utils/RotationUtils.h>

namespace PHYSICSMOTION {
template <typename T>
NESimplifiedDynamicsSequenceSpline<T>::NESimplifiedDynamicsSequenceSpline
(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,
 std::shared_ptr<ArticulatedBody> body,std::shared_ptr<ArticulatedBody> bodySimplified,const Vec3T& G,
 std::shared_ptr<PhaseSequence<T>> phase,
 const std::vector<std::shared_ptr<PositionSequence<T>>>& poses,
 const std::vector<std::shared_ptr<ForceSequence<T>>>& forces,
 const std::vector<std::shared_ptr<EndEffectorBounds>>& ees,bool inherited)
  :NEDynamicsSequence<T>(obj,inherited?name:name+"NESimplifiedDynamicsSequenceSpline",dt,Vec2T::Zero(),body,G,poses,forces,ees,0,true),_bodySimplified(bodySimplified) {
  if(inherited)
    return;
  ASSERT_MSG(_bodySimplified->nrDOF()==6,"NESimplifiedDynamicsSequenceSpline can only handle 6-DOF torso!")
  //G
  _a0=concat<Vec>(Vec3T::Zero(),-G);
  //joints
  _joints.setConstant(1,_bodySimplified->nrJ()-1);
  //create DOF spline
  _DOFSeq.reset(new VectorSequence<T,6>(obj,"NESimplifiedDOF",2,phase));
  _DOFSeq->initConstant(Vec6T::Zero(),obj);
  //discrete test points
  int nrStep=round((double)phase->horizonRef()/(double)_dt)+1;
  _DOFVars.setConstant(6,nrStep,-1);
  //interpolation stencil
  _rangeX[0]=std::min(_DOFSeq->getVar().minCoeff(),_DOFSeq->getDiffVar().minCoeff());
  _rangeX[1]=_DOFSeq->getVar().size()+_DOFSeq->getDiffVar().size();
  STrips tripsQ;
  for(int c=0; c<nrStep; c++) {
    GRAD_INTERP dofGrads[6];
    _DOFSeq->timeInterpolateStencil(obj.init(),c*_dt,dofGrads,NULL);
    for(int dof=0; dof<6; dof++)
      for(const std::pair<const int,T>& v:dofGrads[dof])
        tripsQ.push_back(STrip(v.first-_rangeX[0],c*6+dof,v.second));
  }
  _qInterp.resize(_rangeX[1],nrStep*6);
  _qInterp.setFromTriplets(tripsQ.begin(),tripsQ.end());
  //position, dynamics constraints
  //(_frame.transpose()*(x+C-_ctrBB))[d] \in [minC,maxC]*_res
  //(_frame.transpose()*x)[d] \in [minC,maxC]*_res+(_frame.transpose()*(_ctrBB-C))[d]
  for(int c=_posConsStart; c<nrStep; c++)
    for(int f=0; f<(int)_ees.size(); f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      EndEffectorBounds::Vec3T frameTCtr=ee->_frame.transpose()*ee->_ctrBB;
      for(int r=0; r<3; r++) {
        _gl.push_back(ee->_bb[r+0]*ee->_res+frameTCtr[r]);
        _gu.push_back(ee->_bb[r+3]*ee->_res+frameTCtr[r]);
      }
    }
  for(int c=0; c<_rangeX[1]; c++) {
    _gl.push_back(0);
    _gu.push_back(0);
  }
  _infosNE.resize(nrStep);
}
template <typename T>
int NESimplifiedDynamicsSequenceSpline<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  //position, dynamics constraint
  STrips DJac;
  Vec DCon=Vec::Zero((int)_infosNE.size()*6);
  Vec q=_qInterp.transpose()*x.segment(_rangeX[0],_rangeX[1]);
  OMP_PARALLEL_FOR_
  for(int c=0; c<(int)_infosNE.size(); c++)
    _infosNE[c].reset(*_bodySimplified,q.template segment<6>(c*6));
  OMP_PARALLEL_FOR_
  for(int c=_posConsStart; c<(int)_infosNE.size(); c++) {
    T time=c*_dt;
    MatT Sc,ScCross;
    std::vector<GRAD_INTERP> posGrads(_ees.size()*3),forceGrads(_ees.size()*3);
    Mat3X4T TJ=_infosNE[c].NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1),GK;
    //position constraint
    Mat6XT forces=Mat6XT::Zero(6,_bodySimplified->nrJ());
    Mat3XT forceIfs=Mat3XT::Zero(3,(int)_ees.size());
    Mat3XT poses=Mat3XT::Zero(3,(int)_ees.size());
    int offset=_offset+(c-_posConsStart)*(int)_ees.size()*3;
    for(int f=0; f<(int)_ees.size(); f++) {
      const std::shared_ptr<EndEffectorBounds>& ee=_ees[f];
      //position part
      poses.col(f)=_poses[f]->timeInterpolateStencil(x,time,fjac?&posGrads[f*3]:NULL,NULL);
      Vec3T relPos=poses.col(f)-CTR(TJ);
      Mat3T ROTFrame=ROT(TJ)*ee->_frame.template cast<T>();
      fvec.template segment<3>(offset)=ROTFrame.transpose()*relPos;
      if(fjac)
        for(int r=0; r<3; r++) {
          for(const std::pair<const int,T>& v:posGrads[f*3+r])
            addBlock(*fjac,offset,v.first,ROTFrame.row(r).transpose()*v.second);
          CTR(GK)=-ROTFrame.col(r);
          ROT(GK)=relPos*ee->_frame.col(r).template cast<T>().transpose();
          _infosNE[c].DTG(_bodySimplified->nrJ()-1,*_bodySimplified,GK,[&](int dof,T val) {
            for(typename SMatT::InnerIterator it(_qInterp,c*6+dof); it; ++it)
              fjac->push_back(STrip(offset+r,it.row()+_rangeX[0],it.value()*val));
          });
        }
      offset+=3;
      //dynamics part
      forceIfs.col(f)=_forces[f]->timeInterpolateStencil(x,time,fjac?&forceGrads[f*3]:NULL,NULL);
      Eigen::Block<Mat6XT,6,1> forceI=forces.template block<6,1>(0,_bodySimplified->nrJ()-1);
      forceI.template segment<3>(3)+=forceIfs.col(f);
      forceI.template segment<3>(0)+=poses.col(f).cross(forceIfs.col(f));
    }
    if(c<2)
      continue;
    //set FD
    _infosNE[c]._dqM=(_infosNE[c]._qM-_infosNE[c-1]._qM)/_dt;
    _infosNE[c]._ddqM=(_infosNE[c]._qM-2*_infosNE[c-1]._qM+_infosNE[c-2]._qM)/(_dt*_dt);
    if(fjac)
      _infosNE[c].RNEADerivatives(*_bodySimplified,mapCV(_a0),mapCM(forces));
    else _infosNE[c].RNEA(*_bodySimplified,mapCV(_a0),mapCM(forces));
    //fill
    offset=c*6;
    DCon.template segment<6>(offset)=_infosNE[c]._tauM;
    if(fjac) {
      _infosNE[c].calcH(*_bodySimplified);
      MatT tmp=_infosNE[c]._DtauDqM+_infosNE[c]._DtauDdqM/_dt+_infosNE[c]._HM/(_dt*_dt);
      for(int d=0,off=offset; d<tmp.cols(); d++,off++)
        for(typename SMatT::InnerIterator it(_qInterp,off); it; ++it)
          addBlock(DJac,offset,it.row()+_rangeX[0],tmp.col(d)*it.value());
      tmp=-_infosNE[c]._DtauDdqM/_dt-2*_infosNE[c]._HM/(_dt*_dt);
      for(int d=0,off=offset-6; d<tmp.cols(); d++,off++)
        for(typename SMatT::InnerIterator it(_qInterp,off); it; ++it)
          addBlock(DJac,offset,it.row()+_rangeX[0],tmp.col(d)*it.value());
      tmp=_infosNE[c]._HM/(_dt*_dt);
      for(int d=0,off=offset-12; d<tmp.cols(); d++,off++)
        for(typename SMatT::InnerIterator it(_qInterp,off); it; ++it)
          addBlock(DJac,offset,it.row()+_rangeX[0],tmp.col(d)*it.value());
      //Sc
      Sc.resize(6,_infosNE[c]._qM.size());
      _infosNE[c].Sc(*_bodySimplified,mapCV(_joints),mapM(Sc));
      Sc=(-Sc.transpose()).eval();
      for(int f=0; f<(int)_ees.size(); f++) {
        //fjac
        ScCross=Sc.template block<6,3>(0,0)*cross<T>(poses.col(f));
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:forceGrads[f*3+r])
            addBlock(DJac,offset,v.first,(ScCross.col(r)+Sc.col(3+r))*v.second);
        //pjac
        ScCross=Sc.template block<6,3>(0,0)*cross<T>(-forceIfs.col(f));
        for(int r=0; r<3; r++)
          for(const std::pair<const int,T>& v:posGrads[f*3+r])
            addBlock(DJac,offset,v.first,ScCross.col(r)*v.second);
      }
    }
  }
  //Galerkin projection
  int offset=_offset+((int)_infosNE.size()-_posConsStart)*(int)_ees.size()*3;
  fvec.segment(offset,_rangeX[1])=_qInterp*DCon;
  if(fjac) {
    SMatT DJacM;
    DJacM.resize((int)_infosNE.size()*6,x.size());
    DJacM.setFromTriplets(DJac.begin(),DJac.end());
    addBlock(*fjac,offset,0,SMatT(_qInterp*DJacM));
  }
  return 0;
}
template <typename T>
bool NESimplifiedDynamicsSequenceSpline<T>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  return SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef);
}
template <typename T>
const typename NESimplifiedDynamicsSequenceSpline<T>::Vec& NESimplifiedDynamicsSequenceSpline<T>::controlCoef() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlCoef();
}
template <typename T>
typename NESimplifiedDynamicsSequenceSpline<T>::Vec NESimplifiedDynamicsSequenceSpline<T>::residualToControl() const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::residualToControl();
}
template <typename T>
typename NESimplifiedDynamicsSequenceSpline<T>::Vec NESimplifiedDynamicsSequenceSpline<T>::controlledDOFs(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(keepFunc,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequenceSpline<T>::SMatT NESimplifiedDynamicsSequenceSpline<T>::controlledDOFsProj(std::function<bool(T)> keepFunc,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(keepFunc,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequenceSpline<T>::Vec NESimplifiedDynamicsSequenceSpline<T>::controlledDOFs(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFs(constrained,consSet);
}
template <typename T>
typename NESimplifiedDynamicsSequenceSpline<T>::SMatT NESimplifiedDynamicsSequenceSpline<T>::controlledDOFsProj(bool constrained,std::set<int>* consSet) const {
  FUNCTION_NOT_IMPLEMENTED
  return PBDDynamicsSequence<T>::controlledDOFsProj(constrained,consSet);
}
template <typename T>
const Eigen::Matrix<int,-1,-1>& NESimplifiedDynamicsSequenceSpline<T>::DOFVar() const {
  return _DOFSeq->getVar();
}
template <typename T>
typename NESimplifiedDynamicsSequenceSpline<T>::MatT NESimplifiedDynamicsSequenceSpline<T>::getPoses(const Vec& x) {
  MatT ret;
  ret.resize(_body->nrDOF(),_DOFVars.cols());
  Vec q=_qInterp.transpose()*x.segment(_rangeX[0],_rangeX[1]);
  OMP_PARALLEL_FOR_
  for(int c=0; c<_DOFVars.cols(); c++) {
    T time=c*_dt;
    //base
    Vec DOF=Vec::Zero(_body->nrDOF());
    DOF.template segment<6>(0)=_DOFSeq->timeInterpolateStencil(x,time,NULL,NULL);
    NEArticulatedGradientInfo<T> info(*_bodySimplified,q.template segment<6>(c*6));
    Mat3X4T TJ=info.NEArticulatedGradientInfoMap<T>::getTrans(_bodySimplified->nrJ()-1);
    //xss
    for(int f=0; f<(int)_ees.size(); f++) {
      Vec3T pos=_poses[f]->timeInterpolateStencil(x,time,NULL,NULL);
      Vec3T nor=_poses[f]->getEnv()->phiGrad(pos);
      pos=ROT(TJ).transpose()*(pos-CTR(TJ));
      nor=ROT(TJ).transpose()*nor;
      DOF+=_ees[f]->getDOFAll(*_body,pos.template cast<EndEffectorBounds::T>(),nor.template cast<EndEffectorBounds::T>()).template cast<T>();
    }
    ret.col(c)=DOF;
  }
  return ret;
}
template <typename T>
std::shared_ptr<VectorSequence<T,6>> NESimplifiedDynamicsSequenceSpline<T>::DOFSeq() const {
  return _DOFSeq;
}
template class NESimplifiedDynamicsSequenceSpline<FLOAT>;
}
