#include "VectorSequence.h"
#include <Utils/DebugGradient.h>
#include <Utils/Interp.h>
#include <unordered_set>
#include <random>

namespace PHYSICSMOTION {
template <typename T,int DIM>
VectorSequence<T,DIM>::VectorSequence
(SQPObjectiveCompound<T>& obj,const std::string& name,char even,
 std::shared_ptr<PhaseSequence<T>> phase,bool inherited)
  :SQPObjectiveComponent<T>(obj,inherited?name:(name+"VectorSequence<"+std::to_string(DIM)+">")),_even(even),_phase(phase) {
  if(inherited)
    return;
  int nrC=_even<2?_phase->nrColumn(_even):_phase->nrColumn();
  _diffVar.resize(DIM,nrC);
  _var.resize(DIM,nrC);
  for(int r=0; r<DIM; r++)
    for(int c=0; c<nrC; c++) {
      _var(r,c)=obj.addVar(var(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
      _diffVar(r,c)=obj.addVar(diffVar(r,c),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::MUST_NEW)._id;
    }
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::timeInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  int stage,subStage;
  return timeInterpolateStencil(x,time,stage,subStage,grad,gradTime,gradTimeSpan);
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::timeInterpolateStencil(const Vec& x,T time,int& stage,int& subStage,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  Mat4XT dcdt;
  T alpha;
  int off;
  Vec4T c;
  if(_even<2)
    c=_phase->timeInterpolateStencil(x,time,alpha,stage,subStage,off,_even,&dcdt);
  else c=_phase->timeInterpolateStencil(x,time,alpha,stage,subStage,off,&dcdt);
  return interpolateStencilToPosition(x,dcdt,off,c,grad,gradTime,gradTimeSpan);
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::diffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  int stage,subStage;
  return diffInterpolateStencil(x,time,stage,subStage,grad,gradTime,gradTimeSpan);
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::diffInterpolateStencil(const Vec& x,T time,int& stage,int& subStage,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  Mat4XT dcdt;
  T alpha;
  int off;
  Vec4T c;
  if(_even<2)
    c=_phase->diffInterpolateStencil(x,time,alpha,stage,subStage,off,_even,&dcdt);
  else c=_phase->diffInterpolateStencil(x,time,alpha,stage,subStage,off,&dcdt);
  return interpolateStencilToPosition(x,dcdt,off,c,grad,gradTime,gradTimeSpan);
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::ddiffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  int stage,subStage;
  return diffInterpolateStencil(x,time,stage,subStage,grad,gradTime,gradTimeSpan);
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::ddiffInterpolateStencil(const Vec& x,T time,int& stage,int& subStage,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  Mat4XT dcdt;
  T alpha;
  int off;
  Vec4T c;
  if(_even<2)
    c=_phase->ddiffInterpolateStencil(x,time,alpha,stage,subStage,off,_even,&dcdt);
  else c=_phase->ddiffInterpolateStencil(x,time,alpha,stage,subStage,off,&dcdt);
  return interpolateStencilToPosition(x,dcdt,off,c,grad,gradTime,gradTimeSpan);
}
template <typename T,int DIM>
std::string VectorSequence<T,DIM>::diffVar(int r,int stage) const {
  return _name+"Diff["+std::to_string(stage)+"]["+std::to_string(r)+"]";
}
template <typename T,int DIM>
std::string VectorSequence<T,DIM>::var(int r,int stage) const {
  return _name+"["+std::to_string(stage)+"]["+std::to_string(r)+"]";
}
template <typename T,int DIM>
bool VectorSequence<T,DIM>::debug(int inputs,int nrTrial,T thres,Vec* x0,bool hess,T DELTARef) {
  if(!_phase)
    return true;
  if(!SQPObjectiveComponent<T>::debug(inputs,nrTrial,thres,x0,hess,DELTARef))
    return false;
  DEFINE_NUMERIC_DELTA_T(T)
  if(DELTARef>0)
    DELTA=DELTARef;
  std::random_device rd;
  std::mt19937 gen(rd());
  for(int i=0; i<nrTrial; i++)
    for(int vid=thres!=0?0:-1; vid<int(thres!=0?inputs:0); vid++) {
      VecDT gT;
      GRAD_INTERP g[DIM];
      std::string evenStr=_even?"Even":"Odd";
      Vec x=makeValid(Vec::Random(inputs));
      Vec delta=thres>0?Vec(Vec::Unit(inputs,vid)):Vec(Vec::Random(inputs));
      T t=std::uniform_real_distribution<>(0,(double)_phase->horizon(x))(gen);

      //interpolation mode: even-odd
      VecDT pos=timeInterpolateStencil(x,t,g,&gT);
      VecDT pos2=timeInterpolateStencil(x+delta*DELTA,t,NULL,NULL);
      VecDT dPosdX=dotGrad(delta,g);
      if(!debugGradientConditionalVID("dVardX"+evenStr,vid,dPosdX.norm(),(dPosdX-(pos2-pos)/DELTA).norm(),thres))
        return false;
      pos2=timeInterpolateStencil(x,t+DELTA,NULL,NULL);
      if(!debugGradientConditionalVID("dVardT"+evenStr,vid,gT.norm(),(gT-(pos2-pos)/DELTA).norm(),thres))
        return false;

      //diff
      try {
        pos=diffInterpolateStencil(x,t,g,&gT);
        pos2=diffInterpolateStencil(x+delta*DELTA,t,NULL,NULL);
        dPosdX=dotGrad(delta,g);
        if(!debugGradientConditionalVID("dDVardX"+evenStr,vid,dPosdX.norm(),(dPosdX-(pos2-pos)/DELTA).norm(),thres))
          return false;
        pos2=diffInterpolateStencil(x,t+DELTA,NULL,NULL);
        if(!debugGradientConditionalVID("dDVardT"+evenStr,vid,gT.norm(),(gT-(pos2-pos)/DELTA).norm(),thres))
          return false;
      } catch(...) {
        //these functions can be unimplemented
      }

      //ddiff
      try {
        pos=ddiffInterpolateStencil(x,t,g,&gT);
        pos2=ddiffInterpolateStencil(x+delta*DELTA,t,NULL,NULL);
        dPosdX=dotGrad(delta,g);
        if(!debugGradientConditionalVID("dDDVardX"+evenStr,vid,dPosdX.norm(),(dPosdX-(pos2-pos)/DELTA).norm(),thres))
          return false;
        pos2=ddiffInterpolateStencil(x,t+DELTA,NULL,NULL);
        if(!debugGradientConditionalVID("dDDVardT"+evenStr,vid,gT.norm(),(gT-(pos2-pos)/DELTA).norm(),thres))
          return false;
      } catch(...) {
        //these functions can be unimplemented
      }
    }
  return true;
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::Vec VectorSequence<T,DIM>::makeValid(const Vec& x) const {
  if(!_phase)
    return x;
  return _phase->makeValid(x);
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::dotGrad(const Vec& x,const GRAD_INTERP g[DIM]) const {
  VecDT ret=VecDT::Zero();
  for(int r=0; r<DIM; r++)
    for(const std::pair<const int,T>& v:g[r])
      ret[r]+=x[v.first]*v.second;
  return ret;
}
template <typename T,int DIM>
std::shared_ptr<PhaseSequence<T>> VectorSequence<T,DIM>::getPhase() const {
  return _phase;
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::GRAD_INTERP VectorSequence<T,DIM>::removeNonZero(const GRAD_INTERP& g) {
  GRAD_INTERP ret;
  for(std::pair<int,T> stencil:g)
    if(stencil.second!=0)
      ret[stencil.first]=stencil.second;
  return ret;
}
template <typename T,int DIM>
void VectorSequence<T,DIM>::initConstant(const VecDT& pos,SQPObjectiveCompound<T>& obj) {
  int nrC=_even<2?_phase->nrColumn(_even):_phase->nrColumn();
  for(int c=0; c<nrC; c++)
    for(int r=0; r<DIM; r++) {
      if(_diffVar(r,c)>=0)
        obj.addVar(_diffVar(r,c))._init=0;
      if(_var(r,c)>=0)
        obj.addVar(_var(r,c))._init=pos[r];
    }
}
template <typename T,int DIM>
void VectorSequence<T,DIM>::initLineSegment(const VecDT& a,const VecDT& b,SQPObjectiveCompound<T>& obj,int N) {
  int c=0;
  T time=0,alpha=0;
  for(int s=0; s<std::abs(_phase->stage()); s++) {
    T span=_phase->timeSpanVar(obj.init(),s);
    for(int ss=0; ss<_phase->subStage(); ss++) {
      alpha=time/_phase->horizonRef();
      for(int d=0; d<DIM; d++) {
        obj.addVar(getVar()(d,c))._init=a[d]*(1-alpha)+b[d]*alpha;
        obj.addVar(getDiffVar()(d,c))._init=(b[d]-a[d])/_phase->horizonRef();
      }
      time+=span/_phase->subStage();
      c++;
    }
  }
  alpha=time/_phase->horizonRef();
  for(int d=0; d<DIM; d++) {
    obj.addVar(getVar()(d,c))._init=a[d]*(1-alpha)+b[d]*alpha;
    obj.addVar(getDiffVar()(d,c))._init=(b[d]-a[d])/_phase->horizonRef();
  }
  DEFINE_NUMERIC_DELTA_T(T)
  for(int i=0; i<N; i++) {
    T alpha=(T)i/(T)N;
    VecDT valRef=a*(1-alpha)+b*alpha;
    VecDT val=timeInterpolateStencil(obj.init(),i*_phase->horizonRef()/N,NULL,NULL);
    DEBUG_GRADIENT("initLineSegment",val.norm(),(val-valRef).norm())
  }
}
template <typename T,int DIM>
void VectorSequence<T,DIM>::initLineSegments(const std::vector<std::pair<VecDT,VecDT>>& segments,SQPObjectiveCompound<T>& obj) {
  ASSERT_MSG(_even<2,"initLineSegments can only be called for even/odd segments!")
  ASSERT_MSG(_phase->nrSegment(_even)==(int)segments.size(),"segments.size()!=phase->nrSegment() when calling initLineSegments!")
  int offset=offsetVar();
  GRAD_INTERP grad[DIM];
  Vec init=obj.init();
  STrips trips;
  int NR_MULT=4;
  T time=0,span=0;
  std::vector<VecDT> DOFs;
  for(int i=0,j=0; i<_phase->timeSpanVar().size(); i++) {
    span=obj.addVar(_phase->timeSpanVar()[i])._init;
    if((i%2)!=_even) {
      for(int c=0; c<_phase->subStage()*NR_MULT+1; c++) {
        T alpha=(T)c/(T)(_phase->subStage()*NR_MULT);
        timeInterpolateStencil(init,time+span*alpha,grad,NULL,false);
        for(const std::pair<const int,T>& v:removeNonZero(grad[0]))
          trips.push_back(STrip((int)DOFs.size(),v.first-offset,v.second));
        DOFs.push_back(interp1D(segments[j].first,segments[j].second,alpha));
      }
      j++;
    }
    time+=span;
  }
  //build G*var=RHS, so, in a least square sense, we have: G^TG*var=G^T*RHS
  SMatT G,GTG;
  G.resize(DOFs.size(),nrVar(0));
  G.setFromTriplets(trips.begin(),trips.end());
  GTG=G.transpose()*G;
  //solve
  int nrC=_even<2?_phase->nrColumn(_even):_phase->nrColumn();
  Eigen::SimplicialLDLT<SMatT> sol(GTG);
  for(int r=0; r<DIM; r++) {
    Vec RHS=Vec::Zero(G.rows());
    for(int c=0; c<RHS.size(); c++)
      RHS[c]=DOFs[c][r];
    Vec GTRHS=G.transpose()*RHS;
    Vec vars=sol.solve(GTRHS);
    for(int c=0; c<nrC; c++) {
      if(_var(0,c)>=0)
        obj.setVarInit(_var(r,c),vars[_var(0,c)-offset]);
      if(_diffVar(0,c)>=0)
        obj.setVarInit(_diffVar(r,c),vars[_diffVar(0,c)-offset]);
    }
  }
}
template <typename T,int DIM>
void VectorSequence<T,DIM>::initRegularInterval(const MatT& DOFs,T interval,SQPObjectiveCompound<T>& obj,bool fixInitVar,bool fixInitDiffVar) {
  ASSERT_MSG(DOFs.rows()==DIM,"DOFs.rows()!=DIM when calling initRegularInterval!")
  //we only construct regression matrix for DIM==0, other DIMs are identical
  int offset=offsetVar();
  GRAD_INTERP grad[DIM];
  Vec init=obj.init();
  STrips trips,tripsA;
  for(int c=0; c<DOFs.cols(); c++) {
    timeInterpolateStencil(init,c*interval,grad,NULL,false);
    for(const std::pair<const int,T>& v:removeNonZero(grad[0]))
      trips.push_back(STrip(c,v.first-offset,v.second));
  }
  //build G*var=RHS, so, in a least square sense, we have: G^TG*var=G^T*RHS
  int offA=0;
  SMatT G,GTG,a;
  if(fixInitVar)
    tripsA.push_back(STrip(offA++,_var(0,0)-offset,1));
  if(fixInitDiffVar)
    tripsA.push_back(STrip(offA++,_diffVar(0,0)-offset,1));
  G.resize(DOFs.cols(),nrVar(0));
  G.setFromTriplets(trips.begin(),trips.end());
  GTG=G.transpose()*G;
  if(offA>0) {
    a.resize(offA,nrVar(0));
    a.setFromTriplets(tripsA.begin(),tripsA.end());
    GTG=buildKKT<T,0,int>(GTG,a,0);
  }
  //solve
  int nrC=_even<2?_phase->nrColumn(_even):_phase->nrColumn();
  Eigen::SparseLU<SMatT> sol(GTG);
  for(int r=0; r<DIM; r++) {
    Vec GTRHS=G.transpose()*DOFs.row(r).transpose();
    if(fixInitVar)
      GTRHS=concatRow(GTRHS,Vec::Constant(1,DOFs(r,0)));
    if(fixInitDiffVar)
      GTRHS=concatRow(GTRHS,Vec::Constant(1,(DOFs(r,1)-DOFs(r,0))/interval));
    Vec vars=sol.solve(GTRHS);
    for(int c=0; c<nrC; c++) {
      if(_var(0,c)>=0)
        obj.setVarInit(_var(r,c),vars[_var(0,c)-offset]);
      if(_diffVar(0,c)>=0)
        obj.setVarInit(_diffVar(r,c),vars[_diffVar(0,c)-offset]);
    }
  }
}
template <typename T,int DIM>
typename VectorSequence<T,DIM>::VecDT VectorSequence<T,DIM>::interpolateStencilToPosition(const Vec& x,const Mat4XT& dcdt,int off,Vec4T& c,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const {
  const Eigen::Matrix<int,-1,1>& timeSpanVar=_phase->timeSpanVar();
  if(grad) {
    for(int r=0; r<DIM; r++)
      grad[r].clear();
    //this block makes sure that the jacobian's sparsity pattern is fixed
    //for fixed timeSpanVar is is not needed
    if(timeSpanVar.size()>0)
      for(int r=0; r<DIM; r++)
        for(int i=0; i<_var.cols(); i++) {
          if(_diffVar(r,i)>=0)
            grad[r][_diffVar(r,i)]=0;
          if(_var(r,i)>=0)
            grad[r][_var(r,i)]=0;
        }
  }
  VecDT pos0=VecDT::Zero(),pos1=VecDT::Zero(),posDiff0=VecDT::Zero(),posDiff1=VecDT::Zero();
  for(int r=0; r<DIM; r++) {
    if(_var(r,off)>=0)
      pos0[r]=x[_var(r,off)];
    if(_diffVar(r,off)>=0)
      posDiff0[r]=x[_diffVar(r,off)];
    if(_var(r,off+1)>=0)
      pos1[r]=x[_var(r,off+1)];
    if(_diffVar(r,off+1)>=0)
      posDiff1[r]=x[_diffVar(r,off+1)];
    if(grad) {
      if(_var(r,off)>=0)
        grad[r][_var(r,off)]+=c[0];
      if(_diffVar(r,off)>=0)
        grad[r][_diffVar(r,off)]+=c[1];
      if(_var(r,off+1)>=0)
        grad[r][_var(r,off+1)]+=c[2];
      if(_diffVar(r,off+1)>=0)
        grad[r][_diffVar(r,off+1)]+=c[3];
    }
  }
  if(grad && gradTimeSpan) {
    for(int j=0; j<timeSpanVar.size(); j++) {
      VecDT dpdt=dcdt(0,j)*pos0+dcdt(1,j)*posDiff0+dcdt(2,j)*pos1+dcdt(3,j)*posDiff1;
      for(int r=0; r<DIM; r++)
        grad[r][timeSpanVar[j]]=dpdt[r];
    }
  }
  if(gradTime) {
    int j=dcdt.cols()-1;
    (*gradTime)=dcdt(0,j)*pos0+dcdt(1,j)*posDiff0+dcdt(2,j)*pos1+dcdt(3,j)*posDiff1;
  }
  return c[0]*pos0+c[1]*posDiff0+c[2]*pos1+c[3]*posDiff1;
}
template <typename T,int DIM>
const Eigen::Matrix<int,-1,-1>& VectorSequence<T,DIM>::getVar() const {
  return _var;
}
template <typename T,int DIM>
const Eigen::Matrix<int,-1,-1>& VectorSequence<T,DIM>::getDiffVar() const {
  return _diffVar;
}
template <typename T,int DIM>
int VectorSequence<T,DIM>::nrVar(int r) const {
  std::unordered_set<int> vars;
  for(int c=0; c<_var.cols(); c++)
    if(_var(r,c)>=0)
      vars.insert(_var(r,c));
  for(int c=0; c<_diffVar.cols(); c++)
    if(_diffVar(r,c)>=0)
      vars.insert(_diffVar(r,c));
  return (int)vars.size();
}
template <typename T,int DIM>
int VectorSequence<T,DIM>::offsetVar() const {
  int offset=std::numeric_limits<int>::max();
  for(int r=0; r<_var.rows(); r++)
    for(int c=0; c<_var.cols(); c++)
      if(_var(r,c)>=0)
        offset=std::min(offset,_var(r,c));
  for(int r=0; r<_diffVar.rows(); r++)
    for(int c=0; c<_diffVar.cols(); c++)
      if(_diffVar(r,c)>=0)
        offset=std::min(offset,_diffVar(r,c));
  return offset;
}
template <typename T,int DIM>
T VectorSequence<T,DIM>::even() const {
  return _even;
}
template class VectorSequence<FLOAT,2>;
template class VectorSequence<FLOAT,3>;
template class VectorSequence<FLOAT,6>;
}
