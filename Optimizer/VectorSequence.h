#ifndef VECTOR_SEQUENCE_H
#define VECTOR_SEQUENCE_H

#include "PhaseSequence.h"
#include <Environment/Environment.h>

namespace PHYSICSMOTION {
template <typename T,int DIM=3>
class VectorSequence : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
#ifndef SWIG
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef Eigen::Matrix<T,DIM,1> VecDT;
  typedef std::unordered_map<int,T> GRAD_INTERP;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  using SQPObjectiveComponent<T>::debugGradientConditional;
  using SQPObjectiveComponent<T>::debugGradientConditionalVID;
#endif
 public:
  VectorSequence(SQPObjectiveCompound<T>& obj,const std::string& name,char even,
                 std::shared_ptr<PhaseSequence<T>> phase,bool inherited=false);
  virtual VecDT timeInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan=true) const;
  virtual VecDT timeInterpolateStencil(const Vec& x,T time,int& stage,int& subStage,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan=true) const;
  virtual VecDT diffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan=true) const;
  virtual VecDT diffInterpolateStencil(const Vec& x,T time,int& stage,int& subStage,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan=true) const;
  virtual VecDT ddiffInterpolateStencil(const Vec& x,T time,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan=true) const;
  virtual VecDT ddiffInterpolateStencil(const Vec& x,T time,int& stage,int& subStage,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan=true) const;
  std::string diffVar(int r,int stage) const;
  std::string var(int r,int stage) const;
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0) override;
  virtual Vec makeValid(const Vec& x) const override;
  VecDT dotGrad(const Vec& x,const GRAD_INTERP g[DIM]) const;
  std::shared_ptr<PhaseSequence<T>> getPhase() const;
  static GRAD_INTERP removeNonZero(const GRAD_INTERP& g);
  virtual void initConstant(const VecDT& pos,SQPObjectiveCompound<T>& obj);
  virtual void initLineSegment(const VecDT& a,const VecDT& b,SQPObjectiveCompound<T>& obj,int N=0);
  virtual void initLineSegments(const std::vector<std::pair<VecDT,VecDT>>& segments,SQPObjectiveCompound<T>& obj);
  virtual void initRegularInterval(const MatT& DOFs,T interval,SQPObjectiveCompound<T>& obj,bool fixInitVar,bool fixInitDiffVar);
  VecDT interpolateStencilToPosition(const Vec& x,const Mat4XT& dcdt,int off,Vec4T& c,GRAD_INTERP grad[DIM],VecDT* gradTime,bool gradTimeSpan) const;
  const Eigen::Matrix<int,-1,-1>& getVar() const;
  const Eigen::Matrix<int,-1,-1>& getDiffVar() const;
  int nrVar(int row) const;
  int offsetVar() const;
  T even() const;
 protected:
  char _even;
  Eigen::Matrix<int,-1,-1> _var;
  Eigen::Matrix<int,-1,-1> _diffVar;
  std::shared_ptr<PhaseSequence<T>> _phase;
};
}

#endif
