#ifndef POSITION_SEQUENCE_ABOVE_GROUND_H
#define POSITION_SEQUENCE_ABOVE_GROUND_H

#include "PositionSequence.h"

namespace PHYSICSMOTION {
template <typename T>
class PositionSequenceAboveGround : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef typename PositionSequence<T>::GRAD_INTERP GRAD_INTERP;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  using SQPObjectiveComponent<T>::debugGradientConditionalVID;
 public:
  PositionSequenceAboveGround(SQPObjectiveCompound<T>& obj,const std::string& name,bool above,T dt,std::shared_ptr<PositionSequence<T>> pos,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
 protected:
  std::shared_ptr<PositionSequence<T>> _pos;
  T _dt;
};
}

#endif
