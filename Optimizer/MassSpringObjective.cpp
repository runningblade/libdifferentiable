#include "MassSpringObjective.h"
#include "IPOPTInterface.h"
#include "KNInterface.h"
#include <TinyVisualizer/Drawer.h>
#include <TinyVisualizer/MakeMesh.h>
#include <TinyVisualizer/Box2DShape.h>
#include <TinyVisualizer/Bullet3DShape.h>

namespace PHYSICSMOTION {
template <typename T,int DIM>
MassSpringObjective<T,DIM>::MassSpringObjective(SQPObjectiveCompound<T>& obj,const std::vector<SPRING>& springs,const Eigen::Matrix<T,DIM,1>& g,T coef)
  :SQPObjectiveComponent<T>(obj,"MassSpringObjective"),_springs(springs) {
  Eigen::Matrix<PolyXT,DIM,1> a,b;
  //count node
  _nNode=0;
  for(const SPRING& spring:_springs) {
    _nNode=std::max(_nNode,std::get<0>(spring));
    _nNode=std::max(_nNode,std::get<1>(spring));
  }
  _nNode++;
  //mass energy
  for(int n=0; n<_nNode; n++) {
    for(int i=0; i<DIM; i++) {
      obj.addVar("x"+std::to_string(n*DIM+i),-SQPObjective<T>::infty(),SQPObjective<T>::infty(),SQPObjectiveCompound<T>::NEW_OR_EXIST);
      a[i]=obj.addVarPoly(n*DIM+i);
    }
    this->addPolynomialComponentObjective((-g).template cast<PolyXT>().dot(a));
  }
  //spring energy
  for(const SPRING& spring:_springs) {
    for(int i=0; i<DIM; i++) {
      a[i]=obj.addVarPoly(std::get<0>(spring)*DIM+i);
      b[i]=obj.addVarPoly(std::get<1>(spring)*DIM+i);
    }
    PolyXT diffLen=(a-b).squaredNorm()-PolyXT(std::get<2>(spring)*std::get<2>(spring));
    this->addPolynomialComponentObjective(diffLen*diffLen*PolyXT(coef/2));
  }
}
template <typename T,int DIM>
void MassSpringObjective<T,DIM>::addConstraintInCircle(SQPObjectiveCompound<T>& obj,const Eigen::Matrix<T,DIM,1>& ctr,T rad) {
  Eigen::Matrix<PolyXT,DIM,1> a;
  _gl.assign(_nNode,0);
  _gu.assign(_nNode,SQPObjective<T>::infty());
  for(int n=0; n<(int)_gl.size(); n++) {
    for(int i=0; i<DIM; i++)
      a[i]=obj.addVarPoly(n*DIM+i);
    this->setPolynomialComponentConstraint(n,PolyXT(rad*rad)-(ctr.template cast<PolyXT>()-a).squaredNorm());
  }
}
template <typename T,int DIM>
void MassSpringObjective<T,DIM>::fixNode(SQPObjectiveCompound<T>& obj,int id,const Eigen::Matrix<T,DIM,1>& pos) {
  for(int i=0; i<DIM; i++)
    obj.addVar("x"+std::to_string(id*DIM+i),pos[i],pos[i],SQPObjectiveCompound<T>::MUST_EXIST);
}
template <typename T>
void visualizeMassSpringChain2D(int argc,char** argv,int N,const Eigen::Matrix<T,2,1>& g,T coef,bool cons,bool useIPOPT) {
#define REST_LENGTH .1f
  //initialize objective
  SQPObjectiveCompound<T> obj;
  std::vector<typename MassSpringObjective<T,2>::SPRING> springs;
  for(int i=0; i<N-1; i++)
    springs.push_back(std::make_tuple(i,i+1,REST_LENGTH));
  std::shared_ptr<MassSpringObjective<T,2>> spring(new MassSpringObjective<T,2>(obj,springs,g,coef));
  spring->fixNode(obj,0,Eigen::Matrix<T,2,1>(0,0));
  spring->fixNode(obj,N-1,Eigen::Matrix<T,2,1>((N-1)*REST_LENGTH,0));
  for(int i=0; i<N-1; i++)
    obj.setVarInit("x"+std::to_string(i*2),i*REST_LENGTH);
  //circle
  T rad=(N-1)*REST_LENGTH/2;
  Eigen::Matrix<T,2,1> ctr(rad,0);
  if(cons)
    spring->addConstraintInCircle(obj,ctr,rad);
  obj.addComponent(spring);
  //test derivative
  obj.assemblePoly();
  typename SQPObjectiveComponent<T>::Vec x0=SQPObjectiveComponent<T>::Vec::Random(obj.inputs());
  obj.SQPObjective<T>::debug(10,0,&x0,false);
  //optimize
#if defined(IPOPT_SUPPORT) || defined(KNITRO_SUPPORT)
  double deriv=0;//1e-6f;
#endif
  typename SQPObjectiveCompound<T>::Vec x;
#ifdef IPOPT_SUPPORT
  if(useIPOPT)
    IPOPTInterface<T>::optimize(obj,5,deriv,1e-6f,1000,1000,x);
#endif
#ifdef KNITRO_SUPPORT
  if(!useIPOPT)
    KNInterface<T>(obj,true).optimize(true,deriv,1e-6f,1e-6f,false,1000,1000,x);
#endif
  ASSERT(x.size()>0)
  //visualize
  using namespace DRAWER;
  Drawer drawer(argc,argv);
  //frame
  if(cons) {
    std::shared_ptr<MeshShape> frame=makeCircle(32,false,ctr.template cast<GLfloat>(),(GLfloat)rad);
    frame->setColorDiffuse(GL_LINE_LOOP,.7,.7,.7);
    frame->setUseLight(false);
    drawer.addShape(frame);
  }
  //entries
  std::shared_ptr<MeshShape> entry(new MeshShape);
  for(int i=0; i<N; i++) {
    entry->addVertex(Eigen::Matrix<T,3,1>(x[i*2+0],x[i*2+1],0).template cast<GLfloat>());
    entry->addIndexSingle(i);
  }
  entry->setMode(GL_LINE_STRIP);
  entry->setColorDiffuse(GL_LINE_STRIP,.5,0,0);
  entry->setUseLight(false);
  drawer.addShape(entry);
  //kick off app
  drawer.addCamera2D(N);
  drawer.mainLoop();
#undef REST_LENGTH
}
template <typename T>
void visualizeMassSpringChain3D(int argc,char** argv,int N,const Eigen::Matrix<T,3,1>& g,T coef,bool cons,bool useIPOPT) {
#define REST_LENGTH .1f
  //initialize objective
  SQPObjectiveCompound<T> obj;
  std::vector<typename MassSpringObjective<T,3>::SPRING> springs;
  for(int i=0; i<N-1; i++)
    springs.push_back(std::make_tuple(i,i+1,REST_LENGTH));
  std::shared_ptr<MassSpringObjective<T,3>> spring(new MassSpringObjective<T,3>(obj,springs,g,coef));
  spring->fixNode(obj,0,Eigen::Matrix<T,3,1>(0,0,0));
  spring->fixNode(obj,N-1,Eigen::Matrix<T,3,1>((N-1)*REST_LENGTH,0,0));
  for(int i=0; i<N-1; i++)
    obj.setVarInit("x"+std::to_string(i*3),i*REST_LENGTH);
  //circle
  T rad=(N-1)*REST_LENGTH/2;
  Eigen::Matrix<T,3,1> ctr(rad,0,0);
  if(cons)
    spring->addConstraintInCircle(obj,ctr,rad);
  obj.addComponent(spring);
  //test derivative
  obj.assemblePoly();
  typename SQPObjectiveComponent<T>::Vec x0=SQPObjectiveComponent<T>::Vec::Random(obj.inputs());
  obj.SQPObjective<T>::debug(10,0,&x0,false);
  //optimize
#if defined(IPOPT_SUPPORT) || defined(KNITRO_SUPPORT)
  double deriv=0;//1e-6f;
#endif
  typename SQPObjectiveCompound<T>::Vec x;
#ifdef IPOPT_SUPPORT
  if(useIPOPT)
    IPOPTInterface<T>::optimize(obj,5,deriv,1e-6f,1000,1000,x);
#endif
#ifdef KNITRO_SUPPORT
  if(!useIPOPT)
    KNInterface<T>(obj,true).optimize(true,deriv,1e-6f,1e-6f,false,1000,1000,x);
#endif
  ASSERT(x.size()>0)
  //visualize
  using namespace DRAWER;
  Drawer drawer(argc,argv);
  //frame
  if(cons) {
    std::shared_ptr<MeshShape> frame=makeSphere(32,false,(GLfloat)rad);
    frame->setColorDiffuse(GL_LINES,.7,.7,.7);
    std::shared_ptr<Bullet3DShape> movedFrame(new Bullet3DShape);
    movedFrame->setLocalTranslate(ctr.template cast<GLfloat>());
    movedFrame->addShape(frame);
    drawer.addShape(movedFrame);
  }
  //entries
  std::shared_ptr<MeshShape> entry(new MeshShape);
  for(int i=0; i<N; i++) {
    entry->addVertex(x.template segment<3>(i*3).template cast<GLfloat>());
    entry->addIndexSingle(i);
  }
  entry->setMode(GL_LINE_STRIP);
  entry->setColorDiffuse(GL_LINE_STRIP,.5,0,0);
  drawer.addShape(entry);
  //kick off app
  drawer.clearLight();
  drawer.mainLoop();
#undef REST_LENGTH
}
template void visualizeMassSpringChain2D<FLOAT>(int argc,char** argv,int N,const Eigen::Matrix<FLOAT,2,1>& g,FLOAT coef,bool cons,bool useIPOPT);
template void visualizeMassSpringChain3D<FLOAT>(int argc,char** argv,int N,const Eigen::Matrix<FLOAT,3,1>& g,FLOAT coef,bool cons,bool useIPOPT);
}
