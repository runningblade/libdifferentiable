#ifndef PHASE_SEQUENCE_H
#define PHASE_SEQUENCE_H

#include "SQPObjective.h"

namespace PHYSICSMOTION {
template <typename T>
class PhaseSequence : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  using typename SQPObjectiveComponent<T>::PolyXT;
  using SQPObjectiveComponent<T>::_name;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
  using SQPObjectiveComponent<T>::debugGradientConditionalVID;
  typedef std::function<void(T,T,T,Vec4T&,Vec4T&,Vec4T&,Vec4T&)> HERMITE;
 public:
  //use a negative stage for fixed timeSpan, a positive stage for variable timeSpan
  PhaseSequence(SQPObjectiveCompound<T>& obj,const std::string& name,T eps,T horizon,int stage,int subStage,bool inherited=false);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  Vec4T interpTpl(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad,HERMITE h,HERMITE hFirst,HERMITE hLast) const;
  Vec4T timeInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad) const;
  Vec4T diffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad) const;
  Vec4T ddiffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,bool even,Mat4XT* grad) const;
  T timeInterpolate(const Vec& x,T alpha,int& stage,int& subStage,bool even) const;
  Vec4T interpTpl(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad,HERMITE h,HERMITE hFirst,HERMITE hLast) const;
  Vec4T timeInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad) const;
  Vec4T diffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad) const;
  Vec4T ddiffInterpolateStencil(const Vec& x,T time,T& alpha,int& stage,int& subStage,int& offset,Mat4XT* grad) const;
  T timeInterpolate(const Vec& x,T alpha,int& stage,int& subStage) const;
  std::string timeSpanVar(int stage) const;
  T timeSpanVar(const Vec& x,int stage) const;
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0) override;
  virtual Vec makeValid(const Vec& x) const override;
  virtual T horizon(const Vec& x) const;
  virtual T horizonRef() const;
  virtual T eps() const;
  int stage() const;
  int subStage() const;
  const Eigen::Matrix<int,-1,1>& timeSpanVar() const;
  int segmentId(int column) const;
  int segmentOffset(int column) const;
  int nrSegment(bool even) const;
  int nrColumn(bool even) const;
  int nrColumn() const;
  Vec4T dotGrad(const Vec& x,const Mat4XT& gTime) const;
  void initSameSpan(SQPObjectiveCompound<T>& obj,char even=true,T staticToSwing=3);
  void initSpanSequence(SQPObjectiveCompound<T>& obj,const std::vector<T>& spans);
  void initRandomSpan(SQPObjectiveCompound<T>& obj);
  static void coeffHermite(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void coeffHermiteFirst(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void coeffHermiteLast(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void diffCoeffHermite(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void diffCoeffHermiteFirst(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void diffCoeffHermiteLast(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void ddiffCoeffHermite(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void ddiffCoeffHermiteFirst(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
  static void ddiffCoeffHermiteLast(T s,T t,T t0,Vec4T& c,Vec4T& dcdt,Vec4T& dcdt0,Vec4T& dcds);
 protected:
  //data
  const T _eps;
  const T _horizon;
  const int _stage;
  const int _subStage;
  Eigen::Matrix<int,-1,1> _timeSpanVar;
};
}

#endif
