#include "ForceSequenceInCone.h"
#include <Utils/Utils.h>

namespace PHYSICSMOTION {
template <typename T>
ForceSequenceInCone<T>::ForceSequenceInCone(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,std::shared_ptr<PositionSequence<T>> pos,std::shared_ptr<ForceSequence<T>> force)
  :SQPObjectiveComponent<T>(obj,name+"ForceSequenceInCone"),_pos(pos),_force(force),_n0(0,0,0),_dt(dt) {
  int nrStep=(int)floor(force->getPhase()->horizonRef()/_dt);
  for(int c=0; c<nrStep; c++) {
    _gl.push_back(0);
    _gu.push_back(SQPObjective<T>::infty());
  }
}
template <typename T>
ForceSequenceInCone<T>::ForceSequenceInCone(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec3T& n,std::shared_ptr<ForceSequence<T>> force)
  :SQPObjectiveComponent<T>(obj,name+"ForceSequenceInCone"),_force(force),_n0(n.normalized()),_dt(dt) {
  int nrStep=(int)floor(force->getPhase()->horizonRef()/_dt);
  for(int c=0; c<nrStep; c++) {
    _gl.push_back(0);
    _gu.push_back(SQPObjective<T>::infty());
  }
}
template <typename T>
int ForceSequenceInCone<T>::operator()(const Vec& x,Vec& fvec,STrips* fjac) {
  std::shared_ptr<Environment<T>> env=_pos?_pos->getEnv():NULL;
  T eps=1e-6f;
  OMP_PARALLEL_FOR_
  for(int c=0; c<(int)_gl.size(); c++) {
    Mat3T dndp;
    Vec3T gradTime,pos=Vec3T::Zero(),f=Vec3T::Zero(),n,gf,gp;
    GRAD_INTERP posGrad[3],forceGrad[3];
    f=_force->timeInterpolateStencil(x,c*_dt,fjac?forceGrad:NULL,fjac?&gradTime:NULL);
    if(env) {
      pos=_pos->timeInterpolateStencil(x,c*_dt,fjac?posGrad:NULL,fjac?&gradTime:NULL);
      n=env->phiGrad(pos,&dndp);
    } else n=_n0;
    //constraint
    int offset=_offset+c;
    T fn=n.dot(f);
    T ft=sqrt(f.squaredNorm()-fn*fn+eps);
    fvec[offset]=fn*_force->getMu()-ft;
    gf=n*_force->getMu()-(f-fn*n)/ft;
    if(fjac)
      for(int r=0; r<3; r++)
        for(std::pair<int,T> stencil:forceGrad[r])
          fjac->push_back(STrip(offset,stencil.first,gf[r]*stencil.second));
    //pos-dependent nomral
    if(env) {
      gp=(_force->getMu()+fn/ft)*(dndp.transpose()*f);
      if(fjac)
        for(int r=0; r<3; r++)
          for(std::pair<int,T> stencil:posGrad[r])
            fjac->push_back(STrip(offset,stencil.first,gp[r]*stencil.second));
    }
  }
  return 0;
}
template class ForceSequenceInCone<FLOAT>;
}

