#ifndef FORCE_SEQUENCE_IN_CONE_H
#define FORCE_SEQUENCE_IN_CONE_H

#include "ForceSequence.h"

namespace PHYSICSMOTION {
template <typename T>
class ForceSequenceInCone : public SQPObjectiveComponent<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef typename PositionSequence<T>::GRAD_INTERP GRAD_INTERP;
  using SQPObjectiveComponent<T>::_gl;
  using SQPObjectiveComponent<T>::_gu;
  using SQPObjectiveComponent<T>::_offset;
 public:
  ForceSequenceInCone(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,std::shared_ptr<PositionSequence<T>> pos,std::shared_ptr<ForceSequence<T>> force);  //this version uses a position-dependent normal
  ForceSequenceInCone(SQPObjectiveCompound<T>& obj,const std::string& name,T dt,const Vec3T& n,std::shared_ptr<ForceSequence<T>> force);  //this version uses a constant normal
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
 protected:
  std::shared_ptr<PositionSequence<T>> _pos;
  std::shared_ptr<ForceSequence<T>> _force;
  Vec3T _n0;
  T _dt;
};
}

#endif
