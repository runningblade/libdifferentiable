#ifndef SQP_OBJECTIVE_H
#define SQP_OBJECTIVE_H

#include <Utils/SOSPolynomial.h>
#include <Utils/SparseUtils.h>
#include <unordered_map>

namespace PHYSICSMOTION {
template <typename T>
struct SQPObjective {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  typedef Eigen::Triplet<T,int> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef Eigen::SparseMatrix<T,0,int> SMatT;
  static T infty();
  virtual ~SQPObjective() {}
  virtual Vec lb() const;
  virtual Vec ub() const;
  virtual Vec gl() const;
  virtual Vec gu() const;
  virtual Vec init() const;
  int nnzJ();
  //constraint
  virtual int operator()(const Vec& x,Vec& fvec,MatT* fjac);
  virtual int operator()(const Vec& x,Vec& fvec,SMatT* fjac);
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac);
  //objective
  virtual T operator()(const Vec& x,Vec* fgrad);
  virtual T operator()(const Vec& x,Vec* fvec,MatT* fjac);
  virtual T operator()(const Vec& x,Vec* fvec,SMatT* fjac);
  virtual T operator()(const Vec& x,Vec* fvec,STrips* fjac);
  //problem size
  virtual int inputs() const;
  virtual int values() const;
  virtual bool debug(int nrTrial,T thresErr,Vec* x0=NULL,bool hess=false,T DELTARef=0);
 protected:
  SMatT _tmpFjacs;
  STrips _tmp;
};
template <typename T>
struct SQPObjectiveCompound;
template <typename T>
struct SQPVariable {
  SQPVariable();
  void fix(T val);
  bool isFixed() const;
  bool _isBinary;
  T _l,_u,_init;
  int _id;
};
template <typename T>
struct SQPObjectiveComponent : public SQPObjective<T> {
 public:
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef SOSPolynomial<T,'x'> PolyXT;
  using SQPObjective<T>::operator();
  typedef std::unordered_map<int,std::string> VARMAPINV;
  typedef std::unordered_map<std::string,SQPVariable<T>> VARMAP;
  typedef std::unordered_map<std::string,std::shared_ptr<SQPObjectiveComponent<T>>> CONSMAP;
  SQPObjectiveComponent(SQPObjectiveCompound<T>& obj,const std::string& name,bool force=false);
  virtual ~SQPObjectiveComponent();
  virtual int inputs() const override;
  virtual int values() const override;
  //whether modifying objective function expression is allowed
  virtual void setUpdateCache(const Vec& x,bool);
  //constraint
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  //objective
  virtual T operator()(const Vec& x,Vec* fgrad) override;
  std::shared_ptr<PolyXT>& getPolynomialComponentObjective();
  std::unordered_map<int,PolyXT>& getPolynomialComponentConstraint();
  void addPolynomialComponentObjective(const PolyXT& polyComp);
  void addPolynomialComponentConstraint(int consId,const PolyXT& polyComp);
  void setPolynomialComponentObjective(const PolyXT& polyComp);
  void setPolynomialComponentConstraint(int consId,const PolyXT& polyComp);
  void addSOS1(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<int>& ids);
  void addSOS1SumToOne(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<int>& ids);
  void addSOS2SumToOne(SQPObjectiveCompound<T>& obj,const std::string& name,const std::vector<int>& ids);
  //variable manipulation
  virtual bool debug(int inputs,int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0);
  virtual bool debug(int nrTrial,T thres,Vec* x0=NULL,bool hess=false,T DELTARef=0);
  virtual Vec makeValid(const Vec& x) const;
  virtual bool debugGradientConditional(const std::string& entryStr,T ref,T err,T thres) const;
  virtual bool debugGradientConditionalVID(const std::string& entryStr,int vid,T ref,T err,T thres) const;
  void setOffset(int offset);
  void clear();
  std::string _name;
  std::vector<T> _gl,_gu;
  const VARMAP* _vars;
  int _offset;
 private:
  int _inputs;
  std::shared_ptr<PolyXT> _polyCompObj;
  std::unordered_map<int,PolyXT> _polyCompCons;
};
template <typename T>
struct SQPObjectiveCompound : public SQPObjective<T> {
  enum VARIABLE_OP {
    MUST_NEW,
    MUST_EXIST,
    NEW_OR_EXIST,
  };
  DECL_MAT_VEC_MAP_TYPES_T
  using typename SQPObjective<T>::STrip;
  using typename SQPObjective<T>::STrips;
  using typename SQPObjective<T>::SMatT;
  typedef SOSPolynomial<T,'x'> PolyXT;
  typedef SOSTerm<T,'x'> TermXT;
  typedef std::unordered_map<int,std::string> VARMAPINV;
  typedef std::unordered_map<std::string,SQPVariable<T>> VARMAP;
  typedef std::unordered_map<std::string,std::shared_ptr<SQPObjectiveComponent<T>>> CONSMAP;
  virtual ~SQPObjectiveCompound() {}
  virtual Vec lb() const override;
  virtual Vec ub() const override;
  virtual Vec gl() const override;
  virtual Vec gu() const override;
  virtual Vec init() const override;
  virtual int inputs() const override;
  virtual int values() const override;
  //constraint
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac) override;
  //objective
  virtual T operator()(const Vec& x,Vec* fgrad) override;
  //variable manipulation
  void assemblePoly();
  std::shared_ptr<PolyXT>& getPolynomialComponentObjective();
  std::unordered_map<int,PolyXT>& getPolynomialComponentConstraint();
  const SQPVariable<T>& addBinaryVar(const std::string& name,T l,T u,VARIABLE_OP op=NEW_OR_EXIST);
  const SQPVariable<T>& addVar(const std::string& name,T l,T u,VARIABLE_OP op=NEW_OR_EXIST);
  const SQPVariable<T>& addVar(const std::string& name,VARIABLE_OP op=NEW_OR_EXIST);
  int getVarId(const std::string& name) const;
  const SQPVariable<T>& addVar(int id) const;
  SQPVariable<T>& addVar(int id);
  PolyXT addVarPoly(int id) const;
  TermXT addVarTerm(int id) const;
  const VARMAP& vars() const;
  void setVarInit(const std::string& name,T init);
  void setVarInit(int id,T init);
  void checkViolation(const Vec* at=NULL);
  const CONSMAP& components() const;
  void addComponent(std::shared_ptr<SQPObjectiveComponent<T>> c);
  virtual bool debug(const std::string& str,int nrTrial,T thres,T DELTARef=0);
  template <typename OBJ_TYPE>
  std::shared_ptr<OBJ_TYPE> getComponent() const {
    for(typename std::unordered_map<std::string,std::shared_ptr<SQPObjectiveComponent<T>>>::const_iterator beg=_components.begin(),end=_components.end(); beg!=end; beg++)
      if(std::dynamic_pointer_cast<OBJ_TYPE>(beg->second))
        return std::dynamic_pointer_cast<OBJ_TYPE>(beg->second);
    return NULL;
  }
 private:
  std::shared_ptr<PolyXT> _polyCompObj;
  std::unordered_map<int,PolyXT> _polyCompCons;
  CONSMAP _components;
  VARMAPINV _varsInv;
  VARMAP _vars;
};
}

#endif
